﻿
namespace GridSample001
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgv1_BtnAdd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgv1_Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv1_Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv1_Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2_BtnAdd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgv2_Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2_Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv2_Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(655, 411);
            this.tabControl1.TabIndex = 10;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(647, 381);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "加工管・継ぎ手";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv1_BtnAdd,
            this.dgv1_Column1,
            this.dgv1_Column2,
            this.dgv1_Column3});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(4, 4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(639, 373);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(647, 381);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "加工費";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToResizeRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv2_BtnAdd,
            this.dgv2_Column1,
            this.dgv2_Column2,
            this.dgv2_Column3});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(4, 4);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView2.RowTemplate.Height = 21;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(639, 373);
            this.dataGridView2.TabIndex = 1;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(13, 432);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(100, 25);
            this.btnClear.TabIndex = 20;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(564, 432);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 25);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // dgv1_BtnAdd
            // 
            this.dgv1_BtnAdd.DataPropertyName = "dgv1_BtnAdd";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgv1_BtnAdd.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgv1_BtnAdd.HeaderText = "";
            this.dgv1_BtnAdd.Name = "dgv1_BtnAdd";
            this.dgv1_BtnAdd.Width = 50;
            // 
            // dgv1_Column1
            // 
            this.dgv1_Column1.DataPropertyName = "dgv1_Column1";
            this.dgv1_Column1.HeaderText = "Column1";
            this.dgv1_Column1.Name = "dgv1_Column1";
            this.dgv1_Column1.Width = 150;
            // 
            // dgv1_Column2
            // 
            this.dgv1_Column2.DataPropertyName = "dgv1_Column2";
            this.dgv1_Column2.HeaderText = "Column2";
            this.dgv1_Column2.Name = "dgv1_Column2";
            this.dgv1_Column2.Width = 150;
            // 
            // dgv1_Column3
            // 
            this.dgv1_Column3.DataPropertyName = "dgv1_Column3";
            this.dgv1_Column3.HeaderText = "Column3";
            this.dgv1_Column3.Name = "dgv1_Column3";
            this.dgv1_Column3.Width = 150;
            // 
            // dgv2_BtnAdd
            // 
            this.dgv2_BtnAdd.DataPropertyName = "dgv2_BtnAdd";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgv2_BtnAdd.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgv2_BtnAdd.HeaderText = "";
            this.dgv2_BtnAdd.Name = "dgv2_BtnAdd";
            this.dgv2_BtnAdd.Width = 50;
            // 
            // dgv2_Column1
            // 
            this.dgv2_Column1.DataPropertyName = "dgv2_Column1";
            this.dgv2_Column1.HeaderText = "Column1";
            this.dgv2_Column1.Name = "dgv2_Column1";
            this.dgv2_Column1.Width = 150;
            // 
            // dgv2_Column2
            // 
            this.dgv2_Column2.DataPropertyName = "dgv2_Column2";
            this.dgv2_Column2.HeaderText = "Column2";
            this.dgv2_Column2.Name = "dgv2_Column2";
            this.dgv2_Column2.Width = 150;
            // 
            // dgv2_Column3
            // 
            this.dgv2_Column3.DataPropertyName = "dgv2_Column3";
            this.dgv2_Column3.HeaderText = "Column3";
            this.dgv2_Column3.Name = "dgv2_Column3";
            this.dgv2_Column3.Width = 150;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 470);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "サンプル001（明細内訳）";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.DataGridView dataGridView2;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridViewButtonColumn dgv1_BtnAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv1_Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv1_Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv1_Column3;
        private System.Windows.Forms.DataGridViewButtonColumn dgv2_BtnAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2_Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2_Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv2_Column3;
    }
}

