﻿using GadgetCommon.Const;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace GridSample001.Forms.Business
{
    internal class FormLogic
    {
        #region Member
        private MainForm Body;
        private const int MaxRowCount = 300;
        private static readonly GridDto1 CelHeaderNameDto1 = new GridDto1();
        private static readonly GridDto2 CelHeaderNameDto2 = new GridDto2();
        #endregion

        #region ConstRuctor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        internal FormLogic(MainForm body)
        {
            this.Body = body;
            this.Init();
            this.Body.btnClear.Click += this.BtnClearClick;
            this.Body.btnClose.Click += this.BtnCloseClick;
            this.Body.dataGridView1.CellClick += this.BtnAddClick;
            this.Body.dataGridView2.CellClick += this.BtnAddClick;
            this.Body.dataGridView1.RowPostPaint += (s, e) => { GridRowUtil<GridDto1>.SetRowNum(s, e); };
            this.Body.dataGridView2.RowPostPaint += (s, e) => { GridRowUtil<GridDto2>.SetRowNum(s, e); };
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region ★Event
        /// <summary>
        /// ★追加ボタンクリック
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnAddClick(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            // ★ボタン連打時に予期しない動作が起きることを防ぐため、セルクリックイベントを一時退避
            this.Body.dataGridView1.CellClick -= this.BtnAddClick;
            this.Body.dataGridView2.CellClick -= this.BtnAddClick;
            try
            {
                if (e.RowIndex < 0 || e.ColumnIndex < 0)
                {
                    return;
                }
                var dgv = (DataGridView)s;
                string tgtCellName = dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningColumn.Name;
                if (!tgtCellName.Contains(nameof(CelHeaderNameDto1.dgv1_BtnAdd)) && !tgtCellName.Contains(nameof(CelHeaderNameDto2.dgv2_BtnAdd)))
                {
                    return;
                }
                bool validate = true;
                int rowIndex = 0;
                switch (this.Body.tabControl1.SelectedIndex)
                {
                    case 0:
                        validate = (this.Body.dataGridView1.SelectedRows != null && this.Body.dataGridView1.SelectedRows.Count != 0);
                        if (validate)
                        {
                            rowIndex = this.Body.dataGridView1.Rows.IndexOf(this.Body.dataGridView1.SelectedRows[0]);
                        }
                        break;
                    case 1:
                        validate = (this.Body.dataGridView2.SelectedRows != null && this.Body.dataGridView2.SelectedRows.Count != 0);
                        if (validate)
                        {
                            rowIndex = this.Body.dataGridView2.Rows.IndexOf(this.Body.dataGridView2.SelectedRows[0]);
                        }
                        break;
                }
                if (!validate)
                {
                    CommonUtil.ShowErrorMsg("行を選択してください。");
                    return;
                }
                // グリッドデータセット
                this.SetGridData(
                      this.Body.tabControl1.SelectedIndex
                    , rowIndex
                );
            }
            catch (Exception ex)
            {
                CommonUtil.ShowErrorMsg(ex.Message);
                return;
            }
            finally
            {
                // ★退避したセルクリックイベントを回復
                this.Body.dataGridView1.CellClick += this.BtnAddClick;
                this.Body.dataGridView2.CellClick += this.BtnAddClick;
            }
        }
        /// <summary>
        /// クリアボタン押下
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnClearClick(
              object s
            , EventArgs e
            )
        {
            if (CommonUtil.ShowInfoMsgOKCancel("クリアしますか？") != DialogResult.OK)
            {
                return;
            }
            this.Init();
        }
        /// <summary>
        /// 終了ボタン押下
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClick(
              object s
            , EventArgs e
            )
        {
            if (CommonUtil.ShowInfoMsgOKCancel("終了しますか？") != DialogResult.OK)
            {
                return;
            }
            this.Body.Close();
        }
        #endregion

        #region ★Logic
        /// <summary>
        /// ★グリッドデータセット
        /// </summary>
        /// <param name="tabIndex"></param>
        /// <param name="rowIndex"></param>
        private void SetGridData(
              int tabIndex
            , int rowIndex
            )
        {
            DataGridViewRow r1;
            DataGridViewRow r2;
            switch (tabIndex)
            {
                case 0:
                    r1 = this.Body.dataGridView1.Rows[rowIndex];
                    r1.Cells[nameof(CelHeaderNameDto1.dgv1_Column1)].Value = $"加工管・継ぎ手-{rowIndex + 1}";
                    foreach (DataGridViewCell c in this.Body.dataGridView1.Rows[rowIndex].Cells)
                    {
                        c.Style.BackColor = Colors.BackColorCellEdited;
                    }
                    this.Body.dataGridView1.Show();
                    if (rowIndex < MaxRowCount - 1)
                    {
                        this.Body.dataGridView1.Rows[rowIndex + 1].Selected = true;
                        this.Body.dataGridView1.CurrentCell = this.Body.dataGridView1.Rows[rowIndex + 1].Cells[nameof(CelHeaderNameDto1.dgv1_BtnAdd)];
                    }
                    // ★タブ②へフォーカス
                    this.Body.tabControl1.SelectedIndex = 1;

                    // ★グリッド①からデータ取得
                    var dataList1 = GridRowUtil<GridDto1>.GetAllRowsModel(this.Body.dataGridView1.DataSource)
                        .Where(n => !string.IsNullOrEmpty(n.dgv1_Column1)).ToList();
                    var dataList2 = new List<GridDto2>();
                    foreach (var roDdto in dataList1)
                    {
                        dataList2.Add(new GridDto2() { dgv2_Column1 = $"加工費", dgv2_Column2 = $"{roDdto.dgv1_Column1}", });
                    }
                    // ★グリッド②で直接追加したデータを取得
                    var dataList2Added = GridRowUtil<GridDto2>.GetAllRowsModel(this.Body.dataGridView2.DataSource)
                        .Where(n => !string.IsNullOrEmpty(n.dgv2_Column1) && string.IsNullOrEmpty(n.dgv2_Column2)).ToList();
                    dataList2.AddRange(dataList2Added);

                    // ★グリッド②クリア
                    this.Body.dataGridView2.Rows.Clear();
                    var dataSource2 = new BindingList<GridDto2>();
                    for (int i = 0; i < MaxRowCount; i++)
                    {
                        dataSource2.Add(new GridDto2());
                    }
                    this.Body.dataGridView2.DataSource = dataSource2;

                    // ★グリッド②にデータセット
                    CommonUtil.ShowProgressDialog(() =>
                    {
                        // ★グリッド②に登録するデータを1件ずつループ
                        foreach (var rowDtoGrid2 in dataList2)
                        {
                            foreach (DataGridViewRow row2 in this.Body.dataGridView2.Rows)
                            {
                                // ★グリッド②の行を先頭から1件ずつループ
                                string col1 = CommonUtil.TostringNullForbid(row2.Cells[nameof(CelHeaderNameDto2.dgv2_Column1)].Value);
                                if (!string.IsNullOrEmpty(col1))
                                {
                                    // ★既に行データが存在する場合はスルー
                                    continue;
                                }
                                // ★値セット
                                row2.Cells[nameof(CelHeaderNameDto2.dgv2_Column1)].Value = rowDtoGrid2.dgv2_Column1;
                                row2.Cells[nameof(CelHeaderNameDto2.dgv2_Column2)].Value = rowDtoGrid2.dgv2_Column2;

                                // ★背景色セット
                                foreach (DataGridViewCell c in row2.Cells)
                                {
                                    c.Style.BackColor = Colors.BackColorCellEdited;
                                }
                                // ★値と背景色をセットしたら、②の行ループを抜ける
                                break;
                            }
                        }
                    });
                    // ★タブ①へフォーカス
                    this.Body.tabControl1.SelectedIndex = 0;
                    break;
                case 1:
                    r2 = this.Body.dataGridView2.Rows[rowIndex];
                    r2.Cells[nameof(CelHeaderNameDto2.dgv2_Column1)].Value = $"加工費-{rowIndex + 1}";
                    foreach (DataGridViewCell c in this.Body.dataGridView2.Rows[rowIndex].Cells)
                    {
                        c.Style.BackColor = Colors.BackColorCellEdited;
                    }
                    this.Body.dataGridView2.Show();
                    if (rowIndex < MaxRowCount - 1)
                    {
                        this.Body.dataGridView2.Rows[rowIndex + 1].Selected = true;
                        this.Body.dataGridView2.CurrentCell = this.Body.dataGridView2.Rows[rowIndex + 1].Cells[nameof(CelHeaderNameDto2.dgv2_BtnAdd)];
                    }
                    break;
            }
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            CommonUtil.SetContorolsColor(this.Body);
            this.Body.tabPage1.Focus();
            this.Body.dataGridView1.Rows.Clear();
            this.Body.dataGridView2.Rows.Clear();
            var dataSource1 = new BindingList<GridDto1>();
            var dataSource2 = new BindingList<GridDto2>();
            for (int i = 0; i < MaxRowCount; i++)
            {
                dataSource1.Add(new GridDto1());
                dataSource2.Add(new GridDto2());
            }
            this.Body.dataGridView1.DataSource = dataSource1;
            this.Body.dataGridView2.DataSource = dataSource2;
            this.Body.tabControl1.SelectedIndex = 0;
        }
        #endregion
    }
}
