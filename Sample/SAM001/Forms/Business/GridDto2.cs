﻿namespace GridSample001.Forms.Business
{
    public class GridDto2
    {
        public GridDto2()
        {
            this.dgv2_BtnAdd = "追加";
        }
        public string dgv2_BtnAdd { get; set; }
        public string dgv2_Column1 { get; set; }
        public string dgv2_Column2 { get; set; }
        public string dgv2_Column3 { get; set; }
    }
}
