﻿namespace GridSample001.Forms.Business
{
    public class GridDto1
    {
        public GridDto1()
        {
            this.dgv1_BtnAdd = "追加";
        }
        public string dgv1_BtnAdd { get; set; }
        public string dgv1_Column1 { get; set; }
        public string dgv1_Column2 { get; set; }
        public string dgv1_Column3 { get; set; }
    }
}
