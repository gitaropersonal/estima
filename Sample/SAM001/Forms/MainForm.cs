﻿using GridSample001.Forms.Business;
using System.Windows.Forms;

namespace GridSample001
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();
            new FormLogic(this);
        }
    }
}
