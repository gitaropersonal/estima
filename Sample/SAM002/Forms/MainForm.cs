﻿using SAM002.Forms.Business;
using System.Windows.Forms;
using System.Data;

namespace SAM002.Forms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            this.InitializeComponent();
            //new FormLogic(this);
            var dt = new DataTable();
            dt.Columns.Add("教科", typeof(string));
            dt.Columns.Add("選択", typeof(string));
            dt.Columns.Add("会場ID", typeof(string));
            dt.Columns.Add("団体ID", typeof(string));
            dt.Columns.Add("教室ID", typeof(string));
            dt.Columns.Add("受験ID", typeof(string));
            dt.Columns.Add("生徒ID", typeof(string));
            //dt.Columns.Add("答案", typeof(Byte[]));
            dt.Columns.Add("インデックス", typeof(string));
            //kyoukaText.DataBindings.Add("Text", dt, "教科");
            //sentakucombo.DataBindings.Add("Text", dt, "選択");
            //kaijyouID.DataBindings.Add("Text", dt, "会場ID");
            //groupID.DataBindings.Add("Text", dt, "団体ID");
            //classID.DataBindings.Add("Text", dt, "教室ID");
            //jukenID.DataBindings.Add("Text", dt, "受験ID");
            //seitoID.DataBindings.Add("Text", dt, "生徒ID");
            ////touanImage.DataBindings.Add("Image", dt, "答案");
            //indexText.DataBindings.Add("Text", dt, "インデックス");
            //for (int i = 0; i < lst.Count; i++)
            //{
            //    dt.Rows.Add(kyouka_id, allinfoes[lst[i]].sentakuCode[kyouka_index],
            //        allinfoes[lst[i]].kaijyou_id, allinfoes[lst[i]].group_id,
            //        allinfoes[lst[i]].class_id, allinfoes[lst[i]].juken_id,
            //        allinfoes[lst[i]].seito_id,
            //        lst[i].ToString());
            //}
            this.dataRepeater1.DataSource = dt;
        }
    }
}
