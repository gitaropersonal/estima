﻿using Estima.Forms.Main.MainForm;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Util;
using GadgetCommon.Util;
using System;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace Estima
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            bool createdNew;
            string applicationName = Assembly.GetExecutingAssembly().GetName().Name;
            using (var mutex = new Mutex(true, applicationName, out createdNew))
            {
                try
                {
                    if (!createdNew)
                    {
                        // 多重起動の場合はエラーメッセージを表示して終了
                        CommonUtil.ShowErrorMsg(Messages.MsgErrorDoupleBoot);
                        return;
                    }
                    // アイコンをすべての画面に反映
                    typeof(Form).GetField("defaultIcon", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, new Icon(@"Estima.ico"));

                    // メニュ画面表示
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    var mainForm = new MainForm(GetDemoData());
                    Application.Run(mainForm);
                }
                catch (Exception e)
                {
                    EstimaUtil.ShowErrorMsg(e, applicationName);
                }
            }
        }
        #region Business
        /// <summary>
        /// デモデータ取得
        /// </summary>
        /// <returns></returns>
        private static LoginInfoDto GetDemoData()
        {
            return new LoginInfoDto()
            {
                TantoID = "1",
                KengenKbn = 10,
                Seq = 1,
                TantoName = "担当者001",
                BushoCD = "001001"
            };
        }
        #endregion
    }
}
