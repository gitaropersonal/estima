﻿using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace Estima.Forms.Main.MainForm.Business
{
    internal class FormLogic
    {
        #region Member
        private const int ThisWidth = 680;
        private const int ThisHeight = 510;
        private const int PanelWidth = 325;

        /// <summary>
        /// ボディパネル
        /// </summary>
        private MainForm Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo;
        /// <summary>
        /// システムメニュ一覧
        /// </summary>
        private List<ApplicationInfoDto> SystemMenus = new List<ApplicationInfoDto>();
        /// <summary>
        /// ユーザアプリケーション一覧
        /// </summary>
        private List<ApplicationInfoDto> Applications = new List<ApplicationInfoDto>();
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// ユーザアプリケーションボタン位置
        /// </summary>
        private const int UserApplicationStartX = 6;
        private const int UserApplicationSpan = 52;
        /// <summary>
        /// ログインユーザ情報
        /// </summary>
        private const string LblTxtLoginUserName = "ログイン：{0}";
        private const string LblTxtLoginUserAdmin = "（管理者）";
        /// <summary>
        /// アセンブリ名
        /// </summary>
        private readonly string AssemblyName = Assembly.GetExecutingAssembly().GetName().Name;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        public FormLogic(
               MainForm body
            ,  LoginInfoDto loginInfo
            )
        {
            this.Body = body;
            this.LoginInfo = loginInfo;
            new InitControl(this.Body);
            this.Init();

            #region システムメニュ
            this.Body.btnSysMenu1.Click += (s, e) => { this.BtnSysMenuClick(Enums.SysType.Estima); };
            this.Body.btnSysMenu2.Click += (s, e) => { this.BtnSysMenuClick(Enums.SysType.Master); };
            
            this.Body.btnSysMenu3.Click += (s, e) => { this.BtnSysMenuClick(Enums.SysType.System); };
            this.Body.btnSysMenu4.Click += (s, e) => { this.BtnSysMenuClick(Enums.SysType.Data); };
            #endregion

            #region ユーザアプリケーション
            // １．見積
            this.Body.btnAddEstima.Click += (s, e) => 
            {
                // 伝票ID作成
                string estimaID = EstimaUtil.CreateEstimaID();
                bool success = true;
                try
                {
                    CommonUtil.ShowProgressDialog(() =>
                    {
                        EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogAddLockEntity}：{estimaID}");
                        this.CommonService.AddLock(estimaID);
                    });
                    // 新規作成画面表示
                    var form = new Application.EST.EST0010.EST0010_EditEstima(
                          this.LoginInfo
                        , Enums.ScreenModeEditEstima.Add
                        , estimaID
                    );
                    this.ShowChildForm((Button)s, form, true);
                }
                catch (Exception ex)
                {
                    EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                    success = false;
                }
                finally
                {
                    if (success)
                    {
                        EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogDelLockEntity}：{estimaID}", false);
                        this.CommonService.DelLock(estimaID);
                    }
                }
            };
            this.Body.btnEstimaLooks.Click       += (s, e) => { this.ShowChildForm((Button)s, new Application.EST.EST0020.EST0020_EstimaLooks(this.LoginInfo)); };
            this.Body.btnConditionTemplete.Click += (s, e) => { this.ShowChildForm((Button)s, new Application.EST.EST0030.EST0030_ConditionTemplete(this.LoginInfo)); };
            // ２．マスタメンテナンス
            this.Body.btnMstParentCD.Click       += (s, e) => { this.ShowChildForm((Button)s, new Application.MST.MST0020.MST0020_MstParentCD(this.LoginInfo)); };
            this.Body.btnMstShohin.Click         += (s, e) =>
            {
                this.ShowChildForm((Button)s, new Application.MST.MST0010.MST0010_MstShohin(
                      this.LoginInfo
                    , Enums.ScreenModeMstShohin.Screen
                    , Enums.DetailKbn.All
                ));
            };
            this.Body.btnMstChildCD.Click += (s, e) =>
            {
                this.ShowChildForm((Button)s, new Application.MST.MST0030.MST0030_MstChildCD(
                      this.LoginInfo
                    , Enums.ScreenModeMstShohin.Screen
                ));
            };
            this.Body.btnAddressOffice.Click  += (s, e) => { this.ShowChildForm((Button)s, new Dialog.DE0020.DE0020_MstCommonDialog(Enums.MstType.AddressOffice, ImeMode.Hiragana)); };
            this.Body.btnMstSizeName.Click    += (s, e) => { this.ShowChildForm((Button)s, new Dialog.DE0020.DE0020_MstCommonDialog(Enums.MstType.ShohinSize   , ImeMode.Disable)); };
            // ３．システム
            this.Body.btnSysSearchLog.Click += (s, e) => { this.ShowChildForm((Button)s, new Application.SYS.SYS0010.SYS0010_SearchLog(this.LoginInfo)); };
            this.Body.btnSysReleaseLock.Click += (s, e) => { this.ShowChildForm((Button)s, new Application.SYS.SYS0020.SYS0020_ReleaseLock(this.LoginInfo)); };
            // ４．データ
            this.Body.btnConnectShohin.Click += (s, e) => { };
            #endregion

            #region ボタン
            // ログオフボタン押下
            this.Body.btnLogOff.Click += (s, e) => { this.FormClose(DialogResult.Retry); };
            // 終了ボタン押下
            this.Body.btnClose.Click += (s, e) => { this.FormClose(); };
            // 終了処理
            this.Body.FormClosing += this.ClosingEvent;
            #endregion

            #region ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.D1:
                        this.Body.btnSysMenu1.Focus();
                        this.Body.btnSysMenu1.PerformClick();
                        break;
                    case Keys.D2:
                        this.Body.btnSysMenu2.Focus();
                        this.Body.btnSysMenu2.PerformClick();
                        break;
                    case Keys.D3:
                        this.Body.btnSysMenu3.Focus();
                        this.Body.btnSysMenu3.PerformClick();
                        break;
                    case Keys.D4:
                        this.Body.btnSysMenu4.Focus();
                        this.Body.btnSysMenu4.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                    case Keys.F:
                        this.Body.btnLogOff.Focus();
                        this.Body.btnLogOff.PerformClick();
                        break;
                }
            };
            #endregion
        }
        #endregion

        #region Event
        /// <summary>
        /// システムメニュボタン押下処理
        /// </summary>
        /// <param name="sysType"></param>
        private void BtnSysMenuClick(Enums.SysType sysType)
        {
            this.Body.pnlUserApplication.AutoScroll = false;
            this.Body.pnlUserApplication.Height = ThisHeight * 10;

            // ソート
            this.Applications = this.Applications.OrderBy(n => (int)n.SystemType).ThenBy(n => n.EdaNum).ToList();

            // 選択されたメニュに属するアプリケーション
            bool focued = false;
            var tgtAppList = this.Applications.Where(n => n.SystemType == sysType).ToList();
            for (int i = 0; i < tgtAppList.Count; i++)
            {
                var tgtInfo = tgtAppList[i];
                tgtInfo.AppButton.Visible = true;
                tgtInfo.AppButton.Enabled = tgtInfo.DefaultEnabled;
                tgtInfo.AppButton.TabIndex = ((int)tgtInfo.SystemType * 100) + (i * 5);
                tgtInfo.AppButton.Location = new Point(UserApplicationStartX, this.Body.btnSysMenu1.Location.Y + (UserApplicationSpan * i));

                // 先頭の機能にフォーカス
                if (!focued && tgtInfo.AppButton.Enabled)
                {
                    tgtInfo.AppButton.Focus();
                    focued = true;
                }
            }
            // 選択されなかったメニュに属するアプリケーション
            var otherAppList = this.Applications.Where(n => n.SystemType != sysType).ToList();
            foreach (var otherInfo in otherAppList)
            {
                otherInfo.AppButton.Visible = false;
            }
            this.Body.pnlUserApplication.Height = ThisHeight;
            this.Body.pnlUserApplication.AutoScroll = true;
        }
        /// <summary>
        /// 子画面表示
        /// </summary>
        /// <param name="button"></param>
        /// <param name="childForm"></param>
        private void ShowChildForm(
              Button button
            , Form childForm
            , bool delay = false
            )
        {
            childForm.Text = button.Text;
            this.Body.Visible = false;
            EstimaUtil.AddLogInfo(this.Body.Text, childForm.Text, Enums.LogStatusKbn.Start);
            childForm.ShowDialog();
            CommonUtil.ShowProgressDialog(() =>
            {
                EstimaUtil.AddLogInfo(this.Body.Text, childForm.Text, Enums.LogStatusKbn.End);
                if (delay)
                {
                    Thread.Sleep(1000);
                }
            });
            this.Body.Visible = true;
            button.Focus();
        }
        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="dr"></param>
        private void FormClose(DialogResult dr = DialogResult.Cancel)
        {
            var drConfirm = DialogResult.None;
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                switch (dr)
                {
                    case DialogResult.Cancel:
                        drConfirm = CommonUtil.ShowInfoMsgOKCancel(Messages.MsgAskClose);
                        break;
                    case DialogResult.Retry:
                        drConfirm = CommonUtil.ShowInfoMsgOKCancel(Messages.MsgAskLogOff);
                        break;
                }
                if (drConfirm != DialogResult.OK)
                {
                    cancel = true;
                    return;
                }
                this.Body.DialogResult = dr;
                EstimaUtil.AddLogInfo(this.Body.Text, this.AssemblyName, Enums.LogStatusKbn.End);
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// フォーム終了処理
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                var drConfirm = CommonUtil.ShowInfoMsgOKCancel(Messages.MsgAskClose);
                if (drConfirm == DialogResult.OK)
                {
                    this.Body.DialogResult = drConfirm;
                    EstimaUtil.AddLogInfo(this.Body.Text, this.AssemblyName, Enums.LogStatusKbn.End);
                    return;
                }
                e.Cancel = true;
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            EstimaUtil.AddLogInfo(this.Body.Text, this.AssemblyName, Enums.LogStatusKbn.Start);

            // 画面サイズ
            this.Body.MaximumSize = new Size(ThisWidth, ThisHeight);
            this.Body.MinimumSize = new Size(ThisWidth, ThisHeight);
            // パネルサイズ
            this.Body.pnlUserApplication.Width = PanelWidth;
            // システムメニュを初期化
            this.InitLstSystemMenuInfo();
            // ユーザアプリケーションを初期化
            this.InitUserApplicationInfos();
            // バージョン
            this.Body.lblRevision.Text = CommonUtil.GetVersion();
            // ログイン権限
            this.Body.lblLoginInfo.Text = string.Format(LblTxtLoginUserName, LoginInfo.TantoName);
            if (LoginInfo.KengenKbn == (int)Enums.KengenKbn.Admin)
            {
                this.Body.lblLoginInfo.Text += LblTxtLoginUserAdmin;
            }
            // フォーカス
            this.Body.btnSysMenu1.Focus();
        }
        /// <summary>
        /// 初期化（システムメニュ情報）
        /// </summary>
        private void InitLstSystemMenuInfo()
        {
            this.SystemMenus.Clear();

            //// 管理者のみ使用できるシステムメニュ
            //bool enableAdmin = (this.LoginInfo.KengenKbn == (int)Enums.KengenKbn.Admin);

            // １．見積
            this.SystemMenus.Add(new ApplicationInfoDto(0, Enums.SysType.Estima, this.Body.btnSysMenu1));
            // ２．マスタメンテナンス
            this.SystemMenus.Add(new ApplicationInfoDto(0, Enums.SysType.Master, this.Body.btnSysMenu2));
            // ３．システム
            this.SystemMenus.Add(new ApplicationInfoDto(0, Enums.SysType.System, this.Body.btnSysMenu3));
            // ４．データ
            this.SystemMenus.Add(new ApplicationInfoDto(0, Enums.SysType.Data, this.Body.btnSysMenu4));

            // ボタンテキストの整備
            foreach (var dto in this.SystemMenus)
            {
                dto.AppButton.Enabled = dto.DefaultEnabled;
                dto.AppButton.Text = $"({(int)dto.SystemType}) {dto.AppButton.Text}";
            }
        }
        /// <summary>
        /// 初期化（ユーザアプリケーション情報）
        /// </summary>
        private void InitUserApplicationInfos()
        {
            this.Applications.Clear();

            // １．見積
            this.Applications.Add(new ApplicationInfoDto(1, Enums.SysType.Estima, this.Body.btnAddEstima));
            this.Applications.Add(new ApplicationInfoDto(2, Enums.SysType.Estima, this.Body.btnEstimaLooks));
            this.Applications.Add(new ApplicationInfoDto(3, Enums.SysType.Estima, this.Body.btnConditionTemplete));
            // ２．マスタメンテナンス
            this.Applications.Add(new ApplicationInfoDto(1, Enums.SysType.Master, this.Body.btnMstShohin));
            this.Applications.Add(new ApplicationInfoDto(2, Enums.SysType.Master, this.Body.btnMstParentCD));
            this.Applications.Add(new ApplicationInfoDto(3, Enums.SysType.Master, this.Body.btnMstChildCD));
            this.Applications.Add(new ApplicationInfoDto(4, Enums.SysType.Master, this.Body.btnMstSizeName));
            this.Applications.Add(new ApplicationInfoDto(5, Enums.SysType.Master, this.Body.btnAddressOffice));
            // ３．システム
            this.Applications.Add(new ApplicationInfoDto(1, Enums.SysType.System, this.Body.btnSysSearchLog));
            this.Applications.Add(new ApplicationInfoDto(2, Enums.SysType.System, this.Body.btnSysReleaseLock));
            // ４．データ
            this.Applications.Add(new ApplicationInfoDto(1, Enums.SysType.Data, this.Body.btnConnectShohin, false));

            // ボタンテキストの整備
            foreach (var dto in this.Applications)
            {
                dto.AppButton.Text = $"({(int)dto.SystemType}) - {dto.EdaNum}.{dto.AppButton.Text}";
            }
        }
        #endregion
    }
}
