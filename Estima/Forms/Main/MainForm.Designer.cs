﻿namespace Estima.Forms.Main.MainForm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnLogOff = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.pnlUserApplication = new System.Windows.Forms.Panel();
            this.grpUserApplication = new System.Windows.Forms.GroupBox();
            this.pnlUserAppInner = new System.Windows.Forms.Panel();
            this.btnSysReleaseLock = new System.Windows.Forms.Button();
            this.btnMstChildCD = new System.Windows.Forms.Button();
            this.btnConditionTemplete = new System.Windows.Forms.Button();
            this.btnAddressTanto = new System.Windows.Forms.Button();
            this.btnAddressOffice = new System.Windows.Forms.Button();
            this.btnMstAddressCompany = new System.Windows.Forms.Button();
            this.btnMstTanto = new System.Windows.Forms.Button();
            this.btnSysSearchLog = new System.Windows.Forms.Button();
            this.btnConnectShohin = new System.Windows.Forms.Button();
            this.btnEstimaLooks = new System.Windows.Forms.Button();
            this.btnMstSizeName = new System.Windows.Forms.Button();
            this.btnMstParentCD = new System.Windows.Forms.Button();
            this.btnMstShohin = new System.Windows.Forms.Button();
            this.btnAddEstima = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.grpSystemMenu = new System.Windows.Forms.GroupBox();
            this.pnlSysMenuInner = new System.Windows.Forms.Panel();
            this.btnSysMenu3 = new System.Windows.Forms.Button();
            this.btnSysMenu4 = new System.Windows.Forms.Button();
            this.btnSysMenu2 = new System.Windows.Forms.Button();
            this.btnSysMenu1 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblRevision = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLoginInfo = new System.Windows.Forms.Label();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.pnlFootor.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlExecute.SuspendLayout();
            this.pnlUserApplication.SuspendLayout();
            this.grpUserApplication.SuspendLayout();
            this.pnlUserAppInner.SuspendLayout();
            this.panel2.SuspendLayout();
            this.grpSystemMenu.SuspendLayout();
            this.pnlSysMenuInner.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.panel3);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 847);
            this.pnlFootor.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(938, 75);
            this.pnlFootor.TabIndex = 200;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnLogOff);
            this.panel3.Controls.Add(this.btnClose);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(553, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(385, 75);
            this.panel3.TabIndex = 1000;
            // 
            // btnLogOff
            // 
            this.btnLogOff.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLogOff.Location = new System.Drawing.Point(97, 9);
            this.btnLogOff.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnLogOff.Name = "btnLogOff";
            this.btnLogOff.Size = new System.Drawing.Size(138, 52);
            this.btnLogOff.TabIndex = 1000;
            this.btnLogOff.Text = "ログオフ（F）";
            this.btnLogOff.UseVisualStyleBackColor = true;
            this.btnLogOff.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(235, 9);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 2000;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.pnlUserApplication);
            this.pnlExecute.Controls.Add(this.panel2);
            this.pnlExecute.Controls.Add(this.pnlFootor);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Padding = new System.Windows.Forms.Padding(0, 35, 0, 0);
            this.pnlExecute.Size = new System.Drawing.Size(938, 922);
            this.pnlExecute.TabIndex = 2;
            // 
            // pnlUserApplication
            // 
            this.pnlUserApplication.AutoScroll = true;
            this.pnlUserApplication.Controls.Add(this.grpUserApplication);
            this.pnlUserApplication.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUserApplication.Location = new System.Drawing.Point(332, 35);
            this.pnlUserApplication.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlUserApplication.Name = "pnlUserApplication";
            this.pnlUserApplication.Padding = new System.Windows.Forms.Padding(0, 0, 12, 0);
            this.pnlUserApplication.Size = new System.Drawing.Size(606, 812);
            this.pnlUserApplication.TabIndex = 100;
            // 
            // grpUserApplication
            // 
            this.grpUserApplication.Controls.Add(this.pnlUserAppInner);
            this.grpUserApplication.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpUserApplication.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpUserApplication.Location = new System.Drawing.Point(0, 0);
            this.grpUserApplication.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpUserApplication.Name = "grpUserApplication";
            this.grpUserApplication.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpUserApplication.Size = new System.Drawing.Size(594, 812);
            this.grpUserApplication.TabIndex = 102;
            this.grpUserApplication.TabStop = false;
            this.grpUserApplication.Text = "ユーザアプリケーション";
            // 
            // pnlUserAppInner
            // 
            this.pnlUserAppInner.AutoScroll = true;
            this.pnlUserAppInner.Controls.Add(this.btnSysReleaseLock);
            this.pnlUserAppInner.Controls.Add(this.btnMstChildCD);
            this.pnlUserAppInner.Controls.Add(this.btnConditionTemplete);
            this.pnlUserAppInner.Controls.Add(this.btnAddressTanto);
            this.pnlUserAppInner.Controls.Add(this.btnAddressOffice);
            this.pnlUserAppInner.Controls.Add(this.btnMstAddressCompany);
            this.pnlUserAppInner.Controls.Add(this.btnMstTanto);
            this.pnlUserAppInner.Controls.Add(this.btnSysSearchLog);
            this.pnlUserAppInner.Controls.Add(this.btnConnectShohin);
            this.pnlUserAppInner.Controls.Add(this.btnEstimaLooks);
            this.pnlUserAppInner.Controls.Add(this.btnMstSizeName);
            this.pnlUserAppInner.Controls.Add(this.btnMstParentCD);
            this.pnlUserAppInner.Controls.Add(this.btnMstShohin);
            this.pnlUserAppInner.Controls.Add(this.btnAddEstima);
            this.pnlUserAppInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlUserAppInner.Location = new System.Drawing.Point(3, 25);
            this.pnlUserAppInner.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlUserAppInner.Name = "pnlUserAppInner";
            this.pnlUserAppInner.Size = new System.Drawing.Size(588, 783);
            this.pnlUserAppInner.TabIndex = 100;
            // 
            // btnSysReleaseLock
            // 
            this.btnSysReleaseLock.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysReleaseLock.Location = new System.Drawing.Point(7, 369);
            this.btnSysReleaseLock.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnSysReleaseLock.Name = "btnSysReleaseLock";
            this.btnSysReleaseLock.Size = new System.Drawing.Size(270, 52);
            this.btnSysReleaseLock.TabIndex = 419;
            this.btnSysReleaseLock.Text = "ロック解除";
            this.btnSysReleaseLock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysReleaseLock.UseVisualStyleBackColor = true;
            this.btnSysReleaseLock.Visible = false;
            // 
            // btnMstChildCD
            // 
            this.btnMstChildCD.Enabled = false;
            this.btnMstChildCD.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMstChildCD.Location = new System.Drawing.Point(287, 369);
            this.btnMstChildCD.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnMstChildCD.Name = "btnMstChildCD";
            this.btnMstChildCD.Size = new System.Drawing.Size(270, 52);
            this.btnMstChildCD.TabIndex = 418;
            this.btnMstChildCD.Text = "子商品コード";
            this.btnMstChildCD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMstChildCD.UseVisualStyleBackColor = true;
            this.btnMstChildCD.Visible = false;
            // 
            // btnConditionTemplete
            // 
            this.btnConditionTemplete.Enabled = false;
            this.btnConditionTemplete.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnConditionTemplete.Location = new System.Drawing.Point(7, 109);
            this.btnConditionTemplete.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnConditionTemplete.Name = "btnConditionTemplete";
            this.btnConditionTemplete.Size = new System.Drawing.Size(270, 52);
            this.btnConditionTemplete.TabIndex = 417;
            this.btnConditionTemplete.Text = "条件テンプレート";
            this.btnConditionTemplete.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConditionTemplete.UseVisualStyleBackColor = true;
            this.btnConditionTemplete.Visible = false;
            // 
            // btnAddressTanto
            // 
            this.btnAddressTanto.Enabled = false;
            this.btnAddressTanto.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAddressTanto.Location = new System.Drawing.Point(287, 161);
            this.btnAddressTanto.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnAddressTanto.Name = "btnAddressTanto";
            this.btnAddressTanto.Size = new System.Drawing.Size(270, 52);
            this.btnAddressTanto.TabIndex = 416;
            this.btnAddressTanto.Text = "宛先（担当者）";
            this.btnAddressTanto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddressTanto.UseVisualStyleBackColor = true;
            this.btnAddressTanto.Visible = false;
            // 
            // btnAddressOffice
            // 
            this.btnAddressOffice.Enabled = false;
            this.btnAddressOffice.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAddressOffice.Location = new System.Drawing.Point(287, 109);
            this.btnAddressOffice.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnAddressOffice.Name = "btnAddressOffice";
            this.btnAddressOffice.Size = new System.Drawing.Size(270, 52);
            this.btnAddressOffice.TabIndex = 415;
            this.btnAddressOffice.Text = "宛先（営業所）";
            this.btnAddressOffice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddressOffice.UseVisualStyleBackColor = true;
            this.btnAddressOffice.Visible = false;
            // 
            // btnMstAddressCompany
            // 
            this.btnMstAddressCompany.Enabled = false;
            this.btnMstAddressCompany.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMstAddressCompany.Location = new System.Drawing.Point(287, 57);
            this.btnMstAddressCompany.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnMstAddressCompany.Name = "btnMstAddressCompany";
            this.btnMstAddressCompany.Size = new System.Drawing.Size(270, 52);
            this.btnMstAddressCompany.TabIndex = 413;
            this.btnMstAddressCompany.Text = "宛先（会社）";
            this.btnMstAddressCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMstAddressCompany.UseVisualStyleBackColor = true;
            this.btnMstAddressCompany.Visible = false;
            // 
            // btnMstTanto
            // 
            this.btnMstTanto.Enabled = false;
            this.btnMstTanto.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMstTanto.Location = new System.Drawing.Point(287, 5);
            this.btnMstTanto.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnMstTanto.Name = "btnMstTanto";
            this.btnMstTanto.Size = new System.Drawing.Size(270, 52);
            this.btnMstTanto.TabIndex = 412;
            this.btnMstTanto.Text = "担当者";
            this.btnMstTanto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMstTanto.UseVisualStyleBackColor = true;
            this.btnMstTanto.Visible = false;
            // 
            // btnSysSearchLog
            // 
            this.btnSysSearchLog.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysSearchLog.Location = new System.Drawing.Point(7, 317);
            this.btnSysSearchLog.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnSysSearchLog.Name = "btnSysSearchLog";
            this.btnSysSearchLog.Size = new System.Drawing.Size(270, 52);
            this.btnSysSearchLog.TabIndex = 410;
            this.btnSysSearchLog.Text = "ログ";
            this.btnSysSearchLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysSearchLog.UseVisualStyleBackColor = true;
            this.btnSysSearchLog.Visible = false;
            // 
            // btnConnectShohin
            // 
            this.btnConnectShohin.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnConnectShohin.Location = new System.Drawing.Point(7, 161);
            this.btnConnectShohin.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnConnectShohin.Name = "btnConnectShohin";
            this.btnConnectShohin.Size = new System.Drawing.Size(270, 52);
            this.btnConnectShohin.TabIndex = 409;
            this.btnConnectShohin.Text = "商品マスタ取込";
            this.btnConnectShohin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnectShohin.UseVisualStyleBackColor = true;
            this.btnConnectShohin.Visible = false;
            // 
            // btnEstimaLooks
            // 
            this.btnEstimaLooks.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEstimaLooks.Location = new System.Drawing.Point(7, 57);
            this.btnEstimaLooks.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnEstimaLooks.Name = "btnEstimaLooks";
            this.btnEstimaLooks.Size = new System.Drawing.Size(270, 52);
            this.btnEstimaLooks.TabIndex = 408;
            this.btnEstimaLooks.Text = "書類一覧";
            this.btnEstimaLooks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstimaLooks.UseVisualStyleBackColor = true;
            this.btnEstimaLooks.Visible = false;
            // 
            // btnMstSizeName
            // 
            this.btnMstSizeName.Enabled = false;
            this.btnMstSizeName.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMstSizeName.Location = new System.Drawing.Point(287, 317);
            this.btnMstSizeName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnMstSizeName.Name = "btnMstSizeName";
            this.btnMstSizeName.Size = new System.Drawing.Size(270, 52);
            this.btnMstSizeName.TabIndex = 405;
            this.btnMstSizeName.Text = "サイズ";
            this.btnMstSizeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMstSizeName.UseVisualStyleBackColor = true;
            this.btnMstSizeName.Visible = false;
            // 
            // btnMstParentCD
            // 
            this.btnMstParentCD.Enabled = false;
            this.btnMstParentCD.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMstParentCD.Location = new System.Drawing.Point(287, 213);
            this.btnMstParentCD.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnMstParentCD.Name = "btnMstParentCD";
            this.btnMstParentCD.Size = new System.Drawing.Size(270, 52);
            this.btnMstParentCD.TabIndex = 401;
            this.btnMstParentCD.Text = "親コード";
            this.btnMstParentCD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMstParentCD.UseVisualStyleBackColor = true;
            this.btnMstParentCD.Visible = false;
            // 
            // btnMstShohin
            // 
            this.btnMstShohin.Enabled = false;
            this.btnMstShohin.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnMstShohin.Location = new System.Drawing.Point(287, 265);
            this.btnMstShohin.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnMstShohin.Name = "btnMstShohin";
            this.btnMstShohin.Size = new System.Drawing.Size(270, 52);
            this.btnMstShohin.TabIndex = 400;
            this.btnMstShohin.Text = "商品";
            this.btnMstShohin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMstShohin.UseVisualStyleBackColor = true;
            this.btnMstShohin.Visible = false;
            // 
            // btnAddEstima
            // 
            this.btnAddEstima.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAddEstima.Location = new System.Drawing.Point(7, 5);
            this.btnAddEstima.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnAddEstima.Name = "btnAddEstima";
            this.btnAddEstima.Size = new System.Drawing.Size(270, 52);
            this.btnAddEstima.TabIndex = 100;
            this.btnAddEstima.Text = "書類作成";
            this.btnAddEstima.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddEstima.UseVisualStyleBackColor = true;
            this.btnAddEstima.Visible = false;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.grpSystemMenu);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 35);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.panel2.Size = new System.Drawing.Size(332, 812);
            this.panel2.TabIndex = 0;
            // 
            // grpSystemMenu
            // 
            this.grpSystemMenu.Controls.Add(this.pnlSysMenuInner);
            this.grpSystemMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSystemMenu.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpSystemMenu.Location = new System.Drawing.Point(12, 0);
            this.grpSystemMenu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpSystemMenu.Name = "grpSystemMenu";
            this.grpSystemMenu.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grpSystemMenu.Size = new System.Drawing.Size(308, 812);
            this.grpSystemMenu.TabIndex = 1;
            this.grpSystemMenu.TabStop = false;
            this.grpSystemMenu.Text = "システムメニュ";
            // 
            // pnlSysMenuInner
            // 
            this.pnlSysMenuInner.AutoScroll = true;
            this.pnlSysMenuInner.Controls.Add(this.btnSysMenu3);
            this.pnlSysMenuInner.Controls.Add(this.btnSysMenu4);
            this.pnlSysMenuInner.Controls.Add(this.btnSysMenu2);
            this.pnlSysMenuInner.Controls.Add(this.btnSysMenu1);
            this.pnlSysMenuInner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSysMenuInner.Location = new System.Drawing.Point(3, 25);
            this.pnlSysMenuInner.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlSysMenuInner.Name = "pnlSysMenuInner";
            this.pnlSysMenuInner.Size = new System.Drawing.Size(302, 783);
            this.pnlSysMenuInner.TabIndex = 0;
            // 
            // btnSysMenu3
            // 
            this.btnSysMenu3.Enabled = false;
            this.btnSysMenu3.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysMenu3.Location = new System.Drawing.Point(6, 109);
            this.btnSysMenu3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnSysMenu3.Name = "btnSysMenu3";
            this.btnSysMenu3.Size = new System.Drawing.Size(270, 52);
            this.btnSysMenu3.TabIndex = 2;
            this.btnSysMenu3.Text = "システム";
            this.btnSysMenu3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysMenu3.UseVisualStyleBackColor = true;
            // 
            // btnSysMenu4
            // 
            this.btnSysMenu4.Enabled = false;
            this.btnSysMenu4.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysMenu4.Location = new System.Drawing.Point(6, 161);
            this.btnSysMenu4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnSysMenu4.Name = "btnSysMenu4";
            this.btnSysMenu4.Size = new System.Drawing.Size(270, 52);
            this.btnSysMenu4.TabIndex = 4;
            this.btnSysMenu4.Text = "データ";
            this.btnSysMenu4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysMenu4.UseVisualStyleBackColor = true;
            // 
            // btnSysMenu2
            // 
            this.btnSysMenu2.Enabled = false;
            this.btnSysMenu2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysMenu2.Location = new System.Drawing.Point(6, 57);
            this.btnSysMenu2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnSysMenu2.Name = "btnSysMenu2";
            this.btnSysMenu2.Size = new System.Drawing.Size(270, 52);
            this.btnSysMenu2.TabIndex = 1;
            this.btnSysMenu2.Text = "マスタメンテナンス";
            this.btnSysMenu2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysMenu2.UseVisualStyleBackColor = true;
            // 
            // btnSysMenu1
            // 
            this.btnSysMenu1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSysMenu1.Location = new System.Drawing.Point(6, 5);
            this.btnSysMenu1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnSysMenu1.Name = "btnSysMenu1";
            this.btnSysMenu1.Size = new System.Drawing.Size(270, 52);
            this.btnSysMenu1.TabIndex = 0;
            this.btnSysMenu1.Text = "会計書類";
            this.btnSysMenu1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSysMenu1.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblRevision);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(676, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 12, 12, 0);
            this.panel6.Size = new System.Drawing.Size(262, 31);
            this.panel6.TabIndex = 1;
            // 
            // lblRevision
            // 
            this.lblRevision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRevision.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRevision.Location = new System.Drawing.Point(0, 12);
            this.lblRevision.Name = "lblRevision";
            this.lblRevision.Size = new System.Drawing.Size(250, 19);
            this.lblRevision.TabIndex = 234;
            this.lblRevision.Text = "1.X.X.XXX";
            this.lblRevision.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblLoginInfo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(12, 12, 0, 0);
            this.panel1.Size = new System.Drawing.Size(500, 31);
            this.panel1.TabIndex = 1;
            this.panel1.Visible = false;
            // 
            // lblLoginInfo
            // 
            this.lblLoginInfo.AutoSize = true;
            this.lblLoginInfo.Font = new System.Drawing.Font("Meiryo UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblLoginInfo.Location = new System.Drawing.Point(12, 6);
            this.lblLoginInfo.Name = "lblLoginInfo";
            this.lblLoginInfo.Size = new System.Drawing.Size(59, 17);
            this.lblLoginInfo.TabIndex = 235;
            this.lblLoginInfo.Text = "ログイン：";
            this.lblLoginInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.panel1);
            this.pnlHeader.Controls.Add(this.panel6);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(938, 31);
            this.pnlHeader.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 922);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnlExecute);
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "メインメニュ";
            this.pnlFootor.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pnlExecute.ResumeLayout(false);
            this.pnlUserApplication.ResumeLayout(false);
            this.grpUserApplication.ResumeLayout(false);
            this.pnlUserAppInner.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.grpSystemMenu.ResumeLayout(false);
            this.pnlSysMenuInner.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel pnlFootor;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Button btnLogOff;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.Panel pnlUserApplication;
        private System.Windows.Forms.GroupBox grpUserApplication;
        private System.Windows.Forms.Panel pnlUserAppInner;
        public System.Windows.Forms.Button btnAddressTanto;
        public System.Windows.Forms.Button btnAddressOffice;
        public System.Windows.Forms.Button btnMstAddressCompany;
        public System.Windows.Forms.Button btnMstTanto;
        public System.Windows.Forms.Button btnSysSearchLog;
        public System.Windows.Forms.Button btnConnectShohin;
        public System.Windows.Forms.Button btnEstimaLooks;
        public System.Windows.Forms.Button btnMstSizeName;
        public System.Windows.Forms.Button btnMstParentCD;
        public System.Windows.Forms.Button btnMstShohin;
        public System.Windows.Forms.Button btnAddEstima;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox grpSystemMenu;
        private System.Windows.Forms.Panel pnlSysMenuInner;
        public System.Windows.Forms.Button btnSysMenu3;
        public System.Windows.Forms.Button btnSysMenu4;
        public System.Windows.Forms.Button btnSysMenu2;
        public System.Windows.Forms.Button btnSysMenu1;
        private System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Label lblRevision;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label lblLoginInfo;
        public System.Windows.Forms.Panel pnlHeader;
        public System.Windows.Forms.Button btnConditionTemplete;
        public System.Windows.Forms.Button btnMstChildCD;
        public System.Windows.Forms.Button btnSysReleaseLock;
    }
}