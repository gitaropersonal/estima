﻿using Estima.Forms.Main.MainForm.Business;
using EstimaLib.Dto;
using System.Windows.Forms;

namespace Estima.Forms.Main.MainForm
{
    public partial class MainForm : Form
    {
        public MainForm(LoginInfoDto loginInfo)
        {
            this.InitializeComponent();
            new FormLogic(this, loginInfo);
        }
    }
}
