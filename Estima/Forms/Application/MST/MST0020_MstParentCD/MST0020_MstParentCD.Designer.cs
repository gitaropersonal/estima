﻿namespace Estima.Forms.Application.MST.MST0020
{
    partial class MST0020_MstParentCD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cmbDetailKbn = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtParentCDName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtParentCD = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dgvMaster = new System.Windows.Forms.DataGridView();
            this.pnlBtnAdd = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.ChkSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BtnDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ParentCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentCDName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailKbnName = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.UsedByEstima = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaster)).BeginInit();
            this.pnlBtnAdd.SuspendLayout();
            this.panel14.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(19, 11);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(626, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(168, 75);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(11, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(157, 11);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(138, 52);
            this.btnUpdate.TabIndex = 112;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(344, 75);
            this.panel4.TabIndex = 110;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 536);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(794, 75);
            this.panel3.TabIndex = 106;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(19, 18, 0, 0);
            this.panel1.Size = new System.Drawing.Size(794, 118);
            this.panel1.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.cmbDetailKbn);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.txtParentCDName);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.txtParentCD);
            this.panel7.Controls.Add(this.lblName);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(19, 18);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.panel7.Size = new System.Drawing.Size(410, 100);
            this.panel7.TabIndex = 0;
            // 
            // cmbDetailKbn
            // 
            this.cmbDetailKbn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDetailKbn.FormattingEnabled = true;
            this.cmbDetailKbn.Location = new System.Drawing.Point(112, 61);
            this.cmbDetailKbn.Name = "cmbDetailKbn";
            this.cmbDetailKbn.Size = new System.Drawing.Size(164, 28);
            this.cmbDetailKbn.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(4, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "明細内訳区分";
            // 
            // txtParentCDName
            // 
            this.txtParentCDName.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtParentCDName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtParentCDName.Location = new System.Drawing.Point(112, 32);
            this.txtParentCDName.Margin = new System.Windows.Forms.Padding(4);
            this.txtParentCDName.MaxLength = 25;
            this.txtParentCDName.Name = "txtParentCDName";
            this.txtParentCDName.Size = new System.Drawing.Size(253, 28);
            this.txtParentCDName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(4, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "名称";
            // 
            // txtParentCD
            // 
            this.txtParentCD.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtParentCD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtParentCD.Location = new System.Drawing.Point(112, 3);
            this.txtParentCD.Margin = new System.Windows.Forms.Padding(4);
            this.txtParentCD.MaxLength = 3;
            this.txtParentCD.Name = "txtParentCD";
            this.txtParentCD.Size = new System.Drawing.Size(78, 28);
            this.txtParentCD.TabIndex = 0;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblName.Location = new System.Drawing.Point(4, 6);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(60, 20);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "親コード";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(626, 18);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(168, 100);
            this.panel2.TabIndex = 23;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(11, 37);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(138, 52);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dgvMaster);
            this.panel6.Controls.Add(this.pnlBtnAdd);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel6.Location = new System.Drawing.Point(0, 118);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(19, 12, 19, 6);
            this.panel6.Size = new System.Drawing.Size(794, 418);
            this.panel6.TabIndex = 30;
            // 
            // dgvMaster
            // 
            this.dgvMaster.AllowUserToAddRows = false;
            this.dgvMaster.AllowUserToDeleteRows = false;
            this.dgvMaster.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvMaster.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaster.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChkSelect,
            this.BtnDelete,
            this.ParentCD,
            this.ParentSeq,
            this.ParentCDName,
            this.DetailKbn,
            this.DetailKbnName,
            this.UsedByEstima,
            this.Status});
            this.dgvMaster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaster.Location = new System.Drawing.Point(19, 82);
            this.dgvMaster.Margin = new System.Windows.Forms.Padding(4);
            this.dgvMaster.Name = "dgvMaster";
            this.dgvMaster.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvMaster.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvMaster.RowTemplate.Height = 21;
            this.dgvMaster.Size = new System.Drawing.Size(756, 330);
            this.dgvMaster.TabIndex = 35;
            // 
            // pnlBtnAdd
            // 
            this.pnlBtnAdd.Controls.Add(this.panel14);
            this.pnlBtnAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBtnAdd.Location = new System.Drawing.Point(19, 12);
            this.pnlBtnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.pnlBtnAdd.Name = "pnlBtnAdd";
            this.pnlBtnAdd.Size = new System.Drawing.Size(756, 70);
            this.pnlBtnAdd.TabIndex = 32;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnAdd);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Margin = new System.Windows.Forms.Padding(4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(192, 70);
            this.panel14.TabIndex = 31;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAdd.Location = new System.Drawing.Point(12, 8);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(138, 52);
            this.btnAdd.TabIndex = 31;
            this.btnAdd.Text = "行追加(I)";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // ChkSelect
            // 
            this.ChkSelect.DataPropertyName = "ChkSelect";
            this.ChkSelect.HeaderText = "選択";
            this.ChkSelect.Name = "ChkSelect";
            this.ChkSelect.Width = 60;
            // 
            // BtnDelete
            // 
            this.BtnDelete.DataPropertyName = "BtnDelete";
            this.BtnDelete.HeaderText = "";
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Width = 60;
            // 
            // ParentCD
            // 
            this.ParentCD.DataPropertyName = "ParentCD";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ParentCD.DefaultCellStyle = dataGridViewCellStyle2;
            this.ParentCD.HeaderText = "親コード";
            this.ParentCD.MaxInputLength = 3;
            this.ParentCD.Name = "ParentCD";
            this.ParentCD.Width = 90;
            // 
            // ParentSeq
            // 
            this.ParentSeq.DataPropertyName = "ParentSeq";
            this.ParentSeq.HeaderText = "ParentSeq";
            this.ParentSeq.Name = "ParentSeq";
            this.ParentSeq.Visible = false;
            // 
            // ParentCDName
            // 
            this.ParentCDName.DataPropertyName = "ParentCDName";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ParentCDName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ParentCDName.HeaderText = "名称";
            this.ParentCDName.MaxInputLength = 25;
            this.ParentCDName.Name = "ParentCDName";
            this.ParentCDName.Width = 250;
            // 
            // DetailKbn
            // 
            this.DetailKbn.DataPropertyName = "DetailKbn";
            this.DetailKbn.HeaderText = "明細内訳区分";
            this.DetailKbn.Name = "DetailKbn";
            this.DetailKbn.Visible = false;
            // 
            // DetailKbnName
            // 
            this.DetailKbnName.DataPropertyName = "DetailKbnName";
            this.DetailKbnName.HeaderText = "明細内訳区分名";
            this.DetailKbnName.Name = "DetailKbnName";
            this.DetailKbnName.Width = 150;
            // 
            // UsedByEstima
            // 
            this.UsedByEstima.DataPropertyName = "UsedByEstima";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.NullValue = false;
            this.UsedByEstima.DefaultCellStyle = dataGridViewCellStyle4;
            this.UsedByEstima.HeaderText = "商品マスタで使用";
            this.UsedByEstima.Name = "UsedByEstima";
            this.UsedByEstima.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Status.DefaultCellStyle = dataGridViewCellStyle5;
            this.Status.HeaderText = "ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            this.Status.Width = 83;
            // 
            // MST0020_MstParentCD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 611);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(810, 650);
            this.Name = "MST0020_MstParentCD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "親コード";
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaster)).EndInit();
            this.pnlBtnAdd.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel7;
        public System.Windows.Forms.TextBox txtParentCD;
        public System.Windows.Forms.Label lblName;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.ComboBox cmbDetailKbn;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtParentCDName;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Panel pnlBtnAdd;
        public System.Windows.Forms.Panel panel14;
        public System.Windows.Forms.Button btnAdd;
        public System.Windows.Forms.DataGridView dgvMaster;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ChkSelect;
        private System.Windows.Forms.DataGridViewButtonColumn BtnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentCDName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DetailKbn;
        private System.Windows.Forms.DataGridViewComboBoxColumn DetailKbnName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn UsedByEstima;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}