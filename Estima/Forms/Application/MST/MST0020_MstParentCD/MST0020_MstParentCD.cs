﻿using Estima.Forms.Application.MST.MST0020.Business;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Estima.Forms.Application.MST.MST0020
{
    public partial class MST0020_MstParentCD : Form
    {
        /// <summary>
        /// 選択されたグリッド行dto
        /// </summary>
        public List<MST0020_GridDto> SelectedDtoList = new List<MST0020_GridDto>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        public MST0020_MstParentCD(
              LoginInfoDto loginInfo
            , Enums.ScreenModeMstShohin screenMode = Enums.ScreenModeMstShohin.Screen
            )
        {
            this.InitializeComponent();
            this.SelectedDtoList = new List<MST0020_GridDto>();
            try
            {
                new MST0020_FormLogic(
                      this
                    , loginInfo
                    , screenMode
                );
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
