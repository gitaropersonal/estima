﻿using EstimaLib.Const;

namespace Estima.Forms.Application.MST.MST0020.Business
{
    public class MST0020_GridDto
    {
        public MST0020_GridDto()
        {
            this.ChkSelect = false;
            this.BtnDelete = "削除";
            this.Status = Enums.EditStatus.None;
        }
        public bool ChkSelect { get; set; }
        public string BtnDelete { get; set; }
        public string ParentCD { get; set; }
        public string ParentSeq { get; set; }
        public string ParentCDName { get; set; }
        public string DetailKbn { get; set; }
        public string DetailKbnName { get; set; }
        public bool UsedByEstima { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
