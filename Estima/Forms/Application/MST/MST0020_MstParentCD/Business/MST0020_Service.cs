﻿using EstimaLib.Const;
using EstimaLib.Entity;
using EstimaLib.Util;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace Estima.Forms.Application.MST.MST0020.Business
{
    internal class MST0020_Service
    {
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        internal BindingList<MST0020_GridDto> GetGridDatas(MST0020_GridDto con)
        {
            string cmOutParentCD = EstimaUtil.GetCmout(string.IsNullOrEmpty(con.ParentCD));
            string cmOutParentCDName = EstimaUtil.GetCmout(string.IsNullOrEmpty(con.ParentCDName));
            string cmOutDetailKbn = EstimaUtil.GetCmout(string.IsNullOrEmpty(con.DetailKbn));
            var ret = new BindingList<MST0020_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX02.ParentCD ");
            sb.AppendLine($",   MX02.ParentSeq ");
            sb.AppendLine($",   MX02.ParentCDName ");
            sb.AppendLine($",   MX02.DetailKbn ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX02ParentCD AS MX02 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutParentCD    }AND MX02.ParentCD     LIKE @parentCD ");
            sb.AppendLine($"{cmOutParentCDName}AND MX02.ParentCDName LIKE @parentCDName ");
            sb.AppendLine($"{cmOutDetailKbn   }AND MX02.DetailKbn       = @detailKbn ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    MX02.ParentCD ");
            sb.AppendLine($",   MX02.ParentCDName ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@parentCD", "%" + con.ParentCD + "%");
                    cmd.Parameters.AddWithValue("@parentCDName", "%" + con.ParentCDName + "%");
                    cmd.Parameters.Add(new MySqlParameter("@detailKbn", con.DetailKbn));
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new MST0020_GridDto();
                            dto.ParentCD = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentCD)]);
                            dto.ParentSeq = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentSeq)]);
                            dto.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentCDName)]);
                            dto.DetailKbn = CommonUtil.TostringNullForbid(dr[nameof(dto.DetailKbn)]);
                            dto.DetailKbnName = Enums.DetailKbns.First(n => (int)n.Key == CommonUtil.ToInteger(dto.DetailKbn)).Value;
                            dto.UsedByEstima = this.JudgeExistEstimaHavingParentCD(dto);
                            dto.Status = Enums.EditStatus.Show;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 次のSEQ取得
        /// </summary>
        /// <param name="parentCD"></param>
        /// <returns></returns>
        private int GetNextSeq(string parentCD)
        {
            var ret = 0;

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MAX(MX02.ParentSeq) ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX02ParentCD AS MX02 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"AND MX02.ParentCD = @parentCD ");
            sb.AppendLine($"GROUP BY ");
            sb.AppendLine($"    MX02.ParentCD ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@parentCD", parentCD));
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret = CommonUtil.ToInteger(dr[0]) + 1;
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// データ登録
        /// </summary>
        /// <param name="addData"></param>
        internal void AddData(MX02ParentCD addData)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO MX02ParentCD( ");
            sb.AppendLine($"    ParentCD ");
            sb.AppendLine($",   ParentSeq ");
            sb.AppendLine($",   ParentCDName ");
            sb.AppendLine($",   DetailKbn ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @parentCD ");
            sb.AppendLine($",   @parentSeq ");
            sb.AppendLine($",   @parentCDName ");
            sb.AppendLine($",   @detailKbn ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($") ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@parentCD", addData.ParentCD));
                    cmd.Parameters.Add(new MySqlParameter("@parentSeq", this.GetNextSeq(addData.ParentCD)));
                    cmd.Parameters.Add(new MySqlParameter("@parentCDName", addData.ParentCDName));
                    cmd.Parameters.Add(new MySqlParameter("@detailKbn", addData.DetailKbn) { MySqlDbType = MySqlDbType.Int32});
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// データ更新
        /// </summary>
        /// <param name="updData"></param>
        internal void UpdData(MX02ParentCD updData)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"UPDATE ");
            sb.AppendLine($"    MX02ParentCD ");
            sb.AppendLine($"SET ");
            sb.AppendLine($"    ParentCDName = @parentCDName ");
            sb.AppendLine($",   DetailKbn    = @detailKbn ");
            sb.AppendLine($",   UpdIPAddress = '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   UpdHostName  = '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   UpdDate      = CURRENT_TIMESTAMP ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    ParentCD  = @parentCD ");
            sb.AppendLine($"AND ParentSeq = @parentSeq ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@parentCD", updData.ParentCD));
                    cmd.Parameters.Add(new MySqlParameter("@parentSeq", updData.ParentSeq));
                    cmd.Parameters.Add(new MySqlParameter("@parentCDName", updData.ParentCDName));
                    cmd.Parameters.Add(new MySqlParameter("@detailKbn", updData.DetailKbn) { MySqlDbType = MySqlDbType.Int32 });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// データ削除
        /// </summary>
        /// <param name="delData"></param>
        internal void DelData(MST0020_GridDto delData)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM ");
            sb.AppendLine($"    MX02ParentCD ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    ParentCD = @parentCD ");
            sb.AppendLine($"AND ParentSeq = @parentSeq ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@parentCD", delData.ParentSeq));
                    cmd.Parameters.Add(new MySqlParameter("@parentSeq", delData.ParentSeq));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 対象の親コードを含む商品データが存在するか？
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        internal bool JudgeExistEstimaHavingParentCD(MST0020_GridDto dto)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT COUNT(*) FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"INNER JOIN ");
            sb.AppendLine($"    MX02ParentCD AS MX02 ");
            sb.AppendLine($"ON ");
            sb.AppendLine($"    MX03.ParentCD = MX02.ParentCD ");
            sb.AppendLine($"AND MX03.ParentSeq = MX02.ParentSeq ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    MX02.ParentCD  = @parentCD ");
            sb.AppendLine($"AND MX02.ParentSeq = @parentSeq ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@parentCD", dto.ParentCD) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@parentSeq", dto.ParentSeq) { DbType = DbType.Int32 });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return 0 < CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return false;
        }
    }
}
