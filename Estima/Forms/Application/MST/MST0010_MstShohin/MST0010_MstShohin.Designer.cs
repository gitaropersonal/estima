﻿namespace Estima.Forms.Application.MST.MST0010
{
    partial class MST0010_MstShohin
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbSpanEnd = new System.Windows.Forms.ComboBox();
            this.lblMarkSpan = new System.Windows.Forms.Label();
            this.cmbSpanStart = new System.Windows.Forms.ComboBox();
            this.pnlSpan = new System.Windows.Forms.Panel();
            this.rdoSelectTaishoYM = new System.Windows.Forms.RadioButton();
            this.rdoOnlyNewestTaishoYM = new System.Windows.Forms.RadioButton();
            this.numPriceTo = new System.Windows.Forms.NumericUpDown();
            this.numPriceFrom = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSize = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtShohinCD = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dgvMstShohin = new System.Windows.Forms.DataGridView();
            this.pnlBtnAdd = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.ShohinID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChkSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BtnShowDetail = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnCopy = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ShohinCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentCDName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChildCDName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShohinSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TaishoYM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BDCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCDs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChildCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChildSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SizeCD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShikiriRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RiekiRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Shikiri = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WeldingSize1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WeldingSize2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScrewSize1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScrewSize2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScrewSize3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaddleSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GBSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WeldingCount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WeldingCount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScrewCount1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScrewCount2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScrewCount3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaddleCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GBCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InchDia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCD1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCD2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCD3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCD4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCD5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingSeq1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingSeq2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingSeq3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingSeq4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingSeq5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCDName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCDName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCDName3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCDName4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingChildCDName5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UsedByEstima = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.pnlSpan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceFrom)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstShohin)).BeginInit();
            this.pnlBtnAdd.SuspendLayout();
            this.panel14.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbSpanEnd);
            this.panel1.Controls.Add(this.lblMarkSpan);
            this.panel1.Controls.Add(this.cmbSpanStart);
            this.panel1.Controls.Add(this.pnlSpan);
            this.panel1.Controls.Add(this.numPriceTo);
            this.panel1.Controls.Add(this.numPriceFrom);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtSize);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtShohinCD);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1734, 111);
            this.panel1.TabIndex = 0;
            // 
            // cmbSpanEnd
            // 
            this.cmbSpanEnd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSpanEnd.FormattingEnabled = true;
            this.cmbSpanEnd.Location = new System.Drawing.Point(626, 42);
            this.cmbSpanEnd.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSpanEnd.Name = "cmbSpanEnd";
            this.cmbSpanEnd.Size = new System.Drawing.Size(124, 28);
            this.cmbSpanEnd.TabIndex = 16;
            // 
            // lblMarkSpan
            // 
            this.lblMarkSpan.AutoSize = true;
            this.lblMarkSpan.Location = new System.Drawing.Point(597, 45);
            this.lblMarkSpan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMarkSpan.Name = "lblMarkSpan";
            this.lblMarkSpan.Size = new System.Drawing.Size(25, 20);
            this.lblMarkSpan.TabIndex = 42;
            this.lblMarkSpan.Text = "～";
            // 
            // cmbSpanStart
            // 
            this.cmbSpanStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSpanStart.FormattingEnabled = true;
            this.cmbSpanStart.Location = new System.Drawing.Point(471, 42);
            this.cmbSpanStart.Margin = new System.Windows.Forms.Padding(4);
            this.cmbSpanStart.Name = "cmbSpanStart";
            this.cmbSpanStart.Size = new System.Drawing.Size(124, 28);
            this.cmbSpanStart.TabIndex = 15;
            // 
            // pnlSpan
            // 
            this.pnlSpan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSpan.Controls.Add(this.rdoSelectTaishoYM);
            this.pnlSpan.Controls.Add(this.rdoOnlyNewestTaishoYM);
            this.pnlSpan.Location = new System.Drawing.Point(471, 13);
            this.pnlSpan.Margin = new System.Windows.Forms.Padding(4);
            this.pnlSpan.Name = "pnlSpan";
            this.pnlSpan.Padding = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.pnlSpan.Size = new System.Drawing.Size(279, 28);
            this.pnlSpan.TabIndex = 14;
            // 
            // rdoSelectTaishoYM
            // 
            this.rdoSelectTaishoYM.AutoSize = true;
            this.rdoSelectTaishoYM.Location = new System.Drawing.Point(125, 1);
            this.rdoSelectTaishoYM.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSelectTaishoYM.Name = "rdoSelectTaishoYM";
            this.rdoSelectTaishoYM.Size = new System.Drawing.Size(85, 24);
            this.rdoSelectTaishoYM.TabIndex = 1;
            this.rdoSelectTaishoYM.TabStop = true;
            this.rdoSelectTaishoYM.Text = "指定する";
            this.rdoSelectTaishoYM.UseVisualStyleBackColor = true;
            // 
            // rdoOnlyNewestTaishoYM
            // 
            this.rdoOnlyNewestTaishoYM.AutoSize = true;
            this.rdoOnlyNewestTaishoYM.Location = new System.Drawing.Point(10, 1);
            this.rdoOnlyNewestTaishoYM.Margin = new System.Windows.Forms.Padding(4);
            this.rdoOnlyNewestTaishoYM.Name = "rdoOnlyNewestTaishoYM";
            this.rdoOnlyNewestTaishoYM.Size = new System.Drawing.Size(86, 24);
            this.rdoOnlyNewestTaishoYM.TabIndex = 0;
            this.rdoOnlyNewestTaishoYM.TabStop = true;
            this.rdoOnlyNewestTaishoYM.Text = "最新のみ";
            this.rdoOnlyNewestTaishoYM.UseVisualStyleBackColor = true;
            // 
            // numPriceTo
            // 
            this.numPriceTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numPriceTo.Location = new System.Drawing.Point(249, 71);
            this.numPriceTo.Margin = new System.Windows.Forms.Padding(4);
            this.numPriceTo.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numPriceTo.Name = "numPriceTo";
            this.numPriceTo.Size = new System.Drawing.Size(107, 28);
            this.numPriceTo.TabIndex = 9;
            this.numPriceTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numPriceTo.ThousandsSeparator = true;
            // 
            // numPriceFrom
            // 
            this.numPriceFrom.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numPriceFrom.Location = new System.Drawing.Point(120, 71);
            this.numPriceFrom.Margin = new System.Windows.Forms.Padding(4);
            this.numPriceFrom.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numPriceFrom.Name = "numPriceFrom";
            this.numPriceFrom.Size = new System.Drawing.Size(107, 28);
            this.numPriceFrom.TabIndex = 8;
            this.numPriceFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numPriceFrom.ThousandsSeparator = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(225, 75);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 20);
            this.label10.TabIndex = 31;
            this.label10.Text = "～";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 75);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 20);
            this.label11.TabIndex = 29;
            this.label11.Text = "定価";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 46);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 20);
            this.label8.TabIndex = 24;
            this.label8.Text = "サイズ";
            // 
            // txtSize
            // 
            this.txtSize.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSize.Location = new System.Drawing.Point(120, 42);
            this.txtSize.Margin = new System.Windows.Forms.Padding(4);
            this.txtSize.MaxLength = 25;
            this.txtSize.Name = "txtSize";
            this.txtSize.Size = new System.Drawing.Size(236, 28);
            this.txtSize.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(384, 16);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "開始年月";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 16);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "商品コード";
            // 
            // txtShohinCD
            // 
            this.txtShohinCD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohinCD.Location = new System.Drawing.Point(120, 13);
            this.txtShohinCD.Margin = new System.Windows.Forms.Padding(4);
            this.txtShohinCD.MaxLength = 40;
            this.txtShohinCD.Name = "txtShohinCD";
            this.txtShohinCD.Size = new System.Drawing.Size(236, 28);
            this.txtShohinCD.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1509, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(225, 111);
            this.panel2.TabIndex = 20;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(68, 45);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(138, 52);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 891);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1734, 70);
            this.panel3.TabIndex = 100;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(1509, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(225, 70);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(69, 12);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(548, 70);
            this.panel4.TabIndex = 110;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(156, 12);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(138, 52);
            this.btnUpdate.TabIndex = 112;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(19, 12);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dgvMstShohin);
            this.panel6.Controls.Add(this.pnlBtnAdd);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 111);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(19, 12, 19, 6);
            this.panel6.Size = new System.Drawing.Size(1734, 780);
            this.panel6.TabIndex = 30;
            // 
            // dgvMstShohin
            // 
            this.dgvMstShohin.AllowUserToAddRows = false;
            this.dgvMstShohin.AllowUserToDeleteRows = false;
            this.dgvMstShohin.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMstShohin.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMstShohin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMstShohin.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShohinID,
            this.ChkSelect,
            this.BtnShowDetail,
            this.BtnCopy,
            this.BtnDelete,
            this.ShohinCD,
            this.ParentCDName,
            this.ChildCDName,
            this.ShohinSize,
            this.Price,
            this.TaishoYM,
            this.BDCount,
            this.ProcessingChildCDs,
            this.ParentCD,
            this.ParentSeq,
            this.ChildCD,
            this.ChildSeq,
            this.SizeCD,
            this.Size1,
            this.Size2,
            this.Size3,
            this.Size4,
            this.ShikiriRate,
            this.RiekiRate,
            this.Shikiri,
            this.WeldingSize1,
            this.WeldingSize2,
            this.ScrewSize1,
            this.ScrewSize2,
            this.ScrewSize3,
            this.SaddleSize,
            this.GBSize,
            this.WeldingCount1,
            this.WeldingCount2,
            this.ScrewCount1,
            this.ScrewCount2,
            this.ScrewCount3,
            this.SaddleCount,
            this.GBCount,
            this.InchDia,
            this.ProcessingChildCD1,
            this.ProcessingChildCD2,
            this.ProcessingChildCD3,
            this.ProcessingChildCD4,
            this.ProcessingChildCD5,
            this.ProcessingSeq1,
            this.ProcessingSeq2,
            this.ProcessingSeq3,
            this.ProcessingSeq4,
            this.ProcessingSeq5,
            this.ProcessingChildCDName1,
            this.ProcessingChildCDName2,
            this.ProcessingChildCDName3,
            this.ProcessingChildCDName4,
            this.ProcessingChildCDName5,
            this.UsedByEstima,
            this.Status});
            this.dgvMstShohin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMstShohin.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvMstShohin.Location = new System.Drawing.Point(19, 82);
            this.dgvMstShohin.Margin = new System.Windows.Forms.Padding(4);
            this.dgvMstShohin.MultiSelect = false;
            this.dgvMstShohin.Name = "dgvMstShohin";
            this.dgvMstShohin.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvMstShohin.RowTemplate.Height = 21;
            this.dgvMstShohin.Size = new System.Drawing.Size(1696, 692);
            this.dgvMstShohin.TabIndex = 35;
            // 
            // pnlBtnAdd
            // 
            this.pnlBtnAdd.Controls.Add(this.panel14);
            this.pnlBtnAdd.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBtnAdd.Location = new System.Drawing.Point(19, 12);
            this.pnlBtnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.pnlBtnAdd.Name = "pnlBtnAdd";
            this.pnlBtnAdd.Size = new System.Drawing.Size(1696, 70);
            this.pnlBtnAdd.TabIndex = 31;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnAdd);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Margin = new System.Windows.Forms.Padding(4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(192, 70);
            this.panel14.TabIndex = 31;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAdd.Location = new System.Drawing.Point(12, 8);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(138, 52);
            this.btnAdd.TabIndex = 31;
            this.btnAdd.Text = "行追加(I)";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // ShohinID
            // 
            this.ShohinID.DataPropertyName = "ShohinID";
            this.ShohinID.Frozen = true;
            this.ShohinID.HeaderText = "ShohinID";
            this.ShohinID.Name = "ShohinID";
            this.ShohinID.ReadOnly = true;
            this.ShohinID.Visible = false;
            // 
            // ChkSelect
            // 
            this.ChkSelect.DataPropertyName = "ChkSelect";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.NullValue = false;
            this.ChkSelect.DefaultCellStyle = dataGridViewCellStyle2;
            this.ChkSelect.Frozen = true;
            this.ChkSelect.HeaderText = "選択";
            this.ChkSelect.Name = "ChkSelect";
            this.ChkSelect.Width = 60;
            // 
            // BtnShowDetail
            // 
            this.BtnShowDetail.DataPropertyName = "BtnShowDetail";
            this.BtnShowDetail.Frozen = true;
            this.BtnShowDetail.HeaderText = "";
            this.BtnShowDetail.Name = "BtnShowDetail";
            this.BtnShowDetail.ReadOnly = true;
            this.BtnShowDetail.Width = 60;
            // 
            // BtnCopy
            // 
            this.BtnCopy.DataPropertyName = "BtnCopy";
            this.BtnCopy.Frozen = true;
            this.BtnCopy.HeaderText = "";
            this.BtnCopy.Name = "BtnCopy";
            this.BtnCopy.Width = 60;
            // 
            // BtnDelete
            // 
            this.BtnDelete.DataPropertyName = "BtnDelete";
            this.BtnDelete.Frozen = true;
            this.BtnDelete.HeaderText = "";
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Width = 60;
            // 
            // ShohinCD
            // 
            this.ShohinCD.DataPropertyName = "ShohinCD";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            this.ShohinCD.DefaultCellStyle = dataGridViewCellStyle3;
            this.ShohinCD.Frozen = true;
            this.ShohinCD.HeaderText = "商品コード";
            this.ShohinCD.MaxInputLength = 40;
            this.ShohinCD.Name = "ShohinCD";
            this.ShohinCD.ReadOnly = true;
            this.ShohinCD.Width = 230;
            // 
            // ParentCDName
            // 
            this.ParentCDName.DataPropertyName = "ParentCDName";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            this.ParentCDName.DefaultCellStyle = dataGridViewCellStyle4;
            this.ParentCDName.HeaderText = "親コード名称";
            this.ParentCDName.Name = "ParentCDName";
            this.ParentCDName.ReadOnly = true;
            this.ParentCDName.Width = 200;
            // 
            // ChildCDName
            // 
            this.ChildCDName.DataPropertyName = "ChildCDName";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGray;
            this.ChildCDName.DefaultCellStyle = dataGridViewCellStyle5;
            this.ChildCDName.HeaderText = "子商品名称";
            this.ChildCDName.Name = "ChildCDName";
            this.ChildCDName.ReadOnly = true;
            this.ChildCDName.Width = 250;
            // 
            // ShohinSize
            // 
            this.ShohinSize.DataPropertyName = "ShohinSize";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            this.ShohinSize.DefaultCellStyle = dataGridViewCellStyle6;
            this.ShohinSize.HeaderText = "サイズ";
            this.ShohinSize.Name = "ShohinSize";
            this.ShohinSize.ReadOnly = true;
            this.ShohinSize.Width = 160;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N0";
            this.Price.DefaultCellStyle = dataGridViewCellStyle7;
            this.Price.HeaderText = "定価";
            this.Price.MaxInputLength = 6;
            this.Price.Name = "Price";
            this.Price.Width = 95;
            // 
            // TaishoYM
            // 
            this.TaishoYM.DataPropertyName = "TaishoYM";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.TaishoYM.DefaultCellStyle = dataGridViewCellStyle8;
            this.TaishoYM.HeaderText = "開始年月";
            this.TaishoYM.MaxInputLength = 6;
            this.TaishoYM.Name = "TaishoYM";
            // 
            // BDCount
            // 
            this.BDCount.DataPropertyName = "BDCount";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightGray;
            this.BDCount.DefaultCellStyle = dataGridViewCellStyle9;
            this.BDCount.HeaderText = "BD箇所";
            this.BDCount.MaxInputLength = 6;
            this.BDCount.Name = "BDCount";
            this.BDCount.Width = 85;
            // 
            // ProcessingChildCDs
            // 
            this.ProcessingChildCDs.DataPropertyName = "ProcessingChildCDs";
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.LightGray;
            this.ProcessingChildCDs.DefaultCellStyle = dataGridViewCellStyle10;
            this.ProcessingChildCDs.HeaderText = "加工費商品コード";
            this.ProcessingChildCDs.Name = "ProcessingChildCDs";
            this.ProcessingChildCDs.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ProcessingChildCDs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProcessingChildCDs.Width = 200;
            // 
            // ParentCD
            // 
            this.ParentCD.DataPropertyName = "ParentCD";
            this.ParentCD.HeaderText = "ParentCD";
            this.ParentCD.Name = "ParentCD";
            this.ParentCD.Visible = false;
            // 
            // ParentSeq
            // 
            this.ParentSeq.DataPropertyName = "ParentSeq";
            this.ParentSeq.HeaderText = "ParentSeq";
            this.ParentSeq.Name = "ParentSeq";
            this.ParentSeq.Visible = false;
            // 
            // ChildCD
            // 
            this.ChildCD.DataPropertyName = "ChildCD";
            this.ChildCD.HeaderText = "ChildCD";
            this.ChildCD.Name = "ChildCD";
            this.ChildCD.Visible = false;
            // 
            // ChildSeq
            // 
            this.ChildSeq.DataPropertyName = "ChildSeq";
            this.ChildSeq.HeaderText = "ChildSeq";
            this.ChildSeq.Name = "ChildSeq";
            this.ChildSeq.Visible = false;
            // 
            // SizeCD
            // 
            this.SizeCD.DataPropertyName = "SizeCD";
            this.SizeCD.HeaderText = "SizeCD";
            this.SizeCD.Name = "SizeCD";
            this.SizeCD.Visible = false;
            // 
            // Size1
            // 
            this.Size1.DataPropertyName = "Size1";
            this.Size1.HeaderText = "Size1";
            this.Size1.Name = "Size1";
            this.Size1.Visible = false;
            // 
            // Size2
            // 
            this.Size2.DataPropertyName = "Size2";
            this.Size2.HeaderText = "Size2";
            this.Size2.Name = "Size2";
            this.Size2.Visible = false;
            // 
            // Size3
            // 
            this.Size3.DataPropertyName = "Size3";
            this.Size3.HeaderText = "Size3";
            this.Size3.Name = "Size3";
            this.Size3.Visible = false;
            // 
            // Size4
            // 
            this.Size4.DataPropertyName = "Size4";
            this.Size4.HeaderText = "Size4";
            this.Size4.Name = "Size4";
            this.Size4.Visible = false;
            // 
            // ShikiriRate
            // 
            this.ShikiriRate.DataPropertyName = "ShikiriRate";
            this.ShikiriRate.HeaderText = "ShikiriRate";
            this.ShikiriRate.Name = "ShikiriRate";
            this.ShikiriRate.Visible = false;
            // 
            // RiekiRate
            // 
            this.RiekiRate.DataPropertyName = "RiekiRate";
            this.RiekiRate.HeaderText = "RiekiRate";
            this.RiekiRate.Name = "RiekiRate";
            this.RiekiRate.Visible = false;
            // 
            // Shikiri
            // 
            this.Shikiri.DataPropertyName = "Shikiri";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle11.Format = "N0";
            dataGridViewCellStyle11.NullValue = null;
            this.Shikiri.DefaultCellStyle = dataGridViewCellStyle11;
            this.Shikiri.HeaderText = "Shikiri";
            this.Shikiri.Name = "Shikiri";
            this.Shikiri.ReadOnly = true;
            this.Shikiri.Visible = false;
            // 
            // WeldingSize1
            // 
            this.WeldingSize1.DataPropertyName = "WeldingSize1";
            this.WeldingSize1.HeaderText = "WeldingSize1";
            this.WeldingSize1.Name = "WeldingSize1";
            this.WeldingSize1.Visible = false;
            // 
            // WeldingSize2
            // 
            this.WeldingSize2.DataPropertyName = "WeldingSize2";
            this.WeldingSize2.HeaderText = "WeldingSize2";
            this.WeldingSize2.Name = "WeldingSize2";
            this.WeldingSize2.Visible = false;
            // 
            // ScrewSize1
            // 
            this.ScrewSize1.DataPropertyName = "ScrewSize1";
            this.ScrewSize1.HeaderText = "ScrewSize1";
            this.ScrewSize1.Name = "ScrewSize1";
            this.ScrewSize1.Visible = false;
            // 
            // ScrewSize2
            // 
            this.ScrewSize2.DataPropertyName = "ScrewSize2";
            this.ScrewSize2.HeaderText = "ScrewSize2";
            this.ScrewSize2.Name = "ScrewSize2";
            this.ScrewSize2.Visible = false;
            // 
            // ScrewSize3
            // 
            this.ScrewSize3.DataPropertyName = "ScrewSize3";
            this.ScrewSize3.HeaderText = "ScrewSize3";
            this.ScrewSize3.Name = "ScrewSize3";
            this.ScrewSize3.Visible = false;
            // 
            // SaddleSize
            // 
            this.SaddleSize.DataPropertyName = "SaddleSize";
            this.SaddleSize.HeaderText = "SaddleSize";
            this.SaddleSize.Name = "SaddleSize";
            this.SaddleSize.Visible = false;
            // 
            // GBSize
            // 
            this.GBSize.DataPropertyName = "GBSize";
            this.GBSize.HeaderText = "GBSize";
            this.GBSize.Name = "GBSize";
            this.GBSize.Visible = false;
            // 
            // WeldingCount1
            // 
            this.WeldingCount1.DataPropertyName = "WeldingCount1";
            this.WeldingCount1.HeaderText = "WeldingCount1";
            this.WeldingCount1.Name = "WeldingCount1";
            this.WeldingCount1.Visible = false;
            // 
            // WeldingCount2
            // 
            this.WeldingCount2.DataPropertyName = "WeldingCount2";
            this.WeldingCount2.HeaderText = "WeldingCount2";
            this.WeldingCount2.Name = "WeldingCount2";
            this.WeldingCount2.Visible = false;
            // 
            // ScrewCount1
            // 
            this.ScrewCount1.DataPropertyName = "ScrewCount1";
            this.ScrewCount1.HeaderText = "ScrewCount1";
            this.ScrewCount1.Name = "ScrewCount1";
            this.ScrewCount1.Visible = false;
            // 
            // ScrewCount2
            // 
            this.ScrewCount2.DataPropertyName = "ScrewCount2";
            this.ScrewCount2.HeaderText = "ScrewCount2";
            this.ScrewCount2.Name = "ScrewCount2";
            this.ScrewCount2.Visible = false;
            // 
            // ScrewCount3
            // 
            this.ScrewCount3.DataPropertyName = "ScrewCount3";
            this.ScrewCount3.HeaderText = "ScrewCount3";
            this.ScrewCount3.Name = "ScrewCount3";
            this.ScrewCount3.Visible = false;
            // 
            // SaddleCount
            // 
            this.SaddleCount.DataPropertyName = "SaddleCount";
            this.SaddleCount.HeaderText = "SaddleCount";
            this.SaddleCount.Name = "SaddleCount";
            this.SaddleCount.Visible = false;
            // 
            // GBCount
            // 
            this.GBCount.DataPropertyName = "GBCount";
            this.GBCount.HeaderText = "GBCount";
            this.GBCount.Name = "GBCount";
            this.GBCount.Visible = false;
            // 
            // InchDia
            // 
            this.InchDia.DataPropertyName = "InchDia";
            this.InchDia.HeaderText = "InchDia";
            this.InchDia.Name = "InchDia";
            this.InchDia.Visible = false;
            // 
            // ProcessingChildCD1
            // 
            this.ProcessingChildCD1.DataPropertyName = "ProcessingChildCD1";
            this.ProcessingChildCD1.HeaderText = "ProcessingChildCD1";
            this.ProcessingChildCD1.Name = "ProcessingChildCD1";
            this.ProcessingChildCD1.Visible = false;
            // 
            // ProcessingChildCD2
            // 
            this.ProcessingChildCD2.DataPropertyName = "ProcessingChildCD2";
            this.ProcessingChildCD2.HeaderText = "ProcessingChildCD2";
            this.ProcessingChildCD2.Name = "ProcessingChildCD2";
            this.ProcessingChildCD2.Visible = false;
            // 
            // ProcessingChildCD3
            // 
            this.ProcessingChildCD3.DataPropertyName = "ProcessingChildCD3";
            this.ProcessingChildCD3.HeaderText = "ProcessingChildCD3";
            this.ProcessingChildCD3.Name = "ProcessingChildCD3";
            this.ProcessingChildCD3.Visible = false;
            // 
            // ProcessingChildCD4
            // 
            this.ProcessingChildCD4.DataPropertyName = "ProcessingChildCD4";
            this.ProcessingChildCD4.HeaderText = "ProcessingChildCD4";
            this.ProcessingChildCD4.Name = "ProcessingChildCD4";
            this.ProcessingChildCD4.Visible = false;
            // 
            // ProcessingChildCD5
            // 
            this.ProcessingChildCD5.DataPropertyName = "ProcessingChildCD5";
            this.ProcessingChildCD5.HeaderText = "ProcessingChildCD5";
            this.ProcessingChildCD5.Name = "ProcessingChildCD5";
            this.ProcessingChildCD5.Visible = false;
            // 
            // ProcessingSeq1
            // 
            this.ProcessingSeq1.DataPropertyName = "ProcessingSeq1";
            this.ProcessingSeq1.HeaderText = "ProcessingSeq1";
            this.ProcessingSeq1.Name = "ProcessingSeq1";
            this.ProcessingSeq1.Visible = false;
            // 
            // ProcessingSeq2
            // 
            this.ProcessingSeq2.DataPropertyName = "ProcessingSeq2";
            this.ProcessingSeq2.HeaderText = "ProcessingSeq2";
            this.ProcessingSeq2.Name = "ProcessingSeq2";
            this.ProcessingSeq2.Visible = false;
            // 
            // ProcessingSeq3
            // 
            this.ProcessingSeq3.DataPropertyName = "ProcessingSeq3";
            this.ProcessingSeq3.HeaderText = "ProcessingSeq3";
            this.ProcessingSeq3.Name = "ProcessingSeq3";
            this.ProcessingSeq3.Visible = false;
            // 
            // ProcessingSeq4
            // 
            this.ProcessingSeq4.DataPropertyName = "ProcessingSeq4";
            this.ProcessingSeq4.HeaderText = "ProcessingSeq4";
            this.ProcessingSeq4.Name = "ProcessingSeq4";
            this.ProcessingSeq4.Visible = false;
            // 
            // ProcessingSeq5
            // 
            this.ProcessingSeq5.DataPropertyName = "ProcessingSeq5";
            this.ProcessingSeq5.HeaderText = "ProcessingSeq5";
            this.ProcessingSeq5.Name = "ProcessingSeq5";
            this.ProcessingSeq5.Visible = false;
            // 
            // ProcessingChildCDName1
            // 
            this.ProcessingChildCDName1.DataPropertyName = "ProcessingChildCDName1";
            this.ProcessingChildCDName1.HeaderText = "ProcessingChildCDName1";
            this.ProcessingChildCDName1.Name = "ProcessingChildCDName1";
            this.ProcessingChildCDName1.Visible = false;
            // 
            // ProcessingChildCDName2
            // 
            this.ProcessingChildCDName2.DataPropertyName = "ProcessingChildCDName2";
            this.ProcessingChildCDName2.HeaderText = "ProcessingChildCDName2";
            this.ProcessingChildCDName2.Name = "ProcessingChildCDName2";
            this.ProcessingChildCDName2.Visible = false;
            // 
            // ProcessingChildCDName3
            // 
            this.ProcessingChildCDName3.DataPropertyName = "ProcessingChildCDName3";
            this.ProcessingChildCDName3.HeaderText = "ProcessingChildCDName3";
            this.ProcessingChildCDName3.Name = "ProcessingChildCDName3";
            this.ProcessingChildCDName3.Visible = false;
            // 
            // ProcessingChildCDName4
            // 
            this.ProcessingChildCDName4.DataPropertyName = "ProcessingChildCDName4";
            this.ProcessingChildCDName4.HeaderText = "ProcessingChildCDName4";
            this.ProcessingChildCDName4.Name = "ProcessingChildCDName4";
            this.ProcessingChildCDName4.Visible = false;
            // 
            // ProcessingChildCDName5
            // 
            this.ProcessingChildCDName5.DataPropertyName = "ProcessingChildCDName5";
            this.ProcessingChildCDName5.HeaderText = "ProcessingChildCDName5";
            this.ProcessingChildCDName5.Name = "ProcessingChildCDName5";
            this.ProcessingChildCDName5.Visible = false;
            // 
            // UsedByEstima
            // 
            this.UsedByEstima.DataPropertyName = "UsedByEstima";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.NullValue = false;
            this.UsedByEstima.DefaultCellStyle = dataGridViewCellStyle12;
            this.UsedByEstima.HeaderText = "帳票で使用";
            this.UsedByEstima.Name = "UsedByEstima";
            this.UsedByEstima.ReadOnly = true;
            this.UsedByEstima.Width = 75;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            this.Status.Width = 83;
            // 
            // MST0010_MstShohin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1734, 961);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(1250, 500);
            this.Name = "MST0010_MstShohin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "商品マスタ";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlSpan.ResumeLayout(false);
            this.pnlSpan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPriceFrom)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstShohin)).EndInit();
            this.pnlBtnAdd.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtShohinCD;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtSize;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.NumericUpDown numPriceFrom;
        public System.Windows.Forms.NumericUpDown numPriceTo;
        public System.Windows.Forms.Panel pnlSpan;
        public System.Windows.Forms.RadioButton rdoOnlyNewestTaishoYM;
        public System.Windows.Forms.RadioButton rdoSelectTaishoYM;
        public System.Windows.Forms.ComboBox cmbSpanEnd;
        public System.Windows.Forms.Label lblMarkSpan;
        public System.Windows.Forms.ComboBox cmbSpanStart;
        public System.Windows.Forms.DataGridView dgvMstShohin;
        public System.Windows.Forms.Panel pnlBtnAdd;
        public System.Windows.Forms.Panel panel14;
        public System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ChkSelect;
        private System.Windows.Forms.DataGridViewButtonColumn BtnShowDetail;
        private System.Windows.Forms.DataGridViewButtonColumn BtnCopy;
        private System.Windows.Forms.DataGridViewButtonColumn BtnDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentCDName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChildCDName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaishoYM;
        private System.Windows.Forms.DataGridViewTextBoxColumn BDCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCDs;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChildCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChildSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn SizeCD;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShikiriRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn RiekiRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Shikiri;
        private System.Windows.Forms.DataGridViewTextBoxColumn WeldingSize1;
        private System.Windows.Forms.DataGridViewTextBoxColumn WeldingSize2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScrewSize1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScrewSize2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScrewSize3;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaddleSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn GBSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn WeldingCount1;
        private System.Windows.Forms.DataGridViewTextBoxColumn WeldingCount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScrewCount1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScrewCount2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScrewCount3;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaddleCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn GBCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn InchDia;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCD1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCD2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCD3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCD4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCD5;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingSeq1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingSeq2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingSeq3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingSeq4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingSeq5;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCDName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCDName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCDName3;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCDName4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingChildCDName5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn UsedByEstima;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}

