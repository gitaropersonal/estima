﻿using System;
using EstimaLib.Const;

namespace Estima.Forms.Application.MST.MST0010.Business
{
    public class MST0010_SelCondition
    {
        public MST0010_SelCondition()
        {
            this.ShohinCD = string.Empty;
            this.ShohinSize = string.Empty;
            this.OnlyNewest = true;
            this.DetailKbn = Enums.DetailKbn.All;
        }
        public string TekiyoStart { get; set; }
        public string TekiyoEnd { get; set; }
        public bool OnlyNewest { get; set; }
        public string ShohinCD { get; set; }
        public string ShohinSize { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public Enums.DetailKbn DetailKbn { get; set; }
    }
}
