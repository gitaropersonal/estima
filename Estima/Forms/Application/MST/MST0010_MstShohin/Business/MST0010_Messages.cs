﻿namespace Estima.Forms.Application.MST.MST0010.Business
{
    internal static class MST0010_Messages
    {
        internal const string MsgAskDelete = "削除しますか？";
        internal const string MsgAskUpdate = "更新しますか？";
        internal const string MsgErrorDouple = "{0}が重複しています";
        internal const string MsgErrorMissEmpty = "有効な{0}を入力してください";
        internal const string MsgErrorNotExistDatas = "対象のデータがありません";
        internal const string MsgErrorNotSelectedRow = "行を選択してください";
        internal const string MsgErrorOverEmptyRowCount = "明細に追加可能な行数以上のデータを選択しています\r\nデータの選択数を減らしてください";
        internal const string MsgInfoFinishDelete = "削除しました";
        internal const string MsgInfoFinishUpdate = "更新しました";
        internal const string MsgErrorForbidDelRow = "この行は削除できません";
        internal const string MsgErrorExistEstimaHavingShohinCD = "この商品を使用している見積書・請求書があるので削除できません。";
    }
}
