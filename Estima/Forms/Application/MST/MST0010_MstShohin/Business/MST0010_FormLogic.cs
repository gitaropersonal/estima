﻿using Estima.Forms.Application.MST.MST0020.Business;
using Estima.Forms.Dialog.DE0050;
using Estima.Forms.Dialog.DE0050.Business;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Application.MST.MST0010.Business
{
    internal class MST0010_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private MST0010_MstShohin Body { get; set; }
        /// <summary>
        /// 画面モード
        /// </summary>
        private Enums.ScreenModeMstShohin ScreenMode { get; set; }
        /// <summary>
        /// 詳細区分
        /// </summary>
        private Enums.DetailKbn DetailKbn { get; set; }
        /// <summary>
        /// 検索対象の商品コード
        /// </summary>
        private string SearchShohinCD = string.Empty;
        /// <summary>
        /// 専用サービス
        /// </summary>
        private readonly MST0010_Service PersonalService = new MST0010_Service();
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// ログイン情報
        /// </summary>
        private readonly LoginInfoDto LoginInfo = new LoginInfoDto();
        /// <summary>
        /// 画面名リスト
        /// </summary>
        private readonly IEnumerable<KeyValuePair<Enums.ScreenModeMstShohin, string>> ScreenNameList = new Dictionary<Enums.ScreenModeMstShohin, string>
        {
              { Enums.ScreenModeMstShohin.Screen      , "(2) - 1.商品"}
            , { Enums.ScreenModeMstShohin.DialogSingle, "(2) - 1.商品（単一選択モード）"}
            , { Enums.ScreenModeMstShohin.DialogMulti , "(2) - 1.商品（複数選択モード）"}
            , { Enums.ScreenModeMstShohin.DialogSingleProcessing, "(2) - 1.商品（単一選択モード）"}
            , { Enums.ScreenModeMstShohin.DialogMultiProcessing , "(2) - 1.商品（複数選択モード）"}
        };
        /// <summary>
        /// 列ヘッダdto
        /// </summary>
        private static MST0010_GridDto CellHeaderNameDto = new MST0010_GridDto();
        private readonly string[] EditableGridCells = new string[]
        {
              nameof(CellHeaderNameDto.Status)
            , nameof(CellHeaderNameDto.TaishoYM)
            , nameof(CellHeaderNameDto.Price)
        };
        private int CurrColIndex = 0;
        /// <summary>
        /// 編集フラグ
        /// </summary>
        private bool EditedFlg = false;
        /// <summary>
        /// 削除されたデータ
        /// </summary>
        private List<MST0010_GridDto> DeletedDatas = new List<MST0010_GridDto>();
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        /// <param name="detailKbn"></param>
        internal MST0010_FormLogic(
              MST0010_MstShohin body
            , LoginInfoDto loginInfo
            , Enums.ScreenModeMstShohin screenMode
            , Enums.DetailKbn detailKbn
            , string searchShohinCD
            )
        {
            this.Body = body;
            this.LoginInfo = loginInfo;
            this.ScreenMode = screenMode;
            this.Body.Text = ScreenNameList.First(n => n.Key == this.ScreenMode).Value;
            this.DetailKbn = detailKbn;
            this.SearchShohinCD = searchShohinCD;
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += (s, e) =>
            {
                CommonUtil.BtnClearClickEvent(
                      new Action(() => this.Init())
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了処理
            this.Body.FormClosing += this.ClosingEvent;
            // 検索ボタン押下
            this.Body.btnSearch.Click += (s, e) =>
            {
                CommonUtil.EventWithValidateUpdExist(new Action(() => 
                      this.BtnSearchClickEvent(s, e))
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 更新ボタン押下
            this.Body.btnUpdate.Click += this.BtnUpdClickEvent;
            // 追加ボタン押下
            this.Body.btnAdd.Click += this.BtnAddClickEvent;
            // グリッドRowPostPaint
            this.Body.dgvMstShohin.RowPostPaint += this.GridRowPostPaintEvent;
            // グリッドCellEnter
            this.Body.dgvMstShohin.CellEnter += this.GridCelEnter;
            // グリッドセルクリック
            this.Body.dgvMstShohin.CellClick += this.GridCellClick;
            // グリッドセルダブルクリック
            this.Body.dgvMstShohin.CellDoubleClick += this.GridCellDoubleClickEvent;
            // グリッドCellValueChanged
            this.Body.dgvMstShohin.CellValueChanged += this.GridCellValueChanged;
            // グリッドEditingControlShowing
            this.Body.dgvMstShohin.EditingControlShowing += this.GridKeyPress;
            // 数値コントロールTextChanged
            this.Body.numPriceFrom.TextChanged += this.NumTextChanged;
            this.Body.numPriceTo.TextChanged += this.NumTextChanged;
            // 開始年月ラジオボタン
            this.Body.rdoOnlyNewestTaishoYM.CheckedChanged += (s, e) => this.RdoTaishoYMCheckedChanged();
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.S:
                        this.Body.btnSearch.Focus();
                        this.Body.btnSearch.PerformClick();
                        break;
                    case Keys.I:
                        this.Body.btnAdd.Focus();
                        this.Body.btnAdd.PerformClick();
                        break;
                    case Keys.O:
                        this.Body.btnUpdate.Focus();
                        this.Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// 検索条件の対象年月ラジオボタンCheckedChanged
        /// </summary>
        private void RdoTaishoYMCheckedChanged()
        {
            this.Body.cmbSpanStart.Visible = !this.Body.rdoOnlyNewestTaishoYM.Checked;
            this.Body.cmbSpanEnd.Visible = !this.Body.rdoOnlyNewestTaishoYM.Checked;
            this.Body.lblMarkSpan.Visible = !this.Body.rdoOnlyNewestTaishoYM.Checked;
        }
        /// <summary>
        /// グリッドセルクリック
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellClick(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            if (dgv.DataSource == null)
            {
                return;
            }
            var status = dgv.Rows[e.RowIndex].Cells[nameof(CellHeaderNameDto.Status)].Value.ToString();
            if (status == Enums.EditStatus.None.ToString())
            {
                return;
            }
            try
            {
                switch (dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningColumn.Name)
                {
                    case nameof(CellHeaderNameDto.BtnShowDetail):
                        // 表示ボタン押下
                        this.GridButtonClickEventShow(s, e);
                        break;
                    case nameof(CellHeaderNameDto.BtnCopy):
                        // 複製ボタン押下
                        this.GridButtonClickEventCopy(s, e);
                        break;
                    case nameof(CellHeaderNameDto.BtnDelete):
                        // 削除ボタン押下
                        this.GridButtonClickEventDelete(s, e);
                        break;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// グリッドボタン押下処理（表示）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridButtonClickEventShow(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            dgv.CellValueChanged -= this.GridCellValueChanged;
            try
            {
                // 明細内訳画面表示
                DataGridViewRow r = ((DataGridView)s).Rows[e.RowIndex];
                var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);
                var content = new DE0050_ContentsDto()
                {
                    ShohinID = rowDto.ShohinID,
                    ParentCD = rowDto.ParentCD,
                    ParentSeq = rowDto.ParentSeq,
                    ParentCDName = rowDto.ParentCDName,
                    ChildCD = rowDto.ChildCD,
                    ChildCDName = rowDto.ChildCDName,
                    SizeCD = rowDto.SizeCD,
                    ShohinCD = rowDto.ShohinCD,
                    Size1 = rowDto.Size1,
                    Size2 = rowDto.Size2,
                    Size3 = rowDto.Size3,
                    Size4 = rowDto.Size4,
                    ShohinSize = rowDto.ShohinSize,
                    TaishoYM = rowDto.TaishoYM,
                    Price = rowDto.Price,
                    Shikiri = rowDto.Shikiri,
                    ShikiriRate = rowDto.ShikiriRate,
                    RiekiRate = rowDto.RiekiRate,
                    WeldingSize1 = rowDto.WeldingSize1,
                    WeldingSize2 = rowDto.WeldingSize2,
                    ScrewSize1 = rowDto.ScrewSize1,
                    ScrewSize2 = rowDto.ScrewSize2,
                    ScrewSize3 = rowDto.ScrewSize3,
                    SaddleSize = rowDto.SaddleSize,
                    GBSize = rowDto.GBSize,
                    WeldingCount1 = rowDto.WeldingCount1,
                    WeldingCount2 = rowDto.WeldingCount2,
                    ScrewCount1 = rowDto.ScrewCount1,
                    ScrewCount2 = rowDto.ScrewCount2,
                    ScrewCount3 = rowDto.ScrewCount3,
                    SaddleCount = rowDto.SaddleCount,
                    GBCount = rowDto.GBCount,
                    InchDia = rowDto.InchDia,
                    BDCount = rowDto.BDCount,
                    ProcessingChildCD1 = rowDto.ProcessingChildCD1,
                    ProcessingChildCD2 = rowDto.ProcessingChildCD2,
                    ProcessingChildCD3 = rowDto.ProcessingChildCD3,
                    ProcessingChildCD4 = rowDto.ProcessingChildCD4,
                    ProcessingChildCD5 = rowDto.ProcessingChildCD5,
                    ProcessingSeq1 = rowDto.ProcessingSeq1,
                    ProcessingSeq2 = rowDto.ProcessingSeq2,
                    ProcessingSeq3 = rowDto.ProcessingSeq3,
                    ProcessingSeq4 = rowDto.ProcessingSeq4,
                    ProcessingSeq5 = rowDto.ProcessingSeq5,
                    ProcessingChildCDName1 = rowDto.ProcessingChildCDName1,
                    ProcessingChildCDName2 = rowDto.ProcessingChildCDName2,
                    ProcessingChildCDName3 = rowDto.ProcessingChildCDName3,
                    ProcessingChildCDName4 = rowDto.ProcessingChildCDName4,
                    ProcessingChildCDName5 = rowDto.ProcessingChildCDName5,
                    UsedByEstima = rowDto.UsedByEstima,
                };
                var drDetail = new DE0050_ShohinDetailDialog(
                      this.LoginInfo
                    , content
                    , this.ScreenMode != Enums.ScreenModeMstShohin.Screen
                );
                if (drDetail.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                // ダイアログの編集結果をセット
                this.SetEditedDetailInfo(r, content, drDetail);
                // ステータス更新
                switch (rowDto.Status)
                {
                    case Enums.EditStatus.Show:
                    case Enums.EditStatus.Update:
                        r.Cells[nameof(rowDto.Status)].Value = Enums.EditStatus.Update;
                        break;
                    case Enums.EditStatus.Insert:
                        r.Cells[nameof(rowDto.Status)].Value = Enums.EditStatus.Insert;
                        break;
                }
                // 編集フラグ
                this.EditedFlg = true;
                // 使用可否切り替え（更新ボタン）
                this.SwitchEnableBtnUpdate();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                dgv.CellValueChanged += this.GridCellValueChanged;
            }
        }
        /// <summary>
        /// 行コピーボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridButtonClickEventCopy(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            dgv.CellValueChanged -= this.GridCellValueChanged;
            try
            {
                // 元データDto
                DataGridViewRow r = dgv.Rows[e.RowIndex];
                var originDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);

                // グリッドデータ取得
                var datas = (BindingList<MST0010_GridDto>)this.Body.dgvMstShohin.DataSource;

                // 追加行作成
                var rowDto = new MST0010_GridDto()
                {
                    ParentCD = originDto.ParentCD,
                    ParentSeq = originDto.ParentSeq,
                    ParentCDName = originDto.ParentCDName,
                    ChildCD = originDto.ChildCD,
                    ChildSeq = originDto.ChildSeq,
                    ChildCDName = originDto.ChildCDName,
                    SizeCD = originDto.SizeCD,
                    ShohinCD = originDto.ShohinCD,
                    Size1 = originDto.Size1,
                    Size2 = originDto.Size2,
                    Size3 = originDto.Size3,
                    Size4 = originDto.Size4,
                    ShohinSize = originDto.ShohinSize,
                    TaishoYM = DateTime.Today.ToString(Formats.YYYYMM),
                    Price = originDto.Price,
                    Shikiri = originDto.Shikiri,
                    ShikiriRate = originDto.ShikiriRate,
                    RiekiRate = originDto.RiekiRate,
                    WeldingSize1 = originDto.WeldingSize1,
                    WeldingSize2 = originDto.WeldingSize2,
                    ScrewSize1 = originDto.ScrewSize1,
                    ScrewSize2 = originDto.ScrewSize2,
                    ScrewSize3 = originDto.ScrewSize3,
                    SaddleSize = originDto.SaddleSize,
                    GBSize = originDto.GBSize,
                    WeldingCount1 = originDto.WeldingCount1,
                    WeldingCount2 = originDto.WeldingCount2,
                    ScrewCount1 = originDto.ScrewCount1,
                    ScrewCount2 = originDto.ScrewCount2,
                    ScrewCount3 = originDto.ScrewCount3,
                    SaddleCount = originDto.SaddleCount,
                    GBCount = originDto.GBCount,
                    InchDia = originDto.InchDia,
                    BDCount = originDto.BDCount,
                    ProcessingChildCD1 = originDto.ProcessingChildCD1,
                    ProcessingChildCD2 = originDto.ProcessingChildCD2,
                    ProcessingChildCD3 = originDto.ProcessingChildCD3,
                    ProcessingChildCD4 = originDto.ProcessingChildCD4,
                    ProcessingChildCD5 = originDto.ProcessingChildCD5,
                    ProcessingSeq1 = originDto.ProcessingSeq1,
                    ProcessingSeq2 = originDto.ProcessingSeq2,
                    ProcessingSeq3 = originDto.ProcessingSeq3,
                    ProcessingSeq4 = originDto.ProcessingSeq4,
                    ProcessingSeq5 = originDto.ProcessingSeq5,
                    ProcessingChildCDName1 = originDto.ProcessingChildCDName1,
                    ProcessingChildCDName2 = originDto.ProcessingChildCDName2,
                    ProcessingChildCDName3 = originDto.ProcessingChildCDName3,
                    ProcessingChildCDName4 = originDto.ProcessingChildCDName4,
                    ProcessingChildCDName5 = originDto.ProcessingChildCDName5,
                    ProcessingChildCDs = originDto.ProcessingChildCDs,
                    Status = Enums.EditStatus.Insert,
                };
                rowDto.ShohinID = this.GetNextID();

                // 行追加
                bool dataSourceRefreshFlg = false;
                int addRowIdx = this.Body.dgvMstShohin.Rows.IndexOf(r);
                if (datas == null)
                {
                    datas = new BindingList<MST0010_GridDto>();
                    addRowIdx = 0;
                }
                else
                {
                    // 先頭行が空行の場合、データソースのリフレッシュが必要
                    string shohinCD = GridRowUtil<MST0010_GridDto>.GetRowModel(r).ShohinID;
                    dataSourceRefreshFlg = string.IsNullOrEmpty(GridRowUtil<MST0010_GridDto>.GetRowModel(r).ShohinID);
                }
                if (dataSourceRefreshFlg)
                {
                    // データソースリフレッシュする場合の処理
                    var newData = new BindingList<MST0010_GridDto>();
                    newData.Add(rowDto);
                    this.Body.dgvMstShohin.DataSource = newData;
                }
                else
                {
                    addRowIdx++;
                    datas.Insert(addRowIdx, rowDto);
                    this.Body.dgvMstShohin.DataSource = datas;
                }
                // グリッド背景色をセット
                this.SwitchGridCellStyle(addRowIdx);

                this.Body.dgvMstShohin.Show();

                // 使用可否切替（検索ボタン押下時）
                this.EditedFlg = true;
                this.SwitchEnabledChangeSearch();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                dgv.CellValueChanged += this.GridCellValueChanged;
            }
        }
        /// <summary>
        /// 行削除ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridButtonClickEventDelete(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            try
            {
                // 行データ取得
                var dgv = (DataGridView)s;
                DataGridViewRow r = dgv.Rows[e.RowIndex];
                var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);

                // Validate
                if (this.PersonalService.JudgeExistEstimaHavingShohin(rowDto.ShohinID))
                {
                    CommonUtil.ShowErrorMsg(MST0010_Messages.MsgErrorExistEstimaHavingShohinCD);
                    return;
                }
                // 削除行データを保存
                this.DeletedDatas.Add(rowDto);
                if (string.IsNullOrEmpty(rowDto.ShohinID))
                {
                    this.Body.dgvMstShohin.Rows.Remove(r);
                }
                this.Body.dgvMstShohin.Rows.Remove(r);
                // 行削除によって行がなくなった場合の処理
                if (this.Body.dgvMstShohin.Rows.Count == 0)
                {
                    this.Body.dgvMstShohin.Rows.Clear();
                    this.Body.dgvMstShohin.DataSource = new BindingList<MST0010_GridDto>();
                }
                // 編集フラグ
                this.EditedFlg = true;
                this.SwitchEnabledChangeSearch();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCelEnter(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            this.CurrColIndex = e.ColumnIndex;
            var tgtCol = this.Body.dgvMstShohin.Columns[e.ColumnIndex];
            if (tgtCol == null)
            {
                return;
            }
            // IMEモードを設定
            this.Body.dgvMstShohin.ImeMode = ImeMode.Disable;
        }
        /// <summary>
        /// グリッドRowPostPaint
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridRowPostPaintEvent(
              object s
            , DataGridViewRowPostPaintEventArgs e
            )
        {
            GridRowUtil<MST0010_GridDto>.SetRowNum(s, e);
        }
        /// <summary>
        /// グリッドセルキー入力制御
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridKeyPress(
              object s
            , DataGridViewEditingControlShowingEventArgs e
            )
        {
            if (!this.Body.dgvMstShohin.Focused)
            {
                return;
            }
            //表示されているコントロールがDataGridViewTextBoxEditingControlか調べる
            if (!(e.Control is DataGridViewTextBoxEditingControl))
            {
                return;
            }
            //編集のために表示されているコントロールを取得
            var tb = (DataGridViewTextBoxEditingControl)e.Control;

            //イベントハンドラを削除
            tb.KeyPress -= new KeyPressEventHandler(CommonUtil.GridCellKeyPressUpperInteger);
            tb.KeyPress -= new KeyPressEventHandler(CommonUtil.GridCellKeyPressFloat);

            //列ごとに処理分岐
            string tgtColName = this.Body.dgvMstShohin.Columns[this.CurrColIndex].Name;
            switch (tgtColName)
            {
                case nameof(CellHeaderNameDto.TaishoYM):
                case nameof(CellHeaderNameDto.Price):
                    tb.KeyPress += new KeyPressEventHandler(CommonUtil.GridCellKeyPressUpperInteger);
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            // イベント一旦削除
            var dgv = (DataGridView)s;
            dgv.CellValueChanged -= this.GridCellValueChanged;
            DataGridViewRow r = dgv.Rows[e.RowIndex];
            var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);
            DataGridViewCell c = r.Cells[e.ColumnIndex];
            string tgtCellName = c.OwningColumn.Name;
            var tgtCellVal = c.Value;
            try
            {
                if (!this.EditableGridCells.Contains(tgtCellName))
                {
                    return;
                }
                switch (tgtCellName)
                {
                    case nameof(CellHeaderNameDto.Price):
                        // 定価
                        r.Cells[nameof(CellHeaderNameDto.Shikiri)].Value = EstimaUtil.CalcShikiri(
                              CommonUtil.ToInteger(tgtCellVal)
                            , CommonUtil.ToFloat(rowDto.ShikiriRate)
                            , CommonUtil.ToInteger(rowDto.RiekiRate.Value)
                        );
                        c.Value = tgtCellVal;
                        break;
                    default:
                        c.Value = tgtCellVal;
                        break;
                }
                // ステータス更新
                string status = CommonUtil.TostringNullForbid(r.Cells[nameof(rowDto.Status)].Value);
                if ((status == Enums.EditStatus.Show.ToString() || status == Enums.EditStatus.Update.ToString()))
                {
                    r.Cells[nameof(rowDto.Status)].Value = Enums.EditStatus.Update;
                }
                else
                {
                    r.Cells[nameof(rowDto.Status)].Value = Enums.EditStatus.Insert;
                }
                c.Style.BackColor = Colors.BackColorCellEdited;

                // 編集フラグ
                this.EditedFlg = true;

                // 使用可否切り替え（更新ボタン）
                this.SwitchEnableBtnUpdate();
            }
            finally
            {
                // イベント回復
                dgv.CellValueChanged += this.GridCellValueChanged;
            }
        }
        /// <summary>
        /// グリッドCellDoubleClick
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellDoubleClickEvent(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            var dgv = ((DataGridView)s);
            dgv.Rows[e.RowIndex].Selected = true;
            DataGridViewRow r = dgv.Rows[e.RowIndex];
            if (dgv.DataSource == null || string.IsNullOrEmpty(GridRowUtil<MST0010_GridDto>.GetRowModel(r).ShohinID))
            {
                return;
            }
            var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeMstShohin.DialogSingle:
                case Enums.ScreenModeMstShohin.DialogSingleProcessing:
                    var shohinInfo = new MX03Shohin()
                    {
                        ShohinID = CommonUtil.ToInteger(rowDto.ShohinID),
                        ShohinCD = rowDto.ShohinCD,
                    };
                    this.Body.SelectedShohinInfos.Add(shohinInfo);
                    this.Body.DialogResult = DialogResult.OK;
                    this.Body.FormClosing -= this.ClosingEvent;
                    this.Body.Close();
                    break;
            }
        }
        /// <summary>
        /// 追加ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnAddClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                var content = new DE0050_ContentsDto()
                {
                    TaishoYM = DateTime.Today.ToString("yyyyMM"),
                    RiekiRate = 2,
                };
                var drDetail = new DE0050_ShohinDetailDialog(
                      this.LoginInfo
                    , content
                    , this.ScreenMode != Enums.ScreenModeMstShohin.Screen
                );
                if (drDetail.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                // グリッドデータ取得
                var datas = (BindingList<MST0010_GridDto>)this.Body.dgvMstShohin.DataSource;

                // 追加行作成
                var rowDto = new MST0010_GridDto()
                {
                    ParentCD = drDetail.ParentCD,
                    ParentSeq = drDetail.ParentSeq,
                    ParentCDName = drDetail.ParentCDName,
                    ChildCD = drDetail.ChildCD,
                    ChildSeq = drDetail.ChildSeq,
                    ChildCDName = drDetail.ParentCDName,
                    SizeCD = drDetail.SizeCD,
                    ShohinCD = drDetail.ShohinCD,
                    Size1 = drDetail.Size1,
                    Size2 = drDetail.Size2,
                    Size3 = drDetail.Size3,
                    Size4 = drDetail.Size4,
                    ShohinSize = drDetail.ShohinSize,
                    TaishoYM = drDetail.TaishoYM,
                    Price = drDetail.Price,
                    Shikiri = drDetail.Shikiri,
                    ShikiriRate = drDetail.ShikiriRate,
                    RiekiRate = drDetail.RiekiRate,
                    WeldingSize1 = drDetail.WeldingSize1,
                    WeldingSize2 = drDetail.WeldingSize2,
                    ScrewSize1 = drDetail.ScrewSize1,
                    ScrewSize2 = drDetail.ScrewSize2,
                    ScrewSize3 = drDetail.ScrewSize3,
                    SaddleSize = drDetail.SaddleSize,
                    GBSize = drDetail.GBSize,
                    WeldingCount1 = drDetail.WeldingCount1,
                    WeldingCount2 = drDetail.WeldingCount2,
                    ScrewCount1 = drDetail.ScrewCount1,
                    ScrewCount2 = drDetail.ScrewCount2,
                    ScrewCount3 = drDetail.ScrewCount3,
                    SaddleCount = drDetail.SaddleCount,
                    GBCount = drDetail.GBCount,
                    InchDia = drDetail.InchDia,
                    BDCount = drDetail.BDCount,
                    ProcessingChildCD1 = drDetail.ProcessingChildCD1,
                    ProcessingChildCD2 = drDetail.ProcessingChildCD2,
                    ProcessingChildCD3 = drDetail.ProcessingChildCD3,
                    ProcessingChildCD4 = drDetail.ProcessingChildCD4,
                    ProcessingChildCD5 = drDetail.ProcessingChildCD5,
                    ProcessingSeq1 = drDetail.ProcessingSeq1,
                    ProcessingSeq2 = drDetail.ProcessingSeq2,
                    ProcessingSeq3 = drDetail.ProcessingSeq3,
                    ProcessingSeq4 = drDetail.ProcessingSeq4,
                    ProcessingSeq5 = drDetail.ProcessingSeq5,
                    ProcessingChildCDName1 = drDetail.ProcessingChildCDName1,
                    ProcessingChildCDName2 = drDetail.ProcessingChildCDName2,
                    ProcessingChildCDName3 = drDetail.ProcessingChildCDName3,
                    ProcessingChildCDName4 = drDetail.ProcessingChildCDName4,
                    ProcessingChildCDName5 = drDetail.ProcessingChildCDName5,
                    ProcessingChildCDs = drDetail.ProcessingChildCDs,
                    UsedByEstima = drDetail.UsedByEstima,
                    Status = Enums.EditStatus.Insert,
                };
                rowDto.ShohinID = GetNextID();

                // 行追加
                DataGridViewRow r = this.Body.dgvMstShohin.Rows[0];
                bool dataSourceRefreshFlg = false;
                int addRowIdx = this.Body.dgvMstShohin.Rows.IndexOf(r);
                if (datas == null)
                {
                    datas = new BindingList<MST0010_GridDto>();
                    addRowIdx = 0;
                    dataSourceRefreshFlg = true;
                }
                else
                {
                    // 先頭行が空行の場合、データソースのリフレッシュが必要
                    var topRowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(this.Body.dgvMstShohin.Rows[0]);
                    dataSourceRefreshFlg = string.IsNullOrEmpty(topRowDto.ShohinID) && topRowDto.Status == Enums.EditStatus.None;
                }
                if (dataSourceRefreshFlg)
                {
                    // データソースリフレッシュする場合の処理
                    var newData = new BindingList<MST0010_GridDto>();
                    newData.Add(rowDto);
                    this.Body.dgvMstShohin.DataSource = newData;
                }
                else
                {
                    datas.Insert(addRowIdx, rowDto);
                    this.Body.dgvMstShohin.DataSource = datas;
                }
                // グリッド背景色をセット
                this.SwitchGridCellStyle(addRowIdx);
                this.Body.dgvMstShohin.Show();

                // 使用可否切替（検索ボタン押下時）
                this.EditedFlg = true;
                this.SwitchEnabledChangeSearch();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                DialogResult drConfirm;
                if (this.ScreenMode == Enums.ScreenModeMstShohin.Screen)
                {
                    drConfirm = CommonUtil.ShowInfoMsgOKCancel(MST0020_Messages.MsgAskFinish);
                    if (drConfirm != DialogResult.OK)
                    {
                        cancel = true;
                        return;
                    }
                }
                if (!this.ValidateUnUpdateDataExist())
                {
                    drConfirm = CommonUtil.ShowInfoExcramationOKCancel(MST0020_Messages.MsgAskExistUpdateDatas);
                    if (drConfirm != DialogResult.OK)
                    {
                        cancel = true;
                        return;
                    }
                }
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            if (this.ScreenMode != Enums.ScreenModeMstShohin.Screen)
            {
                return;
            }
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                var drConfirm = CommonUtil.ShowInfoMsgOKCancel(MST0020_Messages.MsgAskFinish);
                if (drConfirm != DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
                if (!this.ValidateUnUpdateDataExist())
                {
                    drConfirm = CommonUtil.ShowInfoExcramationOKCancel(MST0020_Messages.MsgAskExistUpdateDatas);
                    if (drConfirm != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSearchClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnUpdClickEvent(
              object s
            , EventArgs e
            )
        {
            var dataSource = GridRowUtil<MST0010_GridDto>.GetAllRowsModel(this.Body.dgvMstShohin.DataSource);
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeMstShohin.DialogSingle:
                    this.BtnUpdClickEventDialogSingle();
                    return;
                case Enums.ScreenModeMstShohin.DialogMulti:
                    this.BtnUpdClickEventDialogMulti();
                    return;
                case Enums.ScreenModeMstShohin.Screen:
                    this.BtnUpdClickEventScreen();
                    break;
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント（単一ダイアログ）
        /// </summary>
        private void BtnUpdClickEventDialogSingle()
        {
            if (this.Body.dgvMstShohin.SelectedRows == null || this.Body.dgvMstShohin.SelectedRows.Count == 0)
            {
                CommonUtil.ShowErrorMsg(MST0010_Messages.MsgErrorNotSelectedRow);
                return;
            }
            DataGridViewRow r = this.Body.dgvMstShohin.SelectedRows[0];
            var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);
            this.Body.SelectedShohinInfos.Add(new MX03Shohin() { ShohinID = CommonUtil.ToInteger(rowDto.ShohinID), });
            this.Body.DialogResult = DialogResult.OK;
        }
        /// <summary>
        /// 更新ボタン押下イベント（複数ダイアログ）
        /// </summary>
        private void BtnUpdClickEventDialogMulti()
        {
            var tgtDatas = GridRowUtil<MST0010_GridDto>.GetAllRowsModel(this.Body.dgvMstShohin.DataSource).Where(n => n.ChkSelect).ToList();
            if (!tgtDatas.Any())
            {
                CommonUtil.ShowErrorMsg(MST0010_Messages.MsgErrorNotSelectedRow);
                return;
            }
            if (this.Body.GridRowEmptyCount < tgtDatas.Count)
            {
                CommonUtil.ShowErrorMsg(MST0010_Messages.MsgErrorOverEmptyRowCount);
                return;
            }
            foreach (var data in tgtDatas)
            {
                this.Body.SelectedShohinInfos.Add(
                    new MX03Shohin() { ShohinID = CommonUtil.ToInteger(data.ShohinID) }
                );
            }
            this.Body.DialogResult = DialogResult.OK;
        }
        /// <summary>
        /// 更新ボタン押下イベント（画面）
        /// </summary>
        private void BtnUpdClickEventScreen()
        {
            // グリッドデータ取得
            var dataSource = GridRowUtil<MST0010_GridDto>.GetAllRowsModel(this.Body.dgvMstShohin.DataSource);
            dataSource = dataSource.Where(n => n.Status == Enums.EditStatus.Insert || n.Status == Enums.EditStatus.Update).ToList();
            // 更新時Validate
            if (!this.ValidateUpdate(dataSource))
            {
                return;
            }
            // 確認ダイアログ
            if (CommonUtil.ShowInfoMsgOKCancel(MST0010_Messages.MsgAskUpdate) != DialogResult.OK)
            {
                return;
            }
            // 更新処理
            bool success = true;
            CommonUtil.ShowProgressDialog(new Action(() =>
            {
                EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.Start);
                success = this.AddUpdDatas(dataSource);
                EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.End);

            }));
            if (!success)
            {
                return;
            }
            // 更新完了メッセージ
            CommonUtil.ShowInfoMsgOK(MST0010_Messages.MsgInfoFinishUpdate);
            // 再検索
            this.SearchLogic();
        }
        /// <summary>
        /// 数値コントロールTextChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void NumTextChanged(
              object s
            , EventArgs e
            )
        {
            var numeric = (NumericUpDown)s;
            if (string.IsNullOrEmpty(numeric.Text))
            {
                numeric.Value = 0;
                numeric.Text = string.Empty;
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// 検索処理
        /// </summary>
        private void SearchLogic()
        {
            // グリッド初期化
            this.Body.dgvMstShohin.Rows.Clear();

            // Validate
            if (!this.VlalidateSearch())
            {
                return;
            }
            // 条件作成
            bool onlyNewest = this.Body.rdoOnlyNewestTaishoYM.Checked;
            var con = new MST0010_SelCondition()
            {
                TekiyoStart = onlyNewest? string.Empty : this.Body.cmbSpanStart.Text,
                TekiyoEnd = onlyNewest ? string.Empty : this.Body.cmbSpanEnd.Text,
                OnlyNewest = onlyNewest,
                ShohinCD =  this.Body.txtShohinCD.Text,
                ShohinSize = this.Body.txtSize.Text,
                PriceFrom = this.Body.numPriceFrom.Value,
                PriceTo = this.Body.numPriceTo.Value,
                DetailKbn = this.DetailKbn,
            };
            // データ取得
            var gridDtoList = new BindingList<MST0010_GridDto>();
            bool success = true;
            var error = new Exception();
            CommonUtil.ShowProgressDialog(() =>
            {
                try
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.Start);
                    gridDtoList = this.GetGridDatas(con);
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.End);
                }
                catch (Exception ex)
                {
                    success = false;
                    error = ex;
                }
            });
            if (!success)
            {
                EstimaUtil.ShowErrorMsg(error, this.Body.Text);
                return;
            }
            if (!gridDtoList.Any())
            {
                this.Body.dgvMstShohin.ReadOnly = true;
                CommonUtil.ShowErrorMsg(Messages.MsgErrorNotExistDatas);
                return;
            }
            // グリッド表示
            this.Body.dgvMstShohin.DataSource = gridDtoList;

            // グリッド背景色をセット
            this.SwitchGridCellStyle();
            this.Body.dgvMstShohin.Show();
            this.Body.dgvMstShohin.Focus();

            // ボタン使用可否切替え
            this.SwitchEnableBtnUpdate();
        }
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        private BindingList<MST0010_GridDto> GetGridDatas(MST0010_SelCondition con)
        {
            var gridDtoList = new BindingList<MST0010_GridDto>();
            this.PersonalService.SelGridDatas(con).ForEach(n => gridDtoList.Add(n));
            return gridDtoList;
        }
        /// <summary>
        /// 使用可否切り替え（更新ボタン）
        /// </summary>
        private void SwitchEnableBtnUpdate()
        {
            this.Body.btnUpdate.Enabled = this.EditedFlg || this.ScreenMode != Enums.ScreenModeMstShohin.Screen;
        }
        /// <summary>
        /// 使用可否切替（検索ボタン押下時）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void SwitchEnabledChangeSearch()
        {
            this.Body.btnUpdate.Enabled = this.EditedFlg;
        }
        /// <summary>
        /// 次のID取得
        /// </summary>
        /// <returns></returns>
        private string GetNextID()
        {
            // 最大ID（グリッド）
            int maxIDOfGrid = 0;
            if (this.Body.dgvMstShohin.DataSource != null)
            {
                var alldatas = GridRowUtil<MST0010_GridDto>.GetAllRowsModel((object)this.Body.dgvMstShohin.DataSource);
                if (alldatas != null && 0 < alldatas.Count)
                {
                    maxIDOfGrid = alldatas.ToList().Max(n => CommonUtil.ToInteger(n.ShohinID));
                }
            }
            // 最大ID（削除データ）
            int maxIDOfDelDatas = 0;
            var delDatas = this.DeletedDatas.Where(n => n.Status != Enums.EditStatus.Insert && n.Status != Enums.EditStatus.None).ToList();
            if (delDatas != null && 0 < delDatas.Count)
            {
                maxIDOfDelDatas = delDatas.ToList().Max(n => CommonUtil.ToInteger(n.ShohinID));
            }
            // 最大ID（DB）
            int maxIDOfDB = this.PersonalService.GetMaxID();

            // ID選択
            var maxID = Math.Max(Math.Max(maxIDOfGrid, maxIDOfDelDatas), maxIDOfDB);
            return (maxID + 1).ToString();
        }
        /// <summary>
        /// DBへデータ登録・更新
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        private bool AddUpdDatas(List<MST0010_GridDto> gridRowDtoList)
        {
            try
            {
                // 削除処理
                this.DeletedDatas.Where(n => n.Status != Enums.EditStatus.Insert)
                                 .ToList().ForEach(n => this.PersonalService.DelMstShohin(n));

                this.DeletedDatas.Clear();

                // 更新用データ作成
                var updDatas = this.CreateUpdDatas(gridRowDtoList);

                // 更新処理
                updDatas.ForEach(n => this.PersonalService.UpdMstShohin(n));

                // 登録用データ作成
                var addDatas = this.CreateAddDatas(gridRowDtoList);

                // 登録処理
                addDatas.ForEach(n => this.PersonalService.AddMstShohin(n));
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX03Shohin> CreateAddDatas(List<MST0010_GridDto> gridDtoList)
        {
            var ret = new List<MX03Shohin>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => !string.IsNullOrEmpty(n.ShohinID) && n.Status == Enums.EditStatus.Insert).ToList())
            {
                // データ作成
                var entity = new MX03Shohin()
                {
                    ShohinID = CommonUtil.ToInteger(rowDto.ShohinID),
                    ParentCD = CommonUtil.TostringNullForbid(rowDto.ParentCD),
                    ParentSeq = CommonUtil.ToInteger(rowDto.ParentSeq),
                    ChildCD = CommonUtil.TostringNullForbid(rowDto.ChildCD),
                    ChildSeq = CommonUtil.ToInteger(rowDto.ChildSeq),
                    SizeCD = CommonUtil.TostringNullForbid(rowDto.SizeCD),
                    ShohinCD = CommonUtil.TostringNullForbid(rowDto.ShohinCD),
                    Size1 = CommonUtil.TostringNullForbid(rowDto.Size1),
                    Size2 = CommonUtil.TostringNullForbid(rowDto.Size2),
                    Size3 = CommonUtil.TostringNullForbid(rowDto.Size3),
                    Size4 = CommonUtil.TostringNullForbid(rowDto.Size4),
                    ShohinSize = CommonUtil.TostringNullForbid(rowDto.ShohinSize),
                    Price = CommonUtil.ToInteger(rowDto.Price.Value),
                    ShikiriRate = rowDto.ShikiriRate.Value,
                    RiekiRate = rowDto.RiekiRate.Value,
                    Shikiri = rowDto.Shikiri.Value,
                    TaishoYM = CommonUtil.TostringNullForbid(rowDto.TaishoYM),
                    WeldingSize1 = rowDto.WeldingSize1,
                    WeldingSize2 = rowDto.WeldingSize2,
                    ScrewSize1 = rowDto.ScrewSize1,
                    ScrewSize2 = rowDto.ScrewSize2,
                    ScrewSize3 = rowDto.ScrewSize3,
                    SaddleSize = rowDto.SaddleSize,
                    GBSize = rowDto.GBSize,
                    InchDia = rowDto.InchDia,
                    WeldingCount1 = rowDto.WeldingCount1,
                    WeldingCount2 = rowDto.WeldingCount2,
                    ScrewCount1 = rowDto.ScrewCount1,
                    ScrewCount2 = rowDto.ScrewCount2,
                    ScrewCount3 = rowDto.ScrewCount3,
                    SaddleCount = rowDto.SaddleCount,
                    GBCount = rowDto.GBCount,
                    BDCount = rowDto.BDCount,
                    ProcessingChildCD1 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD1),
                    ProcessingSeq1 = CommonUtil.ToInteger(rowDto.ProcessingSeq1),
                    ProcessingChildCD2 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD2),
                    ProcessingSeq2 = CommonUtil.ToInteger(rowDto.ProcessingSeq2),
                    ProcessingChildCD3 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD3),
                    ProcessingSeq3 = CommonUtil.ToInteger(rowDto.ProcessingSeq3),
                    ProcessingChildCD4 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD4),
                    ProcessingSeq4 = CommonUtil.ToInteger(rowDto.ProcessingSeq4),
                    ProcessingChildCD5 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD5),
                    ProcessingSeq5 = CommonUtil.ToInteger(rowDto.ProcessingSeq5),
                    ProcessingChildCDs = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCDs),
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 更新用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX03Shohin> CreateUpdDatas(List<MST0010_GridDto> gridDtoList)
        {
            var ret = new List<MX03Shohin>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => !string.IsNullOrEmpty(n.ShohinID) && n.Status == Enums.EditStatus.Update).ToList())
            {
                // データ作成
                var entity = new MX03Shohin()
                {
                    ShohinID = CommonUtil.ToInteger(rowDto.ShohinID),
                    ParentCD = CommonUtil.TostringNullForbid(rowDto.ParentCD),
                    ParentSeq = CommonUtil.ToInteger(rowDto.ParentSeq),
                    ChildCD = CommonUtil.TostringNullForbid(rowDto.ChildCD),
                    ChildSeq = CommonUtil.ToInteger(rowDto.ChildSeq),
                    SizeCD = CommonUtil.TostringNullForbid(rowDto.SizeCD),
                    ShohinCD = CommonUtil.TostringNullForbid(rowDto.ShohinCD),
                    Size1 = CommonUtil.TostringNullForbid(rowDto.Size1),
                    Size2 = CommonUtil.TostringNullForbid(rowDto.Size2),
                    Size3 = CommonUtil.TostringNullForbid(rowDto.Size3),
                    Size4 = CommonUtil.TostringNullForbid(rowDto.Size4),
                    ShohinSize = CommonUtil.TostringNullForbid(rowDto.ShohinSize),
                    Price = CommonUtil.ToInteger(rowDto.Price.Value),
                    ShikiriRate = rowDto.ShikiriRate.Value,
                    RiekiRate = rowDto.RiekiRate.Value,
                    Shikiri = rowDto.Shikiri.Value,
                    TaishoYM = CommonUtil.TostringNullForbid(rowDto.TaishoYM),
                    WeldingSize1 = rowDto.WeldingSize1,
                    WeldingSize2 = rowDto.WeldingSize2,
                    ScrewSize1 = rowDto.ScrewSize1,
                    ScrewSize2 = rowDto.ScrewSize2,
                    ScrewSize3 = rowDto.ScrewSize3,
                    SaddleSize = rowDto.SaddleSize,
                    GBSize = rowDto.GBSize,
                    InchDia = rowDto.InchDia,
                    WeldingCount1 = rowDto.WeldingCount1,
                    WeldingCount2 = rowDto.WeldingCount2,
                    ScrewCount1 = rowDto.ScrewCount1,
                    ScrewCount2 = rowDto.ScrewCount2,
                    ScrewCount3 = rowDto.ScrewCount3,
                    SaddleCount = rowDto.SaddleCount,
                    GBCount = rowDto.GBCount,
                    BDCount = rowDto.BDCount,
                    ProcessingChildCD1 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD1),
                    ProcessingSeq1 = CommonUtil.ToInteger(rowDto.ProcessingSeq1),
                    ProcessingChildCD2 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD2),
                    ProcessingSeq2 = CommonUtil.ToInteger(rowDto.ProcessingSeq2),
                    ProcessingChildCD3 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD3),
                    ProcessingSeq3 = CommonUtil.ToInteger(rowDto.ProcessingSeq3),
                    ProcessingChildCD4 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD4),
                    ProcessingSeq4 = CommonUtil.ToInteger(rowDto.ProcessingSeq4),
                    ProcessingChildCD5 = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCD5),
                    ProcessingSeq5 = CommonUtil.ToInteger(rowDto.ProcessingSeq5),
                    ProcessingChildCDs = CommonUtil.TostringNullForbid(rowDto.ProcessingChildCDs),
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// グリッドセル使用可否切り替え
        /// </summary>
        /// <param name="rowIdx"></param>
        private void SwitchGridCellStyle(int rowIdx = -1)
        {
            this.Body.dgvMstShohin.AllowUserToAddRows = false;
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeMstShohin.Screen:
                    this.SwitchGridCellStyleScreen(rowIdx);
                    break;
                case Enums.ScreenModeMstShohin.DialogSingle:
                case Enums.ScreenModeMstShohin.DialogSingleProcessing:
                    this.SwitchGridCellStyleDialogSingle();
                    break;
                case Enums.ScreenModeMstShohin.DialogMulti:
                case Enums.ScreenModeMstShohin.DialogMultiProcessing:
                    this.SwitchGridCellStyleDialogMulti();
                    return;
            }
        }
        /// <summary>
        /// グリッドセル使用可否切り替え（画面）
        /// </summary>
        /// <param name="rowIdx"></param>
        private void SwitchGridCellStyleScreen(int rowIdx)
        {
            this.Body.dgvMstShohin.ReadOnly = false;
            int rowIndex = 0;
            foreach (DataGridViewRow r in this.Body.dgvMstShohin.Rows)
            {
                var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);
                foreach (DataGridViewCell c in r.Cells)
                {
                    if (this.EditableGridCells.Contains(c.OwningColumn.Name))
                    {
                        c.ReadOnly = false;
                        if (rowIdx == rowIndex && (c.OwningColumn.Name == nameof(CellHeaderNameDto.TaishoYM)
                            || c.OwningColumn.Name == nameof(CellHeaderNameDto.Price)))
                        {
                            c.Style.BackColor = Colors.BackColorCellEdited;
                        }
                        continue;
                    }
                    c.ReadOnly = true;
                }
                rowIndex++;
            }
        }
        /// <summary>
        /// グリッドセル使用可否切り替え（単一ダイアログ）
        /// </summary>
        private void SwitchGridCellStyleDialogSingle()
        {
            this.Body.dgvMstShohin.ReadOnly = true;
            foreach (DataGridViewColumn c in this.Body.dgvMstShohin.Columns)
            {
                c.DefaultCellStyle.BackColor = Color.Empty;
                c.ReadOnly = true;
            }
        }
        /// <summary>
        /// グリッドセル使用可否切り替え（複数ダイアログ）
        /// </summary>
        private void SwitchGridCellStyleDialogMulti()
        {
            this.Body.dgvMstShohin.ReadOnly = false;
            foreach (DataGridViewColumn c in this.Body.dgvMstShohin.Columns)
            {
                if (c.Name == nameof(CellHeaderNameDto.ChkSelect))
                {
                    c.ReadOnly = false;
                    continue;
                }
                c.DefaultCellStyle.BackColor = Color.Empty;
                c.ReadOnly = true;
            }
        }
        /// <summary>
        /// ダイアログの編集結果を行に反映
        /// </summary>
        /// <param name="r"></param>
        /// <param name="content"></param>
        /// <param name="drDetail"></param>
        private void SetEditedDetailInfo(
              DataGridViewRow r
            , DE0050_ContentsDto content
            , DE0050_ShohinDetailDialog drDetail
            )
        {
            r.Cells[nameof(CellHeaderNameDto.BtnShowDetail)].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[nameof(CellHeaderNameDto.BtnCopy)].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[nameof(CellHeaderNameDto.BtnDelete)].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[nameof(CellHeaderNameDto.ParentCD)].Value = drDetail.ParentCD;
            r.Cells[nameof(CellHeaderNameDto.ParentSeq)].Value = drDetail.ParentSeq;
            r.Cells[nameof(CellHeaderNameDto.ParentCDName)].Value = drDetail.ParentCDName;
            r.Cells[nameof(CellHeaderNameDto.ChildCD)].Value = drDetail.ChildCD;
            r.Cells[nameof(CellHeaderNameDto.ChildSeq)].Value = drDetail.ChildSeq;
            r.Cells[nameof(CellHeaderNameDto.ChildCDName)].Value = drDetail.ChildCDName;
            r.Cells[nameof(CellHeaderNameDto.SizeCD)].Value = drDetail.SizeCD;
            r.Cells[nameof(CellHeaderNameDto.ShohinCD)].Value = drDetail.ShohinCD;
            r.Cells[nameof(CellHeaderNameDto.Size1)].Value = drDetail.Size1;
            r.Cells[nameof(CellHeaderNameDto.Size2)].Value = drDetail.Size2;
            r.Cells[nameof(CellHeaderNameDto.Size3)].Value = drDetail.Size3;
            r.Cells[nameof(CellHeaderNameDto.Size4)].Value = drDetail.Size4;
            r.Cells[nameof(CellHeaderNameDto.ShohinSize)].Value = drDetail.ShohinSize;
            r.Cells[nameof(CellHeaderNameDto.TaishoYM)].Value = drDetail.TaishoYM;
            if (content.TaishoYM != drDetail.TaishoYM)
            {
                r.Cells[nameof(CellHeaderNameDto.TaishoYM)].Style.BackColor = Colors.BackColorCellEdited;
            }
            r.Cells[nameof(CellHeaderNameDto.Price)].Value = drDetail.Price;
            if (content.Shikiri != drDetail.Shikiri)
            {
                r.Cells[nameof(CellHeaderNameDto.Shikiri)].Style.BackColor = Colors.BackColorCellEdited;
            }
            r.Cells[nameof(CellHeaderNameDto.Shikiri)].Value = drDetail.Shikiri;
            r.Cells[nameof(CellHeaderNameDto.ShikiriRate)].Value = drDetail.ShikiriRate;
            r.Cells[nameof(CellHeaderNameDto.RiekiRate)].Value = drDetail.RiekiRate;
            r.Cells[nameof(CellHeaderNameDto.WeldingSize1)].Value = drDetail.WeldingSize1;
            r.Cells[nameof(CellHeaderNameDto.WeldingSize2)].Value = drDetail.WeldingSize2;
            r.Cells[nameof(CellHeaderNameDto.ScrewSize1)].Value = drDetail.ScrewSize1;
            r.Cells[nameof(CellHeaderNameDto.ScrewSize2)].Value = drDetail.ScrewSize2;
            r.Cells[nameof(CellHeaderNameDto.ScrewSize3)].Value = drDetail.ScrewSize3;
            r.Cells[nameof(CellHeaderNameDto.SaddleSize)].Value = drDetail.SaddleSize;
            r.Cells[nameof(CellHeaderNameDto.GBSize)].Value = drDetail.GBSize;
            r.Cells[nameof(CellHeaderNameDto.WeldingCount1)].Value = drDetail.WeldingCount1;
            r.Cells[nameof(CellHeaderNameDto.WeldingCount2)].Value = drDetail.WeldingCount2;
            r.Cells[nameof(CellHeaderNameDto.ScrewCount1)].Value = drDetail.ScrewCount1;
            r.Cells[nameof(CellHeaderNameDto.ScrewCount2)].Value = drDetail.ScrewCount2;
            r.Cells[nameof(CellHeaderNameDto.ScrewCount3)].Value = drDetail.ScrewCount3;
            r.Cells[nameof(CellHeaderNameDto.SaddleCount)].Value = drDetail.SaddleCount;
            r.Cells[nameof(CellHeaderNameDto.GBCount)].Value = drDetail.GBCount;
            r.Cells[nameof(CellHeaderNameDto.InchDia)].Value = drDetail.InchDia;
            r.Cells[nameof(CellHeaderNameDto.BDCount)].Value = drDetail.BDCount;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCD1)].Value = drDetail.ProcessingChildCD1;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCD2)].Value = drDetail.ProcessingChildCD2;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCD3)].Value = drDetail.ProcessingChildCD3;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCD4)].Value = drDetail.ProcessingChildCD4;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCD5)].Value = drDetail.ProcessingChildCD5;
            r.Cells[nameof(CellHeaderNameDto.ProcessingSeq1)].Value = drDetail.ProcessingSeq1;
            r.Cells[nameof(CellHeaderNameDto.ProcessingSeq2)].Value = drDetail.ProcessingSeq2;
            r.Cells[nameof(CellHeaderNameDto.ProcessingSeq3)].Value = drDetail.ProcessingSeq3;
            r.Cells[nameof(CellHeaderNameDto.ProcessingSeq4)].Value = drDetail.ProcessingSeq4;
            r.Cells[nameof(CellHeaderNameDto.ProcessingSeq5)].Value = drDetail.ProcessingSeq5;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCDName1)].Value = drDetail.ProcessingChildCDName1;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCDName2)].Value = drDetail.ProcessingChildCDName2;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCDName3)].Value = drDetail.ProcessingChildCDName3;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCDName4)].Value = drDetail.ProcessingChildCDName4;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCDName5)].Value = drDetail.ProcessingChildCDName5;
            r.Cells[nameof(CellHeaderNameDto.ProcessingChildCDs)].Value = drDetail.ProcessingChildCDs;
            r.Cells[nameof(CellHeaderNameDto.UsedByEstima)].Value = drDetail.UsedByEstima;
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新されていない項目が存在する？
        /// </summary>
        /// <returns></returns>
        private bool ValidateUnUpdateDataExist()
        {
            foreach (DataGridViewRow r in this.Body.dgvMstShohin.Rows)
            {
                // 更新されていない行が含まれる場合
                var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.Insert || rowDto.Status == Enums.EditStatus.Update)
                {
                    return false;
                }
            }
            if (this.DeletedDatas.Any(n => n.Status == Enums.EditStatus.Show || n.Status == Enums.EditStatus.Update))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 検索時Validate
        /// </summary>
        /// <returns></returns>
        private bool VlalidateSearch()
        {
            if (CommonUtil.ToInteger(this.Body.cmbSpanEnd.Text, 999999) < CommonUtil.ToInteger(this.Body.cmbSpanStart.Text))
            {
                this.Body.cmbSpanEnd.Focus();
                CommonUtil.ShowErrorMsg(string.Format(Messages.MsgErrorLargeSmallCollision, "適用期間（終了）", "適用期間（開始）"));
                return false;
            }
            return true;
        }
        /// <summary>
        ///  更新時Validate
        /// </summary>
        /// <param name="allGridDtoList"></param>
        /// <returns></returns>
        private bool ValidateUpdate(List<MST0010_GridDto> allGridDtoList)
        {
            // 更新対象件数チェック
            if (!allGridDtoList.Any()&& !this.DeletedDatas.Any(n => n.Status != Enums.EditStatus.Insert))
            {
                CommonUtil.ShowErrorMsg(MST0010_Messages.MsgErrorNotExistDatas);
                return false;
            }
            // グリッドセル空文字チェック
            if (!this.ValidateEmpty())
            {
                return false;
            }
            // 重複チェック
            if (!this.ValidateDouple(allGridDtoList))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty()
        {
            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in this.Body.dgvMstShohin.Rows)
            {

                var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.Insert && rowDto.Status != Enums.EditStatus.Update)
                {
                    continue;
                }
                // 商品コード
                headerTxt = this.Body.dgvMstShohin.Columns[nameof(rowDto.ShohinCD)].HeaderText;
                CommonUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.ShohinCD), ref errColNames, ref result);

                // 定価
                headerTxt = this.Body.dgvMstShohin.Columns[nameof(rowDto.Price)].HeaderText;
                CommonUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.Price), ref errColNames, ref result);

                // 適用開始日
                headerTxt = this.Body.dgvMstShohin.Columns[nameof(rowDto.TaishoYM)].HeaderText;
                CommonUtil.ValidateEmpty(r, headerTxt, nameof(rowDto.TaishoYM), ref errColNames, ref result);
            }
            if (!result)
            {
                string msg = string.Format(MST0010_Messages.MsgErrorMissEmpty, string.Join("、", errColNames));
                CommonUtil.ShowErrorMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateDouple(List<MST0010_GridDto> gridDatas)
        {
            // 検索データ絞り込み
            var oldDatas = this.PersonalService.SelGridDatas(new MST0010_SelCondition()).ToList();

            string msg = string.Empty;
            bool resutlt = true;
            foreach (var keyInfo in gridDatas)
            {
                if (this.ValidateDoupleInsert(oldDatas, keyInfo) && this.ValidateDoupleUpdatet(gridDatas, keyInfo))
                {
                    continue;
                }
                // 背景色編集
                foreach (DataGridViewRow r in this.Body.dgvMstShohin.Rows)
                {
                    var rowDto = GridRowUtil<MST0010_GridDto>.GetRowModel(r);
                    if (rowDto.ShohinCD == keyInfo.ShohinCD && rowDto.TaishoYM == keyInfo.TaishoYM)
                    {
                        r.Cells[nameof(rowDto.ShohinCD)].Style.BackColor = Color.Red;
                        r.Cells[nameof(rowDto.TaishoYM)].Style.BackColor = Color.Red;
                    }
                }
                resutlt = false;
            }
            if (!resutlt)
            {
                // 重複キー
                CommonUtil.ShowErrorMsg(string.Format(MST0010_Messages.MsgErrorDouple, "同じ商品コードで適用開始日"));
            }
            return resutlt;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDoupleInsert(
              List<MST0010_GridDto> oldDatas
            , MST0010_GridDto keyInfo
            )
        {
            if (keyInfo.Status != Enums.EditStatus.Insert)
            {
                return true;
            }
            if (oldDatas.Exists(n => n.ShohinCD == keyInfo.ShohinCD && n.TaishoYM == keyInfo.TaishoYM))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（更新）
        /// </summary>
        /// <param name="gridDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDoupleUpdatet(
              List<MST0010_GridDto> gridDatas
            , MST0010_GridDto keyInfo
            )
        {
            if (1 < gridDatas.Where(n => n.ShohinCD == keyInfo.ShohinCD　&& n.TaishoYM == keyInfo.TaishoYM).ToList().Count)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            // コントロール初期化
            this.Body.cmbSpanStart.Items.Clear();
            this.Body.cmbSpanStart.Items.Add(string.Empty);
            this.Body.cmbSpanEnd.Items.Clear();
            this.Body.cmbSpanEnd.Items.Add(string.Empty);

            // データ取得
            var taishoYMList = new List<string>();
            CommonUtil.ShowProgressDialog(() =>
            {
                taishoYMList = this.CommonService.SelTaishoYM();
            });
            taishoYMList.ForEach(n =>
            {
                this.Body.cmbSpanStart.Items.Add(n);
                this.Body.cmbSpanEnd.Items.Add(n);
            });
            this.Body.cmbSpanStart.Text = string.Empty;
            this.Body.cmbSpanEnd.Text = string.Empty;
            this.Body.txtShohinCD.Text = string.Empty;
            this.Body.txtSize.Text = string.Empty;
            this.Body.numPriceFrom.Text = string.Empty;
            this.Body.numPriceTo.Text = string.Empty;
            this.Body.rdoOnlyNewestTaishoYM.Checked = true;
            this.RdoTaishoYMCheckedChanged();

            // グリッド
            this.Body.dgvMstShohin.Rows.Clear();
            this.Body.dgvMstShohin.AllowUserToAddRows = false;
            this.Body.dgvMstShohin.ReadOnly = true;

            // 使用可否切り替え
            this.EditedFlg = false;
            this.SwitchEnableBtnUpdate();

            // フォーカス
            this.Body.txtShohinCD.Focus();

            // スクリーンモードに応じて処理分岐
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeMstShohin.Screen:
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.ChkSelect)].Visible = false;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnShowDetail)].Visible = true;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnCopy)].Visible = true;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnDelete)].Visible = true;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.UsedByEstima)].Visible = true;
                    this.Body.dgvMstShohin.MultiSelect = true;
                    this.Body.dgvMstShohin.AllowUserToAddRows = false;
                    this.Body.pnlBtnAdd.Visible = true;
                    break;
                case Enums.ScreenModeMstShohin.DialogSingle:
                case Enums.ScreenModeMstShohin.DialogSingleProcessing:
                    if (!string.IsNullOrEmpty(this.SearchShohinCD))
                    {
                        this.Body.txtShohinCD.Text = this.SearchShohinCD;
                    }
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.ChkSelect)].Visible = false;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnShowDetail)].Visible = true;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnCopy)].Visible = false;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnDelete)].Visible = false;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.UsedByEstima)].Visible = false;
                    this.Body.dgvMstShohin.AllowUserToAddRows = false;
                    this.Body.pnlBtnAdd.Visible = false;
                    this.SearchLogic();
                    this.Body.btnUpdate.Text = "OK（O）";
                    this.Body.btnUpdate.Enabled = true;
                    this.Body.dgvMstShohin.MultiSelect = false;
                    this.Body.dgvMstShohin.Focus();
                    break;
                case Enums.ScreenModeMstShohin.DialogMulti:
                case Enums.ScreenModeMstShohin.DialogMultiProcessing:
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.ChkSelect)].Visible = true;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnShowDetail)].Visible = true;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnCopy)].Visible = false;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.BtnDelete)].Visible = false;
                    this.Body.dgvMstShohin.Columns[nameof(CellHeaderNameDto.UsedByEstima)].Visible = false;
                    this.Body.dgvMstShohin.AllowUserToAddRows = false;
                    this.Body.pnlBtnAdd.Visible = false;
                    this.SearchLogic();
                    this.Body.btnUpdate.Text = "OK（O）";
                    this.Body.btnUpdate.Enabled = true;
                    this.Body.dgvMstShohin.MultiSelect = true;
                    this.Body.dgvMstShohin.Focus();
                    break;
            }
        }
        #endregion
    }
}
