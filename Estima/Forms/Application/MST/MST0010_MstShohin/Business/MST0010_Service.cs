﻿using EstimaLib.Const;
using EstimaLib.Entity;
using EstimaLib.Util;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Estima.Forms.Application.MST.MST0010.Business
{
    internal class MST0010_Service
    {
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        internal List<MST0010_GridDto> SelGridDatas(MST0010_SelCondition con)
        {
            string cmOutShohinCD = CommonUtil.GetCmout(string.IsNullOrEmpty(con.ShohinCD));
            string cmOutShohinSize = CommonUtil.GetCmout(string.IsNullOrEmpty(con.ShohinSize));
            string cmOutPriceFrom = CommonUtil.GetCmout(con.PriceFrom == 0);
            string cmOutPriceTo = CommonUtil.GetCmout(con.PriceTo == 0);
            string cmOutSpanStart = CommonUtil.GetCmout(string.IsNullOrEmpty(con.TekiyoStart));
            string cmOutSpanEnd = CommonUtil.GetCmout(string.IsNullOrEmpty(con.TekiyoEnd));
            string cmOutSpanNewest = CommonUtil.GetCmout(!con.OnlyNewest);

            var ret = new List<MST0010_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VMX01.ShohinID ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($",   VMX01.ParentCD ");
            sb.AppendLine($",   VMX01.ParentSeq ");
            sb.AppendLine($",   VMX01.ParentCDName ");
            sb.AppendLine($",   VMX01.DetailKbn ");
            sb.AppendLine($",   VMX01.ChildCD ");
            sb.AppendLine($",   VMX01.ChildSeq ");
            sb.AppendLine($",   VMX01.ChildCDName ");
            sb.AppendLine($",   VMX01.SizeCD ");
            sb.AppendLine($",   VMX01.ShohinCD ");
            sb.AppendLine($",   VMX01.Size1 ");
            sb.AppendLine($",   VMX01.Size2 ");
            sb.AppendLine($",   VMX01.Size3 ");
            sb.AppendLine($",   VMX01.Size4 ");
            sb.AppendLine($",   VMX01.ShohinSize ");
            sb.AppendLine($",   VMX01.Price ");
            sb.AppendLine($",   VMX01.ShikiriRate ");
            sb.AppendLine($",   VMX01.RiekiRate ");
            sb.AppendLine($",   VMX01.Shikiri ");
            sb.AppendLine($",   VMX01.WeldingSize1 ");
            sb.AppendLine($",   VMX01.WeldingSize2 ");
            sb.AppendLine($",   VMX01.ScrewSize1 ");
            sb.AppendLine($",   VMX01.ScrewSize2 ");
            sb.AppendLine($",   VMX01.ScrewSize3 ");
            sb.AppendLine($",   VMX01.SaddleSize ");
            sb.AppendLine($",   VMX01.GBSize ");
            sb.AppendLine($",   VMX01.InchDia ");
            sb.AppendLine($",   VMX01.WeldingCount1 ");
            sb.AppendLine($",   VMX01.WeldingCount2 ");
            sb.AppendLine($",   VMX01.ScrewCount1 ");
            sb.AppendLine($",   VMX01.ScrewCount2 ");
            sb.AppendLine($",   VMX01.ScrewCount3 ");
            sb.AppendLine($",   VMX01.SaddleCount ");
            sb.AppendLine($",   VMX01.GBCount ");
            sb.AppendLine($",   VMX01.BDCount ");
            sb.AppendLine($",   VMX01.ProcessingChildCD1 ");
            sb.AppendLine($",   VMX01.ProcessingChildCD2 ");
            sb.AppendLine($",   VMX01.ProcessingChildCD3 ");
            sb.AppendLine($",   VMX01.ProcessingChildCD4 ");
            sb.AppendLine($",   VMX01.ProcessingChildCD5 ");
            sb.AppendLine($",   VMX01.ProcessingSeq1 ");
            sb.AppendLine($",   VMX01.ProcessingSeq2 ");
            sb.AppendLine($",   VMX01.ProcessingSeq3 ");
            sb.AppendLine($",   VMX01.ProcessingSeq4 ");
            sb.AppendLine($",   VMX01.ProcessingSeq5 ");
            sb.AppendLine($",   VMX01.ProcessingChildCDName1 ");
            sb.AppendLine($",   VMX01.ProcessingChildCDName2 ");
            sb.AppendLine($",   VMX01.ProcessingChildCDName3 ");
            sb.AppendLine($",   VMX01.ProcessingChildCDName4 ");
            sb.AppendLine($",   VMX01.ProcessingChildCDName5 ");
            sb.AppendLine($",   VMX01.ProcessingChildCDs ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VMX01Shohin AS VMX01 ");
            sb.AppendLine($"{cmOutSpanNewest}INNER JOIN( ");
            sb.AppendLine($"{cmOutSpanNewest}    SELECT ");
            sb.AppendLine($"{cmOutSpanNewest}        M03.ShohinCD ");
            sb.AppendLine($"{cmOutSpanNewest}    ,   MAX(M03.TaishoYM) AS TaishoYM ");
            sb.AppendLine($"{cmOutSpanNewest}    FROM ");
            sb.AppendLine($"{cmOutSpanNewest}        MX03Shohin AS M03 ");
            sb.AppendLine($"{cmOutSpanNewest}    WHERE");
            sb.AppendLine($"{cmOutSpanNewest}        0 = 0 ");
            sb.AppendLine($"{cmOutSpanNewest}    {cmOutSpanStart}AND @tekiyoStart <= M03.TaishoYM ");
            sb.AppendLine($"{cmOutSpanNewest}    {cmOutSpanEnd  }AND M03.TaishoYM <= @tekiyoEnd ");
            sb.AppendLine($"{cmOutSpanNewest}    GROUP BY");
            sb.AppendLine($"{cmOutSpanNewest}        M03.ShohinCD ");
            sb.AppendLine($"{cmOutSpanNewest}) AS MX03Newest ");
            sb.AppendLine($"{cmOutSpanNewest}ON ");
            sb.AppendLine($"{cmOutSpanNewest}    VMX01.ShohinCD = MX03Newest.ShohinCD ");
            sb.AppendLine($"{cmOutSpanNewest}AND VMX01.TaishoYM = MX03Newest.TaishoYM ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutSpanStart}AND @tekiyoStart  <= VMX01.TaishoYM ");
            sb.AppendLine($"{cmOutSpanEnd  }AND VMX01.TaishoYM <= @tekiyoEnd ");
            sb.AppendLine($"{cmOutShohinCD       } AND VMX01.ShohinCD         LIKE @shohinCD ");
            sb.AppendLine($"{cmOutShohinSize     } AND VMX01.ShohinSize       LIKE @shohinSize ");
            sb.AppendLine($"{cmOutPriceFrom      } AND VMX01.Price            >= @priceFrom ");
            sb.AppendLine($"{cmOutPriceTo        } AND VMX01.Price            <= @priceTo ");
            switch (con.DetailKbn)
            {
                case Enums.DetailKbn.TabPlumbing:
                    sb.AppendLine($" AND VMX01.DetailKbn IN(  {(int)Enums.DetailKbn.Plumbing} ");
                    sb.AppendLine($"                        , {(int)Enums.DetailKbn.Joint} ");
                    sb.AppendLine($") ");
                    break;
                case Enums.DetailKbn.TabHousing:
                    sb.AppendLine($" AND VMX01.DetailKbn IN(  {(int)Enums.DetailKbn.Housing} ");
                    sb.AppendLine($") ");
                    break;
                case Enums.DetailKbn.TabProcessing:
                    sb.AppendLine($" AND VMX01.DetailKbn IN(  {(int)Enums.DetailKbn.ProcessingCost} ");;
                    sb.AppendLine($") ");
                    break;
            }
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    VMX01.ShohinCD ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tekiyoStart", con.TekiyoStart) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@tekiyoEnd", con.TekiyoEnd) { DbType = DbType.String });
                    cmd.Parameters.AddWithValue("@shohinCD"    , "%" + con.ShohinCD     + "%");
                    cmd.Parameters.AddWithValue("@shohinSize"  , "%" + con.ShohinSize + "%");
                    cmd.Parameters.Add(new MySqlParameter("@priceFrom", con.PriceFrom) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@priceTo", con.PriceTo) { DbType = DbType.Int32 });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new MST0010_GridDto();
                            dto.ShohinID = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinID)]);
                            dto.ParentCD = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentCD)]);
                            dto.ParentSeq = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentSeq)]);
                            dto.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentCDName)]);
                            dto.ChildCD = CommonUtil.TostringNullForbid(dr[nameof(dto.ChildCD)]);
                            dto.ChildSeq = CommonUtil.TostringNullForbid(dr[nameof(dto.ChildSeq)]);
                            dto.ChildCDName = CommonUtil.TostringNullForbid(dr[nameof(dto.ChildCDName)]);
                            dto.SizeCD = CommonUtil.TostringNullForbid(dr[nameof(dto.SizeCD)]);
                            dto.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinCD)]);
                            dto.Size1 = CommonUtil.TostringNullForbid(dr[nameof(dto.Size1)]);
                            dto.Size2 = CommonUtil.TostringNullForbid(dr[nameof(dto.Size2)]);
                            dto.Size3 = CommonUtil.TostringNullForbid(dr[nameof(dto.Size3)]);
                            dto.Size4 = CommonUtil.TostringNullForbid(dr[nameof(dto.Size4)]);
                            dto.ShohinSize = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinSize)]);
                            dto.TaishoYM = CommonUtil.TostringNullForbid(dr[nameof(dto.TaishoYM)]);
                            dto.Price = CommonUtil.ToInteger(dr[nameof(dto.Price)]);
                            dto.Shikiri = CommonUtil.ToInteger(dr[nameof(dto.Shikiri)]);
                            dto.ShikiriRate = CommonUtil.ToFloat(dr[nameof(dto.ShikiriRate)]);
                            dto.RiekiRate = CommonUtil.ToFloat(dr[nameof(dto.RiekiRate)]);
                            dto.WeldingSize1 = this.ToFloat(dr[nameof(dto.WeldingSize1)]);
                            dto.WeldingSize2 = this.ToFloat(dr[nameof(dto.WeldingSize2)]);
                            dto.ScrewSize1 = this.ToFloat(dr[nameof(dto.ScrewSize1)]);
                            dto.ScrewSize2 = this.ToFloat(dr[nameof(dto.ScrewSize2)]);
                            dto.ScrewSize3 = this.ToFloat(dr[nameof(dto.ScrewSize3)]);
                            dto.SaddleSize = this.ToFloat(dr[nameof(dto.SaddleSize)]);
                            dto.GBSize = this.ToFloat(dr[nameof(dto.GBSize)]);
                            dto.InchDia = this.ToFloat(dr[nameof(dto.InchDia)]);
                            dto.WeldingCount1 = this.ToInteger(dr[nameof(dto.WeldingCount1)]);
                            dto.WeldingCount2 = this.ToInteger(dr[nameof(dto.WeldingCount2)]);
                            dto.ScrewCount1 = this.ToInteger(dr[nameof(dto.ScrewCount1)]);
                            dto.ScrewCount2 = this.ToInteger(dr[nameof(dto.ScrewCount2)]);
                            dto.ScrewCount3 = this.ToInteger(dr[nameof(dto.ScrewCount3)]);
                            dto.SaddleCount = this.ToInteger(dr[nameof(dto.SaddleCount)]);
                            dto.GBCount = this.ToInteger(dr[nameof(dto.GBCount)]);
                            dto.BDCount = this.ToInteger(dr[nameof(dto.BDCount)]);
                            dto.ProcessingChildCD1 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCD1)]);
                            dto.ProcessingChildCD2 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCD2)]);
                            dto.ProcessingChildCD3 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCD3)]);
                            dto.ProcessingChildCD4 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCD4)]);
                            dto.ProcessingChildCD5 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCD5)]);
                            dto.ProcessingSeq1 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingSeq1)]);
                            dto.ProcessingSeq2 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingSeq2)]);
                            dto.ProcessingSeq3 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingSeq3)]);
                            dto.ProcessingSeq4 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingSeq4)]);
                            dto.ProcessingSeq5 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingSeq5)]);
                            dto.ProcessingChildCDName1 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCDName1)]);
                            dto.ProcessingChildCDName2 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCDName2)]);
                            dto.ProcessingChildCDName3 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCDName3)]);
                            dto.ProcessingChildCDName4 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCDName4)]);
                            dto.ProcessingChildCDName5 = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCDName5)]);
                            dto.ProcessingChildCDs = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingChildCDs)]);
                            dto.UsedByEstima = this.JudgeExistEstimaHavingShohin(dto.ShohinID);
                            dto.Status = Enums.EditStatus.Show;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret.OrderBy(n => CommonUtil.ToInteger(n.ParentCD))
                      .ThenBy(n => (n.ShohinCD + "-ZZZ-ZZZ").Split('-')[1])
                      .ThenBy(n => CommonUtil.CutAInteger((n.ShohinCD + "-ZZZ-ZZZ").Split('-')[2]))
                      .ThenBy(n => CommonUtil.ToInteger(n.TaishoYM))
                      .ToList();
        }
        /// <summary>
        /// 数値変換（int）
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private int? ToInteger(object o)
        {
            if (o == null)
            {
                return null;
            }
            if (string.IsNullOrEmpty(o.ToString()))
            {
                return null;
            }
            return CommonUtil.ToInteger(o);
        }
        /// <summary>
        /// 数値変換（float）
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private float? ToFloat(object o)
        {
            if (o == null)
            {
                return null;
            }
            if (string.IsNullOrEmpty(o.ToString()))
            {
                return null;
            }
            return CommonUtil.ToFloat(o);
        }
        /// <summary>
        /// ID取得
        /// </summary>
        /// <returns></returns>
        internal int GetMaxID()
        {
            var ret = 0;

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MAX(MX03.ShohinID) AS MaxID ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX03Shohin AS MX03 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 商品マスタ登録
        /// </summary>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        internal void AddMstShohin(MX03Shohin addEntity)
        {
            string cmOutWeldingSize1 = EstimaUtil.GetCmout(addEntity.WeldingSize1 == null);
            string cmOutWeldingSize2 = EstimaUtil.GetCmout(addEntity.WeldingSize2 == null);
            string cmOutscrewSize1 = EstimaUtil.GetCmout(addEntity.ScrewSize1 == null);
            string cmOutscrewSize2 = EstimaUtil.GetCmout(addEntity.ScrewSize2 == null);
            string cmOutscrewSize3 = EstimaUtil.GetCmout(addEntity.ScrewSize3 == null);
            string cmOutSaddleSize = EstimaUtil.GetCmout(addEntity.SaddleSize == null);
            string cmOutGBSize = EstimaUtil.GetCmout(addEntity.GBSize == null);
            string cmOutInchDia = EstimaUtil.GetCmout(addEntity.InchDia == null);

            string cmOutWeldingCount1 = EstimaUtil.GetCmout(addEntity.WeldingCount1 == null);
            string cmOutWeldingCount2 = EstimaUtil.GetCmout(addEntity.WeldingCount2 == null);
            string cmOutscrewCount1 = EstimaUtil.GetCmout(addEntity.ScrewCount1 == null);
            string cmOutscrewCount2 = EstimaUtil.GetCmout(addEntity.ScrewCount2 == null);
            string cmOutscrewCount3 = EstimaUtil.GetCmout(addEntity.ScrewCount3 == null);
            string cmOutSaddleCount = EstimaUtil.GetCmout(addEntity.SaddleCount == null);
            string cmOutGBCount = EstimaUtil.GetCmout(addEntity.GBCount == null);
            string cmOutBDCount = EstimaUtil.GetCmout(addEntity.BDCount == null);

            string cmOutProcessingSeq1 = EstimaUtil.GetCmout(addEntity.ProcessingSeq1 == null);
            string cmOutProcessingSeq2 = EstimaUtil.GetCmout(addEntity.ProcessingSeq2 == null);
            string cmOutProcessingSeq3 = EstimaUtil.GetCmout(addEntity.ProcessingSeq3 == null);
            string cmOutProcessingSeq4 = EstimaUtil.GetCmout(addEntity.ProcessingSeq4 == null);
            string cmOutProcessingSeq5 = EstimaUtil.GetCmout(addEntity.ProcessingSeq5 == null);

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO MX03Shohin( ");
            sb.AppendLine($"    ShohinID ");
            sb.AppendLine($",   ParentCD ");
            sb.AppendLine($",   ParentSeq ");
            sb.AppendLine($",   ChildCD ");
            sb.AppendLine($",   ChildSeq ");
            sb.AppendLine($",   SizeCD ");
            sb.AppendLine($",   ShohinCD ");
            sb.AppendLine($",   Size1 ");
            sb.AppendLine($",   Size2 ");
            sb.AppendLine($",   Size3 ");
            sb.AppendLine($",   Size4 ");
            sb.AppendLine($",   ShohinSize ");
            sb.AppendLine($",   TaishoYM ");
            sb.AppendLine($",   Price ");
            sb.AppendLine($",   ShikiriRate ");
            sb.AppendLine($",   RiekiRate ");
            sb.AppendLine($",   Shikiri ");
            sb.AppendLine($"{cmOutWeldingSize1 },   WeldingSize1 ");
            sb.AppendLine($"{cmOutWeldingSize2 },   WeldingSize2 ");
            sb.AppendLine($"{cmOutscrewSize1   },   ScrewSize1 ");
            sb.AppendLine($"{cmOutscrewSize2   },   ScrewSize2 ");
            sb.AppendLine($"{cmOutscrewSize3   },   ScrewSize3 ");
            sb.AppendLine($"{cmOutSaddleSize   },   SaddleSize ");
            sb.AppendLine($"{cmOutGBSize       },   GBSize ");
            sb.AppendLine($"{cmOutInchDia      },   InchDia ");
            sb.AppendLine($"{cmOutWeldingCount1},   WeldingCount1 ");
            sb.AppendLine($"{cmOutWeldingCount2},   WeldingCount2 ");
            sb.AppendLine($"{cmOutscrewCount1  },   ScrewCount1 ");
            sb.AppendLine($"{cmOutscrewCount2  },   ScrewCount2 ");
            sb.AppendLine($"{cmOutscrewCount3  },   ScrewCount3 ");
            sb.AppendLine($"{cmOutSaddleCount  },   SaddleCount ");
            sb.AppendLine($"{cmOutGBCount      },   GBCount ");
            sb.AppendLine($"{cmOutBDCount      },   BDCount ");
            sb.AppendLine($",   ProcessingChildCD1 ");
            sb.AppendLine($"{cmOutProcessingSeq1},   ProcessingSeq1 ");
            sb.AppendLine($",   ProcessingChildCD2 ");
            sb.AppendLine($"{cmOutProcessingSeq2},   ProcessingSeq2 ");
            sb.AppendLine($",   ProcessingChildCD3 ");
            sb.AppendLine($"{cmOutProcessingSeq3},   ProcessingSeq3 ");
            sb.AppendLine($",   ProcessingChildCD4 ");
            sb.AppendLine($"{cmOutProcessingSeq4},   ProcessingSeq4 ");
            sb.AppendLine($",   ProcessingChildCD5 ");
            sb.AppendLine($"{cmOutProcessingSeq5},   ProcessingSeq5 ");
            sb.AppendLine($",   ProcessingChildCDs ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @shohinID ");
            sb.AppendLine($",   @parentCD ");
            sb.AppendLine($",   @parentSeq ");
            sb.AppendLine($",   @childCD ");
            sb.AppendLine($",   @childSeq ");
            sb.AppendLine($",   @sizeCD ");
            sb.AppendLine($",   @shohinCD ");
            sb.AppendLine($",   @size1 ");
            sb.AppendLine($",   @size2 ");
            sb.AppendLine($",   @size3 ");
            sb.AppendLine($",   @size4 ");
            sb.AppendLine($",   @shohinSize ");
            sb.AppendLine($",   @taishoYM ");
            sb.AppendLine($",   @price ");
            sb.AppendLine($",   @shikiriRate ");
            sb.AppendLine($",   @riekiRate ");
            sb.AppendLine($",   @shikiri ");
            sb.AppendLine($"{cmOutWeldingSize1 },   @weldingSize1 ");
            sb.AppendLine($"{cmOutWeldingSize2 },   @weldingSize2 ");
            sb.AppendLine($"{cmOutscrewSize1   },   @screwSize1 ");
            sb.AppendLine($"{cmOutscrewSize2   },   @screwSize2 ");
            sb.AppendLine($"{cmOutscrewSize3   },   @screwSize3 ");
            sb.AppendLine($"{cmOutSaddleSize   },   @saddleSize ");
            sb.AppendLine($"{cmOutGBSize       },   @gbSize ");
            sb.AppendLine($"{cmOutInchDia      },   @inchDia ");
            sb.AppendLine($"{cmOutWeldingCount1},   @weldingCount1 ");
            sb.AppendLine($"{cmOutWeldingCount2},   @weldingCount2 ");
            sb.AppendLine($"{cmOutscrewCount1  },   @screwCount1 ");
            sb.AppendLine($"{cmOutscrewCount2  },   @screwCount2 ");
            sb.AppendLine($"{cmOutscrewCount3  },   @screwCount3 ");
            sb.AppendLine($"{cmOutSaddleCount  },   @saddleCount ");
            sb.AppendLine($"{cmOutGBCount      },   @gbCount ");
            sb.AppendLine($"{cmOutBDCount      },   @bDCount ");
            sb.AppendLine($",   @processingChildCD1 ");
            sb.AppendLine($"{cmOutProcessingSeq1},   @processingSeq1 ");
            sb.AppendLine($",   @processingChildCD2 ");
            sb.AppendLine($"{cmOutProcessingSeq2},   @processingSeq2 ");
            sb.AppendLine($",   @processingChildCD3 ");
            sb.AppendLine($"{cmOutProcessingSeq3},   @processingSeq3 ");
            sb.AppendLine($",   @processingChildCD4 ");
            sb.AppendLine($"{cmOutProcessingSeq4},   @processingSeq4 ");
            sb.AppendLine($",   @processingChildCD5 ");
            sb.AppendLine($"{cmOutProcessingSeq5},   @processingSeq5 ");
            sb.AppendLine($",   @processingChildCDs ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($"); ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@shohinID", addEntity.ShohinID));
                    cmd.Parameters.Add(new MySqlParameter("@parentCD", addEntity.ParentCD));
                    cmd.Parameters.Add(new MySqlParameter("@parentSeq", addEntity.ParentSeq));
                    cmd.Parameters.Add(new MySqlParameter("@childCD", addEntity.ChildCD));
                    cmd.Parameters.Add(new MySqlParameter("@childSeq", addEntity.ChildSeq));
                    cmd.Parameters.Add(new MySqlParameter("@sizeCD", addEntity.SizeCD));
                    cmd.Parameters.Add(new MySqlParameter("@shohinCD", addEntity.ShohinCD));
                    cmd.Parameters.Add(new MySqlParameter("@size1", addEntity.Size1));
                    cmd.Parameters.Add(new MySqlParameter("@size2", addEntity.Size2));
                    cmd.Parameters.Add(new MySqlParameter("@size3", addEntity.Size3));
                    cmd.Parameters.Add(new MySqlParameter("@size4", addEntity.Size4));
                    cmd.Parameters.Add(new MySqlParameter("@shohinSize", addEntity.ShohinSize));
                    cmd.Parameters.Add(new MySqlParameter("@taishoYM", addEntity.TaishoYM));
                    cmd.Parameters.Add(new MySqlParameter("@price", addEntity.Price));
                    cmd.Parameters.Add(new MySqlParameter("@shikiriRate", addEntity.ShikiriRate));
                    cmd.Parameters.Add(new MySqlParameter("@riekiRate", addEntity.RiekiRate));
                    cmd.Parameters.Add(new MySqlParameter("@shikiri", addEntity.Shikiri));
                    cmd.Parameters.Add(new MySqlParameter("@weldingSize1", addEntity.WeldingSize1));
                    cmd.Parameters.Add(new MySqlParameter("@weldingSize2", addEntity.WeldingSize2));
                    cmd.Parameters.Add(new MySqlParameter("@screwSize1", addEntity.ScrewSize1));
                    cmd.Parameters.Add(new MySqlParameter("@screwSize2", addEntity.ScrewSize2));
                    cmd.Parameters.Add(new MySqlParameter("@screwSize3", addEntity.ScrewSize3));
                    cmd.Parameters.Add(new MySqlParameter("@saddleSize", addEntity.SaddleSize));
                    cmd.Parameters.Add(new MySqlParameter("@gbSize", addEntity.GBSize));
                    cmd.Parameters.Add(new MySqlParameter("@inchDia", addEntity.InchDia));
                    cmd.Parameters.Add(new MySqlParameter("@weldingCount1", addEntity.WeldingCount1));
                    cmd.Parameters.Add(new MySqlParameter("@weldingCount2", addEntity.WeldingCount2));
                    cmd.Parameters.Add(new MySqlParameter("@screwCount1", addEntity.ScrewCount1));
                    cmd.Parameters.Add(new MySqlParameter("@screwCount2", addEntity.ScrewCount2));
                    cmd.Parameters.Add(new MySqlParameter("@screwCount3", addEntity.ScrewCount3));
                    cmd.Parameters.Add(new MySqlParameter("@saddleCount", addEntity.SaddleCount));
                    cmd.Parameters.Add(new MySqlParameter("@gbCount", addEntity.GBCount));
                    cmd.Parameters.Add(new MySqlParameter("@bdCount", addEntity.BDCount));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD1", addEntity.ProcessingChildCD1));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq1", addEntity.ProcessingSeq1));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD2", addEntity.ProcessingChildCD2));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq2", addEntity.ProcessingSeq2));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD3", addEntity.ProcessingChildCD3));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq3", addEntity.ProcessingSeq3));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD4", addEntity.ProcessingChildCD4));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq4", addEntity.ProcessingSeq4));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD5", addEntity.ProcessingChildCD5));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq5", addEntity.ProcessingSeq5));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCDs", addEntity.ProcessingChildCDs));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 商品マスタ更新
        /// </summary>
        /// <param name="updEntity"></param>
        internal void UpdMstShohin(MX03Shohin updEntity)
        {
            string cmOutWeldingSize1 = EstimaUtil.GetCmout(updEntity.WeldingSize1 == null);
            string cmOutWeldingSize2 = EstimaUtil.GetCmout(updEntity.WeldingSize2 == null);
            string cmOutscrewSize1 = EstimaUtil.GetCmout(updEntity.ScrewSize1 == null);
            string cmOutscrewSize2 = EstimaUtil.GetCmout(updEntity.ScrewSize2 == null);
            string cmOutscrewSize3 = EstimaUtil.GetCmout(updEntity.ScrewSize3 == null);
            string cmOutSaddleSize = EstimaUtil.GetCmout(updEntity.SaddleSize == null);
            string cmOutGBSize = EstimaUtil.GetCmout(updEntity.GBSize == null);
            string cmOutInchDia = EstimaUtil.GetCmout(updEntity.InchDia == null);

            string cmOutWeldingCount1 = EstimaUtil.GetCmout(updEntity.WeldingCount1 == null);
            string cmOutWeldingCount2 = EstimaUtil.GetCmout(updEntity.WeldingCount2 == null);
            string cmOutscrewCount1 = EstimaUtil.GetCmout(updEntity.ScrewCount1 == null);
            string cmOutscrewCount2 = EstimaUtil.GetCmout(updEntity.ScrewCount2 == null);
            string cmOutscrewCount3 = EstimaUtil.GetCmout(updEntity.ScrewCount3 == null);
            string cmOutSaddleCount = EstimaUtil.GetCmout(updEntity.SaddleCount == null);
            string cmOutGBCount = EstimaUtil.GetCmout(updEntity.GBCount == null);
            string cmOutBDCount = EstimaUtil.GetCmout(updEntity.BDCount == null);

            string cmOutProcessingSeq1 = EstimaUtil.GetCmout(updEntity.ProcessingSeq1 == null);
            string cmOutProcessingSeq2 = EstimaUtil.GetCmout(updEntity.ProcessingSeq2 == null);
            string cmOutProcessingSeq3 = EstimaUtil.GetCmout(updEntity.ProcessingSeq3 == null);
            string cmOutProcessingSeq4 = EstimaUtil.GetCmout(updEntity.ProcessingSeq4 == null);
            string cmOutProcessingSeq5 = EstimaUtil.GetCmout(updEntity.ProcessingSeq5 == null);

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"UPDATE ");
            sb.AppendLine($"    MX03Shohin ");
            sb.AppendLine($"SET ");
            sb.AppendLine($"    ParentCD           = @parentCD ");
            sb.AppendLine($",   ParentSeq          = @parentSeq ");
            sb.AppendLine($",   ChildCD            = @childCD ");
            sb.AppendLine($",   ChildSeq           = @childSeq ");
            sb.AppendLine($",   SizeCD             = @sizeCD ");
            sb.AppendLine($",   ShohinCD           = @shohinCD ");
            sb.AppendLine($",   Size1              = @size1 ");
            sb.AppendLine($",   Size2              = @size2 ");
            sb.AppendLine($",   Size3              = @size3 ");
            sb.AppendLine($",   Size4              = @size4 ");
            sb.AppendLine($",   ShohinSize         = @shohinSize ");
            sb.AppendLine($",   Price              = @price ");
            sb.AppendLine($",   ShikiriRate        = @shikiriRate ");
            sb.AppendLine($",   RiekiRate          = @riekiRate ");
            sb.AppendLine($",   Shikiri            = @shikiri ");
            sb.AppendLine($",   TaishoYM           = @taishoYM ");
            sb.AppendLine($"{cmOutWeldingSize1 },   WeldingSize1       = @weldingSize1 ");
            sb.AppendLine($"{cmOutWeldingSize2 },   WeldingSize2       = @weldingSize2 ");
            sb.AppendLine($"{cmOutscrewSize1   },   ScrewSize1         = @screwSize1 ");
            sb.AppendLine($"{cmOutscrewSize2   },   ScrewSize2         = @screwSize2 ");
            sb.AppendLine($"{cmOutscrewSize3   },   ScrewSize3         = @screwSize1 ");
            sb.AppendLine($"{cmOutSaddleSize   },   SaddleSize         = @saddleSize ");
            sb.AppendLine($"{cmOutGBSize       },   GBSize             = @gbSize ");
            sb.AppendLine($"{cmOutInchDia      },   InchDia            = @inchDia ");
            sb.AppendLine($"{cmOutWeldingCount1},   WeldingCount1      = @weldingCount1 ");
            sb.AppendLine($"{cmOutWeldingCount2},   WeldingCount2      = @weldingCount2 ");
            sb.AppendLine($"{cmOutscrewCount1  },   ScrewCount1        = @screwCount1 ");
            sb.AppendLine($"{cmOutscrewCount2  },   ScrewCount2        = @screwCount2 ");
            sb.AppendLine($"{cmOutscrewCount3  },   ScrewCount3        = @screwCount1 ");
            sb.AppendLine($"{cmOutSaddleCount  },   SaddleCount        = @saddleCount ");
            sb.AppendLine($"{cmOutGBCount      },   GBCount            = @gbCount ");
            sb.AppendLine($"{cmOutBDCount      },   BDCount            = @bdCount ");
            sb.AppendLine($",   ProcessingChildCD1 = @processingChildCD1 ");
            sb.AppendLine($"{cmOutProcessingSeq1},   ProcessingSeq1 = @processingSeq1 ");
            sb.AppendLine($",   ProcessingChildCD2 = @processingChildCD2 ");
            sb.AppendLine($"{cmOutProcessingSeq2},   ProcessingSeq2 = @processingSeq2 ");
            sb.AppendLine($",   ProcessingChildCD3 = @processingChildCD3 ");
            sb.AppendLine($"{cmOutProcessingSeq3},   ProcessingSeq3 = @processingSeq3 ");
            sb.AppendLine($",   ProcessingChildCD4 = @processingChildCD4 ");
            sb.AppendLine($"{cmOutProcessingSeq4},   ProcessingSeq4 = @processingSeq4 ");
            sb.AppendLine($",   ProcessingChildCD5 = @processingChildCD5 ");
            sb.AppendLine($"{cmOutProcessingSeq5},   ProcessingSeq5 = @processingSeq5 ");
            sb.AppendLine($",   ProcessingChildCDs = @processingChildCDs ");
            sb.AppendLine($",   UpdIPAddress       = '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   UpdHostName        = '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   UpdDate            = CURRENT_TIMESTAMP ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    ShohinID = @shohinID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@shohinID", updEntity.ShohinID));
                    cmd.Parameters.Add(new MySqlParameter("@parentCD", updEntity.ParentCD));
                    cmd.Parameters.Add(new MySqlParameter("@parentSeq", updEntity.ParentSeq));
                    cmd.Parameters.Add(new MySqlParameter("@childCD", updEntity.ChildCD));
                    cmd.Parameters.Add(new MySqlParameter("@childSeq", updEntity.ChildSeq));
                    cmd.Parameters.Add(new MySqlParameter("@sizeCD", updEntity.SizeCD));
                    cmd.Parameters.Add(new MySqlParameter("@shohinCD", updEntity.ShohinCD));
                    cmd.Parameters.Add(new MySqlParameter("@size1", updEntity.Size1));
                    cmd.Parameters.Add(new MySqlParameter("@size2", updEntity.Size2));
                    cmd.Parameters.Add(new MySqlParameter("@size3", updEntity.Size3));
                    cmd.Parameters.Add(new MySqlParameter("@size4", updEntity.Size4));
                    cmd.Parameters.Add(new MySqlParameter("@shohinSize", updEntity.ShohinSize));
                    cmd.Parameters.Add(new MySqlParameter("@taishoYM", updEntity.TaishoYM));
                    cmd.Parameters.Add(new MySqlParameter("@price", updEntity.Price));
                    cmd.Parameters.Add(new MySqlParameter("@shikiriRate", updEntity.ShikiriRate));
                    cmd.Parameters.Add(new MySqlParameter("@riekiRate", updEntity.RiekiRate));
                    cmd.Parameters.Add(new MySqlParameter("@shikiri", updEntity.Shikiri));
                    cmd.Parameters.Add(new MySqlParameter("@weldingSize1", updEntity.WeldingSize1));
                    cmd.Parameters.Add(new MySqlParameter("@weldingSize2", updEntity.WeldingSize2));
                    cmd.Parameters.Add(new MySqlParameter("@screwSize1", updEntity.ScrewSize1));
                    cmd.Parameters.Add(new MySqlParameter("@screwSize2", updEntity.ScrewSize2));
                    cmd.Parameters.Add(new MySqlParameter("@screwSize3", updEntity.ScrewSize3));
                    cmd.Parameters.Add(new MySqlParameter("@saddleSize", updEntity.SaddleSize));
                    cmd.Parameters.Add(new MySqlParameter("@gbSize", updEntity.GBSize));
                    cmd.Parameters.Add(new MySqlParameter("@inchDia", updEntity.InchDia));
                    cmd.Parameters.Add(new MySqlParameter("@weldingCount1", updEntity.WeldingCount1));
                    cmd.Parameters.Add(new MySqlParameter("@weldingCount2", updEntity.WeldingCount2));
                    cmd.Parameters.Add(new MySqlParameter("@screwCount1", updEntity.ScrewCount1));
                    cmd.Parameters.Add(new MySqlParameter("@screwCount2", updEntity.ScrewCount2));
                    cmd.Parameters.Add(new MySqlParameter("@screwCount3", updEntity.ScrewCount3));
                    cmd.Parameters.Add(new MySqlParameter("@saddleCount", updEntity.SaddleCount));
                    cmd.Parameters.Add(new MySqlParameter("@gbCount", updEntity.GBCount));
                    cmd.Parameters.Add(new MySqlParameter("@bdCount", updEntity.BDCount));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD1", updEntity.ProcessingChildCD1));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq1", updEntity.ProcessingSeq1));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD2", updEntity.ProcessingChildCD2));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq2", updEntity.ProcessingSeq2));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD3", updEntity.ProcessingChildCD3));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq3", updEntity.ProcessingSeq3));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD4", updEntity.ProcessingChildCD4));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq4", updEntity.ProcessingSeq4));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD5", updEntity.ProcessingChildCD5));
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq5", updEntity.ProcessingSeq5));
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCDs", updEntity.ProcessingChildCDs));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 商品マスタ削除
        /// </summary>
        /// <param name="delEntity"></param>
        internal void DelMstShohin(MST0010_GridDto delEntity)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine("DELETE FROM ");
            sb.AppendLine("    MX03Shohin ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    ShohinID = @shohinID ");
            sb.AppendLine("; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@shohinID", delEntity.ShohinID) { DbType = DbType.Int32 });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 対象の商品コードを含む書類データが存在するか？
        /// </summary>
        /// <param name="shohinID"></param>
        /// <returns></returns>
        internal bool JudgeExistEstimaHavingShohin(string shohinID)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT COUNT(*) FROM VEX03EstimaData AS VEX03 ");
            sb.AppendLine($"INNER JOIN ");
            sb.AppendLine($"    MX03Shohin AS MX03 ");
            sb.AppendLine($"ON ");
            sb.AppendLine($"    VEX03.ShohinID = MX03.ShohinID ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    VEX03.ShohinID = @shohinID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@shohinID", shohinID) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return 0 < CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return false;
        }
    }
}
