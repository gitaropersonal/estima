﻿using Estima.Forms.Application.MST.MST0010.Business;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Util;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Estima.Forms.Application.MST.MST0010
{
    public partial class MST0010_MstShohin : Form
    {
        /// <summary>
        /// 選択されたマスタ情報
        /// </summary>
        public List<MX03Shohin> SelectedShohinInfos { get; set; }
        /// <summary>
        /// 空行数
        /// </summary>
        public int GridRowEmptyCount { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        /// <param name="detailKbn"></param>
        /// <param name="gridRowEmptyCount"></param>
        /// <param name="searchShohinCD"></param>
        public MST0010_MstShohin(
              LoginInfoDto loginInfo
            , Enums.ScreenModeMstShohin screenMode
            , Enums.DetailKbn detailKbn
            , int gridRowEmptyCount = 0
            , string searchShohinCD = ""
            )
        {
            this.InitializeComponent();
            try
            {
                this.SelectedShohinInfos = new List<MX03Shohin>();
                this.GridRowEmptyCount = gridRowEmptyCount;
                new MST0010_FormLogic(
                      this
                    , loginInfo
                    , screenMode
                    , detailKbn
                    , searchShohinCD
                );
            }
            catch(Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
