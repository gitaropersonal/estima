﻿using EstimaLib.Const;
using EstimaLib.Entity;
using EstimaLib.Util;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Data;
using System.Text;

namespace Estima.Forms.Application.MST.MST0030.Business
{
    internal class MST0030_Service
    {
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        internal BindingList<MST0030_GridDto> GetGridDatas(MST0030_GridDto con)
        {
            string cmOutChildCD = EstimaUtil.GetCmout(string.IsNullOrEmpty(con.ChildCD));
            string cmOutChildCDName = EstimaUtil.GetCmout(string.IsNullOrEmpty(con.ChildCDName));
            var ret = new BindingList<MST0030_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX02.ChildCD ");
            sb.AppendLine($",   MX02.ChildSeq ");
            sb.AppendLine($",   MX02.ChildCDName ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX04ChildCD AS MX02 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutChildCD    }AND MX02.ChildCD     LIKE @childCD ");
            sb.AppendLine($"{cmOutChildCDName}AND MX02.ChildCDName LIKE @childCDName ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    MX02.ChildCD ");
            sb.AppendLine($",   MX02.ChildCDName ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@childCD", "%" + con.ChildCD + "%");
                    cmd.Parameters.AddWithValue("@childCDName", "%" + con.ChildCDName + "%");
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new MST0030_GridDto();
                            dto.ChildCD = CommonUtil.TostringNullForbid(dr[nameof(dto.ChildCD)]);
                            dto.ChildSeq = CommonUtil.TostringNullForbid(dr[nameof(dto.ChildSeq)]);
                            dto.ChildCDName = CommonUtil.TostringNullForbid(dr[nameof(dto.ChildCDName)]);
                            dto.UsedByEstima = this.JudgeExistEstimaHavingChildCD(dto);
                            dto.Status = Enums.EditStatus.Show;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 次のSEQ取得
        /// </summary>
        /// <param name="childCD"></param>
        /// <returns></returns>
        private int GetNextSeq(string childCD)
        {
            var ret = 0;

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MAX(MX02.ChildSeq) ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX04ChildCD AS MX02 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"AND MX02.ChildCD = @childCD ");
            sb.AppendLine($"GROUP BY ");
            sb.AppendLine($"    MX02.ChildCD ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@childCD", childCD));
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret = CommonUtil.ToInteger(dr[0]) + 1;
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// データ登録
        /// </summary>
        /// <param name="addData"></param>
        internal void AddData(MX04ChildCD addData)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO MX04ChildCD( ");
            sb.AppendLine($"    ChildCD ");
            sb.AppendLine($",   ChildSeq ");
            sb.AppendLine($",   ChildCDName ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @childCD ");
            sb.AppendLine($",   @childSeq ");
            sb.AppendLine($",   @childCDName ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($") ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@childCD", addData.ChildCD));
                    cmd.Parameters.Add(new MySqlParameter("@childSeq", this.GetNextSeq(addData.ChildCD)));
                    cmd.Parameters.Add(new MySqlParameter("@childCDName", addData.ChildCDName));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// データ更新
        /// </summary>
        /// <param name="updData"></param>
        internal void UpdData(MX04ChildCD updData)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"UPDATE ");
            sb.AppendLine($"    MX04ChildCD ");
            sb.AppendLine($"SET ");
            sb.AppendLine($"    ChildCDName       = @childCDName ");
            sb.AppendLine($",   UpdIPAddress      = '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   UpdHostName       = '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   UpdDate           = CURRENT_TIMESTAMP ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    ChildCD  = @childCD ");
            sb.AppendLine($"AND ChildSeq = @childSeq ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@childCD", updData.ChildCD));
                    cmd.Parameters.Add(new MySqlParameter("@childSeq", updData.ChildSeq));
                    cmd.Parameters.Add(new MySqlParameter("@childCDName", updData.ChildCDName));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// データ削除
        /// </summary>
        /// <param name="delData"></param>
        internal void DelData(MST0030_GridDto delData)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM ");
            sb.AppendLine($"    MX04ChildCD ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    ChildCD  = @childCD ");
            sb.AppendLine($"AND ChildSeq = @childSeq ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@childCD", delData.ChildCD));
                    cmd.Parameters.Add(new MySqlParameter("@childSeq", delData.ChildSeq));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 対象の子商品コードを含む商品データが存在するか？
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        internal bool JudgeExistEstimaHavingChildCD(MST0030_GridDto dto)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"WITH ");
            sb.AppendLine($" Count1 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX04ChildCD AS MX04 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX03.ChildCD = MX04.ChildCD ");
            sb.AppendLine($"    AND MX03.ChildCD = @childCD ");
            sb.AppendLine($"    AND MX03.ChildSeq = @childSeq ");
            sb.AppendLine($") ");
            sb.AppendLine($",Count2 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX04ChildCD AS MX04 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX03.ProcessingChildCD1 = MX04.ChildCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq1     = MX04.ChildSeq ");
            sb.AppendLine($"    AND MX03.ProcessingChildCD1 = @childCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq1     = @childSeq ");
            sb.AppendLine($") ");
            sb.AppendLine($",Count3 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX04ChildCD AS MX04 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX03.ProcessingChildCD2 = MX04.ChildCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq2     = MX04.ChildSeq ");
            sb.AppendLine($"    AND MX03.ProcessingChildCD2 = @childCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq2     = @childSeq ");
            sb.AppendLine($") ");
            sb.AppendLine($",Count4 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX04ChildCD AS MX04 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX03.ProcessingChildCD3 = MX04.ChildCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq3     = MX04.ChildSeq ");
            sb.AppendLine($"    AND MX03.ProcessingChildCD3 = @childCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq3     = @childSeq ");
            sb.AppendLine($") ");
            sb.AppendLine($",Count5 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX04ChildCD AS MX04 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX03.ProcessingChildCD4 = MX04.ChildCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq4     = MX04.ChildSeq ");
            sb.AppendLine($"    AND MX03.ProcessingChildCD4 = @childCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq4     = @childSeq ");
            sb.AppendLine($") ");
            sb.AppendLine($",Count6 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX04ChildCD AS MX04 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX03.ProcessingChildCD5 = MX04.ChildCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq5     = MX04.ChildSeq ");
            sb.AppendLine($"    AND MX03.ProcessingChildCD5 = @childCD ");
            sb.AppendLine($"    AND MX03.ProcessingSeq5     = @childSeq ");
            sb.AppendLine($") ");
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    Count1.Count ");
            sb.AppendLine($"+   Count2.Count ");
            sb.AppendLine($"+   Count3.Count ");
            sb.AppendLine($"+   Count4.Count ");
            sb.AppendLine($"+   Count5.Count ");
            sb.AppendLine($"+   Count6.Count AS Count ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    Count1 ");
            sb.AppendLine($",   Count2 ");
            sb.AppendLine($",   Count3 ");
            sb.AppendLine($",   Count4 ");
            sb.AppendLine($",   Count5 ");
            sb.AppendLine($",   Count6 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@childCD", dto.ChildCD) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@childSeq", dto.ChildSeq) { DbType = DbType.Int32 });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return 0 < CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return false;
        }
    }
}
