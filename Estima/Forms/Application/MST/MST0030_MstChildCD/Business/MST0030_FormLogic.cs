﻿using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Application.MST.MST0030.Business
{
    internal class MST0030_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private MST0030_MstChildCD Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo;
        /// <summary>
        /// 専用サービス
        /// </summary>
        private readonly MST0030_Service PersonalService = new MST0030_Service();
        /// <summary>
        /// 削除されたデータ
        /// </summary>
        private List<MST0030_GridDto> DeletedDatas = new List<MST0030_GridDto>();
        /// <summary>
        /// 列ヘッダdto
        /// </summary>
        private static MST0030_GridDto CellHeaderNameDto = new MST0030_GridDto();
        private readonly string[] EditableGridCells = new string[]
        {
              nameof(CellHeaderNameDto.ChildCD)
            , nameof(CellHeaderNameDto.ChildCDName)
        };
        private readonly string[] ForbidUpdateGridCells = new string[]
        {
              nameof(CellHeaderNameDto.ChildCD)
            , nameof(CellHeaderNameDto.UsedByEstima)
        };
        /// <summary>
        /// 編集フラグ
        /// </summary>
        private bool EditedFlg = false;
        /// <summary>
        /// 画面モード
        /// </summary>
        private Enums.ScreenModeMstShohin ScreenMode = Enums.ScreenModeMstShohin.Screen;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        internal MST0030_FormLogic(
              MST0030_MstChildCD body
            , LoginInfoDto loginInfo
            , Enums.ScreenModeMstShohin screenMode
            )
        {
            this.Body = body;
            this.LoginInfo = loginInfo;
            this.ScreenMode = screenMode;
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += (s, e) =>
            {
                CommonUtil.BtnClearClickEvent(
                      new Action(() => this.Init())
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了処理
            this.Body.FormClosing += this.ClosingEvent;
            // 検索ボタン押下
            this.Body.btnSearch.Click += (s, e) =>
            {
                CommonUtil.EventWithValidateUpdExist(
                      new Action(() => this.BtnSearchClickEvent(s, e))
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 追加ボタン押下
            this.Body.btnAdd.Click += this.BtnAddClickEvent;
            // 更新ボタン押下
            this.Body.btnUpdate.Click += this.BtnUpdClickEvent;
            // グリッドCellEnter
            this.Body.dgvMaster.CellEnter += this.GridCelEnter;
            // グリッドRowPostPaint
            this.Body.dgvMaster.RowPostPaint += this.GridRowPostPaintEvent;
            // グリッドセルクリック
            this.Body.dgvMaster.CellClick += this.GridCellClick;
            // グリッドセルダブルクリック
            this.Body.dgvMaster.CellDoubleClick += this.GridCellDoubleClickEvent;
            // グリッドCellValueChanged
            this.Body.dgvMaster.CellValueChanged += this.GridCellValueChanged;
            // グリッドセルPainting
            this.Body.dgvMaster.CellPainting += this.GridCellPainting;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        this.Body.btnSearch.Focus();
                        this.Body.btnSearch.PerformClick();
                        break;
                    case Keys.I:
                        this.Body.btnAdd.Focus();
                        this.Body.btnAdd.PerformClick();
                        break;
                    case Keys.O:
                        this.Body.btnUpdate.Focus();
                        this.Body.btnUpdate.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// グリッドセルクリック
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellClick(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            if (dgv.ReadOnly)
            {
                return;
            }
            if (dgv.DataSource == null)
            {
                return;
            }
            var status = dgv.Rows[e.RowIndex].Cells[nameof(CellHeaderNameDto.Status)].Value.ToString();
            if (status == Enums.EditStatus.None.ToString())
            {
                return;
            }
            try
            {
                switch (dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningColumn.Name)
                {
                    case nameof(CellHeaderNameDto.BtnDelete):
                        // 削除ボタン押下
                        this.BtnRowDelClickEvent(s, e);
                        break;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// グリッドCellDoubleClick
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellDoubleClickEvent(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            var dgv = ((DataGridView)s);
            dgv.Rows[e.RowIndex].Selected = true;
            DataGridViewRow r = dgv.Rows[e.RowIndex];
            if (dgv.DataSource == null || string.IsNullOrEmpty(GridRowUtil<MST0030_GridDto>.GetRowModel(dgv.Rows[0]).ChildCD))
            {
                return;
            }
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeMstShohin.DialogSingleProcessing:
                    this.Body.SelectedDtoList.Add(GridRowUtil<MST0030_GridDto>.GetRowModel(r));
                    this.Body.DialogResult = DialogResult.OK;
                    this.Body.FormClosing -= this.ClosingEvent;
                    this.Body.Close();
                    break;
            }
        }
        /// <summary>
        /// グリッドセルPainting
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellPainting(
              object s
            , EventArgs e
            )
        {
            if (this.ScreenMode == Enums.ScreenModeMstShohin.Screen)
            {
                return;
            }
            var dgv = (DataGridView)s;
            foreach (DataGridViewColumn c in dgv.Columns)
            {
                if (c.Name == nameof(CellHeaderNameDto.ChkSelect))
                {
                    continue;
                }
                c.ReadOnly = true;
                c.DefaultCellStyle.BackColor = Color.White;
            }
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCelEnter(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            var tgtCol = this.Body.dgvMaster.Columns[e.ColumnIndex];
            if (tgtCol == null)
            {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName)
            {
                case nameof(CellHeaderNameDto.ChildCDName):
                    this.Body.dgvMaster.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    this.Body.dgvMaster.ImeMode = ImeMode.Disable;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            // イベント一旦削除
            var dgv = (DataGridView)s;
            dgv.CellValueChanged -= this.GridCellValueChanged;

            var dto = new MST0030_GridDto();
            DataGridViewRow r = dgv.Rows[e.RowIndex];
            DataGridViewCell c = r.Cells[e.ColumnIndex];
            string tgtCellName = c.OwningColumn.Name;
            var tgtCellVal = c.Value;

            if (this.EditableGridCells.Contains(tgtCellName))
            {
                // グリッドCellValueChanged（編集可能セル）
                this.EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex);

                // セル背景色
                c.Style.BackColor = Colors.BackColorCellEdited;

                // ステータス更新
                string identifyCol = CommonUtil.TostringNullForbid(r.Cells[nameof(dto.ChildCD)].Value);
                string status = CommonUtil.TostringNullForbid(r.Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(identifyCol)
                    && (status == Enums.EditStatus.Show.ToString() || status == Enums.EditStatus.Update.ToString()))
                {
                    r.Cells[nameof(dto.Status)].Value = Enums.EditStatus.Update;
                }
                else
                {
                    r.Cells[nameof(dto.Status)].Value = Enums.EditStatus.Insert;
                }
            }
            // イベント回復
            dgv.CellValueChanged += this.GridCellValueChanged;

            var dataSource = GridRowUtil<MST0030_GridDto>.GetAllRowsModel(this.Body.dgvMaster.DataSource);

            // 使用可否切り替え（更新ボタン）
            this.EditedFlg = true;
            this.SwitchEnabledChangeSearch();
        }
        /// <summary>
        /// グリッドRowPostPaint
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridRowPostPaintEvent(
              object s
            , DataGridViewRowPostPaintEventArgs e
            )
        {
            GridRowUtil<MST0030_GridDto>.SetRowNum(s, e);
        }
        /// <summary>
        /// 行削除ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnRowDelClickEvent(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            try
            {
                // 行データ取得
                var dgv = (DataGridView)s;
                DataGridViewRow r = dgv.Rows[e.RowIndex];
                var rowDto = GridRowUtil<MST0030_GridDto>.GetRowModel(r);
                // Validate
                if (this.PersonalService.JudgeExistEstimaHavingChildCD(rowDto))
                {
                    CommonUtil.ShowErrorMsg(MST0030_Messages.MsgErrorExistEstimaHavingChildCD);
                    return;
                }
                // 削除行データを保存
                this.DeletedDatas.Add(rowDto);
                this.Body.dgvMaster.Rows.Remove(r);
                // 行削除によって行がなくなった場合の処理
                if (this.Body.dgvMaster.Rows.Count == 0)
                {
                    this.Body.dgvMaster.Rows.Clear();
                    this.Body.dgvMaster.DataSource = new BindingList<MST0030_GridDto>();
                }
                // 使用可否切替（検索ボタン押下時）
                this.EditedFlg = true;
                this.SwitchEnabledChangeSearch();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                DialogResult drConfirm;
                if (this.ScreenMode == Enums.ScreenModeMstShohin.Screen)
                {
                    drConfirm = CommonUtil.ShowInfoMsgOKCancel(MST0030_Messages.MsgAskFinish);
                    if (drConfirm != DialogResult.OK)
                    {
                        cancel = true;
                        return;
                    }
                }
                if (!this.ValidateUnUpdateDataExist())
                {
                    drConfirm = CommonUtil.ShowInfoExcramationOKCancel(MST0030_Messages.MsgAskExistUpdateDatas);
                    if (drConfirm != DialogResult.OK)
                    {
                        cancel = true;
                        return;
                    }
                }
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                DialogResult drConfirm;
                if (this.ScreenMode == Enums.ScreenModeMstShohin.Screen)
                {
                    drConfirm = CommonUtil.ShowInfoMsgOKCancel(MST0030_Messages.MsgAskFinish);
                    if (drConfirm != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                if (!this.ValidateUnUpdateDataExist())
                {
                    drConfirm = CommonUtil.ShowInfoExcramationOKCancel(MST0030_Messages.MsgAskExistUpdateDatas);
                    if (drConfirm != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSearchClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// 追加ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnAddClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                // グリッドデータ取得
                var datas = (BindingList<MST0030_GridDto>)this.Body.dgvMaster.DataSource;

                // 追加行作成
                var rowDto = new MST0030_GridDto()
                {
                    ChildCD = string.Empty,
                    ChildSeq = "0",
                    ChildCDName = string.Empty,
                    Status = Enums.EditStatus.Insert,
                };
                // 行追加
                bool dataSourceRefreshFlg = false;
                int addRowIdx = 0;
                if (datas == null)
                {
                    datas = new BindingList<MST0030_GridDto>();
                    addRowIdx = 0;
                    dataSourceRefreshFlg = true;
                }
                else
                {
                    // 先頭行が空行の場合、データソースのリフレッシュが必要
                    var topRowDto = GridRowUtil<MST0030_GridDto>.GetRowModel(this.Body.dgvMaster.Rows[0]);
                    dataSourceRefreshFlg = string.IsNullOrEmpty(topRowDto.ChildCD) && topRowDto.Status == Enums.EditStatus.None;
                }
                if (dataSourceRefreshFlg)
                {
                    // データソースリフレッシュする場合の処理
                    var newData = new BindingList<MST0030_GridDto>();
                    newData.Add(rowDto);
                    this.Body.dgvMaster.DataSource = newData;
                }
                else
                {
                    datas.Insert(addRowIdx, rowDto);
                    this.Body.dgvMaster.DataSource = datas;
                }
                // グリッド背景色をセット
                DataGridViewRow r = this.Body.dgvMaster.Rows[addRowIdx];
                var dto = GridRowUtil<MST0030_GridDto>.GetRowModel(r);
                foreach (DataGridViewCell c in r.Cells)
                {
                    if (this.EditableGridCells.Contains(c.OwningColumn.Name))
                    {
                        c.Style.BackColor = Colors.BackColorCellEdited;
                        c.ReadOnly = false;
                    }
                }
                // 行追加不可にする
                this.Body.dgvMaster.AllowUserToAddRows = false;
                this.Body.dgvMaster.Rows[addRowIdx].Cells[nameof(CellHeaderNameDto.ChildCD)].Style.BackColor = Colors.BackColorCellEdited;
                this.Body.dgvMaster.Rows[addRowIdx].Cells[nameof(CellHeaderNameDto.ChildCDName)].Style.BackColor = Colors.BackColorCellEdited;

                // 使用可否切替（検索ボタン押下時）
                this.EditedFlg = true;
                this.SwitchEnabledChangeSearch();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnUpdClickEvent(
              object s
            , EventArgs e
            )
        {
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeMstShohin.Screen:
                    this.BtnUpdClickEventUpdate(s, e);
                    break;
                case Enums.ScreenModeMstShohin.DialogSingleProcessing:
                    this.BtnUpdClickEventDialogSingle(s, e);
                    break;
                case Enums.ScreenModeMstShohin.DialogMultiProcessing:
                    this.BtnUpdClickEventDialogMulti(s, e);
                    break;
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント（通常更新処理）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnUpdClickEventUpdate(
              object s
            , EventArgs e
            )
        {
            var gridRowDtoList = GridRowUtil<MST0030_GridDto>.GetAllRowsModel(this.Body.dgvMaster.DataSource);

            // 更新時Validate
            if (!this.UpdValidate(gridRowDtoList))
            {
                return;
            }
            // 確認ダイアログ
            if (CommonUtil.ShowInfoMsgOKCancel(MST0030_Messages.MsgAskUpdate) != DialogResult.OK)
            {
                return;
            }
            // 更新処理
            var dataSource = GridRowUtil<MST0030_GridDto>.GetAllRowsModel(this.Body.dgvMaster.DataSource);
            bool success = true;
            CommonUtil.ShowProgressDialog(new Action(() =>
            {
                EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.Start);
                success = this.AddUpdDatas(dataSource);
                EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.End);
            }));
            if (!success)
            {
                return;
            }
            // 更新完了メッセージ
            CommonUtil.ShowInfoMsgOK(MST0030_Messages.MsgInfoFinishUpdate);

            // 再検索
            this.SearchLogic();
        }
        /// <summary>
        /// 更新ボタン押下イベント（単独選択ダイアログ）
        /// </summary>
        private void BtnUpdClickEventDialogSingle(
              object s
            , EventArgs e
            )
        {
            if (Body.dgvMaster.DataSource == null || this.Body.dgvMaster.SelectedRows == null || this.Body.dgvMaster.SelectedRows.Count == 0)
            {
                CommonUtil.ShowErrorMsg(MST0030_Messages.MsgErrorNotSelectedRow);
                return;
            }
            DataGridViewRow r = this.Body.dgvMaster.SelectedRows[0];
            this.Body.SelectedDtoList.Add(GridRowUtil<MST0030_GridDto>.GetRowModel(r));
            this.Body.DialogResult = DialogResult.OK;
            this.Body.FormClosing -= this.ClosingEvent;
            this.Body.Close();
        }
        /// <summary>
        /// 更新ボタン押下イベント（複数選択ダイアログ）
        /// </summary>
        private void BtnUpdClickEventDialogMulti(
              object s
            , EventArgs e
            )
        {
            var selectedRowDtoList = GridRowUtil<MST0030_GridDto>.GetAllRowsModel(this.Body.dgvMaster.DataSource).Where(n => n.ChkSelect).ToList();
            if (!selectedRowDtoList.Any())
            {
                CommonUtil.ShowErrorMsg(MST0030_Messages.MsgErrorNotSelectedRow);
                return;
            }
            this.Body.SelectedDtoList.AddRange(selectedRowDtoList);
            this.Body.DialogResult = DialogResult.OK;
            this.Body.FormClosing -= this.ClosingEvent;
            this.Body.Close();
        }
        #endregion

        #region Business
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool EditableCellValueChanged(
              object tgtCellVal
            , string tgtCellName
            , int rowIndex
            )
        {
            try
            {
                if (tgtCellVal == null)
                {
                    return false;
                }
                this.Body.dgvMaster.Rows[rowIndex].Cells[tgtCellName].Value = CommonUtil.TostringNullForbid(tgtCellVal);
                return true;
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                return false;
            }
        }
        /// <summary>
        /// 検索処理
        /// </summary>
        private void SearchLogic()
        {
            // グリッド初期化
            this.Body.dgvMaster.Rows.Clear();

            // 検索条件
            var con = new MST0030_GridDto()
            {
                ChildCD = this.Body.txtChildCD.Text,
                ChildCDName = this.Body.txtChildCDName.Text,
            };
            // グリッドデータ
            var bindingList = new BindingList<MST0030_GridDto>();
            CommonUtil.ShowProgressDialog((new Action(() =>
            {
                try
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.Start);
                    bindingList = this.PersonalService.GetGridDatas(con);
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.End);
                }
                catch (Exception ex)
                {
                    EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                    return;
                }
            })));
            if (!bindingList.Any())
            {
                CommonUtil.ShowErrorMsg(MST0030_Messages.MsgErrorNotExistDatas);
                return;
            }
            // グリッドデータセット
            this.Body.dgvMaster.DataSource = bindingList;
            this.Body.dgvMaster.AllowUserToAddRows = false;

            // グリッドセル背景色
            if (this.ScreenMode == Enums.ScreenModeMstShohin.Screen)
            {
                foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
                {
                    var rowDto = GridRowUtil<MST0030_GridDto>.GetRowModel(r);
                    if (rowDto.Status == Enums.EditStatus.None)
                    {
                        continue;
                    }
                    foreach (DataGridViewCell c in r.Cells)
                    {
                        if (ForbidUpdateGridCells.Contains(c.OwningColumn.Name))
                        {
                            c.Style.BackColor = Colors.BackColorBtnReadOnly;
                            c.ReadOnly = true;
                        }
                    }
                }
            }
            // 編集フラグ
            this.EditedFlg = false;
            this.SwitchEnabledChangeSearch();

            // フォーカス
            this.Body.dgvMaster.Focus();
        }
        /// <summary>
        /// DBへデータ登録・更新
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        private bool AddUpdDatas(List<MST0030_GridDto> gridRowDtoList)
        {
            try
            {
                // 削除用データ作成
                var delDatas = this.DeletedDatas.Where(n => n.Status != Enums.EditStatus.Insert).ToList();
                // 登録用データ作成
                var addDatas = this.CreateAddDatas(gridRowDtoList);
                // 更新用データ作成
                var updDatas = this.CreateUpdDatas(gridRowDtoList);
                // 更新処理
                delDatas.ForEach(n => this.PersonalService.DelData(n));
                updDatas.ForEach(n => this.PersonalService.UpdData(n));
                addDatas.ForEach(n => this.PersonalService.AddData(n));
                // 削除データクリア
                this.DeletedDatas.Clear();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX04ChildCD> CreateAddDatas(List<MST0030_GridDto> gridDtoList)
        {
            var ret = new List<MX04ChildCD>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => !string.IsNullOrEmpty(n.ChildCD) && n.Status == Enums.EditStatus.Insert).ToList())
            {
                // データ作成
                var entity = new MX04ChildCD()
                {
                    ChildCD = CommonUtil.TostringNullForbid(rowDto.ChildCD),
                    ChildSeq = CommonUtil.ToInteger(rowDto.ChildSeq),
                    ChildCDName = CommonUtil.TostringNullForbid(rowDto.ChildCDName),
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 更新用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<MX04ChildCD> CreateUpdDatas(List<MST0030_GridDto> gridDtoList)
        {
            var ret = new List<MX04ChildCD>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => !string.IsNullOrEmpty(n.ChildCD) && n.Status == Enums.EditStatus.Update).ToList())
            {
                // データ作成
                var entity = new MX04ChildCD()
                {
                    ChildCD = CommonUtil.TostringNullForbid(rowDto.ChildCD),
                    ChildSeq = CommonUtil.ToInteger(rowDto.ChildSeq),
                    ChildCDName = CommonUtil.TostringNullForbid(rowDto.ChildCDName),
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 使用可否切替（検索ボタン押下時）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void SwitchEnabledChangeSearch()
        {
            this.Body.btnUpdate.Enabled = (this.EditedFlg || this.ScreenMode != Enums.ScreenModeMstShohin.Screen);
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新されていない項目が存在する？
        /// </summary>
        /// <returns></returns>
        private bool ValidateUnUpdateDataExist()
        {
            if (this.ScreenMode != Enums.ScreenModeMstShohin.Screen)
            {
                return true;
            }
            foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
            {
                // 更新されていない行が含まれる場合
                var rowDto = GridRowUtil<MST0030_GridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.Insert || rowDto.Status == Enums.EditStatus.Update)
                {
                    return false;
                }
            }
            if (this.DeletedDatas.Any(n => n.Status == Enums.EditStatus.Show || n.Status == Enums.EditStatus.Update))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<MST0030_GridDto> gridRowDtoList)
        {
            // 更新対象件数チェック
            if (!gridRowDtoList.Any(n => n.Status == Enums.EditStatus.Insert || n.Status == Enums.EditStatus.Update)
                && !this.DeletedDatas.Any(n => n.Status != Enums.EditStatus.Insert))
            {
                CommonUtil.ShowErrorMsg(MST0030_Messages.MsgErrorNotExistDatas);
                return false;
            }
            // グリッドセル空文字チェック
            if (!this.ValidateEmpty())
            {
                return false;
            }
            // 重複チェック
            var allGridDtoList = new List<MST0030_GridDto>();
            foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
            {
                var rowDto = GridRowUtil<MST0030_GridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.Insert || rowDto.Status == Enums.EditStatus.Update)
                {
                    gridRowDtoList.Add(rowDto);
                }
                allGridDtoList.Add(rowDto);
            }
            if (!this.ValidateDouple(allGridDtoList))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty()
        {
            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
            {
                var rowDto = GridRowUtil<MST0030_GridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.Insert && rowDto.Status != Enums.EditStatus.Update)
                {
                    continue;
                }
                if (string.IsNullOrEmpty(rowDto.ChildCD))
                {
                    // コード
                    headerTxt = this.Body.dgvMaster.Columns[nameof(rowDto.ChildCD)].HeaderText;
                    if (!errColNames.Contains(headerTxt))
                    {
                        errColNames.Add(headerTxt);
                    }
                    r.Cells[nameof(rowDto.ChildCD)].Style.BackColor = Colors.BackColorCellError;
                    result = false;
                }
                if (string.IsNullOrEmpty(rowDto.ChildCDName))
                {
                    // 名称
                    headerTxt = this.Body.dgvMaster.Columns[nameof(rowDto.ChildCDName)].HeaderText;
                    if (!errColNames.Contains(headerTxt))
                    {
                        errColNames.Add(headerTxt);
                    }
                    r.Cells[nameof(rowDto.ChildCDName)].Style.BackColor = Colors.BackColorCellError;
                    result = false;
                }
            }
            if (!result)
            {
                string msg = string.Format(MST0030_Messages.MsgErrorMissEmpty, string.Join("、", errColNames));
                CommonUtil.ShowErrorMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateDouple(List<MST0030_GridDto> keyInfos)
        {
            // 検索データ絞り込み
            var oldDatas = this.PersonalService.GetGridDatas(new MST0030_GridDto()).ToList();
            string msg = string.Empty;
            bool resutlt = true;
            foreach (var keyInfo in keyInfos)
            {
                if (this.ValidateDoupleInsert(oldDatas, keyInfo) && this.ValidateDoupleUpdatet(keyInfos, keyInfo))
                {
                    continue;
                }
                // 背景色編集
                foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
                {
                    var rowDto = GridRowUtil<MST0030_GridDto>.GetRowModel(r);
                    if (rowDto.ChildCD == keyInfo.ChildCD)
                    {
                        r.Cells[nameof(rowDto.ChildCD)].Style.BackColor = Colors.BackColorCellError;
                        r.Cells[nameof(rowDto.ChildCDName)].Style.BackColor = Colors.BackColorCellError;
                    }
                }
                resutlt = false;
            }
            if (!resutlt)
            {
                // 重複キー
                var dto = new MST0030_GridDto();
                msg = this.Body.dgvMaster.Columns[nameof(dto.ChildCD)].HeaderText;
                msg = string.Concat(msg, "、", this.Body.dgvMaster.Columns[nameof(dto.ChildCDName)].HeaderText);
                CommonUtil.ShowErrorMsg(string.Format(MST0030_Messages.MsgErrorDouple, msg));
            }
            return resutlt;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDoupleInsert(
              List<MST0030_GridDto> oldDatas
            , MST0030_GridDto keyInfo
            )
        {
            if (keyInfo.Status == Enums.EditStatus.Insert
                    && oldDatas.Exists(n => n.ChildCD == keyInfo.ChildCD)
                    && oldDatas.Exists(n => n.ChildCDName == keyInfo.ChildCDName))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（更新）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDoupleUpdatet(
              List<MST0030_GridDto> keyInfos
            , MST0030_GridDto keyInfo
            )
        {
            if (1 < keyInfos.Where(
                    n => n.ChildCD == keyInfo.ChildCD
                &&  n.ChildCDName == keyInfo.ChildCDName
                ).ToList().Count)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            // 検索条件
            this.Body.txtChildCD.Text = string.Empty;
            this.Body.txtChildCDName.Text = string.Empty;

            // 削除データ
            this.DeletedDatas.Clear();

            // グリッド
            this.Body.dgvMaster.Rows.Clear();
            this.Body.dgvMaster.AllowUserToAddRows = false;

            // スクリーンモードに応じて処理分岐
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeMstShohin.DialogSingleProcessing:
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.ChkSelect)].Visible = false;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.BtnDelete)].Visible = false;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.UsedByEstima)].Visible = false;
                    this.Body.pnlBtnAdd.Visible = false;
                    this.Body.dgvMaster.MultiSelect = false;
                    this.Body.btnUpdate.Text = "OK（O）";
                    this.BtnSearchClickEvent(new object(), new EventArgs());
                    break;
                case Enums.ScreenModeMstShohin.DialogMultiProcessing:
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.ChkSelect)].Visible = true;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.BtnDelete)].Visible = false;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.UsedByEstima)].Visible = false;
                    this.Body.pnlBtnAdd.Visible = true;
                    this.Body.dgvMaster.MultiSelect = false;
                    this.Body.btnUpdate.Text = "OK（O）";
                    this.BtnSearchClickEvent(new object(), new EventArgs());
                    break;
                default:
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.ChkSelect)].Visible = false;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.BtnDelete)].Visible = true;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.UsedByEstima)].Visible = true;
                    this.Body.pnlBtnAdd.Visible = true;
                    break;
            }
            // 編集フラグ
            this.EditedFlg = false;
            this.SwitchEnabledChangeSearch();

            // フォーカス
            this.Body.txtChildCD.Focus();
        }
        #endregion
    }
}
