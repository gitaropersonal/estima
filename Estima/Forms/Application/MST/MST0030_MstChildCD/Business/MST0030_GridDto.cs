﻿using EstimaLib.Const;

namespace Estima.Forms.Application.MST.MST0030.Business
{
    public class MST0030_GridDto
    {
        public MST0030_GridDto()
        {
            this.ChkSelect = false;
            this.BtnDelete = "削除";
            this.Status = Enums.EditStatus.None;
        }
        public bool ChkSelect { get; set; }
        public string BtnDelete { get; set; }
        public string ChildCD { get; set; }
        public string ChildSeq { get; set; }
        public string ChildCDName { get; set; }
        public bool UsedByEstima { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
