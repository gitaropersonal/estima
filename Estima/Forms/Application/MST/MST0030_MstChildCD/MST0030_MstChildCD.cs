﻿using Estima.Forms.Application.MST.MST0030.Business;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Estima.Forms.Application.MST.MST0030
{
    public partial class MST0030_MstChildCD : Form
    {
        /// <summary>
        /// 選択されたグリッド行dto
        /// </summary>
        public List<MST0030_GridDto> SelectedDtoList = new List<MST0030_GridDto>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        public MST0030_MstChildCD(
              LoginInfoDto loginInfo
            , Enums.ScreenModeMstShohin screenMode = Enums.ScreenModeMstShohin.Screen
            )
        {
            this.InitializeComponent();
            this.SelectedDtoList = new List<MST0030_GridDto>();
            try
            {
                new MST0030_FormLogic(
                      this
                    , loginInfo
                    , screenMode
                );
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
