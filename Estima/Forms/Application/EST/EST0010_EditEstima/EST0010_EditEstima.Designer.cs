﻿namespace Estima.Forms.Application.EST.EST0010
{
    partial class EST0010_EditEstima
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnHakko = new System.Windows.Forms.Button();
            this.btnCSV = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.grpDetail = new System.Windows.Forms.GroupBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.dgvBodys = new System.Windows.Forms.DataGridView();
            this.BtnShow = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnDel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.MeisaiNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeisaiNoDisp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MeisaiType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShohinName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Suryo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlumbingKingaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HousingKingaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingKingaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kingaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SummarryRowFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ConsumptionRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InspectionRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ExpenceRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.grpCondition = new System.Windows.Forms.GroupBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.dgvConditions = new System.Windows.Forms.DataGridView();
            this.Condition_Con = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status_Con = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.btnTemplete = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pnlPostage = new System.Windows.Forms.Panel();
            this.rdoPreRequestPostage = new System.Windows.Forms.RadioButton();
            this.rdoActualPostage = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.numPostage = new System.Windows.Forms.NumericUpDown();
            this.panel17 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtSumPrice = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSumShikiri = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSumShikiriHousing = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSumShikiriProcessing = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblRateHousing = new System.Windows.Forms.Label();
            this.numRateHousing = new System.Windows.Forms.NumericUpDown();
            this.lblRatePlumbing = new System.Windows.Forms.Label();
            this.numRatePlumbing = new System.Windows.Forms.NumericUpDown();
            this.panel12 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtProcessingNum = new System.Windows.Forms.TextBox();
            this.lblEstimateNumPrefix = new System.Windows.Forms.Label();
            this.txtAddressTanto = new System.Windows.Forms.TextBox();
            this.txtAddressCompany = new System.Windows.Forms.TextBox();
            this.txtAddressToriName = new System.Windows.Forms.TextBox();
            this.dtpLimitDate = new System.Windows.Forms.DateTimePicker();
            this.lblLimitDate = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDeadLine = new System.Windows.Forms.TextBox();
            this.txtFieldRange = new System.Windows.Forms.TextBox();
            this.txtFieldName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbAddressOffice = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpHakkoDate = new System.Windows.Forms.DateTimePicker();
            this.lblHakkoDate = new System.Windows.Forms.Label();
            this.lblProcessingNum = new System.Windows.Forms.Label();
            this.lblOrderNum = new System.Windows.Forms.Label();
            this.txtOrderNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlFormat = new System.Windows.Forms.Panel();
            this.rdoSeikyu = new System.Windows.Forms.RadioButton();
            this.rdoEstima = new System.Windows.Forms.RadioButton();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.grpDetail.SuspendLayout();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBodys)).BeginInit();
            this.panel11.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel7.SuspendLayout();
            this.grpCondition.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConditions)).BeginInit();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel20.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.pnlPostage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPostage)).BeginInit();
            this.panel17.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel10.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRateHousing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRatePlumbing)).BeginInit();
            this.panel12.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlFormat.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 891);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1734, 70);
            this.panel3.TabIndex = 101;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(1509, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(225, 70);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(53, 9);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnHakko);
            this.panel4.Controls.Add(this.btnCSV);
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(792, 70);
            this.panel4.TabIndex = 110;
            // 
            // btnHakko
            // 
            this.btnHakko.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnHakko.Location = new System.Drawing.Point(295, 9);
            this.btnHakko.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnHakko.Name = "btnHakko";
            this.btnHakko.Size = new System.Drawing.Size(138, 52);
            this.btnHakko.TabIndex = 113;
            this.btnHakko.Text = "発行（H）";
            this.btnHakko.UseVisualStyleBackColor = true;
            // 
            // btnCSV
            // 
            this.btnCSV.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCSV.Location = new System.Drawing.Point(433, 9);
            this.btnCSV.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCSV.Name = "btnCSV";
            this.btnCSV.Size = new System.Drawing.Size(138, 52);
            this.btnCSV.TabIndex = 114;
            this.btnCSV.Text = "CSV出力（C）";
            this.btnCSV.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(157, 9);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(138, 52);
            this.btnUpdate.TabIndex = 112;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(19, 9);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(19, 12, 6, 12);
            this.panel1.Size = new System.Drawing.Size(1734, 891);
            this.panel1.TabIndex = 60;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Controls.Add(this.panel7);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(569, 12);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.panel8.Size = new System.Drawing.Size(1159, 867);
            this.panel8.TabIndex = 40;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.grpDetail);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(12, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.panel9.Size = new System.Drawing.Size(1135, 418);
            this.panel9.TabIndex = 50;
            // 
            // grpDetail
            // 
            this.grpDetail.Controls.Add(this.panel15);
            this.grpDetail.Controls.Add(this.panel11);
            this.grpDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpDetail.Location = new System.Drawing.Point(0, 0);
            this.grpDetail.Margin = new System.Windows.Forms.Padding(4);
            this.grpDetail.Name = "grpDetail";
            this.grpDetail.Padding = new System.Windows.Forms.Padding(4);
            this.grpDetail.Size = new System.Drawing.Size(1135, 406);
            this.grpDetail.TabIndex = 50;
            this.grpDetail.TabStop = false;
            this.grpDetail.Text = "明細一覧";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.dgvBodys);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(4, 95);
            this.panel15.Margin = new System.Windows.Forms.Padding(4);
            this.panel15.Name = "panel15";
            this.panel15.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.panel15.Size = new System.Drawing.Size(1127, 307);
            this.panel15.TabIndex = 53;
            // 
            // dgvBodys
            // 
            this.dgvBodys.AllowUserToAddRows = false;
            this.dgvBodys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBodys.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BtnShow,
            this.BtnDel,
            this.MeisaiNo,
            this.MeisaiNoDisp,
            this.MeisaiType,
            this.ShohinName,
            this.Suryo,
            this.PlumbingKingaku,
            this.HousingKingaku,
            this.ProcessingKingaku,
            this.Kingaku,
            this.SummarryRowFlg,
            this.ConsumptionRate,
            this.InspectionRate,
            this.ExpenceRate,
            this.Status});
            this.dgvBodys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBodys.Location = new System.Drawing.Point(12, 0);
            this.dgvBodys.Margin = new System.Windows.Forms.Padding(4);
            this.dgvBodys.Name = "dgvBodys";
            this.dgvBodys.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvBodys.RowTemplate.Height = 21;
            this.dgvBodys.Size = new System.Drawing.Size(1103, 307);
            this.dgvBodys.TabIndex = 55;
            // 
            // BtnShow
            // 
            this.BtnShow.DataPropertyName = "BtnShow";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnShow.DefaultCellStyle = dataGridViewCellStyle1;
            this.BtnShow.HeaderText = "";
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Width = 60;
            // 
            // BtnDel
            // 
            this.BtnDel.DataPropertyName = "BtnDel";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnDel.DefaultCellStyle = dataGridViewCellStyle2;
            this.BtnDel.HeaderText = "";
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Width = 60;
            // 
            // MeisaiNo
            // 
            this.MeisaiNo.DataPropertyName = "MeisaiNo";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightGray;
            this.MeisaiNo.DefaultCellStyle = dataGridViewCellStyle3;
            this.MeisaiNo.HeaderText = "明細№非表示";
            this.MeisaiNo.Name = "MeisaiNo";
            this.MeisaiNo.ReadOnly = true;
            this.MeisaiNo.Visible = false;
            this.MeisaiNo.Width = 70;
            // 
            // MeisaiNoDisp
            // 
            this.MeisaiNoDisp.DataPropertyName = "MeisaiNoDisp";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray;
            this.MeisaiNoDisp.DefaultCellStyle = dataGridViewCellStyle4;
            this.MeisaiNoDisp.HeaderText = "明細№";
            this.MeisaiNoDisp.Name = "MeisaiNoDisp";
            this.MeisaiNoDisp.ReadOnly = true;
            this.MeisaiNoDisp.Width = 70;
            // 
            // MeisaiType
            // 
            this.MeisaiType.DataPropertyName = "MeisaiType";
            this.MeisaiType.HeaderText = "明細種別";
            this.MeisaiType.Name = "MeisaiType";
            this.MeisaiType.Visible = false;
            this.MeisaiType.Width = 140;
            // 
            // ShohinName
            // 
            this.ShohinName.DataPropertyName = "ShohinName";
            this.ShohinName.HeaderText = "品名";
            this.ShohinName.Name = "ShohinName";
            this.ShohinName.Width = 310;
            // 
            // Suryo
            // 
            this.Suryo.DataPropertyName = "Suryo";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightGray;
            this.Suryo.DefaultCellStyle = dataGridViewCellStyle5;
            this.Suryo.HeaderText = "数量";
            this.Suryo.Name = "Suryo";
            this.Suryo.ReadOnly = true;
            this.Suryo.Width = 50;
            // 
            // PlumbingKingaku
            // 
            this.PlumbingKingaku.DataPropertyName = "PlumbingKingaku";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            this.PlumbingKingaku.DefaultCellStyle = dataGridViewCellStyle6;
            this.PlumbingKingaku.HeaderText = "小計(配管・継手)";
            this.PlumbingKingaku.Name = "PlumbingKingaku";
            this.PlumbingKingaku.ReadOnly = true;
            this.PlumbingKingaku.Width = 125;
            // 
            // HousingKingaku
            // 
            this.HousingKingaku.DataPropertyName = "HousingKingaku";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.LightGray;
            this.HousingKingaku.DefaultCellStyle = dataGridViewCellStyle7;
            this.HousingKingaku.HeaderText = "小計(ﾊｳｼﾞﾝｸﾞ)";
            this.HousingKingaku.Name = "HousingKingaku";
            this.HousingKingaku.ReadOnly = true;
            this.HousingKingaku.Width = 125;
            // 
            // ProcessingKingaku
            // 
            this.ProcessingKingaku.DataPropertyName = "ProcessingKingaku";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.LightGray;
            this.ProcessingKingaku.DefaultCellStyle = dataGridViewCellStyle8;
            this.ProcessingKingaku.HeaderText = "小計(加工費)";
            this.ProcessingKingaku.Name = "ProcessingKingaku";
            this.ProcessingKingaku.ReadOnly = true;
            this.ProcessingKingaku.Width = 125;
            // 
            // Kingaku
            // 
            this.Kingaku.DataPropertyName = "Kingaku";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightGray;
            this.Kingaku.DefaultCellStyle = dataGridViewCellStyle9;
            this.Kingaku.HeaderText = "合計金額";
            this.Kingaku.Name = "Kingaku";
            this.Kingaku.ReadOnly = true;
            this.Kingaku.Width = 125;
            // 
            // SummarryRowFlg
            // 
            this.SummarryRowFlg.DataPropertyName = "SummarryRowFlg";
            this.SummarryRowFlg.HeaderText = "集計行フラグ";
            this.SummarryRowFlg.Name = "SummarryRowFlg";
            this.SummarryRowFlg.Visible = false;
            // 
            // ConsumptionRate
            // 
            this.ConsumptionRate.DataPropertyName = "ConsumptionRate";
            this.ConsumptionRate.HeaderText = "ConsumptionRate";
            this.ConsumptionRate.Name = "ConsumptionRate";
            this.ConsumptionRate.Visible = false;
            // 
            // InspectionRate
            // 
            this.InspectionRate.DataPropertyName = "InspectionRate";
            this.InspectionRate.HeaderText = "InspectionRate";
            this.InspectionRate.Name = "InspectionRate";
            this.InspectionRate.Visible = false;
            // 
            // ExpenceRate
            // 
            this.ExpenceRate.DataPropertyName = "ExpenceRate";
            this.ExpenceRate.HeaderText = "ExpenceRate";
            this.ExpenceRate.Name = "ExpenceRate";
            this.ExpenceRate.Visible = false;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel14);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(4, 25);
            this.panel11.Margin = new System.Windows.Forms.Padding(4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1127, 70);
            this.panel11.TabIndex = 51;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnAdd);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Margin = new System.Windows.Forms.Padding(4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(192, 70);
            this.panel14.TabIndex = 52;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAdd.Location = new System.Drawing.Point(12, 8);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(138, 52);
            this.btnAdd.TabIndex = 51;
            this.btnAdd.Text = "明細追加(I)";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.grpCondition);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(12, 418);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.panel7.Size = new System.Drawing.Size(1135, 449);
            this.panel7.TabIndex = 60;
            // 
            // grpCondition
            // 
            this.grpCondition.Controls.Add(this.panel16);
            this.grpCondition.Controls.Add(this.panel18);
            this.grpCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCondition.Location = new System.Drawing.Point(0, 0);
            this.grpCondition.Margin = new System.Windows.Forms.Padding(4);
            this.grpCondition.Name = "grpCondition";
            this.grpCondition.Padding = new System.Windows.Forms.Padding(4);
            this.grpCondition.Size = new System.Drawing.Size(1135, 437);
            this.grpCondition.TabIndex = 60;
            this.grpCondition.TabStop = false;
            this.grpCondition.Text = "御見積条件";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.dgvConditions);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(4, 95);
            this.panel16.Margin = new System.Windows.Forms.Padding(4);
            this.panel16.Name = "panel16";
            this.panel16.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.panel16.Size = new System.Drawing.Size(1127, 338);
            this.panel16.TabIndex = 70;
            // 
            // dgvConditions
            // 
            this.dgvConditions.AllowUserToAddRows = false;
            this.dgvConditions.AllowUserToDeleteRows = false;
            this.dgvConditions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConditions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Condition_Con,
            this.Status_Con});
            this.dgvConditions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvConditions.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.dgvConditions.Location = new System.Drawing.Point(12, 0);
            this.dgvConditions.Margin = new System.Windows.Forms.Padding(4);
            this.dgvConditions.Name = "dgvConditions";
            this.dgvConditions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvConditions.RowTemplate.Height = 21;
            this.dgvConditions.Size = new System.Drawing.Size(1103, 338);
            this.dgvConditions.TabIndex = 70;
            // 
            // Condition_Con
            // 
            this.Condition_Con.DataPropertyName = "Condition";
            this.Condition_Con.HeaderText = "条件";
            this.Condition_Con.MaxInputLength = 80;
            this.Condition_Con.Name = "Condition_Con";
            this.Condition_Con.Width = 1000;
            // 
            // Status_Con
            // 
            this.Status_Con.DataPropertyName = "Status";
            this.Status_Con.HeaderText = "ステータス";
            this.Status_Con.Name = "Status_Con";
            this.Status_Con.Visible = false;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(4, 25);
            this.panel18.Margin = new System.Windows.Forms.Padding(4);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(1127, 70);
            this.panel18.TabIndex = 60;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.btnTemplete);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Margin = new System.Windows.Forms.Padding(4);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(192, 70);
            this.panel19.TabIndex = 60;
            // 
            // btnTemplete
            // 
            this.btnTemplete.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTemplete.Location = new System.Drawing.Point(12, 9);
            this.btnTemplete.Margin = new System.Windows.Forms.Padding(4);
            this.btnTemplete.Name = "btnTemplete";
            this.btnTemplete.Size = new System.Drawing.Size(138, 52);
            this.btnTemplete.TabIndex = 60;
            this.btnTemplete.Text = "テンプレート(T)";
            this.btnTemplete.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel20);
            this.panel6.Controls.Add(this.panel17);
            this.panel6.Controls.Add(this.panel10);
            this.panel6.Controls.Add(this.panel12);
            this.panel6.Controls.Add(this.panel2);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(19, 12);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 0, 0, 6);
            this.panel6.Size = new System.Drawing.Size(550, 867);
            this.panel6.TabIndex = 0;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.groupBox5);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 763);
            this.panel20.Margin = new System.Windows.Forms.Padding(4);
            this.panel20.Name = "panel20";
            this.panel20.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.panel20.Size = new System.Drawing.Size(550, 115);
            this.panel20.TabIndex = 40;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.pnlPostage);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.numPostage);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(550, 103);
            this.groupBox5.TabIndex = 40;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "運搬交通費";
            // 
            // pnlPostage
            // 
            this.pnlPostage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPostage.Controls.Add(this.rdoPreRequestPostage);
            this.pnlPostage.Controls.Add(this.rdoActualPostage);
            this.pnlPostage.Location = new System.Drawing.Point(182, 23);
            this.pnlPostage.Margin = new System.Windows.Forms.Padding(4);
            this.pnlPostage.Name = "pnlPostage";
            this.pnlPostage.Size = new System.Drawing.Size(218, 35);
            this.pnlPostage.TabIndex = 41;
            // 
            // rdoPreRequestPostage
            // 
            this.rdoPreRequestPostage.AutoSize = true;
            this.rdoPreRequestPostage.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoPreRequestPostage.Location = new System.Drawing.Point(115, 0);
            this.rdoPreRequestPostage.Margin = new System.Windows.Forms.Padding(4);
            this.rdoPreRequestPostage.Name = "rdoPreRequestPostage";
            this.rdoPreRequestPostage.Size = new System.Drawing.Size(91, 33);
            this.rdoPreRequestPostage.TabIndex = 1;
            this.rdoPreRequestPostage.TabStop = true;
            this.rdoPreRequestPostage.Text = "事前精算";
            this.rdoPreRequestPostage.UseVisualStyleBackColor = true;
            // 
            // rdoActualPostage
            // 
            this.rdoActualPostage.AutoSize = true;
            this.rdoActualPostage.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoActualPostage.Location = new System.Drawing.Point(0, 0);
            this.rdoActualPostage.Margin = new System.Windows.Forms.Padding(4);
            this.rdoActualPostage.Name = "rdoActualPostage";
            this.rdoActualPostage.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.rdoActualPostage.Size = new System.Drawing.Size(115, 33);
            this.rdoActualPostage.TabIndex = 0;
            this.rdoActualPostage.TabStop = true;
            this.rdoActualPostage.Text = "実費精算";
            this.rdoActualPostage.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 30);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 20);
            this.label16.TabIndex = 18;
            this.label16.Text = "精算";
            // 
            // numPostage
            // 
            this.numPostage.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numPostage.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numPostage.Location = new System.Drawing.Point(182, 59);
            this.numPostage.Margin = new System.Windows.Forms.Padding(4);
            this.numPostage.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numPostage.Name = "numPostage";
            this.numPostage.Size = new System.Drawing.Size(119, 28);
            this.numPostage.TabIndex = 42;
            this.numPostage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numPostage.ThousandsSeparator = true;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.groupBox4);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 591);
            this.panel17.Margin = new System.Windows.Forms.Padding(4);
            this.panel17.Name = "panel17";
            this.panel17.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.panel17.Size = new System.Drawing.Size(550, 172);
            this.panel17.TabIndex = 30;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtSumPrice);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtSumShikiri);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.txtSumShikiriHousing);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txtSumShikiriProcessing);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(550, 160);
            this.groupBox4.TabIndex = 30;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "合計金額";
            // 
            // txtSumPrice
            // 
            this.txtSumPrice.Location = new System.Drawing.Point(182, 29);
            this.txtSumPrice.Margin = new System.Windows.Forms.Padding(4);
            this.txtSumPrice.MaxLength = 12;
            this.txtSumPrice.Name = "txtSumPrice";
            this.txtSumPrice.ReadOnly = true;
            this.txtSumPrice.Size = new System.Drawing.Size(155, 28);
            this.txtSumPrice.TabIndex = 31;
            this.txtSumPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 31);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 20);
            this.label14.TabIndex = 35;
            this.label14.Text = "定価合計";
            // 
            // txtSumShikiri
            // 
            this.txtSumShikiri.Location = new System.Drawing.Point(182, 117);
            this.txtSumShikiri.Margin = new System.Windows.Forms.Padding(4);
            this.txtSumShikiri.MaxLength = 12;
            this.txtSumShikiri.Name = "txtSumShikiri";
            this.txtSumShikiri.ReadOnly = true;
            this.txtSumShikiri.Size = new System.Drawing.Size(155, 28);
            this.txtSumShikiri.TabIndex = 34;
            this.txtSumShikiri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 120);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 20);
            this.label15.TabIndex = 24;
            this.label15.Text = "仕切合計";
            // 
            // txtSumShikiriHousing
            // 
            this.txtSumShikiriHousing.Location = new System.Drawing.Point(182, 88);
            this.txtSumShikiriHousing.Margin = new System.Windows.Forms.Padding(4);
            this.txtSumShikiriHousing.MaxLength = 12;
            this.txtSumShikiriHousing.Name = "txtSumShikiriHousing";
            this.txtSumShikiriHousing.ReadOnly = true;
            this.txtSumShikiriHousing.Size = new System.Drawing.Size(155, 28);
            this.txtSumShikiriHousing.TabIndex = 33;
            this.txtSumShikiriHousing.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 90);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(135, 20);
            this.label10.TabIndex = 20;
            this.label10.Text = "仕切（ハウジング）";
            // 
            // txtSumShikiriProcessing
            // 
            this.txtSumShikiriProcessing.Location = new System.Drawing.Point(182, 58);
            this.txtSumShikiriProcessing.Margin = new System.Windows.Forms.Padding(4);
            this.txtSumShikiriProcessing.MaxLength = 12;
            this.txtSumShikiriProcessing.Name = "txtSumShikiriProcessing";
            this.txtSumShikiriProcessing.ReadOnly = true;
            this.txtSumShikiriProcessing.Size = new System.Drawing.Size(155, 28);
            this.txtSumShikiriProcessing.TabIndex = 32;
            this.txtSumShikiriProcessing.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(25, 61);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(161, 20);
            this.label13.TabIndex = 18;
            this.label13.Text = "仕切（配管・加工費）";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.groupBox3);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 484);
            this.panel10.Margin = new System.Windows.Forms.Padding(4);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.panel10.Size = new System.Drawing.Size(550, 107);
            this.panel10.TabIndex = 20;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lblRateHousing);
            this.groupBox3.Controls.Add(this.numRateHousing);
            this.groupBox3.Controls.Add(this.lblRatePlumbing);
            this.groupBox3.Controls.Add(this.numRatePlumbing);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(550, 95);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "掛率";
            // 
            // lblRateHousing
            // 
            this.lblRateHousing.AutoSize = true;
            this.lblRateHousing.Location = new System.Drawing.Point(25, 58);
            this.lblRateHousing.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRateHousing.Name = "lblRateHousing";
            this.lblRateHousing.Size = new System.Drawing.Size(71, 20);
            this.lblRateHousing.TabIndex = 21;
            this.lblRateHousing.Text = "ハウジング";
            // 
            // numRateHousing
            // 
            this.numRateHousing.DecimalPlaces = 2;
            this.numRateHousing.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numRateHousing.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numRateHousing.Location = new System.Drawing.Point(182, 55);
            this.numRateHousing.Margin = new System.Windows.Forms.Padding(4);
            this.numRateHousing.Name = "numRateHousing";
            this.numRateHousing.Size = new System.Drawing.Size(119, 28);
            this.numRateHousing.TabIndex = 22;
            this.numRateHousing.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblRatePlumbing
            // 
            this.lblRatePlumbing.AutoSize = true;
            this.lblRatePlumbing.Location = new System.Drawing.Point(25, 28);
            this.lblRatePlumbing.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRatePlumbing.Name = "lblRatePlumbing";
            this.lblRatePlumbing.Size = new System.Drawing.Size(57, 20);
            this.lblRatePlumbing.TabIndex = 18;
            this.lblRatePlumbing.Text = "加工管";
            // 
            // numRatePlumbing
            // 
            this.numRatePlumbing.DecimalPlaces = 2;
            this.numRatePlumbing.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numRatePlumbing.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numRatePlumbing.Location = new System.Drawing.Point(182, 26);
            this.numRatePlumbing.Margin = new System.Windows.Forms.Padding(4);
            this.numRatePlumbing.Name = "numRatePlumbing";
            this.numRatePlumbing.Size = new System.Drawing.Size(119, 28);
            this.numRatePlumbing.TabIndex = 20;
            this.numRatePlumbing.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.groupBox2);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 84);
            this.panel12.Margin = new System.Windows.Forms.Padding(4);
            this.panel12.Name = "panel12";
            this.panel12.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.panel12.Size = new System.Drawing.Size(550, 400);
            this.panel12.TabIndex = 10;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.panel13);
            this.groupBox2.Controls.Add(this.txtAddressTanto);
            this.groupBox2.Controls.Add(this.txtAddressCompany);
            this.groupBox2.Controls.Add(this.txtAddressToriName);
            this.groupBox2.Controls.Add(this.dtpLimitDate);
            this.groupBox2.Controls.Add(this.lblLimitDate);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txtDeadLine);
            this.groupBox2.Controls.Add(this.txtFieldRange);
            this.groupBox2.Controls.Add(this.txtFieldName);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbAddressOffice);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.dtpHakkoDate);
            this.groupBox2.Controls.Add(this.lblHakkoDate);
            this.groupBox2.Controls.Add(this.lblProcessingNum);
            this.groupBox2.Controls.Add(this.lblOrderNum);
            this.groupBox2.Controls.Add(this.txtOrderNum);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(550, 388);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ヘッダー情報";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(471, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "御中";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(487, 237);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 20);
            this.label3.TabIndex = 25;
            this.label3.Text = "様";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(487, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 20);
            this.label2.TabIndex = 24;
            this.label2.Text = "様";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.txtProcessingNum);
            this.panel13.Controls.Add(this.lblEstimateNumPrefix);
            this.panel13.Location = new System.Drawing.Point(182, 60);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(193, 29);
            this.panel13.TabIndex = 12;
            // 
            // txtProcessingNum
            // 
            this.txtProcessingNum.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtProcessingNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtProcessingNum.Location = new System.Drawing.Point(33, 0);
            this.txtProcessingNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtProcessingNum.MaxLength = 7;
            this.txtProcessingNum.Name = "txtProcessingNum";
            this.txtProcessingNum.Size = new System.Drawing.Size(135, 28);
            this.txtProcessingNum.TabIndex = 25;
            // 
            // lblEstimateNumPrefix
            // 
            this.lblEstimateNumPrefix.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblEstimateNumPrefix.Location = new System.Drawing.Point(0, 0);
            this.lblEstimateNumPrefix.Name = "lblEstimateNumPrefix";
            this.lblEstimateNumPrefix.Size = new System.Drawing.Size(33, 29);
            this.lblEstimateNumPrefix.TabIndex = 24;
            this.lblEstimateNumPrefix.Text = "MK";
            this.lblEstimateNumPrefix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAddressTanto
            // 
            this.txtAddressTanto.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtAddressTanto.Location = new System.Drawing.Point(182, 205);
            this.txtAddressTanto.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddressTanto.MaxLength = 30;
            this.txtAddressTanto.Name = "txtAddressTanto";
            this.txtAddressTanto.Size = new System.Drawing.Size(305, 28);
            this.txtAddressTanto.TabIndex = 17;
            // 
            // txtAddressCompany
            // 
            this.txtAddressCompany.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtAddressCompany.Location = new System.Drawing.Point(182, 147);
            this.txtAddressCompany.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddressCompany.MaxLength = 30;
            this.txtAddressCompany.Name = "txtAddressCompany";
            this.txtAddressCompany.Size = new System.Drawing.Size(286, 28);
            this.txtAddressCompany.TabIndex = 15;
            // 
            // txtAddressToriName
            // 
            this.txtAddressToriName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtAddressToriName.Location = new System.Drawing.Point(182, 234);
            this.txtAddressToriName.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddressToriName.MaxLength = 30;
            this.txtAddressToriName.Name = "txtAddressToriName";
            this.txtAddressToriName.Size = new System.Drawing.Size(305, 28);
            this.txtAddressToriName.TabIndex = 18;
            // 
            // dtpLimitDate
            // 
            this.dtpLimitDate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpLimitDate.Location = new System.Drawing.Point(182, 118);
            this.dtpLimitDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpLimitDate.Name = "dtpLimitDate";
            this.dtpLimitDate.Size = new System.Drawing.Size(192, 28);
            this.dtpLimitDate.TabIndex = 14;
            // 
            // lblLimitDate
            // 
            this.lblLimitDate.AutoSize = true;
            this.lblLimitDate.Location = new System.Drawing.Point(25, 123);
            this.lblLimitDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLimitDate.Name = "lblLimitDate";
            this.lblLimitDate.Size = new System.Drawing.Size(73, 20);
            this.lblLimitDate.TabIndex = 23;
            this.lblLimitDate.Text = "有効期限";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 239);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 20);
            this.label11.TabIndex = 21;
            this.label11.Text = "宛先（取引先）";
            // 
            // txtDeadLine
            // 
            this.txtDeadLine.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtDeadLine.Location = new System.Drawing.Point(182, 321);
            this.txtDeadLine.Margin = new System.Windows.Forms.Padding(4);
            this.txtDeadLine.MaxLength = 30;
            this.txtDeadLine.Multiline = true;
            this.txtDeadLine.Name = "txtDeadLine";
            this.txtDeadLine.Size = new System.Drawing.Size(328, 56);
            this.txtDeadLine.TabIndex = 21;
            // 
            // txtFieldRange
            // 
            this.txtFieldRange.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFieldRange.Location = new System.Drawing.Point(182, 292);
            this.txtFieldRange.Margin = new System.Windows.Forms.Padding(4);
            this.txtFieldRange.MaxLength = 30;
            this.txtFieldRange.Name = "txtFieldRange";
            this.txtFieldRange.Size = new System.Drawing.Size(328, 28);
            this.txtFieldRange.TabIndex = 20;
            // 
            // txtFieldName
            // 
            this.txtFieldName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFieldName.Location = new System.Drawing.Point(182, 263);
            this.txtFieldName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFieldName.MaxLength = 30;
            this.txtFieldName.Name = "txtFieldName";
            this.txtFieldName.Size = new System.Drawing.Size(328, 28);
            this.txtFieldName.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 325);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "納期";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 295);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "工事範囲";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 267);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 20);
            this.label9.TabIndex = 15;
            this.label9.Text = "現場名";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 210);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "宛先（個人）";
            // 
            // cmbAddressOffice
            // 
            this.cmbAddressOffice.FormattingEnabled = true;
            this.cmbAddressOffice.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.cmbAddressOffice.Location = new System.Drawing.Point(182, 176);
            this.cmbAddressOffice.Margin = new System.Windows.Forms.Padding(4);
            this.cmbAddressOffice.MaxLength = 30;
            this.cmbAddressOffice.Name = "cmbAddressOffice";
            this.cmbAddressOffice.Size = new System.Drawing.Size(328, 28);
            this.cmbAddressOffice.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 181);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "宛先（営業所）";
            // 
            // dtpHakkoDate
            // 
            this.dtpHakkoDate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpHakkoDate.Location = new System.Drawing.Point(182, 89);
            this.dtpHakkoDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtpHakkoDate.Name = "dtpHakkoDate";
            this.dtpHakkoDate.Size = new System.Drawing.Size(192, 28);
            this.dtpHakkoDate.TabIndex = 13;
            // 
            // lblHakkoDate
            // 
            this.lblHakkoDate.AutoSize = true;
            this.lblHakkoDate.Location = new System.Drawing.Point(25, 93);
            this.lblHakkoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHakkoDate.Name = "lblHakkoDate";
            this.lblHakkoDate.Size = new System.Drawing.Size(57, 20);
            this.lblHakkoDate.TabIndex = 9;
            this.lblHakkoDate.Text = "発行日";
            // 
            // lblProcessingNum
            // 
            this.lblProcessingNum.AutoSize = true;
            this.lblProcessingNum.Location = new System.Drawing.Point(25, 64);
            this.lblProcessingNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProcessingNum.Name = "lblProcessingNum";
            this.lblProcessingNum.Size = new System.Drawing.Size(57, 20);
            this.lblProcessingNum.TabIndex = 8;
            this.lblProcessingNum.Text = "加工№";
            // 
            // lblOrderNum
            // 
            this.lblOrderNum.AutoSize = true;
            this.lblOrderNum.Location = new System.Drawing.Point(25, 34);
            this.lblOrderNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOrderNum.Name = "lblOrderNum";
            this.lblOrderNum.Size = new System.Drawing.Size(57, 20);
            this.lblOrderNum.TabIndex = 6;
            this.lblOrderNum.Text = "発注№";
            // 
            // txtOrderNum
            // 
            this.txtOrderNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtOrderNum.Location = new System.Drawing.Point(182, 31);
            this.txtOrderNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderNum.MaxLength = 12;
            this.txtOrderNum.Name = "txtOrderNum";
            this.txtOrderNum.Size = new System.Drawing.Size(168, 28);
            this.txtOrderNum.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 152);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "宛先（会社名）";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            this.panel2.Size = new System.Drawing.Size(550, 84);
            this.panel2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pnlFormat);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(550, 72);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "フォーマット";
            // 
            // pnlFormat
            // 
            this.pnlFormat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFormat.Controls.Add(this.rdoSeikyu);
            this.pnlFormat.Controls.Add(this.rdoEstima);
            this.pnlFormat.Location = new System.Drawing.Point(8, 24);
            this.pnlFormat.Margin = new System.Windows.Forms.Padding(4);
            this.pnlFormat.Name = "pnlFormat";
            this.pnlFormat.Size = new System.Drawing.Size(218, 35);
            this.pnlFormat.TabIndex = 0;
            // 
            // rdoSeikyu
            // 
            this.rdoSeikyu.AutoSize = true;
            this.rdoSeikyu.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoSeikyu.Location = new System.Drawing.Point(99, 0);
            this.rdoSeikyu.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSeikyu.Name = "rdoSeikyu";
            this.rdoSeikyu.Size = new System.Drawing.Size(75, 33);
            this.rdoSeikyu.TabIndex = 1;
            this.rdoSeikyu.TabStop = true;
            this.rdoSeikyu.Text = "請求書";
            this.rdoSeikyu.UseVisualStyleBackColor = true;
            // 
            // rdoEstima
            // 
            this.rdoEstima.AutoSize = true;
            this.rdoEstima.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoEstima.Location = new System.Drawing.Point(0, 0);
            this.rdoEstima.Margin = new System.Windows.Forms.Padding(4);
            this.rdoEstima.Name = "rdoEstima";
            this.rdoEstima.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.rdoEstima.Size = new System.Drawing.Size(99, 33);
            this.rdoEstima.TabIndex = 0;
            this.rdoEstima.TabStop = true;
            this.rdoEstima.Text = "見積書";
            this.rdoEstima.UseVisualStyleBackColor = true;
            // 
            // EST0010_EditEstima
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1734, 961);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MinimumSize = new System.Drawing.Size(1496, 723);
            this.Name = "EST0010_EditEstima";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.grpDetail.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBodys)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.grpCondition.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConditions)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.pnlPostage.ResumeLayout(false);
            this.pnlPostage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPostage)).EndInit();
            this.panel17.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRateHousing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRatePlumbing)).EndInit();
            this.panel12.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.pnlFormat.ResumeLayout(false);
            this.pnlFormat.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Panel panel12;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox txtAddressToriName;
        public System.Windows.Forms.DateTimePicker dtpLimitDate;
        public System.Windows.Forms.Label lblLimitDate;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtDeadLine;
        public System.Windows.Forms.TextBox txtFieldRange;
        public System.Windows.Forms.TextBox txtFieldName;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cmbAddressOffice;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.DateTimePicker dtpHakkoDate;
        public System.Windows.Forms.Label lblHakkoDate;
        public System.Windows.Forms.Label lblProcessingNum;
        public System.Windows.Forms.Label lblOrderNum;
        public System.Windows.Forms.TextBox txtOrderNum;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Panel pnlFormat;
        public System.Windows.Forms.RadioButton rdoEstima;
        public System.Windows.Forms.RadioButton rdoSeikyu;
        public System.Windows.Forms.Panel panel8;
        public System.Windows.Forms.Panel panel9;
        public System.Windows.Forms.GroupBox grpDetail;
        public System.Windows.Forms.Panel panel7;
        public System.Windows.Forms.GroupBox grpCondition;
        public System.Windows.Forms.Panel panel10;
        public System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.Label lblRateHousing;
        public System.Windows.Forms.NumericUpDown numRateHousing;
        public System.Windows.Forms.Label lblRatePlumbing;
        public System.Windows.Forms.NumericUpDown numRatePlumbing;
        public System.Windows.Forms.Button btnCSV;
        public System.Windows.Forms.Panel panel11;
        public System.Windows.Forms.Panel panel14;
        public System.Windows.Forms.Button btnAdd;
        public System.Windows.Forms.Panel panel15;
        public System.Windows.Forms.DataGridView dgvBodys;
        public System.Windows.Forms.Panel panel17;
        public System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.TextBox txtSumShikiriProcessing;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox txtSumShikiriHousing;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtSumShikiri;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox txtSumPrice;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Button btnHakko;
        public System.Windows.Forms.TextBox txtAddressCompany;
        public System.Windows.Forms.TextBox txtAddressTanto;
        private System.Windows.Forms.Panel panel13;
        public System.Windows.Forms.TextBox txtProcessingNum;
        public System.Windows.Forms.Label lblEstimateNumPrefix;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Panel panel16;
        public System.Windows.Forms.DataGridView dgvConditions;
        private System.Windows.Forms.DataGridViewTextBoxColumn Condition_Con;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status_Con;
        public System.Windows.Forms.Panel panel18;
        public System.Windows.Forms.Panel panel19;
        public System.Windows.Forms.Button btnTemplete;
        public System.Windows.Forms.Panel panel20;
        public System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.Panel pnlPostage;
        public System.Windows.Forms.RadioButton rdoPreRequestPostage;
        public System.Windows.Forms.RadioButton rdoActualPostage;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.NumericUpDown numPostage;
        private System.Windows.Forms.DataGridViewButtonColumn BtnShow;
        private System.Windows.Forms.DataGridViewButtonColumn BtnDel;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeisaiNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeisaiNoDisp;
        private System.Windows.Forms.DataGridViewTextBoxColumn MeisaiType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Suryo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlumbingKingaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn HousingKingaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingKingaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kingaku;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SummarryRowFlg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConsumptionRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn InspectionRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ExpenceRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}