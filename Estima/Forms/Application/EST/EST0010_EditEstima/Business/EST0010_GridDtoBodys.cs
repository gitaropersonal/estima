﻿using EstimaLib.Const;

namespace Estima.Forms.Application.EST.EST0010.Business
{
    public class EST0010_GridDtoBodys
    {
        public EST0010_GridDtoBodys()
        {
            this.BtnShow = "表示";
            this.BtnDel = "削除";
            this.Status = Enums.EditStatus.None;
            this.SummarryRowFlg = false;
        }
        public string MeisaiNoDisp { get; set; }
        public string MeisaiNo { get; set; }
        public int MeisaiType { get; set; }
        public string BtnShow { get; set; }
        public string BtnDel { get; set; }
        public string ShohinName { get; set; }
        public string Suryo { get; set; }
        public string PlumbingKingaku { get; set; }
        public string HousingKingaku { get; set; }
        public string ProcessingKingaku { get; set; }
        public string Kingaku { get; set; }
        public bool SummarryRowFlg { get; set; }
        public float ConsumptionRate { get; set; }
        public float InspectionRate { get; set; }
        public float ExpenceRate { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
