﻿using EstimaLib.Const;

namespace Estima.Forms.Application.EST.EST0010.Business
{
    public class EST1010_GridDtoConditions
    {
        public EST1010_GridDtoConditions()
        {
            this.Status_Con = Enums.EditStatus.None;
        }
        public string Condition_Con { get; set; }
        public Enums.EditStatus Status_Con { get; set; }
    }
}
