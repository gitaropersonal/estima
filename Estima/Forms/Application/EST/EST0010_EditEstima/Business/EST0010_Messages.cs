﻿namespace Estima.Forms.Application.EST.EST0010.Business
{
    internal static class EST0010_Messages
    {
        internal const string MsgErrorNotExistDatas = "対象のデータがありません";
        internal const string MsgErrorFileAlreadyOpend = "帳票が既に開かれています";
        internal const string MsgAskOpenFile = "作成しました\r\nファイルを開きますか？";
        internal const string MsgInfoFinishUpdate = "更新しました";
        internal const string MsgAskClear  = "クリアしますか？";
        internal const string MsgAskDelete = "削除しますか？";
        internal const string MsgAskHakko = "発行しますか？";
        internal const string MsgAskUpdate = "更新しますか？";
        internal const string MsgAskFinish = "終了しますか？";
        internal const string MsgAskExistWorkTable = "未保存の明細データがありますが\r\nよろしいですか？";
        internal const string MsgAskExistUnSavedDatas = "未更新のデータがありますが\r\nよろしいですか？";
        internal const string MsgAskCopyDataNotSaved = "複製したデータは保存されませんが\r\nよろしいですか？";
        internal const string MsgErrorRequired = "{0}を入力してください";
        internal const string MsgErrorNotSaved = "データを更新してください";
        internal const string MsgErrorEstimateNoAlreadyExist = "既に使用されている{0}が入力されています";
        internal const string MsgErrorLargeSmallCollision = "{0}は{1}より大きい値を入力してください";
    }
}
