﻿using EstimaLib.Const;
using EstimaLib.Entity;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace Estima.Forms.Application.EST.EST0010.Business
{
    internal class EST0010_Service
    {
        #region 明細番号
        /// <summary>
        /// 明細番号取得
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <param name="delBodyDtoList"></param>
        /// <returns></returns>
        internal int GetNextMeisaiNo(
              string tempEstimaID
            , List<EST0010_GridDtoBodys> delBodyDtoList
            )
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    WX01.TempEstimaID ");
            sb.AppendLine($",   MAX(WX01.MeisaiNo) AS MeisaiNo ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    WX01TempEstimaDetail AS WX01 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    WX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            int maxDel = 0;
                            if (delBodyDtoList.Any())
                            {
                                maxDel = delBodyDtoList.Max(n => CommonUtil.ToInteger(n.MeisaiNo));
                            }
                            if (CommonUtil.ToInteger(dr[1]) < maxDel)
                            {
                                return maxDel + 1;
                            }
                            return CommonUtil.ToInteger(dr[1]) + 1;
                        }
                    }
                }
            }
            return 1;
        }
        #endregion

        #region 伝票
        /// <summary>
        /// 伝票抽出（ヘッダー）
        /// </summary>
        /// <param name="estimaID"></param>
        /// <param name="estimaIDForCopy"></param>
        /// <returns></returns>
        internal EX01EstimaHeader SelEstimaHeader(
              string estimaID
            , string estimaIDForCopy = ""
            )
        {
            var ret = new EX01EstimaHeader();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    EX01.EstimaID ");
            sb.AppendLine($",   EX01.EstimaKbn ");
            sb.AppendLine($",   EX01.EstimateNo ");
            sb.AppendLine($",   EX01.OrderNo ");
            sb.AppendLine($",   EX01.ProcessingNo ");
            sb.AppendLine($",   EX01.HakkoDate ");
            sb.AppendLine($",   EX01.LimitDate ");
            sb.AppendLine($",   EX01.AddressCompany ");
            sb.AppendLine($",   EX01.AddressOffice ");
            sb.AppendLine($",   EX01.AddressTanto ");
            sb.AppendLine($",   EX01.AddressSuppliers ");
            sb.AppendLine($",   EX01.FieldName ");
            sb.AppendLine($",   EX01.FieldRange ");
            sb.AppendLine($",   EX01.DeadLine ");
            sb.AppendLine($",   EX01.PlumbingRate ");
            sb.AppendLine($",   EX01.HousingRate ");
            sb.AppendLine($",   EX01.Postage ");
            sb.AppendLine($",   EX01.Condition1 ");
            sb.AppendLine($",   EX01.Condition2 ");
            sb.AppendLine($",   EX01.Condition3 ");
            sb.AppendLine($",   EX01.Condition4 ");
            sb.AppendLine($",   EX01.Condition5 ");
            sb.AppendLine($",   EX01.Condition6 ");
            sb.AppendLine($",   EX01.Condition7 ");
            sb.AppendLine($",   EX01.Condition8 ");
            sb.AppendLine($",   EX01.Condition9 ");
            sb.AppendLine($",   EX01.Condition10 ");
            sb.AppendLine($",   EX01.Condition11 ");
            sb.AppendLine($",   EX01.Condition12 ");
            sb.AppendLine($",   EX01.Condition13 ");
            sb.AppendLine($",   EX01.Condition14 ");
            sb.AppendLine($",   EX01.Condition15 ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX01EstimaHeader AS EX01 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EX01.EstimaID = @estimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.EstimaID = string.IsNullOrEmpty(estimaIDForCopy)? CommonUtil.TostringNullForbid(dr[nameof(ret.EstimaID)]) : estimaIDForCopy;
                            ret.EstimaKbn = CommonUtil.ToInteger(dr[nameof(ret.EstimaKbn)]);
                            ret.EstimateNo = CommonUtil.TostringNullForbid(dr[nameof(ret.EstimateNo)]);
                            ret.OrderNo = CommonUtil.TostringNullForbid(dr[nameof(ret.OrderNo)]);
                            ret.ProcessingNo = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingNo)]);
                            ret.HakkoDate = CommonUtil.ToDateTime((object)dr[nameof(ret.HakkoDate)]);
                            ret.LimitDate = CommonUtil.ToDateTime((object)dr[nameof(ret.LimitDate)]);
                            ret.AddressCompany = CommonUtil.TostringNullForbid(dr[nameof(ret.AddressCompany)]);
                            ret.AddressOffice = CommonUtil.TostringNullForbid(dr[nameof(ret.AddressOffice)]);
                            ret.AddressTanto = CommonUtil.TostringNullForbid(dr[nameof(ret.AddressTanto)]);
                            ret.AddressSuppliers = CommonUtil.TostringNullForbid(dr[nameof(ret.AddressSuppliers)]);
                            ret.FieldName = CommonUtil.TostringNullForbid(dr[nameof(ret.FieldName)]);
                            ret.FieldRange = CommonUtil.TostringNullForbid(dr[nameof(ret.FieldRange)]);
                            ret.DeadLine = CommonUtil.TostringNullForbid(dr[nameof(ret.DeadLine)]);
                            ret.PlumbingRate = CommonUtil.ToFloat(dr[nameof(ret.PlumbingRate)]);
                            ret.HousingRate = CommonUtil.ToFloat(dr[nameof(ret.HousingRate)]);
                            string postage = CommonUtil.TostringNullForbid(dr[nameof(ret.Postage)]);
                            if (string.IsNullOrEmpty(postage))
                            {
                                ret.Postage = null;
                            }
                            else
                            {
                                ret.Postage = CommonUtil.ToInteger(postage);
                            }
                            ret.Condition1 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition1)]);
                            ret.Condition2 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition2)]);
                            ret.Condition3 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition3)]);
                            ret.Condition4 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition4)]);
                            ret.Condition5 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition5)]);
                            ret.Condition6 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition6)]);
                            ret.Condition7 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition7)]);
                            ret.Condition8 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition8)]);
                            ret.Condition9 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition9)]);
                            ret.Condition10 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition10)]);
                            ret.Condition11 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition11)]);
                            ret.Condition12 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition12)]);
                            ret.Condition13 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition13)]);
                            ret.Condition14 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition14)]);
                            ret.Condition15 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition15)]);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 伝票抽出（明細）
        /// </summary>
        /// <param name="estimaID"></param>
        /// <param name="delBodyDtoList"></param>
        /// <returns></returns>
        internal BindingList<EST0010_GridDtoBodys> SelEstimaBody(
              string estimaID
            , List<EST0010_GridDtoBodys> delBodyDtoList
            )
        {
            var ret = new BindingList<EST0010_GridDtoBodys>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"WITH UnitPlumbing AS( ");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       EX02.EstimaID ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"   ,   1 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(EX02.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       EX02EstimaBody AS EX02 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($"   AND EX02.MeisaiNo  ");
            sb.AppendLine($"   AND EX02.MeisaiType = 1");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitHousing AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       EX02.EstimaID ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"   ,   2 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(EX02.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       EX02EstimaBody AS EX02 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($"   AND EX02.MeisaiNo  ");
            sb.AppendLine($"   AND EX02.MeisaiType IN(2) ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitProcessing AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       EX02.EstimaID ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"   ,   3 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(EX02.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       EX02EstimaBody AS EX02 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($"   AND EX02.MeisaiNo  ");
            sb.AppendLine($"   AND EX02.MeisaiType = 3 ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitAll AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       EX02.EstimaID ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"   ,   MIN(EX02.MeisaiType)  AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(EX02.Kingaku)     AS Kingaku ");
            sb.AppendLine($"   ,   MAX(EX02.ConsumptionRate) AS ConsumptionRate ");
            sb.AppendLine($"   ,   MAX(EX02.InspectionRate) AS InspectionRate ");
            sb.AppendLine($"   ,   MAX(EX02.ExpenceRate) AS ExpenceRate ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       EX02EstimaBody AS EX02 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitShohinName AS( ");
            sb.AppendLine($"   SELECT  ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo  ");
            sb.AppendLine($"   ,   MinUnit.MeisaiType  ");
            sb.AppendLine($"   ,   EX02.ShohinName  ");
            sb.AppendLine($"   FROM  ");
            sb.AppendLine($"       EX02EstimaBody AS EX02  ");
            sb.AppendLine($"   INNER JOIN(  ");
            sb.AppendLine($"       SELECT  ");
            sb.AppendLine($"           EX02.EstimaID  ");
            sb.AppendLine($"       ,   EX02.MeisaiNo  ");
            sb.AppendLine($"       ,   MIN(EX02.MeisaiType) AS MeisaiType  ");
            sb.AppendLine($"       FROM  ");
            sb.AppendLine($"           EX02EstimaBody AS EX02  ");
            sb.AppendLine($"       WHERE   ");
            sb.AppendLine($"           EX02.EstimaID = @estimaID  ");
            sb.AppendLine($"       GROUP BY  ");
            sb.AppendLine($"           EX02.EstimaID   ");
            sb.AppendLine($"       ,   EX02.MeisaiNo   ");
            sb.AppendLine($"   )AS MinUnit  ");
            sb.AppendLine($"   ON  ");
            sb.AppendLine($"       EX02.EstimaID   = MinUnit.EstimaID  ");
            sb.AppendLine($"   AND EX02.MeisaiNo   = MinUnit.MeisaiNo  ");
            sb.AppendLine($"   AND EX02.MeisaiType = MinUnit.MeisaiType  ");
            sb.AppendLine($"   WHERE   ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($") ");
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    UA.EstimaID ");
            sb.AppendLine($",   UA.MeisaiNo ");
            sb.AppendLine($",   UA.MeisaiType ");
            sb.AppendLine($",   UA.ConsumptionRate ");
            sb.AppendLine($",   UA.InspectionRate ");
            sb.AppendLine($",   UA.ExpenceRate ");
            sb.AppendLine($",   US.ShohinName ");
            sb.AppendLine($",   1 AS Suryo ");
            sb.AppendLine($",   CASE WHEN UP.Kingaku IS NULL THEN 0 ELSE UP.Kingaku END AS PlumbingKingaku ");
            sb.AppendLine($",   CASE WHEN UH.Kingaku IS NULL THEN 0 ELSE UH.Kingaku END AS HousingKingaku ");
            sb.AppendLine($",   CASE WHEN UR.Kingaku IS NULL THEN 0 ELSE UR.Kingaku END AS ProcessingKingaku ");
            sb.AppendLine($",   UA.Kingaku ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    UnitAll AS UA ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    UnitPlumbing AS UP ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.EstimaID   = UP.EstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo   = UP.MeisaiNo ");
            sb.AppendLine($"AND UP.MeisaiType = 1 ");
            sb.AppendLine($"LEFT OUTER JOIN  ");
            sb.AppendLine($"    UnitHousing AS UH ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.EstimaID   = UH.EstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo   = UH.MeisaiNo ");
            sb.AppendLine($"AND UH.MeisaiType = 2 ");
            sb.AppendLine($"LEFT OUTER JOIN  ");
            sb.AppendLine($"    UnitProcessing AS UR ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.EstimaID   = UR.EstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo   = UR.MeisaiNo ");
            sb.AppendLine($"AND UR.MeisaiType = 3 ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    UnitShohinName AS US ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.EstimaID   = US.EstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo   = US.MeisaiNo ");
            sb.AppendLine($"AND UA.MeisaiType = US.MeisaiType ");
            sb.AppendLine($"WHERE  ");
            sb.AppendLine($"    UA.EstimaID = @estimaID ");
            sb.AppendLine($"ORDER BY  ");
            sb.AppendLine($"    UA.EstimaID ");
            sb.AppendLine($",   UA.MeisaiNo ");
            sb.AppendLine($";  ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    
                    using (var dr = cmd.ExecuteReader())
                    {
                        int rowNum = 1;
                        while (dr.Read())
                        {
                            var dto = new EST0010_GridDtoBodys();
                            dto.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinName)]);
                            dto.MeisaiNo = CommonUtil.TostringNullForbid(dr[nameof(dto.MeisaiNo)]);
                            dto.MeisaiType = CommonUtil.ToInteger(dr[nameof(dto.MeisaiType)]);
                            dto.Suryo = CommonUtil.ToInteger(dr[nameof(dto.Suryo)]).ToString("#,0");
                            dto.PlumbingKingaku = CommonUtil.ToInteger(dr[nameof(dto.PlumbingKingaku)]).ToString("#,0");
                            dto.HousingKingaku = CommonUtil.ToInteger(dr[nameof(dto.HousingKingaku)]).ToString("#,0");
                            dto.ProcessingKingaku = CommonUtil.ToInteger(dr[nameof(dto.ProcessingKingaku)]).ToString("#,0");
                            dto.Kingaku = CommonUtil.ToInteger(dr[nameof(dto.Kingaku)]).ToString("#,0");
                            dto.ConsumptionRate = CommonUtil.ToFloat(dr[nameof(dto.ConsumptionRate)]);
                            dto.InspectionRate = CommonUtil.ToFloat(dr[nameof(dto.InspectionRate)]);
                            dto.ExpenceRate = CommonUtil.ToFloat(dr[nameof(dto.ExpenceRate)]);
                            dto.Status = Enums.EditStatus.Show;
                            if (delBodyDtoList.Any(n => n.MeisaiNo == dto.MeisaiNo))
                            {
                                continue;
                            }
                            dto.MeisaiNoDisp = rowNum.ToString();
                            ret.Add(dto);
                            rowNum++;
                        }
                    }
                }
            }
            // 明細行から小計行作成
            if (ret.Any())
            {
                ret.Add(this.GetSummarryDto(ret.ToList()));
            }
            return ret;
        }
        /// <summary>
        /// 伝票ヘッダー登録
        /// </summary>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        internal void AddEstimaHeader(EX01EstimaHeader addEntity)
        {
            // コメントアウト
            string cmOutPostage = CommonUtil.GetCmout(addEntity.Postage == null);

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO EX01EstimaHeader( ");
            sb.AppendLine($"    EstimaID ");
            sb.AppendLine($",   EstimaKbn ");
            sb.AppendLine($",   EstimateNo ");
            sb.AppendLine($",   OrderNo ");
            sb.AppendLine($",   ProcessingNo ");
            sb.AppendLine($",   HakkoDate ");
            sb.AppendLine($",   LimitDate ");
            sb.AppendLine($",   AddressCompany ");
            sb.AppendLine($",   AddressOffice ");
            sb.AppendLine($",   AddressTanto ");
            sb.AppendLine($",   AddressSuppliers ");
            sb.AppendLine($",   FieldName ");
            sb.AppendLine($",   FieldRange ");
            sb.AppendLine($",   DeadLine ");
            sb.AppendLine($",   PlumbingRate ");
            sb.AppendLine($",   HousingRate ");
            sb.AppendLine($"{cmOutPostage},   Postage ");
            sb.AppendLine($",   Condition1 ");
            sb.AppendLine($",   Condition2 ");
            sb.AppendLine($",   Condition3 ");
            sb.AppendLine($",   Condition4 ");
            sb.AppendLine($",   Condition5 ");
            sb.AppendLine($",   Condition6 ");
            sb.AppendLine($",   Condition7 ");
            sb.AppendLine($",   Condition8 ");
            sb.AppendLine($",   Condition9 ");
            sb.AppendLine($",   Condition10 ");
            sb.AppendLine($",   Condition11 ");
            sb.AppendLine($",   Condition12 ");
            sb.AppendLine($",   Condition13 ");
            sb.AppendLine($",   Condition14 ");
            sb.AppendLine($",   Condition15 ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @estimaID ");
            sb.AppendLine($",   @estimaKbn ");
            sb.AppendLine($",   @estimateNo ");
            sb.AppendLine($",   @orderNo ");
            sb.AppendLine($",   @processingNo ");
            sb.AppendLine($",   @hakkoDate ");
            sb.AppendLine($",   @limitDate ");
            sb.AppendLine($",   @addressCompany ");
            sb.AppendLine($",   @addressOffice ");
            sb.AppendLine($",   @addressTanto ");
            sb.AppendLine($",   @addressSuppliers ");
            sb.AppendLine($",   @fieldName ");
            sb.AppendLine($",   @fieldRange ");
            sb.AppendLine($",   @deadLine ");
            sb.AppendLine($",   @plumbingRate ");
            sb.AppendLine($",   @housingRate ");
            sb.AppendLine($"{cmOutPostage},   @postage ");
            sb.AppendLine($",   @condition1 ");
            sb.AppendLine($",   @condition2 ");
            sb.AppendLine($",   @condition3 ");
            sb.AppendLine($",   @condition4 ");
            sb.AppendLine($",   @condition5 ");
            sb.AppendLine($",   @condition6 ");
            sb.AppendLine($",   @condition7 ");
            sb.AppendLine($",   @condition8 ");
            sb.AppendLine($",   @condition9 ");
            sb.AppendLine($",   @condition10 ");
            sb.AppendLine($",   @condition11 ");
            sb.AppendLine($",   @condition12 ");
            sb.AppendLine($",   @condition13 ");
            sb.AppendLine($",   @condition14 ");
            sb.AppendLine($",   @condition15 ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($"); ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", addEntity.EstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@estimaKbn", addEntity.EstimaKbn) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@estimateNo", addEntity.EstimateNo) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@orderNo", addEntity.OrderNo) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@processingNo", addEntity.ProcessingNo) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@hakkoDate", addEntity.HakkoDate) { DbType = DbType.Date });
                    cmd.Parameters.Add(new MySqlParameter("@limitDate", addEntity.LimitDate) { DbType = DbType.Date });
                    cmd.Parameters.Add(new MySqlParameter("@addressCompany", addEntity.AddressCompany) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@addressOffice", addEntity.AddressOffice) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@addressTanto", addEntity.AddressTanto) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@addressSuppliers", addEntity.AddressSuppliers) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@fieldName", addEntity.FieldName) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@fieldRange", addEntity.FieldRange) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@deadLine", addEntity.DeadLine) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@plumbingRate", addEntity.PlumbingRate) { DbType = DbType.Decimal });
                    cmd.Parameters.Add(new MySqlParameter("@housingRate", addEntity.HousingRate) { DbType = DbType.Decimal });
                    cmd.Parameters.Add(new MySqlParameter("@postage", addEntity.Postage));
                    cmd.Parameters.Add(new MySqlParameter("@condition1", addEntity.Condition1) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition2", addEntity.Condition2) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition3", addEntity.Condition3) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition4", addEntity.Condition4) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition5", addEntity.Condition5) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition6", addEntity.Condition6) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition7", addEntity.Condition7) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition8", addEntity.Condition8) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition9", addEntity.Condition9) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition10", addEntity.Condition10) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition11", addEntity.Condition11) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition12", addEntity.Condition12) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition13", addEntity.Condition13) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition14", addEntity.Condition14) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition15", addEntity.Condition15) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 伝票明細登録
        /// </summary>
        /// <param name="addEntity"></param>
        internal void AddEstimaBody(EX02EstimaBody addEntity)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"WITH DATAS AS( ");
            sb.AppendLine($"    SELECT ");
            sb.AppendLine($"        EX03.EstimaID ");
            sb.AppendLine($"    ,   EX03.MeisaiNo ");
            sb.AppendLine($"    ,   EX03.MeisaiType ");
            sb.AppendLine($"    ,   EX03.ShohinSizeName ");
            sb.AppendLine($"    ,   EX03.Tanka * EX03.Suryo AS Tanka ");
            sb.AppendLine($"    ,   EX03.Tanka * EX03.Suryo AS Kingaku ");
            sb.AppendLine($"    ,   CASE WHEN EX03.BDCount IS NULL THEN 0 ELSE EX03.Tanka * EX03.BDCount * EX03.Suryo END AS KingakuBD ");
            sb.AppendLine($"    FROM ");
            sb.AppendLine($"        EX03EstimaDetail AS EX03 ");
            sb.AppendLine($") ");
            sb.AppendLine($", VEX02EstimaBody AS( ");
            sb.AppendLine($"    SELECT ");
            sb.AppendLine($"        UNIT.EstimaID ");
            sb.AppendLine($"    ,   UNIT.MeisaiNo ");
            sb.AppendLine($"    ,   UNIT.MeisaiType ");
            sb.AppendLine($"    ,   UNIT.ShohinName ");
            sb.AppendLine($"    ,   UNIT.Suryo ");
            sb.AppendLine($"    ,   UNIT.Tanka ");
            sb.AppendLine($"    ,   UNIT.Kingaku ");
            sb.AppendLine($"    FROM( ");
            sb.AppendLine($"        SELECT ");
            sb.AppendLine($"            DATAS.EstimaID ");
            sb.AppendLine($"        ,   DATAS.MeisaiNo ");
            sb.AppendLine($"        ,   DATAS.MeisaiType ");
            sb.AppendLine($"        ,   CONCAT(MX02.ParentCDName, ' ', DATAS.ShohinSizeName, ' 他') AS ShohinName ");
            sb.AppendLine($"        ,   1  AS Suryo ");
            sb.AppendLine($"        ,   SUM(DATAS.Tanka)   AS Tanka ");
            sb.AppendLine($"        ,   SUM(DATAS.Kingaku) AS Kingaku ");
            sb.AppendLine($"        FROM DATAS ");
            sb.AppendLine($"        LEFT OUTER JOIN( ");
            sb.AppendLine($"            SELECT ");
            sb.AppendLine($"                EX03.EstimaID ");
            sb.AppendLine($"            ,   EX03.MeisaiNo ");
            sb.AppendLine($"            ,   EX03.MeisaiType ");
            sb.AppendLine($"            ,   MAX(MX03.ShohinID ) AS ShohinID ");
            sb.AppendLine($"            FROM ");
            sb.AppendLine($"                EX03EstimaDetail AS EX03 ");
            sb.AppendLine($"            INNER JOIN( ");
            sb.AppendLine($"                SELECT ");
            sb.AppendLine($"                    EX03.EstimaID ");
            sb.AppendLine($"                ,   EX03.MeisaiNo ");
            sb.AppendLine($"                ,   EX03.MeisaiType ");
            sb.AppendLine($"                ,   MIN(EX03.Seq ) AS Seq ");
            sb.AppendLine($"                FROM ");
            sb.AppendLine($"                    EX03EstimaDetail AS EX03 ");
            sb.AppendLine($"                WHERE ");
            sb.AppendLine($"                    EX03.MeisaiType IN(1, 2) ");
            sb.AppendLine($"                GROUP BY ");
            sb.AppendLine($"                    EX03.EstimaID ");
            sb.AppendLine($"                ,   EX03.MeisaiNo ");
            sb.AppendLine($"                ,   EX03.MeisaiType ");
            sb.AppendLine($"            )AS MinSeq ");
            sb.AppendLine($"            ON ");
            sb.AppendLine($"                EX03.EstimaID   = MinSeq.EstimaID ");
            sb.AppendLine($"            AND EX03.MeisaiNo   = MinSeq.MeisaiNo ");
            sb.AppendLine($"            AND EX03.MeisaiType = MinSeq.MeisaiType  ");
            sb.AppendLine($"            AND EX03.Seq        = MinSeq.Seq  ");
            sb.AppendLine($"            LEFT OUTER JOIN ");
            sb.AppendLine($"                MX03Shohin AS MX03 ");
            sb.AppendLine($"            ON ");
            sb.AppendLine($"                MX03.ShohinID = EX03.ShohinID ");
            sb.AppendLine($"            WHERE ");
            sb.AppendLine($"                EX03.MeisaiType IN(1, 2) ");
            sb.AppendLine($"            GROUP BY ");
            sb.AppendLine($"                EX03.EstimaID ");
            sb.AppendLine($"            ,   EX03.MeisaiNo ");
            sb.AppendLine($"            ,   EX03.MeisaiType ");
            sb.AppendLine($"        ) AS MaxShohinID ");
            sb.AppendLine($"        ON ");
            sb.AppendLine($"            DATAS.EstimaID     = MaxShohinID.EstimaID ");
            sb.AppendLine($"        AND DATAS.MeisaiNo     = MaxShohinID.MeisaiNo  ");
            sb.AppendLine($"        AND DATAS.MeisaiType   = MaxShohinID.MeisaiType  ");
            sb.AppendLine($"        LEFT OUTER JOIN ");
            sb.AppendLine($"            MX03Shohin AS MX03 ");
            sb.AppendLine($"        ON ");
            sb.AppendLine($"            MaxShohinID.ShohinID = MX03.ShohinID ");
            sb.AppendLine($"        LEFT OUTER JOIN ");
            sb.AppendLine($"            MX02ParentCD AS MX02 ");
            sb.AppendLine($"        ON ");
            sb.AppendLine($"            LEFT(CONCAT(MX03.ShohinCD,'000'),3) = MX02.ParentCD ");
            sb.AppendLine($"        AND MX03.ParentSeq = MX02.ParentSeq ");
            sb.AppendLine($"        WHERE ");
            sb.AppendLine($"            DATAS.MeisaiType IN(1, 2) ");
            sb.AppendLine($"        GROUP BY ");
            sb.AppendLine($"            DATAS.EstimaID ");
            sb.AppendLine($"        ,   DATAS.MeisaiNo ");
            sb.AppendLine($"        ,   DATAS.MeisaiType ");
            sb.AppendLine($"    UNION ALL ");
            sb.AppendLine($"        SELECT ");
            sb.AppendLine($"            DATAS.EstimaID ");
            sb.AppendLine($"        ,   DATAS.MeisaiNo ");
            sb.AppendLine($"        ,   DATAS.MeisaiType ");
            sb.AppendLine($"        ,   CONCAT(MX02.ParentCDName, ' 他') AS ShohinName ");
            sb.AppendLine($"        ,   1  AS Suryo ");
            sb.AppendLine($"        ,   SUM(DATAS.KingakuBD)   AS Tanka ");
            sb.AppendLine($"        ,   SUM(DATAS.KingakuBD)   AS Kingaku ");
            sb.AppendLine($"        FROM DATAS ");
            sb.AppendLine($"        LEFT OUTER JOIN( ");
            sb.AppendLine($"            SELECT ");
            sb.AppendLine($"                EX03.EstimaID ");
            sb.AppendLine($"            ,   EX03.MeisaiNo ");
            sb.AppendLine($"            ,   EX03.MeisaiType ");
            sb.AppendLine($"            ,   EX03.ShohinID ");
            sb.AppendLine($"            FROM ");
            sb.AppendLine($"                EX03EstimaDetail AS EX03 ");
            sb.AppendLine($"            INNER JOIN( ");
            sb.AppendLine($"                SELECT ");
            sb.AppendLine($"                    EX03.EstimaID ");
            sb.AppendLine($"                ,   EX03.MeisaiNo ");
            sb.AppendLine($"                ,   EX03.MeisaiType ");
            sb.AppendLine($"                ,   MIN(EX03.Seq ) AS Seq ");
            sb.AppendLine($"                FROM ");
            sb.AppendLine($"                    EX03EstimaDetail AS EX03 ");
            sb.AppendLine($"                WHERE ");
            sb.AppendLine($"                    EX03.MeisaiType = 3 ");
            sb.AppendLine($"                GROUP BY ");
            sb.AppendLine($"                    EX03.EstimaID ");
            sb.AppendLine($"                ,   EX03.MeisaiNo ");
            sb.AppendLine($"                ,   EX03.MeisaiType ");
            sb.AppendLine($"            )AS MinSeq ");
            sb.AppendLine($"            ON ");
            sb.AppendLine($"                EX03.EstimaID   = MinSeq.EstimaID ");
            sb.AppendLine($"            AND EX03.MeisaiNo   = MinSeq.MeisaiNo ");
            sb.AppendLine($"            AND EX03.MeisaiType = MinSeq.MeisaiType  ");
            sb.AppendLine($"            AND EX03.Seq        = MinSeq.Seq  ");
            sb.AppendLine($"            WHERE ");
            sb.AppendLine($"                EX03.MeisaiType = 3 ");
            sb.AppendLine($"            GROUP BY ");
            sb.AppendLine($"                EX03.EstimaID ");
            sb.AppendLine($"            ,   EX03.MeisaiNo ");
            sb.AppendLine($"            ,   EX03.MeisaiType ");
            sb.AppendLine($"        ) AS MaxShohinID ");
            sb.AppendLine($"        ON ");
            sb.AppendLine($"            DATAS.EstimaID     = MaxShohinID.EstimaID ");
            sb.AppendLine($"        AND DATAS.MeisaiNo     = MaxShohinID.MeisaiNo  ");
            sb.AppendLine($"        AND DATAS.MeisaiType   = MaxShohinID.MeisaiType  ");
            sb.AppendLine($"        LEFT OUTER JOIN ");
            sb.AppendLine($"            MX03Shohin AS MX03 ");
            sb.AppendLine($"        ON ");
            sb.AppendLine($"            MaxShohinID.ShohinID = MX03.ShohinID ");
            sb.AppendLine($"        LEFT OUTER JOIN ");
            sb.AppendLine($"            MX02ParentCD AS MX02 ");
            sb.AppendLine($"        ON ");
            sb.AppendLine($"            LEFT(CONCAT(MX03.ShohinCD,'000'),3) = MX02.ParentCD ");
            sb.AppendLine($"        AND MX03.ParentSeq = MX02.ParentSeq ");
            sb.AppendLine($"        WHERE ");
            sb.AppendLine($"            DATAS.MeisaiType = 3 ");
            sb.AppendLine($"        GROUP BY ");
            sb.AppendLine($"            DATAS.EstimaID ");
            sb.AppendLine($"        ,   DATAS.MeisaiNo ");
            sb.AppendLine($"        ,   DATAS.MeisaiType ");
            sb.AppendLine($"    )AS UNIT ");
            sb.AppendLine($") ");
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VEX02.EstimaID ");
            sb.AppendLine($",   VEX02.MeisaiNo ");
            sb.AppendLine($",   VEX02.MeisaiType ");
            sb.AppendLine($",   VEX02.Tanka ");
            sb.AppendLine($",   VEX02.Suryo ");
            sb.AppendLine($",   VEX02.Kingaku ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VEX02EstimaBody AS VEX02 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    VEX02.EstimaID   = @estimaID ");
            sb.AppendLine($"AND VEX02.MeisaiNo   = @meisaiNo ");
            sb.AppendLine($"AND VEX02.MeisaiType = @meisaiType ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", addEntity.EstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo", addEntity.MeisaiNo) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiType", addEntity.MeisaiType) { DbType = DbType.Int32 });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            addEntity.Tanka = CommonUtil.ToInteger(dr[nameof(addEntity.Tanka)]);
                            addEntity.Suryo = CommonUtil.ToInteger(dr[nameof(addEntity.Suryo)]);
                            addEntity.Kingaku = CommonUtil.ToInteger(dr[nameof(addEntity.Kingaku)]);
                        }
                    }
                }
            }
            // SQL文の作成
            sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO EX02EstimaBody( ");
            sb.AppendLine($"    EstimaID ");
            sb.AppendLine($",   MeisaiNo ");
            sb.AppendLine($",   MeisaiType ");
            sb.AppendLine($",   ShohinName ");
            sb.AppendLine($",   Tanka ");
            sb.AppendLine($",   Suryo ");
            sb.AppendLine($",   Kingaku ");
            sb.AppendLine($",   ConsumptionRate ");
            sb.AppendLine($",   InspectionRate ");
            sb.AppendLine($",   ExpenceRate ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @estimaID ");
            sb.AppendLine($",   @meisaiNo ");
            sb.AppendLine($",   @meisaiType ");
            sb.AppendLine($",   @shohinName ");
            sb.AppendLine($",   @tanka ");
            sb.AppendLine($",   @suryo ");
            sb.AppendLine($",   @kingaku ");
            sb.AppendLine($",   @consumptionRate ");
            sb.AppendLine($",   @inspectionRate ");
            sb.AppendLine($",   @expenceRate ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($") ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", addEntity.EstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo", addEntity.MeisaiNo) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiType", addEntity.MeisaiType) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@shohinName", addEntity.ShohinName) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@tanka", addEntity.Tanka) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@suryo", addEntity.Suryo) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@kingaku", addEntity.Kingaku) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@consumptionRate", addEntity.ConsumptionRate) { DbType = DbType.Decimal });
                    cmd.Parameters.Add(new MySqlParameter("@inspectionRate", addEntity.InspectionRate) { DbType = DbType.Decimal });
                    cmd.Parameters.Add(new MySqlParameter("@expenceRate", addEntity.ExpenceRate) { DbType = DbType.Decimal });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 明細内訳登録
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <param name="estimaIDForCopy"></param>
        internal void AddEstimaDetail(
              string tempEstimaID
            , string estimaIDForCopy = ""
            )
        {
            string sqlPart = "WX01.TempEstimaID";
            if (!string.IsNullOrEmpty(estimaIDForCopy))
            {
                sqlPart = $"'{estimaIDForCopy}'";
            }
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO EX03EstimaDetail ");
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    {sqlPart} ");
            sb.AppendLine($",   WX01.MeisaiNo ");
            sb.AppendLine($",   WX01.MeisaiType ");
            sb.AppendLine($",   WX01.Seq ");
            sb.AppendLine($",   WX01.ShohinID ");
            sb.AppendLine($",   WX01.ShohinCD ");
            sb.AppendLine($",   WX01.ParentCDName ");
            sb.AppendLine($",   '' AS ZaishitsuName ");
            sb.AppendLine($",   WX01.ShohinSizeName ");
            sb.AppendLine($",   WX01.ShohinName ");
            sb.AppendLine($",   WX01.Tanka ");
            sb.AppendLine($",   WX01.Suryo ");
            sb.AppendLine($",   WX01.Unit ");
            sb.AppendLine($",   WX01.BDCount ");
            sb.AppendLine($",   WX01.ManualFlg ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    WX01TempEstimaDetail AS WX01 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    WX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 伝票ヘッダー更新
        /// </summary>
        /// <param name="updEntity"></param>
        /// <returns></returns>
        internal void UpdEstimaHeader(EX01EstimaHeader updEntity)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"UPDATE");
            sb.AppendLine($"    EX01EstimaHeader ");
            sb.AppendLine($"SET ");
            sb.AppendLine($"    EstimaKbn        = @estimaKbn ");
            sb.AppendLine($",   EstimateNo       = @estimateNo ");
            sb.AppendLine($",   OrderNo          = @orderNo ");
            sb.AppendLine($",   ProcessingNo     = @processingNo ");
            sb.AppendLine($",   HakkoDate        = @hakkoDate ");
            sb.AppendLine($",   LimitDate        = @limitDate ");
            sb.AppendLine($",   AddressCompany   = @addressCompany ");
            sb.AppendLine($",   AddressOffice    = @addressOffice ");
            sb.AppendLine($",   AddressTanto     = @addressTanto ");
            sb.AppendLine($",   AddressSuppliers = @addressSuppliers ");
            sb.AppendLine($",   FieldName        = @fieldName ");
            sb.AppendLine($",   FieldRange       = @fieldRange ");
            sb.AppendLine($",   DeadLine         = @deadLine ");
            sb.AppendLine($",   PlumbingRate     = @plumbingRate ");
            sb.AppendLine($",   HousingRate      = @housingRate ");
            sb.AppendLine($",   Postage          = @postage ");
            sb.AppendLine($",   Condition1       = @condition1 ");
            sb.AppendLine($",   Condition2       = @condition2 ");
            sb.AppendLine($",   Condition3       = @condition3 ");
            sb.AppendLine($",   Condition4       = @condition4 ");
            sb.AppendLine($",   Condition5       = @condition5 ");
            sb.AppendLine($",   Condition6       = @condition6 ");
            sb.AppendLine($",   Condition7       = @condition7 ");
            sb.AppendLine($",   Condition8       = @condition8 ");
            sb.AppendLine($",   Condition9       = @condition9 ");
            sb.AppendLine($",   Condition10      = @condition10 ");
            sb.AppendLine($",   Condition11      = @condition11 ");
            sb.AppendLine($",   Condition12      = @condition12 ");
            sb.AppendLine($",   Condition13      = @condition13 ");
            sb.AppendLine($",   Condition14      = @condition14 ");
            sb.AppendLine($",   Condition15      = @condition15 ");
            sb.AppendLine($",   UpdIPAddress     = '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   UpdHostName      = '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   UpdDate          = CURRENT_TIMESTAMP ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EstimaID = @estimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", updEntity.EstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@estimaKbn", updEntity.EstimaKbn) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@estimateNo", updEntity.EstimateNo) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@orderNo", updEntity.OrderNo) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@processingNo", updEntity.ProcessingNo) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@hakkoDate", updEntity.HakkoDate) { DbType = DbType.Date });
                    cmd.Parameters.Add(new MySqlParameter("@limitDate", updEntity.LimitDate) { DbType = DbType.Date });
                    cmd.Parameters.Add(new MySqlParameter("@addressCompany", updEntity.AddressCompany) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@addressOffice", updEntity.AddressOffice) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@addressTanto", updEntity.AddressTanto) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@addressSuppliers", updEntity.AddressSuppliers) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@fieldName", updEntity.FieldName) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@fieldRange", updEntity.FieldRange) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@deadLine", updEntity.DeadLine) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@plumbingRate", updEntity.PlumbingRate) { DbType = DbType.Decimal });
                    cmd.Parameters.Add(new MySqlParameter("@housingRate", updEntity.HousingRate) { DbType = DbType.Decimal });
                    cmd.Parameters.Add(new MySqlParameter("@postage", updEntity.Postage));
                    cmd.Parameters.Add(new MySqlParameter("@condition1", updEntity.Condition1) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition2", updEntity.Condition2) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition3", updEntity.Condition3) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition4", updEntity.Condition4) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition5", updEntity.Condition5) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition6", updEntity.Condition6) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition7", updEntity.Condition7) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition8", updEntity.Condition8) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition9", updEntity.Condition9) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition10", updEntity.Condition10) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition11", updEntity.Condition11) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition12", updEntity.Condition12) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition13", updEntity.Condition13) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition14", updEntity.Condition14) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@condition15", updEntity.Condition15) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 明細削除
        /// </summary>
        /// <param name="estimaID"></param>
        internal void DelEstimaBody(string estimaID)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM EX02EstimaBody ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EstimaID = @estimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 明細内訳削除
        /// </summary>
        /// <param name="estimaID"></param>
        internal void DelEstimaDetail(string estimaID)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM EX03EstimaDetail ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EstimaID = @estimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 見積書№が存在するか？
        /// </summary>
        /// <param name="estimateNo"></param>
        /// <returns></returns>
        internal bool ValidateExistEstimateNo(string estimateNo)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    COUNT(*)");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX01EstimaHeader AS EX01 ");
            sb.AppendLine($"WHERE  ");
            sb.AppendLine($"    EX01.EstimateNo = @estimateNo ");
            sb.AppendLine($";  ");

            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimateNo", estimateNo) { DbType = DbType.String });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return CommonUtil.ToInteger(dr[0]) == 0;
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// 見積書№が存在するか？
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        internal bool ValidateExistOrderNo(string orderNo)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    COUNT(*)");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX01EstimaHeader AS EX01 ");
            sb.AppendLine($"WHERE  ");
            sb.AppendLine($"    EX01.OrderNo = @orderNo ");
            sb.AppendLine($";  ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@orderNo", orderNo) { DbType = DbType.String });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return CommonUtil.ToInteger(dr[0]) == 0;
                        }
                    }
                }
            }
            return false;
        }
        #endregion

        #region 条件
        /// <summary>
        /// テンプレート取得
        /// </summary>
        /// <param name="estimaKbn"></param>
        /// <returns></returns>
        internal List<EST1010_GridDtoConditions> GetTemplete(int estimaKbn)
        {
            var ret = new List<EST1010_GridDtoConditions>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    EX05.ConditionVal AS Condition_Con ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX05ConditionTemplete AS EX05 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EX05.EstimaKbn = @estimaKbn ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    EX05.EstimaKbn ");
            sb.AppendLine($",   EX05.Seq ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaKbn", estimaKbn) { DbType = DbType.Int32 });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new EST1010_GridDtoConditions();
                            dto.Condition_Con = CommonUtil.TostringNullForbid(dr[nameof(dto.Condition_Con)]);
                            dto.Status_Con = Enums.EditStatus.Show;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion

        #region ワークテーブル（明細内訳）
        /// <summary>
        /// ワークテーブル存在確認
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <returns></returns>
        internal bool ValidateExistWorkTableEstimaDetail(string tempEstimaID)
        {
            var ret = new BindingList<EST0010_GridDtoBodys>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT  ");
            sb.AppendLine($"    COUNT(*)  ");
            sb.AppendLine($"FROM  ");
            sb.AppendLine($"    WX01TempEstimaDetail AS WX01  ");
            sb.AppendLine($"WHERE  ");
            sb.AppendLine($"    WX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($";  ");

            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return 0 == CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// ワークテーブル抽出
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <param name="delBodyDtoList"></param>
        /// <returns></returns>
        internal BindingList<EST0010_GridDtoBodys> SelWorkTableEstimaDetail(
              string tempEstimaID
            , List<EST0010_GridDtoBodys> delBodyDtoList
            )
        {
            var ret = new BindingList<EST0010_GridDtoBodys>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"WITH UnitPlumbing AS( ");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       VEX01.TempEstimaID ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo ");
            sb.AppendLine($"   ,   1 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(VEX01.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       VEX01TempEstimaBody AS VEX01 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       VEX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"   AND VEX01.MeisaiNo  ");
            sb.AppendLine($"   AND VEX01.MeisaiType = 1 ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       VEX01.TempEstimaID  ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitHousing AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       VEX01.TempEstimaID ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo ");
            sb.AppendLine($"   ,   2 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(VEX01.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       VEX01TempEstimaBody AS VEX01 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       VEX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"   AND VEX01.MeisaiNo  ");
            sb.AppendLine($"   AND VEX01.MeisaiType IN(2) ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       VEX01.TempEstimaID  ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitProcessing AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       VEX01.TempEstimaID ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo ");
            sb.AppendLine($"   ,   3 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(VEX01.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       VEX01TempEstimaBody AS VEX01 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       VEX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"   AND VEX01.MeisaiNo  ");
            sb.AppendLine($"   AND VEX01.MeisaiType = 3 ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       VEX01.TempEstimaID  ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitAll AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       VEX01.TempEstimaID ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo ");
            sb.AppendLine($"   ,   MIN(VEX01.MeisaiType)  AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(VEX01.Kingaku)     AS Kingaku ");
            sb.AppendLine($"   ,   MAX(VEX01.ConsumptionRate) AS ConsumptionRate ");
            sb.AppendLine($"   ,   MAX(VEX01.InspectionRate) AS InspectionRate ");
            sb.AppendLine($"   ,   MAX(VEX01.ExpenceRate) AS ExpenceRate ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       VEX01TempEstimaBody AS VEX01 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       VEX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       VEX01.TempEstimaID  ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitShohinName AS( ");
            sb.AppendLine($"   SELECT  ");
            sb.AppendLine($"       VEX01.TempEstimaID  ");
            sb.AppendLine($"   ,   VEX01.MeisaiNo  ");
            sb.AppendLine($"   ,   MinUnit.MeisaiType  ");
            sb.AppendLine($"   ,   VEX01.ShohinName  ");
            sb.AppendLine($"   FROM  ");
            sb.AppendLine($"       VEX01TempEstimaBody AS VEX01  ");
            sb.AppendLine($"   INNER JOIN(  ");
            sb.AppendLine($"       SELECT  ");
            sb.AppendLine($"           VEX01.TempEstimaID  ");
            sb.AppendLine($"       ,   VEX01.MeisaiNo  ");
            sb.AppendLine($"       ,   VEX01.MeisaiType  ");
            sb.AppendLine($"       ,   MIN(VEX01.Seq) AS Seq  ");
            sb.AppendLine($"       FROM  ");
            sb.AppendLine($"           VEX01TempEstimaBody AS VEX01  ");
            sb.AppendLine($"       INNER JOIN(  ");
            sb.AppendLine($"           SELECT  ");
            sb.AppendLine($"               VEX01.TempEstimaID  ");
            sb.AppendLine($"           ,   VEX01.MeisaiNo  ");
            sb.AppendLine($"           ,   MIN(VEX01.MeisaiType) AS MeisaiType ");
            sb.AppendLine($"           FROM  ");
            sb.AppendLine($"               VEX01TempEstimaBody AS VEX01  ");
            sb.AppendLine($"           WHERE   ");
            sb.AppendLine($"               VEX01.TempEstimaID = @tempEstimaID  ");
            sb.AppendLine($"           GROUP BY  ");
            sb.AppendLine($"               VEX01.TempEstimaID   ");
            sb.AppendLine($"           ,   VEX01.MeisaiNo   ");
            sb.AppendLine($"       )AS MinType  ");
            sb.AppendLine($"       ON  ");
            sb.AppendLine($"           VEX01.TempEstimaID = MinType.TempEstimaID  ");
            sb.AppendLine($"       AND VEX01.MeisaiNo     = MinType.MeisaiNo  ");
            sb.AppendLine($"       AND VEX01.MeisaiType   = MinType.MeisaiType  ");
            sb.AppendLine($"       WHERE   ");
            sb.AppendLine($"           VEX01.TempEstimaID = @tempEstimaID  ");
            sb.AppendLine($"       GROUP BY  ");
            sb.AppendLine($"           VEX01.TempEstimaID   ");
            sb.AppendLine($"       ,   VEX01.MeisaiNo   ");
            sb.AppendLine($"       ,   VEX01.MeisaiType   ");
            sb.AppendLine($"   )AS MinUnit  ");
            sb.AppendLine($"   ON  ");
            sb.AppendLine($"       VEX01.TempEstimaID = MinUnit.TempEstimaID  ");
            sb.AppendLine($"   AND VEX01.MeisaiNo     = MinUnit.MeisaiNo  ");
            sb.AppendLine($"   AND VEX01.MeisaiType   = MinUnit.MeisaiType  ");
            sb.AppendLine($"   AND VEX01.Seq          = MinUnit.Seq  ");
            sb.AppendLine($"   WHERE   ");
            sb.AppendLine($"       VEX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($") ");
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    UA.TempEstimaID ");
            sb.AppendLine($",   UA.MeisaiNo ");
            sb.AppendLine($",   UA.MeisaiType ");
            sb.AppendLine($",   UA.ConsumptionRate ");
            sb.AppendLine($",   UA.InspectionRate ");
            sb.AppendLine($",   UA.ExpenceRate ");
            sb.AppendLine($",   US.ShohinName ");
            sb.AppendLine($",   1 AS Suryo ");
            sb.AppendLine($",   CASE WHEN UP.Kingaku IS NULL THEN 0 ELSE UP.Kingaku END AS PlumbingKingaku ");
            sb.AppendLine($",   CASE WHEN UH.Kingaku IS NULL THEN 0 ELSE UH.Kingaku END AS HousingKingaku ");
            sb.AppendLine($",   CASE WHEN UR.Kingaku IS NULL THEN 0 ELSE UR.Kingaku END AS ProcessingKingaku ");
            sb.AppendLine($",   UA.Kingaku ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    UnitAll AS UA ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    UnitPlumbing AS UP ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.TempEstimaID = UP.TempEstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo     = UP.MeisaiNo ");
            sb.AppendLine($"AND UP.MeisaiType   = 1 ");
            sb.AppendLine($"LEFT OUTER JOIN  ");
            sb.AppendLine($"    UnitHousing AS UH ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.TempEstimaID = UH.TempEstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo     = UH.MeisaiNo ");
            sb.AppendLine($"AND UH.MeisaiType   = 2 ");
            sb.AppendLine($"LEFT OUTER JOIN  ");
            sb.AppendLine($"    UnitProcessing AS UR ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.TempEstimaID = UR.TempEstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo     = UR.MeisaiNo ");
            sb.AppendLine($"AND UR.MeisaiType   = 3 ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    UnitShohinName AS US ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.TempEstimaID = US.TempEstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo     = US.MeisaiNo ");
            sb.AppendLine($"AND UA.MeisaiType   = US.MeisaiType ");
            sb.AppendLine($"WHERE  ");
            sb.AppendLine($"    UA.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"ORDER BY  ");
            sb.AppendLine($"    UA.TempEstimaID  ");
            sb.AppendLine($",   UA.MeisaiNo  ");
            sb.AppendLine($";  ");

            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        int rowNum = 1;
                        while (dr.Read())
                        {
                            var dto = new EST0010_GridDtoBodys();
                            dto.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinName)]);
                            dto.MeisaiNo = CommonUtil.TostringNullForbid(dr[nameof(dto.MeisaiNo)]);
                            dto.MeisaiType = CommonUtil.ToInteger(dr[nameof(dto.MeisaiType)]);
                            dto.ProcessingKingaku = CommonUtil.ToInteger(dr[nameof(dto.ProcessingKingaku)]).ToString("#,0");
                            dto.Suryo = CommonUtil.ToInteger(dr[nameof(dto.Suryo)]).ToString("#,0");
                            dto.Kingaku = CommonUtil.ToInteger(dr[nameof(dto.Kingaku)]).ToString("#,0");
                            dto.PlumbingKingaku = CommonUtil.ToFloat(dr[nameof(dto.PlumbingKingaku)]).ToString("#,0");
                            dto.HousingKingaku = CommonUtil.ToFloat(dr[nameof(dto.HousingKingaku)]).ToString("#,0");
                            dto.ConsumptionRate = CommonUtil.ToFloat(dr[nameof(dto.ConsumptionRate)]);
                            dto.InspectionRate = CommonUtil.ToFloat(dr[nameof(dto.InspectionRate)]);
                            dto.ExpenceRate = CommonUtil.ToFloat(dr[nameof(dto.ExpenceRate)]);
                            dto.Status = Enums.EditStatus.Show;
                            if (delBodyDtoList.Any(n => n.MeisaiNo == dto.MeisaiNo))
                            {
                                continue;
                            }
                            dto.MeisaiNoDisp = rowNum.ToString();
                            ret.Add(dto);
                            rowNum++;
                        }
                    }
                }
            }
            // 明細行から小計行作成
            if (ret.Any())
            {
                ret.Add(this.GetSummarryDto(ret.ToList()));
            }
            return ret;
        }
        /// <summary>
        /// 小計行dto取得
        /// </summary>
        /// <param name="bodies"></param>
        /// <returns></returns>
        private EST0010_GridDtoBodys GetSummarryDto(List<EST0010_GridDtoBodys> bodies)
        {
            return new EST0010_GridDtoBodys()
            {
                BtnShow = "-",
                BtnDel = "-",
                ShohinName = "（小計）",
                MeisaiNo = string.Empty,
                MeisaiNoDisp = "-",
                Suryo = bodies.Sum(n => CommonUtil.ToInteger(n.Suryo)).ToString("#,0"),
                PlumbingKingaku = bodies.Sum(n => CommonUtil.ToInteger(n.PlumbingKingaku)).ToString("#,0"),
                HousingKingaku = bodies.Sum(n => CommonUtil.ToInteger(n.HousingKingaku)).ToString("#,0"),
                ProcessingKingaku = bodies.Sum(n => CommonUtil.ToInteger(n.ProcessingKingaku)).ToString("#,0"),
                Kingaku = bodies.Sum(n => CommonUtil.ToInteger(n.Kingaku)).ToString("#,0"),
                SummarryRowFlg = true,
            };
        }
        /// <summary>
        /// ワークテーブル登録
        /// </summary>
        /// <param name="delBodyDtoList"></param>
        /// <param name="estimaID"></param>
        /// <param name="estimaIDForCopy"></param>
        internal void AddWorkTable(
              List<EST0010_GridDtoBodys> delBodyDtoList
            , string estimaID
            , string estimaIDForCopy = ""
            )
        {
            string sqlPart = "EX03.EstimaID";
            if (!string.IsNullOrEmpty(estimaIDForCopy))
            {
                sqlPart = "@estimaID";
            }
            var detailList = new List<EX03EstimaDetail>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    {sqlPart} AS EstimaID ");
            sb.AppendLine($",   EX03.MeisaiNo ");
            sb.AppendLine($",   EX03.MeisaiType ");
            sb.AppendLine($",   EX03.Seq ");
            sb.AppendLine($",   EX03.ShohinID ");
            sb.AppendLine($",   EX03.ShohinCD ");
            sb.AppendLine($",   EX03.ParentCDName ");
            sb.AppendLine($",   EX03.ZaishitsuName ");
            sb.AppendLine($",   EX03.ShohinSizeName ");
            sb.AppendLine($",   EX03.ShohinName ");
            sb.AppendLine($",   EX03.Tanka ");
            sb.AppendLine($",   EX03.Suryo ");
            sb.AppendLine($",   EX03.Unit ");
            sb.AppendLine($",   EX03.BDCount ");
            sb.AppendLine($",   EX03.ManualFlg ");
            sb.AppendLine($",   EX03.AddIPAddress ");
            sb.AppendLine($",   EX03.AddHostName ");
            sb.AppendLine($",   EX03.AddDate ");
            sb.AppendLine($",   EX03.UpdIPAddress ");
            sb.AppendLine($",   EX03.UpdHostName ");
            sb.AppendLine($",   EX03.UpdDate ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX03EstimaDetail AS EX03 ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    WX01TempEstimaDetail AS WX01 ");
            sb.AppendLine($"ON ");
            sb.AppendLine($"    WX01.TempEstimaID = {sqlPart} ");
            sb.AppendLine($"AND EX03.MeisaiNo     = WX01.MeisaiNo ");
            sb.AppendLine($"AND EX03.MeisaiType   = WX01.MeisaiType ");
            sb.AppendLine($"AND EX03.Seq          = WX01.Seq ");
            sb.AppendLine($"AND EX03.ShohinID     = WX01.ShohinID ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EX03.EstimaID = @estimaID ");
            sb.AppendLine($"AND WX01.AddIpAddress IS NULL ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var entity = new EX03EstimaDetail();
                            entity.EstimaID = CommonUtil.TostringNullForbid(dr[nameof(entity.EstimaID)]);
                            entity.MeisaiNo = CommonUtil.ToInteger(dr[nameof(entity.MeisaiNo)]);
                            entity.MeisaiType = CommonUtil.ToInteger(dr[nameof(entity.MeisaiType)]);
                            entity.Seq = CommonUtil.ToInteger(dr[nameof(entity.Seq)]);
                            entity.ShohinID = CommonUtil.ToInteger(dr[nameof(entity.ShohinID)]);
                            entity.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(entity.ShohinCD)]);
                            entity.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(entity.ParentCDName)]);
                            entity.ZaishitsuName = CommonUtil.TostringNullForbid(dr[nameof(entity.ZaishitsuName)]);
                            entity.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(entity.ShohinSizeName)]);
                            entity.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(entity.ShohinName)]);
                            entity.Tanka = CommonUtil.ToInteger(dr[nameof(entity.Tanka)]);
                            entity.Suryo = CommonUtil.ToInteger(dr[nameof(entity.Suryo)]);
                            entity.Unit = CommonUtil.TostringNullForbid(dr[nameof(entity.Unit)]);
                            entity.BDCount = CommonUtil.ToInteger(dr[nameof(entity.BDCount)]);
                            entity.ManualFlg = CommonUtil.ToInteger(dr[nameof(entity.ManualFlg)]);
                            detailList.Add(entity);
                        }
                    }
                }
            }
            string compEstimaID = estimaID;
            if (!string.IsNullOrEmpty(estimaIDForCopy))
            {
                sqlPart = estimaIDForCopy;
            }
            foreach (var entity in detailList)
            {
                if (delBodyDtoList.Any(n => compEstimaID == entity.EstimaID && CommonUtil.ToInteger(n.MeisaiNo) == entity.MeisaiNo))
                {
                    continue;
                }
                sb = new StringBuilder();
                sb.AppendLine($"INSERT INTO WX01TempEstimaDetail( ");
                sb.AppendLine($"    TempEstimaID");
                sb.AppendLine($",   MeisaiNo ");
                sb.AppendLine($",   MeisaiType ");
                sb.AppendLine($",   Seq ");
                sb.AppendLine($",   ShohinID ");
                sb.AppendLine($",   ShohinCD ");
                sb.AppendLine($",   ParentCDName ");
                sb.AppendLine($",   ShohinSizeName ");
                sb.AppendLine($",   ShohinName ");
                sb.AppendLine($",   Tanka ");
                sb.AppendLine($",   Suryo ");
                sb.AppendLine($",   Unit ");
                sb.AppendLine($",   BDCount ");
                sb.AppendLine($",   ManualFlg ");
                sb.AppendLine($",   AddIPAddress ");
                sb.AppendLine($",   AddHostName ");
                sb.AppendLine($",   AddDate ");
                sb.AppendLine($",   UpdIPAddress ");
                sb.AppendLine($",   UpdHostName ");
                sb.AppendLine($",   UpdDate ");
                sb.AppendLine($")VALUES( ");
                sb.AppendLine($"    @estimaID");
                sb.AppendLine($",   @meisaiNo ");
                sb.AppendLine($",   @meisaiType ");
                sb.AppendLine($",   @seq ");
                sb.AppendLine($",   @shohinID ");
                sb.AppendLine($",   @shohinCD ");
                sb.AppendLine($",   @parentCDName ");
                sb.AppendLine($",   @shohinSizeName ");
                sb.AppendLine($",   @shohinName ");
                sb.AppendLine($",   @tanka ");
                sb.AppendLine($",   @suryo ");
                sb.AppendLine($",   @unit ");
                sb.AppendLine($",   @bdCount ");
                sb.AppendLine($",   @manualFlg ");
                sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
                sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
                sb.AppendLine($",   CURRENT_TIMESTAMP ");
                sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
                sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
                sb.AppendLine($",   CURRENT_TIMESTAMP ");
                sb.AppendLine($") ");
                sb.AppendLine($"; ");
                using (var cn = Config.CreateSqlConnection())
                {
                    using (var cmd = cn.CreateCommand())
                    {
                        cn.Open();
                        cmd.CommandText = sb.ToString();
                        cmd.Parameters.Add(new MySqlParameter("@estimaID", entity.EstimaID) { DbType = DbType.String });
                        cmd.Parameters.Add(new MySqlParameter("@meisaiNo", entity.MeisaiNo) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@meisaiType", entity.MeisaiType) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@seq", entity.Seq) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@shohinID", entity.ShohinID) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@shohinCD", entity.ShohinCD) { DbType = DbType.String });
                        cmd.Parameters.Add(new MySqlParameter("@parentCDName", entity.ParentCDName) { DbType = DbType.String });
                        cmd.Parameters.Add(new MySqlParameter("@shohinSizeName", entity.ShohinSizeName) { DbType = DbType.String });
                        cmd.Parameters.Add(new MySqlParameter("@shohinName", entity.ShohinName) { DbType = DbType.String });
                        cmd.Parameters.Add(new MySqlParameter("@tanka", entity.Tanka) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@suryo", entity.Suryo) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@unit", entity.Unit) { DbType = DbType.String });
                        cmd.Parameters.Add(new MySqlParameter("@bdCount", entity.BDCount) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@manualFlg", entity.ManualFlg) { DbType = DbType.Int32 });
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            // ワークテーブル登録（経費）
            this.AddWorkTableExpence(estimaID);
        }
        /// <summary>
        /// ワークテーブル削除
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <param name="meisaiNo"></param>
        internal void DelWorkTableEstimaDetail(
              string tempEstimaID
            , int meisaiNo = -1
            , int meisaiType = -1
            )
        {
            string cmOutMeisaiNo = CommonUtil.GetCmout(meisaiNo == -1);
            string cmOutMeisaiType = CommonUtil.GetCmout(meisaiType == -1);

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM WX01TempEstimaDetail ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"{cmOutMeisaiNo}AND MeisaiNo     = @meisaiNo ");
            sb.AppendLine($"{cmOutMeisaiType}AND MeisaiType   = @meisaiType ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo", meisaiNo) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiType", meisaiType) { DbType = DbType.Int32 });
                    cmd.ExecuteNonQuery();
                }
            }
            // ワークテーブル削除（経費）
            this.DelWorkTableExpence(tempEstimaID, meisaiNo);
        }
        /// <summary>
        /// ワークテーブル登録（経費）
        /// </summary>
        
        /// <param name="estimaID"></param>
        internal void AddWorkTableExpence(string estimaID)
        {
            string tempEstimaID = estimaID;
            if (!this.ValidateExistWorkTableExpence(tempEstimaID))
            {
                return;
            }
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO WX02TempEstimaRate ");
            sb.AppendLine($"SELECT DISTINCT ");
            sb.AppendLine($"    @tempEstimaID ");
            sb.AppendLine($",   EX02.MeisaiNo ");
            sb.AppendLine($",   EX02.ConsumptionRate ");
            sb.AppendLine($",   EX02.InspectionRate ");
            sb.AppendLine($",   EX02.ExpenceRate ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX02EstimaBody AS EX02 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EX02.EstimaID = @estimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// ワークテーブル登録（経費）が存在するか？
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <returns></returns>
        internal bool ValidateExistWorkTableExpence(string tempEstimaID)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    COUNT(*)");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    WX02TempEstimaRate AS WX02 ");
            sb.AppendLine($"WHERE  ");
            sb.AppendLine($"    WX02.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($";  ");

            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return CommonUtil.ToInteger(dr[0]) == 0;
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// ワークテーブル削除（経費）
        /// </summary>
        /// <param name=""></param>
        /// <param name="tempEstimaID"></param>
        /// <param name="meisaiNo"></param>
        internal void DelWorkTableExpence(
              string tempEstimaID
            , int meisaiNo = -1
            )
        {
            string cmOutMeisaiNo = CommonUtil.GetCmout(meisaiNo == -1);

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM WX02TempEstimaRate ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"{cmOutMeisaiNo}AND MeisaiNo     = @meisaiNo ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo", meisaiNo) { DbType = DbType.Int32 });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion
    }
}
