﻿using Estima.Forms.Dialog.DE0040;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Report;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Application.EST.EST0010.Business
{
    internal class EST0010_FormLogic
    {
        #region Property
        /// <summary>
        /// 帳票ID
        /// </summary>
        private string EstimaID { get; set; }
        /// <summary>
        /// 画面モード
        /// </summary>
        public Enums.ScreenModeEditEstima ScreenMode { get; set; }
        #endregion

        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private EST0010_EditEstima Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private readonly LoginInfoDto LoginInfo = new LoginInfoDto();
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// 個別サービス
        /// </summary>
        private readonly EST0010_Service PersonalService = new EST0010_Service();
        /// <summary>
        /// 帳票作成ロジック
        /// </summary>
        private readonly CreateEstimaLogic CreateEstimaLogic = new CreateEstimaLogic();
        /// <summary>
        /// 列ヘッダdto
        /// </summary>
        private static EST0010_GridDtoBodys CellHeaderNameDto_Bod = new EST0010_GridDtoBodys();
        private static EST1010_GridDtoConditions CellHeaderNameDto_Con = new EST1010_GridDtoConditions();
        private readonly string[] EditableGridCells = new string[]
        {
              nameof(CellHeaderNameDto_Con.Status_Con)
            , nameof(CellHeaderNameDto_Con.Condition_Con)
        };
        /// <summary>
        /// 削除明細リスト
        /// </summary>
        private List<EST0010_GridDtoBodys> DelBodyDtoList = new List<EST0010_GridDtoBodys>();
        /// <summary>
        /// 条件の最大数
        /// </summary>
        private const int MaxConditionCount = 15;
        /// <summary>
        /// 編集フラグ
        /// </summary>
        private bool EditedFlg = false;
        /// <summary>
        /// 明細更新フラグ
        /// </summary>
        private bool MeisaiUpdFlg = false;
        /// <summary>
        /// 複製帳票ID
        /// </summary>
        private string EstimaIDForCopy = string.Empty;
        /// <summary>
        /// エクスプローラのexe名
        /// </summary>
        private const string ExplorerExeName = "EXPLORER.EXE";
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        /// <param name="estimaID"></param>
        /// <param name="estimaIDForCopy"></param>
        internal EST0010_FormLogic(
              EST0010_EditEstima body
            , LoginInfoDto loginInfo
            , Enums.ScreenModeEditEstima screenMode
            , string estimaID
            , string estimaIDForCopy
            )
        {
            this.Body = body;
            this.LoginInfo = loginInfo;
            this.ScreenMode = screenMode;
            this.EstimaID = estimaID;
            this.EstimaIDForCopy = this.ScreenMode == Enums.ScreenModeEditEstima.Copy ? estimaIDForCopy : string.Empty;
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += this.BtnClearClickEvent;
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // フォーム終了
            this.Body.FormClosing += this.ClosingEvent;
            // 形式切替
            this.Body.rdoEstima.CheckedChanged += this.RdoEstimaCheckedChanged;
            // 精算方法切替
            this.Body.rdoActualPostage.CheckedChanged += (s, e) =>
            {
                this.Body.numPostage.Visible = !this.Body.rdoActualPostage.Checked;
                this.EditedFlg = true;
            };
            // 見積№と加工№
            this.Body.txtProcessingNum.Validated += CommonUtil.ZeroPaddingTextBox;
            // 追加ボタン押下
            this.Body.btnAdd.Click += this.BtnAddClickEvent;
            // テンプレートボタン押下
            this.Body.btnTemplete.Click += this.BtnTempleteClickEvent;
            // 更新ボタン押下
            this.Body.btnUpdate.Click += this.BtnUpdClickEvent;
            // 発行ボタン押下
            this.Body.btnHakko.Click += this.BtnHakkoClickEvent;
            //// CSV出力ボタン押下
            //this.Body.btnCSV.Click += (s, e) => { };
            // グリッドRowPostPaint
            this.Body.dgvBodys.RowPostPaint += (s, e) => { GridRowUtil<EST0010_GridDtoBodys>.SetRowNum(s, e); };
            this.Body.dgvConditions.RowPostPaint += (s, e) => { GridRowUtil<EST1010_GridDtoConditions>.SetRowNum(s, e); };
            // グリッドCellEnter
            this.Body.dgvConditions.CellEnter += this.GridCelEnter;
            // グリッドセルクリック
            this.Body.dgvBodys.CellClick += this.GridCellClick;
            // グリッドセルダブルクリック
            this.Body.dgvBodys.CellDoubleClick += this.GridCellDoubleClickEvent;
            // グリッドCellValueChanged
            this.Body.dgvBodys.CellValueChanged += this.GridCellValueChangedBody;
            this.Body.dgvConditions.CellValueChanged += this.GridCellValueChangedCondition;
            // グリッドCellPainting
            this.Body.dgvBodys.CellPainting += this.GridCellPainting;
            // 仕切率ValueChanged
            this.Body.numRatePlumbing.ValueChanged += (s, e) => { this.CalcKingaku(); };
            this.Body.numRateHousing.ValueChanged += (s, e) => { this.CalcKingaku(); };
            // コントロールの値変更イベント
            this.ManageControlValueChanged(true);
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.I:
                        this.Body.btnAdd.Focus();
                        this.Body.btnAdd.PerformClick();
                        break;
                    case Keys.T:
                        this.Body.btnTemplete.Focus();
                        this.Body.btnTemplete.PerformClick();
                        break;
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.O:
                        this.Body.btnUpdate.Focus();
                        this.Body.btnUpdate.PerformClick();
                        break;
                    case Keys.H:
                        this.Body.btnHakko.Focus();
                        this.Body.btnHakko.PerformClick();
                        break;
                    case Keys.C:
                        this.Body.btnCSV.Focus();
                        this.Body.btnCSV.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// コントロールの値変更イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ControlValueChanged(
              object s
            , EventArgs e
            )
        {
            this.EditedFlg = true;
        }
        /// <summary>
        /// グリッドCellPainting
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellPainting(
              object s
            , DataGridViewCellPaintingEventArgs e
            )
        {
            if (this.Body.dgvBodys.Rows.Count == 0)
            {
                return;
            }
            if (e.RowIndex != this.Body.dgvBodys.Rows.Count - 1)
            {
                return;
            }
            DataGridViewRow r = this.Body.dgvBodys.Rows[e.RowIndex];
            foreach (DataGridViewCell c in r.Cells)
            {
                c.ReadOnly = true;
                c.Style.BackColor = Colors.BackColorBtnReadOnly;
                if (c.OwningColumn.Name == nameof(CellHeaderNameDto_Bod.ShohinName))
                {
                    c.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
            }
            e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.InsetDouble;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCelEnter(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            var dgv = (DataGridView)s;
            var tgtCol = dgv.Columns[e.ColumnIndex];
            if (tgtCol == null)
            {
                return;
            }
            // IMEモードを設定
            dgv.ImeMode = ImeMode.Hiragana;
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChangedCondition(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            // イベント一旦削除
            var dgv = (DataGridView)s;
            dgv.CellValueChanged -= this.GridCellValueChangedCondition;

            try
            {
                DataGridViewRow r = dgv.Rows[e.RowIndex];
                DataGridViewCell c = r.Cells[e.ColumnIndex];
                string tgtCellName = c.OwningColumn.Name;
                var tgtCellVal = c.Value;

                if (this.EditableGridCells.Contains(tgtCellName))
                {
                    // 値のセット
                    c.Value = tgtCellVal;

                    // セル背景色
                    c.Style.BackColor = Colors.BackColorCellEdited;

                    // ステータス更新
                    string identifyCol = CommonUtil.TostringNullForbid(r.Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value);
                    string status = CommonUtil.TostringNullForbid(r.Cells[nameof(CellHeaderNameDto_Con.Status_Con)].Value);
                    if (!string.IsNullOrEmpty(identifyCol)
                        && (status == Enums.EditStatus.Show.ToString() || status == Enums.EditStatus.Update.ToString()))
                    {
                        r.Cells[nameof(CellHeaderNameDto_Con.Status_Con)].Value = Enums.EditStatus.Update;
                    }
                    else
                    {
                        r.Cells[nameof(CellHeaderNameDto_Con.Status_Con)].Value = Enums.EditStatus.Insert;
                    }
                }
                // 使用可否切り替え（更新ボタン）
                this.SwitchEnabled();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                // イベント回復
                dgv.CellValueChanged += this.GridCellValueChangedCondition;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChangedBody(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            // イベント一旦削除
            var dgv = (DataGridView)s;
            dgv.CellValueChanged -= this.GridCellValueChangedBody;
            try
            {
                DataGridViewRow r = dgv.Rows[e.RowIndex];
                DataGridViewCell c = r.Cells[e.ColumnIndex];
                string tgtCellName = c.OwningColumn.Name;
                var tgtCellVal = c.Value;

                if (tgtCellName == nameof(CellHeaderNameDto_Bod.ShohinName))
                {
                    // 値のセット
                    c.Value = tgtCellVal;

                    // セル背景色
                    c.Style.BackColor = Colors.BackColorCellEdited;

                    // ステータス更新
                    string identifyCol = CommonUtil.TostringNullForbid(r.Cells[nameof(CellHeaderNameDto_Bod.MeisaiNo)].Value);
                    string status = CommonUtil.TostringNullForbid(r.Cells[nameof(CellHeaderNameDto_Bod.Status)].Value);
                    if (!string.IsNullOrEmpty(identifyCol)
                        && (status == Enums.EditStatus.Show.ToString() || status == Enums.EditStatus.Update.ToString()))
                    {
                        r.Cells[nameof(CellHeaderNameDto_Bod.Status)].Value = Enums.EditStatus.Update;
                    }
                    else
                    {
                        r.Cells[nameof(CellHeaderNameDto_Bod.Status)].Value = Enums.EditStatus.Insert;
                    }
                }
                // 使用可否切り替え（更新ボタン）
                this.EditedFlg = true;
                this.SwitchEnabled();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                // イベント回復
                dgv.CellValueChanged += this.GridCellValueChangedBody;
            }
        }
        /// <summary>
        /// グリッドセルクリック
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellClick(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            if (dgv.ReadOnly)
            {
                return;
            }
            if (dgv.DataSource == null)
            {
                return;
            }
            var rowDto = GridRowUtil<EST0010_GridDtoBodys>.GetRowModel(dgv.Rows[e.RowIndex]);
            if (rowDto.SummarryRowFlg)
            {
                return;
            }
            try
            {
                switch (dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningColumn.Name)
                {
                    case nameof(CellHeaderNameDto_Bod.BtnShow):
                        // 表示ボタン押下
                        this.GridButtonClickEventShow(s, e);
                        break;
                    case nameof(CellHeaderNameDto_Bod.BtnDel):
                        // 削除ボタン押下
                        this.GridButtonClickEventDelete(s, e);
                        break;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// グリッドボタン押下処理（表示）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridButtonClickEventShow(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            int rowIndex = e.RowIndex;

            // 明細内訳画面表示
            var rowDto = GridRowUtil<EST0010_GridDtoBodys>.GetRowModel(((DataGridView)s).Rows[e.RowIndex]);
            var drMeisai = new DE0040_EstimaDetailDialog(
                  this.LoginInfo
                , this.EstimaID
                , CommonUtil.ToInteger(rowDto.MeisaiNo)
                , rowDto.MeisaiType
                , rowDto.ConsumptionRate
                , rowDto.InspectionRate
                , rowDto.ExpenceRate
                , this.ScreenMode == Enums.ScreenModeEditEstima.ReadOnly
                , this.Body.dtpHakkoDate.Value.ToString(Formats.YYYYMM)
            );
            if (drMeisai.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            this.MeisaiUpdFlg = true;
            this.EditedFlg = false;

            // ワークテーブルから明細取得
            var dataSource = this.PersonalService.SelWorkTableEstimaDetail(
                  this.EstimaID
                , this.DelBodyDtoList
            );
            if (dataSource.Any())
            {
                // グリッドデータ取得
                var datas = (BindingList<EST0010_GridDtoBodys>)this.Body.dgvBodys.DataSource;
                // 小計行
                var summaryRowDto = dataSource[dataSource.Count - 1];
                if (datas == null)
                {
                    datas = new BindingList<EST0010_GridDtoBodys>();
                    datas.Add(summaryRowDto);
                }
                else
                {
                    datas.RemoveAt(datas.Count - 1);
                    datas.Add(summaryRowDto);
                }
                // 追加行
                var editRowDto = dataSource[rowIndex];
                editRowDto.Status = Enums.EditStatus.Update;
                datas.RemoveAt(rowIndex);
                datas.Insert(rowIndex, editRowDto);
                this.Body.dgvBodys.DataSource = datas;

                // セル背景色
                DataGridViewRow editRow = this.Body.dgvBodys.Rows[rowIndex];
                foreach (DataGridViewCell c in editRow.Cells)
                {
                    switch (c.OwningColumn.Name)
                    {
                        case nameof(CellHeaderNameDto_Bod.ShohinName):
                            c.ReadOnly = false;
                            c.Style.BackColor = Colors.BackColorCellEdited;
                            break;
                        default:
                            c.ReadOnly = true;
                            c.Style.BackColor = Colors.BackColorBtnReadOnly;
                            break;
                    }
                }
            }
            // 金額計算
            this.CalcKingaku();

            // 使用可否切替
            this.SwitchEnabled();
        }
        /// <summary>
        /// グリッドボタン押下処理（削除）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridButtonClickEventDelete(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (CommonUtil.ShowInfoMsgOKCancel(EST0010_Messages.MsgAskDelete) != DialogResult.OK)
            {
                return;
            }
            this.EditedFlg = true;

            // 削除明細追加
            var dgv = (DataGridView)s;
            DataGridViewRow r = dgv.Rows[e.RowIndex];
            var dto = GridRowUtil<EST0010_GridDtoBodys>.GetRowModel(r);
            this.DelBodyDtoList.Add(dto);

            // ワークテーブル削除
            this.PersonalService.DelWorkTableEstimaDetail(
                  this.EstimaID
                , CommonUtil.ToInteger(dto.MeisaiNo)
            );
            // 明細削除処理
            dgv.Rows.Remove(r);

            // 明細行がすべて削除された場合、グリッドの行全体をクリアする
            if (dgv.Rows.Count == 1)
            {
                var lastDto = GridRowUtil<EST0010_GridDtoBodys>.GetRowModel(dgv.Rows[0]);
                if (lastDto.SummarryRowFlg)
                {
                    dgv.Rows.Clear();
                }
            }
            // ワークテーブルから明細取得
            var dataSource = this.PersonalService.SelWorkTableEstimaDetail(
                  this.EstimaID
                , this.DelBodyDtoList
            );
            if (dataSource.Any())
            {
                // グリッドデータ取得
                var datas = (BindingList<EST0010_GridDtoBodys>)this.Body.dgvBodys.DataSource;
                // 小計行
                var summaryRowDto = dataSource[dataSource.Count - 1];
                if (datas == null)
                {
                    datas = new BindingList<EST0010_GridDtoBodys>();
                    datas.Add(summaryRowDto);
                }
                else
                {
                    datas.RemoveAt(datas.Count - 1);
                    datas.Add(summaryRowDto);
                }
            }
            // 明細番号再セット
            int meisaiNoDisp = 1;
            foreach (DataGridViewRow row in this.Body.dgvBodys.Rows)
            {
                var rowDto = GridRowUtil<EST0010_GridDtoBodys>.GetRowModel(row);
                if (rowDto.BtnShow == "-")
                {
                    continue;
                }
                row.Cells[nameof(CellHeaderNameDto_Bod.MeisaiNoDisp)].Value = meisaiNoDisp.ToString();
                meisaiNoDisp++;
            }
            // 金額計算
            this.CalcKingaku();
        }
        /// <summary>
        /// グリッドCellDoubleClick
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellDoubleClickEvent(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            var dgv = ((DataGridView)s);
            dgv.Rows[e.RowIndex].Selected = true;
        }
        /// <summary>
        /// 見積区分ラジオボタンCheckedChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void RdoEstimaCheckedChanged(
              object s
            , EventArgs e
            )
        {
            if (this.ScreenMode == Enums.ScreenModeEditEstima.Edit)
            {
                return;
            }
            this.SwitchFormat();
        }
        /// <summary>
        /// 明細追加ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnAddClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                // 明細内訳画面表示
                var drMeisai = new DE0040_EstimaDetailDialog(
                      this.LoginInfo
                    , this.EstimaID
                    , this.PersonalService.GetNextMeisaiNo(this.EstimaID, this.DelBodyDtoList)
                    , 1
                    , Config.ConsumptionRate
                    , Config.InspectionRate
                    , Config.ExpenceRate
                    , false
                    , this.Body.dtpHakkoDate.Value.ToString(Formats.YYYYMM)
                );
                if (drMeisai.ShowDialog() != DialogResult.OK)
                {
                    return;
                };
                this.MeisaiUpdFlg = true;
                this.EditedFlg = true;

                // グリッド表示
                var dataSource = this.PersonalService.SelWorkTableEstimaDetail(
                      this.EstimaID
                    , this.DelBodyDtoList
                );
                if (dataSource.Any())
                {
                    // グリッドデータ取得
                    var datas = (BindingList<EST0010_GridDtoBodys>)this.Body.dgvBodys.DataSource;
                    // 小計行
                    var summaryRowDto = dataSource[dataSource.Count - 1];
                    if (datas == null)
                    {
                        datas = new BindingList<EST0010_GridDtoBodys>();
                        datas.Add(summaryRowDto);
                    }
                    else
                    {
                        if (0 < datas.Count)
                        {
                            datas.RemoveAt(datas.Count - 1);
                        }
                        datas.Add(summaryRowDto);
                    }
                    // 追加行
                    int addRowIndex = dataSource.Count - 2;
                    var addRowDto = dataSource[addRowIndex];
                    addRowDto.Status = Enums.EditStatus.Insert;
                    datas.Insert(addRowIndex, addRowDto);
                    this.Body.dgvBodys.DataSource = datas;

                    // セル背景色
                    DataGridViewRow addRow = this.Body.dgvBodys.Rows[addRowIndex];
                    foreach (DataGridViewCell c in addRow.Cells)
                    {
                        if (c.OwningColumn.Name == nameof(CellHeaderNameDto_Bod.ShohinName))
                        {
                            c.ReadOnly = false;
                            c.Style.BackColor = Colors.BackColorCellEdited;
                        }
                        else
                        {
                            c.ReadOnly = true;
                            c.Style.BackColor = Colors.BackColorBtnReadOnly;
                        }
                    }
                }
                this.Body.dgvBodys.Show();

                // 金額計算
                this.CalcKingaku();

                // 使用可否切替
                this.SwitchEnabled();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// テンプレートボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnTempleteClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                int estimaKbn = this.Body.rdoEstima.Checked ? (int)Enums.EstimaKbn.Estimate : (int)Enums.EstimaKbn.Invoice;
                var conditions = new List<EST1010_GridDtoConditions>();
                CommonUtil.ShowProgressDialog(() =>
                {
                    conditions = this.PersonalService.GetTemplete(estimaKbn);
                });
                if (!conditions.Any())
                {
                    CommonUtil.ShowErrorMsg(EST0010_Messages.MsgErrorNotExistDatas);
                    return;
                }
                this.Body.dgvConditions.Rows.Clear();
                for (int i = 0; i < MaxConditionCount; i++)
                {
                    this.Body.dgvConditions.Rows.Add();
                }
                int index = 0;
                foreach (var condition in conditions)
                {
                    this.Body.dgvConditions.Rows[index].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = condition.Condition_Con;
                    index++;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// クリアボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnClearClickEvent(
              object s
            , EventArgs e
            )
        {
            if (CommonUtil.ShowInfoMsgOKCancel(EST0010_Messages.MsgAskClear) != DialogResult.OK)
            {
                return;
            }
            if (!this.ValidateUnUpdateDataExist())
            {
                var drConfirm = CommonUtil.ShowInfoExcramationOKCancel(EST0010_Messages.MsgAskExistUnSavedDatas);
                if (drConfirm != DialogResult.OK)
                {
                    return;
                }
            }
            if (this.MeisaiUpdFlg && !this.PersonalService.ValidateExistWorkTableEstimaDetail(this.EstimaID))
            {
                var drConfirm = CommonUtil.ShowInfoExcramationOKCancel(EST0010_Messages.MsgAskExistWorkTable);
                if (drConfirm != DialogResult.OK)
                {
                    return;
                }
            }
            try
            {
                // ワークテーブル削除
                this.PersonalService.DelWorkTableEstimaDetail(this.EstimaID);
                this.PersonalService.DelWorkTableEstimaDetail(this.EstimaIDForCopy);
                // ワークテーブル登録
                switch (this.ScreenMode)
                {
                    case Enums.ScreenModeEditEstima.ReadOnly:
                    case Enums.ScreenModeEditEstima.Edit:
                    case Enums.ScreenModeEditEstima.Copy:
                        this.PersonalService.AddWorkTable(this.DelBodyDtoList, this.EstimaID, this.EstimaIDForCopy);
                        break;
                }
                // 初期化処理
                this.Init();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            bool cancel = false;
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                if (CommonUtil.ShowInfoMsgOKCancel(EST0010_Messages.MsgAskFinish) != DialogResult.OK)
                {
                    cancel = true;
                    return;
                }
                if (!this.ValidateUnUpdateDataExist())
                {
                    var drConfirm = CommonUtil.ShowInfoExcramationOKCancel(EST0010_Messages.MsgAskExistUnSavedDatas);
                    if (drConfirm != DialogResult.OK)
                    {
                        cancel = true;
                        return;
                    }
                }
                switch (this.ScreenMode)
                {
                    case Enums.ScreenModeEditEstima.Copy:
                        if (CommonUtil.ShowInfoExcramationOKCancel(EST0010_Messages.MsgAskCopyDataNotSaved) != DialogResult.OK)
                        {
                            cancel = true;
                            return;
                        }
                        break;
                }
                this.PersonalService.DelWorkTableEstimaDetail(this.EstimaID);
                this.PersonalService.DelWorkTableEstimaDetail(this.EstimaIDForCopy);
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// フォーム終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                if (CommonUtil.ShowInfoMsgOKCancel(EST0010_Messages.MsgAskFinish) != DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
                if (!this.ValidateUnUpdateDataExist())
                {
                    var drConfirm = CommonUtil.ShowInfoExcramationOKCancel(EST0010_Messages.MsgAskExistUnSavedDatas);
                    if (drConfirm != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                if (!this.ClosingEventByScreenMode(s, e))
                {
                    return;
                }
                this.PersonalService.DelWorkTableEstimaDetail(this.EstimaID);
                this.PersonalService.DelWorkTableEstimaDetail(this.EstimaIDForCopy);
                this.Body.Close();
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// フォーム終了イベント（画面モードに応じて処理分岐）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private bool ClosingEventByScreenMode(
              object s
            , FormClosingEventArgs e
            )
        {
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeEditEstima.Add:
                case Enums.ScreenModeEditEstima.Edit:
                    if (this.MeisaiUpdFlg && !this.PersonalService.ValidateExistWorkTableEstimaDetail(this.EstimaID))
                    {
                        if (CommonUtil.ShowInfoExcramationOKCancel(EST0010_Messages.MsgAskExistWorkTable) != DialogResult.OK)
                        {
                            e.Cancel = true;
                            return false;
                        }
                    }
                    break;
                case Enums.ScreenModeEditEstima.Copy:
                    if (CommonUtil.ShowInfoExcramationOKCancel(EST0010_Messages.MsgAskCopyDataNotSaved) != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return false;
                    }
                    break;
            }
            return true;
        }
        /// <summary>
        /// 更新ボタン押下処理
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnUpdClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                if (!this.ValidateUpdate())
                {
                    return;
                }
                if (CommonUtil.ShowInfoMsgOKCancel(EST0010_Messages.MsgAskUpdate) != DialogResult.OK)
                {
                    return;
                }
                this.UpdLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 発行ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnHakkoClickEvent(
              object s
            , EventArgs e
            )
        {
            // Validate①
            if (!this.ValidateUnUpdateDataExist())
            {
                CommonUtil.ShowErrorMsg(EST0010_Messages.MsgErrorNotSaved);
                return;
            }
            // 出力フォルダ選択
            if (!SelectFolderUtil.ShowDialog())
            {
                return;
            }
            // Validate②
            string filePath = this.CreateEstimaLogic.GetFilePath(this.EstimaID, SelectFolderUtil.SelectedFolderPath);
            if (!CommonUtil.ValidateFileOpened(filePath))
            {
                CommonUtil.ShowErrorMsg(EST0010_Messages.MsgErrorFileAlreadyOpend);
                return;
            }
            // 確認ダイアログ
            if (CommonUtil.ShowInfoMsgOKCancel(EST0010_Messages.MsgAskHakko) != DialogResult.OK)
            {
                return;
            }
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                // 帳票作成
                bool success = true;
                CommonUtil.ShowProgressDialog(() =>
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogHakko}：{this.EstimaID}", Enums.LogStatusKbn.Start);
                    success = this.CreateEstimaLogic.Main(
                          this.EstimaID
                        , SelectFolderUtil.SelectedFolderPath
                    );
                    EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogHakko}：{this.EstimaID}", Enums.LogStatusKbn.End);
                });
                if (!success)
                {
                    return;
                }
                // 確認ダイアログ
                if (CommonUtil.ShowInfoMsgOKCancel(EST0010_Messages.MsgAskOpenFile) != DialogResult.OK)
                {
                    return;
                }
                // 帳票オープン
                CommonUtil.ShowProgressDialog(() =>
                {
                    Process.Start(ExplorerExeName, this.CreateEstimaLogic.EstimaFullPath);
                });
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// 形式区分に応じた切替
        /// </summary>
        private void SwitchFormat()
        {
            bool isEstima = this.Body.rdoEstima.Checked;
            this.Body.lblOrderNum.Visible = !isEstima;
            this.Body.txtOrderNum.Visible = !isEstima;
            this.Body.lblProcessingNum.Text = isEstima ? "見積№" : "加工№";
            this.Body.grpCondition.Text = isEstima ? "御見積条件" : "御請求条件";
            this.Body.lblEstimateNumPrefix.Text = isEstima ? "MK" : string.Empty;
            this.Body.lblEstimateNumPrefix.Visible = isEstima;
            this.Body.txtProcessingNum.MaxLength = isEstima ? 5 : 7;
            this.Body.txtProcessingNum.Text = string.Empty;
        }
        /// <summary>
        /// 使用可否切替
        /// </summary>
        /// <param name="readOnly"></param>
        private void SwitchEnabled(bool readOnly = false)
        {
            if (this.ScreenMode != Enums.ScreenModeEditEstima.Add)
            {
                return;
            }
            this.Body.btnCSV.Enabled = this.EditedFlg;
            this.Body.btnHakko.Enabled = this.EditedFlg;
        }
        /// <summary>
        /// 金額計算
        /// </summary>
        private void CalcKingaku()
        {
            if (this.Body.dgvBodys.Rows.Count == 0)
            {
                this.Body.txtSumPrice.Text = "0";
                this.Body.txtSumShikiriProcessing.Text = "0";
                this.Body.txtSumShikiriHousing.Text = "0";
                this.Body.txtSumShikiri.Text = "0";
                return;
            }
            int sumProcessing = 0;
            int sumHousing = 0;
            foreach (DataGridViewRow r in this.Body.dgvBodys.Rows)
            {
                var dto = GridRowUtil<EST0010_GridDtoBodys>.GetRowModel(r);
                if (dto.SummarryRowFlg)
                {
                    continue;
                }
                sumProcessing += CommonUtil.ToInteger(dto.PlumbingKingaku.Replace(",", string.Empty));
                sumProcessing += CommonUtil.ToInteger(dto.ProcessingKingaku.Replace(",", string.Empty));
                sumHousing += CommonUtil.ToInteger(dto.HousingKingaku.Replace(",", string.Empty));
            }
            this.Body.txtSumPrice.Text = (sumProcessing + sumHousing).ToString("#,0");
            this.Body.txtSumShikiriProcessing.Text = Math.Round(
                  (double)(sumProcessing * this.Body.numRatePlumbing.Value)
                , 0
                , MidpointRounding.AwayFromZero
            ).ToString("#,0");
            this.Body.txtSumShikiriHousing.Text = Math.Round(
                  (double)(sumHousing * this.Body.numRateHousing.Value)
                , 0
                , MidpointRounding.AwayFromZero
            ).ToString("#,0");
            int sumShikiri = CommonUtil.ToInteger(this.Body.txtSumShikiriProcessing.Text.Replace(",", string.Empty))
                             + CommonUtil.ToInteger(this.Body.txtSumShikiriHousing.Text.Replace(",", string.Empty));
            this.Body.txtSumShikiri.Text = sumShikiri.ToString("#,0");
        }
        /// <summary>
        /// 更新処理
        /// </summary>
        private void UpdLogic()
        {
            // ヘッダーデータ作成
            var header = this.GetEstimaHeaderEntityFromScreen();
            // 明細データ作成
            var bodies = this.GetEstimaBodyEntitiesFromGrid();
            // プログレスダイアログ処理
            var mstAddressOfficeDatas = new List<MX06AddressOffice>();
            CommonUtil.ShowProgressDialog(() => 
            {
                EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.Start);
                switch (this.ScreenMode)
                {
                    case Enums.ScreenModeEditEstima.Add:
                        this.UpdLogicAdd(header, bodies);
                        break;
                    case Enums.ScreenModeEditEstima.Edit:
                        this.UpdLogicUpdate(header, bodies);
                        break;
                    case Enums.ScreenModeEditEstima.Copy:
                        this.UpdLogicCopy(header, bodies);
                        break;
                }
                // 宛先（営業所）取得
                mstAddressOfficeDatas = this.CommonService.SelMstAddressOffice();
                EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.End);
            });
            // Bodyクラスの更新フラグをONにする
            this.Body.Updated = true;
            // 明細更新フラグをOFFにする
            this.MeisaiUpdFlg = false;
            // 使用可否切替
            this.SwitchEnabled();
            // 削除明細一覧クリア
            this.DelBodyDtoList.Clear();
            // 完了メッセージ
            CommonUtil.ShowInfoMsgOK(EST0010_Messages.MsgInfoFinishUpdate);
            // コントロールの値変更イベント管理
            this.ManageControlValueChanged(false);
            // カーソル
            var preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            // データリフレッシュ
            try
            {
                // 登録済みの伝票データ表示処理
                this.ShowSelectedEstimaData(mstAddressOfficeDatas, true);
                if (this.ScreenMode == Enums.ScreenModeEditEstima.ReadOnly)
                {
                    // コントロールを使用不可にする
                    this.ControlsLock();
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                // カーソル
                Cursor.Current = preCursor;
                // コントロールの値変更イベント管理
                this.ManageControlValueChanged(true);
            }
        }
        /// <summary>
        /// グリッドから明細データ取得
        /// </summary>
        /// <returns></returns>
        private List<EX02EstimaBody> GetEstimaBodyEntitiesFromGrid()
        {
            var bodies = new List<EX02EstimaBody>();
            foreach (DataGridViewRow r in this.Body.dgvBodys.Rows)
            {
                var rowDto = GridRowUtil<EST0010_GridDtoBodys>.GetRowModel(r);
                if (rowDto.SummarryRowFlg)
                {
                    continue;
                }
                var addBody1 = new EX02EstimaBody()
                {
                    EstimaID = this.ScreenMode == Enums.ScreenModeEditEstima.Copy ? this.EstimaIDForCopy : this.EstimaID,
                    MeisaiNo = CommonUtil.ToInteger(rowDto.MeisaiNo),
                    MeisaiType = 1,
                    ShohinName = rowDto.ShohinName,
                    ConsumptionRate = rowDto.ConsumptionRate,
                    InspectionRate = rowDto.InspectionRate,
                    ExpenceRate = rowDto.ExpenceRate,
                };
                if (CommonUtil.ToInteger(rowDto.PlumbingKingaku) != 0)
                {
                    bodies.Add(addBody1);
                }
                var addBody2 = new EX02EstimaBody()
                {
                    EstimaID = this.ScreenMode == Enums.ScreenModeEditEstima.Copy ? this.EstimaIDForCopy : this.EstimaID,
                    MeisaiNo = CommonUtil.ToInteger(rowDto.MeisaiNo),
                    MeisaiType = 2,
                    ShohinName = rowDto.ShohinName,
                    ConsumptionRate = rowDto.ConsumptionRate,
                    InspectionRate = rowDto.InspectionRate,
                    ExpenceRate = rowDto.ExpenceRate,
                };
                if (CommonUtil.ToInteger(rowDto.HousingKingaku) != 0)
                {
                    bodies.Add(addBody2);
                }
                var addBody3 = new EX02EstimaBody()
                {
                    EstimaID = this.ScreenMode == Enums.ScreenModeEditEstima.Copy ? this.EstimaIDForCopy : this.EstimaID,
                    MeisaiNo = CommonUtil.ToInteger(rowDto.MeisaiNo),
                    MeisaiType = 3,
                    ShohinName = rowDto.ShohinName,
                    ConsumptionRate = rowDto.ConsumptionRate,
                    InspectionRate = rowDto.InspectionRate,
                    ExpenceRate = rowDto.ExpenceRate,
                };
                if (CommonUtil.ToInteger(rowDto.ProcessingKingaku) != 0)
                {
                    bodies.Add(addBody3);
                }
            }
            return bodies;
        }
        /// <summary>
        /// 更新処理（追加モード）
        /// </summary>
        /// <param name="header"></param>
        /// <param name="bodies"></param>
        private void UpdLogicAdd(
              EX01EstimaHeader header
            , List<EX02EstimaBody> bodies
            )
        {
            // ヘッダー登録
            this.PersonalService.AddEstimaHeader(header);
            // 明細内訳登録
            this.PersonalService.AddEstimaDetail(this.EstimaID);
            // 明細登録
            bodies.ForEach(n => this.PersonalService.AddEstimaBody(n));
            // ワークテーブル削除
            this.PersonalService.DelWorkTableEstimaDetail(this.EstimaID);
            // モード変更
            this.ScreenMode = Enums.ScreenModeEditEstima.Edit;
        }
        /// <summary>
        /// 更新処理（更新モード）
        /// </summary>
        /// <param name="header"></param>
        /// <param name="bodies"></param>
        private void UpdLogicUpdate(
              EX01EstimaHeader header
            , List<EX02EstimaBody> bodies
            )
        {
            // 明細削除
            this.PersonalService.DelEstimaBody(this.EstimaID);
            // 明細内訳削除
            this.PersonalService.DelEstimaDetail(this.EstimaID);
            // ヘッダー更新
            this.PersonalService.UpdEstimaHeader(header);
            // 明細内訳登録
            this.PersonalService.AddEstimaDetail(this.EstimaID);
            // 明細登録
            bodies.ForEach(n => this.PersonalService.AddEstimaBody(n));
            // ワークテーブル削除
            this.PersonalService.DelWorkTableEstimaDetail(this.EstimaID);
        }
        /// <summary>
        /// 更新処理（複製モード）
        /// </summary>
        /// <param name="header"></param>
        /// <param name="bodies"></param>
        private void UpdLogicCopy(
              EX01EstimaHeader header
            , List<EX02EstimaBody> bodies
            )
        {
            // IDセット
            header.EstimaID = this.EstimaIDForCopy;
            // ヘッダー登録
            this.PersonalService.AddEstimaHeader(header);
            // 明細内訳登録
            this.PersonalService.AddEstimaDetail(this.EstimaID, this.EstimaIDForCopy);
            // 明細登録
            bodies.ForEach(n =>
            {
                if (!this.DelBodyDtoList.Any(x => CommonUtil.ToInteger(x.MeisaiNo) == n.MeisaiNo))
                {
                    this.PersonalService.AddEstimaBody(n);
                }
            });
            // ワークテーブル削除
            this.PersonalService.DelWorkTableEstimaDetail(this.EstimaID);
            this.EstimaID = this.EstimaIDForCopy;
            this.ScreenMode = Enums.ScreenModeEditEstima.Edit;
        }
        /// <summary>
        /// 画面コントロールからヘッダーデータ作成
        /// </summary>
        /// <returns></returns>
        private EX01EstimaHeader GetEstimaHeaderEntityFromScreen()
        {
            string estimaNo = string.Concat(this.Body.lblEstimateNumPrefix.Text, this.Body.txtProcessingNum.Text);
            var ret = new EX01EstimaHeader()
            {
                EstimaID = this.EstimaID,
                EstimaKbn = this.Body.rdoEstima.Checked ? (int)Enums.EstimaKbn.Estimate : (int)Enums.EstimaKbn.Invoice,
                EstimateNo = this.Body.rdoEstima.Checked ? estimaNo : string.Empty,
                OrderNo = this.Body.rdoSeikyu.Checked ? this.Body.txtOrderNum.Text : string.Empty,
                ProcessingNo = this.Body.rdoSeikyu.Checked ? this.Body.txtProcessingNum.Text : string.Empty,
                HakkoDate = this.Body.dtpHakkoDate.Value,
                LimitDate = this.Body.dtpLimitDate.Value,
                AddressCompany = this.Body.txtAddressCompany.Text,
                AddressOffice = this.Body.cmbAddressOffice.Text,
                AddressTanto = this.Body.txtAddressTanto.Text,
                AddressSuppliers = this.Body.txtAddressToriName.Text,
                FieldName = this.Body.txtFieldName.Text,
                FieldRange = this.Body.txtFieldRange.Text,
                DeadLine = this.Body.txtDeadLine.Text,
                PlumbingRate = (float)this.Body.numRatePlumbing.Value,
                HousingRate = (float)this.Body.numRateHousing.Value,
                Postage = null,
                Condition1 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[0].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition2 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[1].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition3 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[2].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition4 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[3].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition5 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[4].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition6 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[5].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition7 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[6].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition8 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[7].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition9 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[8].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition10 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[9].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition11 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[10].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition12 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[11].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition13 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[12].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition14 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[13].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
                Condition15 = CommonUtil.TostringNullForbid(this.Body.dgvConditions.Rows[14].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value),
            };
            if (this.Body.rdoPreRequestPostage.Checked)
            {
                ret.Postage = (int)this.Body.numPostage.Value;
            }
            return ret;
        }
        /// <summary>
        /// コントロール初期化
        /// </summary>
        /// <param name="mstAddressOfficeDatas"></param>
        private void InitControls(List<MX06AddressOffice> mstAddressOfficeDatas)
        {
            // コントロール初期化
            this.Body.cmbAddressOffice.Items.Clear();
            this.Body.cmbAddressOffice.Items.Add(string.Empty);
            mstAddressOfficeDatas.ForEach(n => this.Body.cmbAddressOffice.Items.Add(n.OfficeName));
            this.Body.cmbAddressOffice.Text = string.Empty;

            this.Body.txtAddressCompany.Text = string.Empty;
            this.Body.txtAddressTanto.Text = string.Empty;
            this.Body.txtOrderNum.Text = string.Empty;
            this.Body.txtProcessingNum.Text = string.Empty;
            this.Body.txtAddressToriName.Text = string.Empty;
            this.Body.dtpLimitDate.Value = this.Body.dtpHakkoDate.Value.AddDays(30);
            this.Body.txtFieldName.Text = string.Empty;
            this.Body.txtFieldRange.Text = string.Empty;
            this.Body.txtDeadLine.Text = string.Empty;
            this.Body.numRatePlumbing.Value = (decimal)0.25;
            this.Body.numRateHousing.Value = (decimal)0.36;
            this.Body.dgvBodys.Rows.Clear();
            this.Body.dgvConditions.Rows.Clear();
            for (int i = 0; i < MaxConditionCount; i++)
            {
                this.Body.dgvConditions.Rows.Add();
            }
            this.Body.dgvConditions.AllowUserToAddRows = false;
            this.Body.pnlFormat.Enabled = true;

            // 合計金額
            this.Body.txtSumPrice.Text = "0";
            this.Body.txtSumShikiriProcessing.Text = "0";
            this.Body.txtSumShikiriHousing.Text = "0";
            this.Body.txtSumShikiri.Text = "0";

            // 精算方法
            this.Body.rdoActualPostage.Checked = true;
            this.Body.numPostage.Visible = false;
            this.Body.numPostage.Value = 0;

            // 使用可否切り替え
            this.EditedFlg = false;
            this.Body.rdoEstima.Select();
            this.SwitchFormat();
            this.SwitchEnabled();

            // 伝票ID
            this.EstimaID = EstimaUtil.CreateEstimaID();

            // フォーカス
            this.Body.rdoEstima.Focus();
        }
        /// <summary>
        /// 登録済みの伝票データ表示処理
        /// </summary>
        /// <param name="mstAddressOfficeDatas"></param>
        /// <param name="finishUpdate"></param>
        private void ShowSelectedEstimaData(
              List<MX06AddressOffice> mstAddressOfficeDatas
            , bool finishUpdate = false
            )
        {
            // ヘッダー抽出
            var header = this.PersonalService.SelEstimaHeader(this.EstimaID, this.EstimaIDForCopy);
            // 明細内訳抽出
            var dataSourceBodys = this.PersonalService.SelEstimaBody(this.EstimaID, this.DelBodyDtoList);
            // フォーマット
            bool enableFormat = (!finishUpdate && this.ScreenMode == Enums.ScreenModeEditEstima.Copy);
            this.Body.rdoEstima.CheckedChanged -= (s, e) => { this.SwitchFormat(); };
            this.Body.rdoEstima.CheckedChanged -= (s, e) => { this.SwitchFormat(); };
            this.Body.rdoEstima.Checked = (header.EstimaKbn == (int)Enums.EstimaKbn.Estimate);
            this.Body.rdoSeikyu.Checked = !this.Body.rdoEstima.Checked;
            this.Body.pnlFormat.Enabled = enableFormat;
            // フラグ
            this.EditedFlg = false;
            this.SwitchFormat();
            // ボタン
            this.Body.btnHakko.Enabled = !enableFormat;
            this.Body.btnCSV.Enabled = false;
            // 発注№
            this.Body.txtOrderNum.Text = header.OrderNo;
            this.Body.txtOrderNum.Enabled = enableFormat;
            // 加工№
            if (header.EstimaKbn == (int)Enums.EstimaKbn.Estimate)
            {
                this.Body.txtProcessingNum.Text = header.EstimateNo.Substring(2, 5);
                this.Body.txtProcessingNum.Enabled = enableFormat;
            }
            else
            {
                this.Body.txtProcessingNum.Text = header.ProcessingNo; ;
            }
            // 発行日
            this.Body.dtpHakkoDate.Value = header.HakkoDate;
            // 有効期限
            this.Body.dtpLimitDate.Value = header.LimitDate;
            // 宛先（会社名）
            this.Body.txtAddressCompany.Text = header.AddressCompany;
            // 宛先（営業所）
            this.Body.cmbAddressOffice.Items.Clear();
            this.Body.cmbAddressOffice.Items.Add(string.Empty);
            mstAddressOfficeDatas.ForEach(n => this.Body.cmbAddressOffice.Items.Add(n.OfficeName));
            if (!this.Body.cmbAddressOffice.Items.Contains(header.AddressOffice)) { this.Body.cmbAddressOffice.Items.Add(header.AddressOffice); }
            this.Body.cmbAddressOffice.Text = header.AddressOffice;
            // 宛先（担当者）
            this.Body.txtAddressTanto.Text = header.AddressTanto;
            // 宛先（取引先）
            this.Body.txtAddressToriName.Text = header.AddressSuppliers;
            // 現場名
            this.Body.txtFieldName.Text = header.FieldName;
            // 工事範囲
            this.Body.txtFieldRange.Text = header.FieldRange;
            // 納期
            this.Body.txtDeadLine.Text = header.DeadLine;
            // 掛率（加工管）
            this.Body.numRatePlumbing.Value = (decimal)header.PlumbingRate;
            // 掛率（ハウジング）
            this.Body.numRateHousing.Value = (decimal)header.HousingRate;
            // 送料
            if (header.Postage == null)
            {
                this.Body.rdoActualPostage.Checked = true;
                this.Body.numPostage.Value = 0;
                this.Body.numPostage.Visible = false;
            }
            else
            {
                this.Body.rdoPreRequestPostage.Checked = true;
                this.Body.numPostage.Value = header.Postage.Value;
                this.Body.numPostage.Visible = true;
            }
            // 明細
            this.Body.dgvBodys.Rows.Clear();
            this.Body.dgvBodys.DataSource = dataSourceBodys;
            this.Body.dgvBodys.AllowUserToAddRows = false;
            if (this.ScreenMode == Enums.ScreenModeEditEstima.Add)
            {
                foreach (DataGridViewRow r in this.Body.dgvBodys.Rows)
                {
                    r.Cells[nameof(CellHeaderNameDto_Bod.ShohinName)].Style.BackColor = Colors.BackColorBtnReadOnly;
                }
            }
            this.Body.dgvBodys.Show();
            // 条件
            this.ShowSelectedEstimaConditions(header);
            // 金額計算
            this.CalcKingaku();
            // 画面モードに応じて処理分岐
            this.ManageFocusWorkTable();
        }
        /// <summary>
        /// フォーカスとワークテーブル管理
        /// </summary>
        private void ManageFocusWorkTable()
        {
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeEditEstima.ReadOnly:
                case Enums.ScreenModeEditEstima.Edit:
                    // フォーカス
                    if (this.Body.rdoEstima.Checked) { this.Body.dtpHakkoDate.Focus(); }
                    else { this.Body.txtProcessingNum.Focus(); }
                    // ワークテーブル
                    this.PersonalService.AddWorkTable(this.DelBodyDtoList, this.EstimaID, this.EstimaIDForCopy);
                    break;
                case Enums.ScreenModeEditEstima.Copy:
                    // フォーカス
                    if (this.Body.rdoEstima.Checked) { this.Body.rdoEstima.Focus(); }
                    else { this.Body.rdoSeikyu.Focus(); }
                    // ワークテーブル
                    this.PersonalService.AddWorkTable(this.DelBodyDtoList, this.EstimaID, this.EstimaIDForCopy);
                    break;
                default:
                    // フォーカス
                    if (this.Body.rdoEstima.Checked) { this.Body.dtpHakkoDate.Focus(); }
                    else { this.Body.txtProcessingNum.Focus(); }
                    break;
            }
        }
        /// <summary>
        /// 登録済みの条件一覧表示処理
        /// </summary>
        /// <param name="header"></param>
        private void ShowSelectedEstimaConditions(EX01EstimaHeader header)
        {
            this.Body.dgvConditions.Rows.Clear();
            for (int i = 0; i < MaxConditionCount; i++)
            {
                this.Body.dgvConditions.Rows.Add();
            }
            this.Body.dgvConditions.CellValueChanged -= this.GridCellValueChangedCondition;
            this.Body.dgvConditions.CellValueChanged -= this.GridCellValueChangedCondition;
            this.Body.dgvConditions.Rows[0].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition1;
            this.Body.dgvConditions.Rows[1].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition2;
            this.Body.dgvConditions.Rows[2].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition3;
            this.Body.dgvConditions.Rows[3].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition4;
            this.Body.dgvConditions.Rows[4].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition5;
            this.Body.dgvConditions.Rows[5].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition6;
            this.Body.dgvConditions.Rows[6].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition7;
            this.Body.dgvConditions.Rows[7].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition8;
            this.Body.dgvConditions.Rows[8].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition9;
            this.Body.dgvConditions.Rows[9].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition10;
            this.Body.dgvConditions.Rows[10].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition11;
            this.Body.dgvConditions.Rows[11].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition12;
            this.Body.dgvConditions.Rows[12].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition13;
            this.Body.dgvConditions.Rows[13].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition14;
            this.Body.dgvConditions.Rows[14].Cells[nameof(CellHeaderNameDto_Con.Condition_Con)].Value = header.Condition15;
            for (int i = 0; i < MaxConditionCount; i++)
            {
                this.Body.dgvConditions.Rows[i].Cells[nameof(CellHeaderNameDto_Con.Status_Con)].Value = Enums.EditStatus.Show;
            }
            this.Body.dgvConditions.AllowUserToAddRows = false;
            this.Body.dgvConditions.CellValueChanged += this.GridCellValueChangedCondition;
        }
        /// <summary>
        /// コントロールを使用不可にする
        /// </summary>
        private void ControlsLock()
        {
            this.Body.cmbAddressOffice.Enabled = false;
            this.Body.txtAddressCompany.Enabled = false;
            this.Body.txtAddressTanto.Enabled = false;
            this.Body.txtOrderNum.Enabled = false;
            this.Body.txtProcessingNum.Enabled = false;
            this.Body.txtAddressToriName.Enabled = false;
            this.Body.dtpHakkoDate.Enabled = false;
            this.Body.dtpLimitDate.Enabled = false;
            this.Body.txtFieldName.Enabled = false;
            this.Body.txtFieldRange.Enabled = false;
            this.Body.txtDeadLine.Enabled = false;
            this.Body.numRatePlumbing.Enabled = false;
            this.Body.numRateHousing.Enabled = false;
            this.Body.dgvBodys.Columns[nameof(CellHeaderNameDto_Bod.BtnDel)].Visible = false;
            this.Body.dgvBodys.Columns[nameof(CellHeaderNameDto_Bod.ShohinName)].DefaultCellStyle.BackColor = Colors.BackColorBtnReadOnly;
            this.Body.dgvBodys.Columns[nameof(CellHeaderNameDto_Bod.ShohinName)].ReadOnly = true;
            this.Body.dgvConditions.Columns[nameof(CellHeaderNameDto_Con.Condition_Con)].DefaultCellStyle.BackColor = Colors.BackColorBtnReadOnly;
            this.Body.dgvConditions.Columns[nameof(CellHeaderNameDto_Con.Condition_Con)].ReadOnly = true;
            this.Body.btnHakko.Enabled = false;
            this.Body.btnUpdate.Enabled = false;
            this.Body.btnAdd.Enabled = false;
            this.Body.btnTemplete.Enabled = false;
            this.Body.pnlPostage.Enabled = false;
        }
        /// <summary>
        /// コントロールの値変更イベント管理
        /// </summary>
        /// <param name="add"></param>
        private void ManageControlValueChanged(bool add)
        {
            if (add)
            {
                this.Body.txtProcessingNum.TextChanged += this.ControlValueChanged;
                this.Body.txtOrderNum.TextChanged += this.ControlValueChanged;
                this.Body.txtAddressCompany.TextChanged += this.ControlValueChanged;
                this.Body.txtAddressTanto.TextChanged += this.ControlValueChanged;
                this.Body.txtAddressToriName.TextChanged += this.ControlValueChanged;
                this.Body.txtDeadLine.TextChanged += this.ControlValueChanged;
                this.Body.txtFieldRange.TextChanged += this.ControlValueChanged;
                this.Body.txtFieldName.TextChanged += this.ControlValueChanged;
                this.Body.numRateHousing.TextChanged += this.ControlValueChanged;
                this.Body.numRatePlumbing.TextChanged += this.ControlValueChanged;
                this.Body.numPostage.TextChanged += this.ControlValueChanged;
                this.Body.cmbAddressOffice.TextChanged += this.ControlValueChanged;
                return;
            }
            this.Body.txtProcessingNum.TextChanged -= this.ControlValueChanged;
            this.Body.txtOrderNum.TextChanged -= this.ControlValueChanged;
            this.Body.txtAddressCompany.TextChanged -= this.ControlValueChanged;
            this.Body.txtAddressTanto.TextChanged -= this.ControlValueChanged;
            this.Body.txtAddressToriName.TextChanged -= this.ControlValueChanged;
            this.Body.txtDeadLine.TextChanged -= this.ControlValueChanged;
            this.Body.txtFieldRange.TextChanged -= this.ControlValueChanged;
            this.Body.txtFieldName.TextChanged -= this.ControlValueChanged;
            this.Body.numRateHousing.TextChanged -= this.ControlValueChanged;
            this.Body.numRatePlumbing.TextChanged -= this.ControlValueChanged;
            this.Body.numPostage.TextChanged -= this.ControlValueChanged;
            this.Body.cmbAddressOffice.TextChanged -= this.ControlValueChanged;
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新されていない項目が存在する？
        /// </summary>
        /// <returns></returns>
        private bool ValidateUnUpdateDataExist()
        {
            if (this.EditedFlg)
            {
                return false;
            }
            foreach (DataGridViewRow r in this.Body.dgvBodys.Rows)
            {
                // 更新されていない行が含まれる場合
                var rowDto = GridRowUtil<EST0010_GridDtoBodys>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.Insert || rowDto.Status == Enums.EditStatus.Update)
                {
                    return false;
                }
            }
            foreach (DataGridViewRow r in this.Body.dgvConditions.Rows)
            {
                // 更新されていない行が含まれる場合
                var rowDto = GridRowUtil<EST1010_GridDtoConditions>.GetRowModel(r);
                if (rowDto.Status_Con == Enums.EditStatus.Insert || rowDto.Status_Con == Enums.EditStatus.Update)
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <returns></returns>
        private bool ValidateUpdate()
        {
            if (!this.ValidateUpdateByScreenMode())
            {
                return false;
            }
            if (string.IsNullOrEmpty(this.Body.txtProcessingNum.Text))
            {
                this.Body.txtProcessingNum.Focus();
                CommonUtil.ShowErrorMsg(string.Format(
                      EST0010_Messages.MsgErrorRequired
                    , this.Body.lblProcessingNum.Text
                ));
                return false;
            }
            if (this.Body.dtpLimitDate.Value < this.Body.dtpHakkoDate.Value)
            {
                this.Body.dtpLimitDate.Focus();
                CommonUtil.ShowErrorMsg(string.Format(
                      EST0010_Messages.MsgErrorLargeSmallCollision
                    , this.Body.lblLimitDate.Text
                    , this.Body.lblHakkoDate.Text
                ));
                return false;
            }
            if (this.Body.numRatePlumbing.Value == 0)
            {
                this.Body.numRatePlumbing.Focus();
                CommonUtil.ShowErrorMsg(string.Format(
                      EST0010_Messages.MsgErrorLargeSmallCollision
                    , this.Body.lblRatePlumbing.Text
                    , 0
                ));
                return false;
            }
            if (this.Body.numRateHousing.Value == 0)
            {
                this.Body.numRateHousing.Focus();
                CommonUtil.ShowErrorMsg(string.Format(
                      EST0010_Messages.MsgErrorLargeSmallCollision
                    , this.Body.lblRateHousing.Text
                    , 0
                ));
                return false;
            }
            if (this.Body.dgvBodys.Rows.Count == 0)
            {
                this.Body.grpDetail.Focus();
                CommonUtil.ShowErrorMsg(string.Format(
                      EST0010_Messages.MsgErrorRequired
                    , this.Body.grpDetail.Text
                ));
                return false;
            }
            return true;
        }
        /// <summary>
        /// 更新時Validate（画面モードに応じて処理分岐）
        /// </summary>
        /// <returns></returns>
        private bool ValidateUpdateByScreenMode()
        {
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeEditEstima.Add:
                case Enums.ScreenModeEditEstima.Copy:
                    if (this.Body.rdoEstima.Checked)
                    {
                        string estimateNo = string.Concat(this.Body.lblEstimateNumPrefix.Text, this.Body.txtProcessingNum.Text);
                        if (!this.PersonalService.ValidateExistEstimateNo(estimateNo))
                        {
                            this.Body.txtProcessingNum.Focus();
                            CommonUtil.ShowErrorMsg(string.Format(EST0010_Messages.MsgErrorEstimateNoAlreadyExist, "見積№"));
                            return false;
                        }
                    }
                    if (this.Body.rdoSeikyu.Checked)
                    {
                        if (string.IsNullOrEmpty(this.Body.txtOrderNum.Text))
                        {
                            this.Body.txtOrderNum.Focus();
                            CommonUtil.ShowErrorMsg(string.Format(
                                  EST0010_Messages.MsgErrorRequired
                                , this.Body.lblOrderNum.Text
                            ));
                            return false;
                        }
                        if (!this.PersonalService.ValidateExistOrderNo(this.Body.txtOrderNum.Text))
                        {
                            this.Body.txtOrderNum.Focus();
                            CommonUtil.ShowErrorMsg(string.Format(EST0010_Messages.MsgErrorEstimateNoAlreadyExist, "発注№"));
                            return false;
                        }
                    }
                    break;
            }
            return true;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            // ワークテーブル削除
            this.PersonalService.DelWorkTableEstimaDetail(this.EstimaID);
            
            this.DelBodyDtoList.Clear();
            this.MeisaiUpdFlg = false;
            this.Body.dgvBodys.Columns[nameof(CellHeaderNameDto_Bod.PlumbingKingaku)].HeaderText = "小計金額\r\n(配管・継手)";
            this.Body.dgvBodys.Columns[nameof(CellHeaderNameDto_Bod.HousingKingaku)].HeaderText = "小計金額\r\n(ハウジング)";
            this.Body.dgvBodys.Columns[nameof(CellHeaderNameDto_Bod.ProcessingKingaku)].HeaderText = "小計金額\r\n(加工費)";

            // 宛先（営業所）取得
            var mstAddressOfficeDatas = this.CommonService.SelMstAddressOffice();
            switch (this.ScreenMode)
            {
                case Enums.ScreenModeEditEstima.Add:
                    // コントロール初期化
                    this.InitControls(mstAddressOfficeDatas);
                    break;
                case Enums.ScreenModeEditEstima.ReadOnly:
                    // ワークテーブル登録
                    this.PersonalService.AddWorkTable(this.DelBodyDtoList, this.EstimaID, this.EstimaIDForCopy);
                    // 登録済みの伝票データ表示処理
                    this.ShowSelectedEstimaData(mstAddressOfficeDatas);
                    // コントロールを使用不可にする
                    this.ControlsLock();
                    break;
                case Enums.ScreenModeEditEstima.Edit:
                    // ワークテーブル登録
                    this.PersonalService.AddWorkTable(this.DelBodyDtoList, this.EstimaID, this.EstimaIDForCopy);
                    // 登録済みの伝票データ表示処理
                    this.ShowSelectedEstimaData(mstAddressOfficeDatas);
                    break;
                case Enums.ScreenModeEditEstima.Copy:
                    // ワークテーブル登録
                    this.PersonalService.AddWorkTable(this.DelBodyDtoList, this.EstimaID, this.EstimaIDForCopy);
                    // 登録済みの伝票データ表示処理
                    this.ShowSelectedEstimaData(mstAddressOfficeDatas);
                    break;
            }
            // 編集フラグ
            this.EditedFlg = false;
        }
        #endregion
    }
}
