﻿using Estima.Forms.Application.EST.EST0010.Business;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Application.EST.EST0010
{
    public partial class EST0010_EditEstima : Form
    {
        /// <summary>
        /// 更新された？
        /// </summary>
        public bool Updated { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        /// <param name="estimaID"></param>
        /// <param name="estimaIDForCopy"></param>
        public EST0010_EditEstima(
              LoginInfoDto loginInfo
            , Enums.ScreenModeEditEstima screenMode
            , string estimaID
            , string estimaIDForCopy = ""
            )
        {
            this.InitializeComponent();
            try
            {
                new EST0010_FormLogic(
                      this
                    , loginInfo
                    , screenMode
                    , estimaID
                    , estimaIDForCopy
                );
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
