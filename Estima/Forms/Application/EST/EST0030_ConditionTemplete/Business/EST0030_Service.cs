﻿using EstimaLib.Const;
using EstimaLib.Entity;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;

namespace Estima.Forms.Application.EST.EST0030.Business
{
    internal class EST0030_Service
    {
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="estimaKbn"></param>
        /// <returns></returns>
        internal BindingList<EST0030_GridDto> GetGridData(int estimaKbn)
        {
            var ret = new BindingList<EST0030_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    EX05.ConditionVal ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX05ConditionTemplete AS EX05 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EX05.EstimaKbn = @estimaKbn ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    EX05.EstimaKbn ");
            sb.AppendLine($",   EX05.Seq ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaKbn", estimaKbn) { DbType = DbType.Int32 });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new EST0030_GridDto();
                            dto.ConditionVal = CommonUtil.TostringNullForbid(dr[nameof(dto.ConditionVal)]);
                            dto.Status = EstimaLib.Const.Enums.EditStatus.Show;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 条件更新
        /// </summary>
        /// <param name="updEntities"></param>
        /// <returns></returns>
        internal void UpdCondition(List<EX05ConditionTemplete> updEntities)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM");
            sb.AppendLine($"    EX05ConditionTemplete ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EstimaKbn = @estimaKbn ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaKbn", updEntities[0].EstimaKbn) { DbType = DbType.Int32 });
                    cmd.ExecuteNonQuery();
                }
            }
            foreach (var updEntity in updEntities)
            {
                // SQL文の作成
                sb = new StringBuilder();
                sb.AppendLine($"INSERT INTO EX05ConditionTemplete( ");
                sb.AppendLine($"    EstimaKbn ");
                sb.AppendLine($",   Seq ");
                sb.AppendLine($",   ConditionVal ");
                sb.AppendLine($",   AddIPAddress ");
                sb.AppendLine($",   AddHostName ");
                sb.AppendLine($",   AddDate ");
                sb.AppendLine($",   UpdIPAddress ");
                sb.AppendLine($",   UpdHostName ");
                sb.AppendLine($",   UpdDate ");
                sb.AppendLine($")VALUES( ");
                sb.AppendLine($"    @estimaKbn ");
                sb.AppendLine($",   @seq ");
                sb.AppendLine($",   @condition ");
                sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
                sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
                sb.AppendLine($",   CURRENT_TIMESTAMP ");
                sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
                sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
                sb.AppendLine($",   CURRENT_TIMESTAMP ");
                sb.AppendLine($"); ");
                using (var cn = Config.CreateSqlConnection())
                {
                    using (var cmd = cn.CreateCommand())
                    {
                        cn.Open();
                        cmd.CommandText = sb.ToString();
                        cmd.Parameters.Add(new MySqlParameter("@estimaKbn", updEntity.EstimaKbn) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@seq", updEntity.Seq) { DbType = DbType.Int32 });
                        cmd.Parameters.Add(new MySqlParameter("@condition", updEntity.ConditionVal) { DbType = DbType.String });
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
