﻿using EstimaLib.Const;

namespace Estima.Forms.Application.EST.EST0030.Business
{
    public class EST0030_GridDto
    {
        public EST0030_GridDto()
        {
            this.ConditionVal = string.Empty;
            this.Status = Enums.EditStatus.None;
        }
        public string ConditionVal { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
