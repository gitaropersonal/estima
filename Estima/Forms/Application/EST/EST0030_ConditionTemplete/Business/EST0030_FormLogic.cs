﻿using Estima.Forms.Application.EST.EST0010.Business;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Application.EST.EST0030.Business
{
    internal class EST0030_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private EST0030_ConditionTemplete Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private readonly LoginInfoDto LoginInfo = new LoginInfoDto();
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// 個別サービス
        /// </summary>
        private readonly EST0030_Service PersonalService = new EST0030_Service();
        /// <summary>
        /// 列ヘッダdto
        /// </summary>
        private static EST0030_GridDto CellHeaderNameDto_Con = new EST0030_GridDto();
        private readonly string[] EditableGridCells = new string[]
        {
              nameof(CellHeaderNameDto_Con.Status)
            , nameof(CellHeaderNameDto_Con.ConditionVal)
        };
        /// <summary>
        /// 更新フラグ
        /// </summary>
        private bool UpdateFlg = false;
        /// <summary>
        /// 条件の最大数
        /// </summary>
        private const int MaxConditionCount = 15;
        /// <summary>
        /// 初期化モード
        /// </summary>
        private enum InitMode
        {
            All = 0,
            Condition = 1,
        }
        /// <summary>
        /// 初期化モード
        /// </summary>
        private InitMode CurrInitMode = InitMode.All;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        internal EST0030_FormLogic(
              EST0030_ConditionTemplete body
            , LoginInfoDto lobinInfo
            )
        {
            this.Body = body;
            this.LoginInfo = lobinInfo;
            new InitControl(this.Body);
            this.Init();
            // 検索ボタン押下
            this.Body.btnSearch.Click += (s, e) =>
            {
                CommonUtil.EventWithValidateUpdExist(
                      new Action(() => this.BtnSearchClickEvent(s, e))
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 更新ボタン押下
            this.Body.btnUpdate.Click += this.BtnUpdClickEvent;
            // クリアボタン押下
            this.Body.btnClear.Click += (s, e) =>
            {
                CommonUtil.BtnClearClickEvent(
                      new Action(() => this.Init())
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了イベント
            this.Body.FormClosing += this.ClosingEvent;
            // グリッドRowPostPaint
            this.Body.dgvConditions.RowPostPaint += (s, e) => { GridRowUtil<EST0030_GridDto>.SetRowNum(s, e); };
            // グリッドCellValueChanged
            this.Body.dgvConditions.CellValueChanged += this.GridCellValueChanged;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        this.Body.btnSearch.Focus();
                        this.Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        this.Body.btnUpdate.Focus();
                        this.Body.btnUpdate.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCelEnter(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            var dgv = (DataGridView)s;
            var tgtCol = dgv.Columns[e.ColumnIndex];
            if (tgtCol == null)
            {
                return;
            }
            // IMEモードを設定
            dgv.ImeMode = ImeMode.Hiragana;
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            // イベント一旦削除
            var dgv = (DataGridView)s;
            dgv.CellValueChanged -= this.GridCellValueChanged;

            try
            {
                DataGridViewRow r = dgv.Rows[e.RowIndex];
                DataGridViewCell c = r.Cells[e.ColumnIndex];
                string tgtCellName = c.OwningColumn.Name;
                var tgtCellVal = c.Value;

                if (this.EditableGridCells.Contains(tgtCellName))
                {
                    // 値のセット
                    c.Value = tgtCellVal;

                    // セル背景色
                    c.Style.BackColor = Colors.BackColorCellEdited;

                    // ステータス更新
                    string identifyCol = CommonUtil.TostringNullForbid(r.Cells[nameof(CellHeaderNameDto_Con.ConditionVal)].Value);
                    string status = CommonUtil.TostringNullForbid(r.Cells[nameof(CellHeaderNameDto_Con.Status)].Value);
                    if (!string.IsNullOrEmpty(identifyCol)
                        && (status == Enums.EditStatus.Show.ToString() || status == Enums.EditStatus.Update.ToString()))
                    {
                        r.Cells[nameof(CellHeaderNameDto_Con.Status)].Value = Enums.EditStatus.Update;
                    }
                    else
                    {
                        r.Cells[nameof(CellHeaderNameDto_Con.Status)].Value = Enums.EditStatus.Insert;
                    }
                }
                // 使用可否切り替え（更新ボタン）
                this.UpdateFlg = true;
                this.SwitchEnabled();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                // イベント回復
                dgv.CellValueChanged += this.GridCellValueChanged;
            }
        }
        /// <summary>
        /// クリアボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnClearClickEvent(
              object s
            , EventArgs e
            )
        {
            if (CommonUtil.ShowInfoMsgOKCancel(EST0030_Messages.MsgAskClear) != DialogResult.OK)
            {
                return;
            }
            this.Init();
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                if (CommonUtil.ShowInfoMsgOKCancel(EST0030_Messages.MsgAskFinish) != DialogResult.OK)
                {
                    cancel = true;
                    return;
                }
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                if (CommonUtil.ShowInfoMsgOKCancel(EST0030_Messages.MsgAskFinish) != DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSearchClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try { this.SearchLogic(); }
            catch (Exception ex) { EstimaUtil.ShowErrorMsg(ex, this.Body.Text); }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// 更新ボタン押下処理
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnUpdClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                if (CommonUtil.ShowInfoMsgOKCancel(EST0010_Messages.MsgAskUpdate) != DialogResult.OK) { return; }
                this.UpdLogic();
                this.BtnSearchClickEvent(s, e);
            }
            catch (Exception ex) { EstimaUtil.ShowErrorMsg(ex, this.Body.Text); }
        }
        #endregion

        #region Business
        /// <summary>
        /// 使用可否切替
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void SwitchEnabled()
        {
            this.Body.btnUpdate.Enabled = this.UpdateFlg;
        }
        /// <summary>
        /// 検索処理
        /// </summary>
        private void SearchLogic()
        {
            // グリッド初期化
            this.Body.dgvConditions.Rows.Clear();

            // グリッドデータ
            var bindingList = new BindingList<EST0030_GridDto>();
            int estimaKbn = this.Body.rdoEstima.Checked ? (int)Enums.EstimaKbn.Estimate : (int)Enums.EstimaKbn.Invoice;
            CommonUtil.ShowProgressDialog((new Action(() =>
            {
                try
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.Start);
                    bindingList = this.PersonalService.GetGridData(estimaKbn);
                    for (int i = bindingList.Count; i < MaxConditionCount; i++)
                    {
                        bindingList.Add(new EST0030_GridDto());
                    }
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.End);
                }
                catch (Exception ex)
                {
                    EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                    return;
                }
            })));
            if (!bindingList.Any())
            {
                CommonUtil.ShowErrorMsg(EST0030_Messages.MsgErrorNotExistDatas);
                return;
            }
            // グリッドデータセット
            this.Body.dgvConditions.DataSource = bindingList;
            this.Body.dgvConditions.AllowUserToAddRows = false;

            // 検索条件
            this.Body.grpCondition.Enabled = false;

            // 使用可否切替
            this.CurrInitMode = InitMode.Condition;
            this.Body.dgvConditions.Enabled = true;

            // フォーカス
            this.Body.dgvConditions.Focus();

        }
        /// <summary>
        /// 更新処理
        /// </summary>
        private void UpdLogic()
        {
            // データ作成
            var datas = new List<EX05ConditionTemplete>();
            var gridDtos = GridRowUtil<EST0030_GridDto>.GetAllRowsModel(this.Body.dgvConditions.DataSource)
                                                    .Where(n => !string.IsNullOrEmpty(CommonUtil.CutSpace(n.ConditionVal))).ToList();
            int seq = 1;
            int estimaKbn = this.Body.rdoEstima.Checked ? (int)Enums.EstimaKbn.Estimate : (int)Enums.EstimaKbn.Invoice;
            foreach (var dto in gridDtos)
            {
                var entity = new EX05ConditionTemplete()
                {
                    EstimaKbn = estimaKbn,
                    Seq = seq,
                    ConditionVal = dto.ConditionVal,
                };
                datas.Add(entity);
                seq++;
            }
            CommonUtil.ShowProgressDialog(() =>
            {
                EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.Start);
                this.PersonalService.UpdCondition(datas);
                EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.End);
            });
            // 使用可否切替
            this.UpdateFlg = true;
            this.SwitchEnabled();

            // 完了メッセージ
            CommonUtil.ShowInfoMsgOK(EST0030_Messages.MsgInfoFinishUpdate);
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新されていない項目が存在する？
        /// </summary>
        /// <returns></returns>
        private bool ValidateUnUpdateDataExist()
        {
            if (this.CurrInitMode == InitMode.All)
            {
                return true;
            }
            foreach (DataGridViewRow r in this.Body.dgvConditions.Rows)
            {
                // 更新されていない行が含まれる場合
                var rowDto = GridRowUtil<EST0030_GridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.Insert || rowDto.Status == Enums.EditStatus.Update)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init()
        {
            // コントロール初期化
            switch (this.CurrInitMode)
            {
                case InitMode.All:
                    this.Body.dgvConditions.Rows.Clear();
                    this.Body.dgvConditions.Enabled = true;
                    this.Body.grpCondition.Enabled = true;

                    // 使用可否切り替え
                    this.Body.rdoEstima.Checked = true;
                    this.UpdateFlg = false;
                    this.SwitchEnabled();

                    // フォーカス
                    this.Body.rdoEstima.Focus();
                    break;
                case InitMode.Condition:
                    this.Body.grpCondition.Enabled = true;
                    this.Body.dgvConditions.Enabled = false;
                    this.CurrInitMode = InitMode.All;

                    // フォーカス
                    if (this.Body.rdoEstima.Checked)
                    {
                        this.Body.rdoEstima.Focus();
                    }
                    else
                    {
                        this.Body.rdoSeikyu.Focus();
                    }
                    break;
            }
        }
        #endregion
    }
}
