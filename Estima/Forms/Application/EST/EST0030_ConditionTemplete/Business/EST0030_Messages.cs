﻿namespace Estima.Forms.Application.EST.EST0030.Business
{
    internal static class EST0030_Messages
    {
        internal const string MsgErrorNotExistDatas = "対象のデータがありません";
        internal const string MsgErrorNotExistMaster = "マスタ未登録の{0}が入力されています";
        internal const string MsgErrorIrregalZero = "{0}には0より大きい値を入力してください";
        internal const string MsgInfoDelete = "削除しました";
        internal const string MsgAskDelete = "削除しますか？";
        internal const string MsgAskHakko = "発行しますか？";
        internal const string MsgAskUpdate = "更新しますか？";
        internal const string MsgAskClear = "クリアしますか？";
        internal const string MsgAskFinish = "終了しますか？";
        internal const string MsgInfoFinishUpdate = "更新しました";
        internal const string MsgAskNotCommitedDataExist = "未更新のデータがありますが\r\nよろしいですか？";
        internal const string MsgErrorLockes = "他のユーザが開いているため、削除できません";
        internal const string MsgExOpenMeisaiReadOnly = "他のユーザが開いているため、\r\n読み取り専用で開きます";
        internal const string MsgInfoStartCopy = "このデータを複製します\r\n複製データを保存する場合は[更新]ボタンをクリックしてください";
    }
}
