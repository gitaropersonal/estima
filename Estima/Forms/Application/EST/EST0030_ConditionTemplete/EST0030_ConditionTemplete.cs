﻿using Estima.Forms.Application.EST.EST0030.Business;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Application.EST.EST0030
{
    public partial class EST0030_ConditionTemplete : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        public EST0030_ConditionTemplete(LoginInfoDto loginInfo)
        {
            this.InitializeComponent();
            try
            {
                new EST0030_FormLogic(this, loginInfo);
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
