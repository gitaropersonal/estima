﻿namespace Estima.Forms.Application.EST.EST0030
{
    partial class EST0030_ConditionTemplete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCondition = new System.Windows.Forms.Panel();
            this.grpCondition = new System.Windows.Forms.GroupBox();
            this.pnlFormat = new System.Windows.Forms.Panel();
            this.rdoSeikyu = new System.Windows.Forms.RadioButton();
            this.rdoEstima = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.dgvConditions = new System.Windows.Forms.DataGridView();
            this.ConditionVal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlCondition.SuspendLayout();
            this.grpCondition.SuspendLayout();
            this.pnlFormat.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConditions)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCondition
            // 
            this.pnlCondition.Controls.Add(this.grpCondition);
            this.pnlCondition.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCondition.Location = new System.Drawing.Point(0, 0);
            this.pnlCondition.Name = "pnlCondition";
            this.pnlCondition.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.pnlCondition.Size = new System.Drawing.Size(984, 112);
            this.pnlCondition.TabIndex = 0;
            // 
            // grpCondition
            // 
            this.grpCondition.Controls.Add(this.panel14);
            this.grpCondition.Controls.Add(this.pnlFormat);
            this.grpCondition.Controls.Add(this.label1);
            this.grpCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCondition.Location = new System.Drawing.Point(15, 10);
            this.grpCondition.Name = "grpCondition";
            this.grpCondition.Size = new System.Drawing.Size(954, 92);
            this.grpCondition.TabIndex = 0;
            this.grpCondition.TabStop = false;
            this.grpCondition.Text = "検索条件";
            // 
            // pnlFormat
            // 
            this.pnlFormat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFormat.Controls.Add(this.rdoSeikyu);
            this.pnlFormat.Controls.Add(this.rdoEstima);
            this.pnlFormat.Location = new System.Drawing.Point(110, 27);
            this.pnlFormat.Margin = new System.Windows.Forms.Padding(4);
            this.pnlFormat.Name = "pnlFormat";
            this.pnlFormat.Size = new System.Drawing.Size(192, 28);
            this.pnlFormat.TabIndex = 0;
            // 
            // rdoSeikyu
            // 
            this.rdoSeikyu.AutoSize = true;
            this.rdoSeikyu.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoSeikyu.Location = new System.Drawing.Point(99, 0);
            this.rdoSeikyu.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSeikyu.Name = "rdoSeikyu";
            this.rdoSeikyu.Size = new System.Drawing.Size(75, 26);
            this.rdoSeikyu.TabIndex = 2;
            this.rdoSeikyu.TabStop = true;
            this.rdoSeikyu.Text = "請求書";
            this.rdoSeikyu.UseVisualStyleBackColor = true;
            // 
            // rdoEstima
            // 
            this.rdoEstima.AutoSize = true;
            this.rdoEstima.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoEstima.Location = new System.Drawing.Point(0, 0);
            this.rdoEstima.Margin = new System.Windows.Forms.Padding(4);
            this.rdoEstima.Name = "rdoEstima";
            this.rdoEstima.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.rdoEstima.Size = new System.Drawing.Size(99, 26);
            this.rdoEstima.TabIndex = 1;
            this.rdoEstima.TabStop = true;
            this.rdoEstima.Text = "見積書";
            this.rdoEstima.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 40;
            this.label1.Text = "フォーマット";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 691);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(984, 70);
            this.panel3.TabIndex = 102;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(759, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(225, 70);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(69, 8);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(377, 70);
            this.panel4.TabIndex = 110;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(153, 9);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(138, 52);
            this.btnUpdate.TabIndex = 114;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(15, 9);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvConditions);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 112);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.panel2.Size = new System.Drawing.Size(984, 579);
            this.panel2.TabIndex = 30;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(50, 6);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(138, 52);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "検索(S)";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnSearch);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(759, 24);
            this.panel14.Margin = new System.Windows.Forms.Padding(4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(192, 65);
            this.panel14.TabIndex = 20;
            // 
            // dgvConditions
            // 
            this.dgvConditions.AllowUserToAddRows = false;
            this.dgvConditions.AllowUserToDeleteRows = false;
            this.dgvConditions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConditions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ConditionVal,
            this.Status});
            this.dgvConditions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvConditions.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.dgvConditions.Location = new System.Drawing.Point(15, 10);
            this.dgvConditions.Margin = new System.Windows.Forms.Padding(4);
            this.dgvConditions.Name = "dgvConditions";
            this.dgvConditions.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvConditions.RowTemplate.Height = 21;
            this.dgvConditions.Size = new System.Drawing.Size(954, 559);
            this.dgvConditions.TabIndex = 30;
            // 
            // ConditionVal
            // 
            this.ConditionVal.DataPropertyName = "ConditionVal";
            this.ConditionVal.HeaderText = "条件";
            this.ConditionVal.MaxInputLength = 80;
            this.ConditionVal.Name = "ConditionVal";
            this.ConditionVal.Width = 900;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // EST0030_ConditionTemplete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 761);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnlCondition);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MinimumSize = new System.Drawing.Size(1000, 800);
            this.Name = "EST0030_ConditionTemplete";
            this.Text = "条件テンプレート";
            this.pnlCondition.ResumeLayout(false);
            this.grpCondition.ResumeLayout(false);
            this.grpCondition.PerformLayout();
            this.pnlFormat.ResumeLayout(false);
            this.pnlFormat.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConditions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCondition;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.GroupBox grpCondition;
        public System.Windows.Forms.Panel pnlFormat;
        public System.Windows.Forms.RadioButton rdoSeikyu;
        public System.Windows.Forms.RadioButton rdoEstima;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Panel panel14;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.DataGridView dgvConditions;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConditionVal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}