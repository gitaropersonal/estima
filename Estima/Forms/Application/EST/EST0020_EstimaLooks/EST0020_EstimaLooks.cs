﻿using Estima.Forms.Application.EST.EST0020.Business;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Application.EST.EST0020
{
    public partial class EST0020_EstimaLooks : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        public EST0020_EstimaLooks(LoginInfoDto loginInfo)
        {
            this.InitializeComponent();
            try
            {
                new EST0020_FormLogic(this, loginInfo);
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
