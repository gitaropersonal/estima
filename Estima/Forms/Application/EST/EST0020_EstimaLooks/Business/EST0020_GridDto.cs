﻿using EstimaLib.Const;

namespace Estima.Forms.Application.EST.EST0020.Business
{
    public class EST0020_GridDto
    {
        public EST0020_GridDto()
        {
            this.ChkSelect = false;
            this.BtnEdit = "編集";
            this.BtnCopy = "複製";
            this.BtnDel  = "削除";
            this.Status = Enums.EditStatus.None;
        }
        public bool ChkSelect { get; set; }
        public string BtnEdit { get; set; }
        public string BtnCopy { get; set; }
        public string BtnDel { get; set; }
        public string EstimaID { get; set; }
        public string EstimaKbn { get; set; }
        public string EstimateNo { get; set; }
        public string OrderNo { get; set; }
        public string ProcessingNo { get; set; }
        public string FieldName { get; set; }
        public string AddressCompany { get; set; }
        public string AddressOffice { get; set; }
        public string AddressTanto { get; set; }
        public string HakkoDate { get; set; }
        public string LimitDate { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
