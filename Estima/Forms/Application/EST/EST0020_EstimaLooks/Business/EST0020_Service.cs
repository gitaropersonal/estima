﻿using EstimaLib.Const;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Data;
using System.Text;

namespace Estima.Forms.Application.EST.EST0020.Business
{
    internal class EST0020_Service
    {
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        internal BindingList<EST0020_GridDto> GetGridData(EST0020_ConDto con)
        {
            string cmOutOrderNo = CommonUtil.GetCmout(string.IsNullOrEmpty(con.OrderNo));
            string cmOutEstimateNo = CommonUtil.GetCmout(string.IsNullOrEmpty(con.EstimateNo));
            string cmOutProcessingNo = CommonUtil.GetCmout(string.IsNullOrEmpty(con.ProcessingNo));
            string cmOutFieldName = CommonUtil.GetCmout(string.IsNullOrEmpty(con.FieldName));
            string cmOutAddressCompany = CommonUtil.GetCmout(string.IsNullOrEmpty(con.AddressCompany));
            string cmOutAddressOffice = CommonUtil.GetCmout(string.IsNullOrEmpty(con.AddressOffice));
            string cmOutAddressTanto = CommonUtil.GetCmout(string.IsNullOrEmpty(con.AddressTanto));
            string cmOutHakkoDateFrom = CommonUtil.GetCmout(string.IsNullOrEmpty(con.HakkoDateFrom));
            string cmOutHakkoDateTo = CommonUtil.GetCmout(string.IsNullOrEmpty(con.HakkoDateTo));
            string cmOutLimitDateFrom = CommonUtil.GetCmout(string.IsNullOrEmpty(con.LimitDateFrom));
            string cmOutLimitDateTo = CommonUtil.GetCmout(string.IsNullOrEmpty(con.LimitDateTo));
            var ret = new BindingList<EST0020_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    EX01.EstimaID ");
            sb.AppendLine($",   EX01.EstimaKbn ");
            sb.AppendLine($",   EX01.EstimateNo ");
            sb.AppendLine($",   EX01.OrderNo ");
            sb.AppendLine($",   EX01.ProcessingNo ");
            sb.AppendLine($",   EX01.FieldName ");
            sb.AppendLine($",   EX01.AddressCompany ");
            sb.AppendLine($",   EX01.AddressOffice ");
            sb.AppendLine($",   EX01.AddressTanto ");
            sb.AppendLine($",   DATE_FORMAT(EX01.HakkoDate, '%Y-%m-%d') AS HakkoDate ");
            sb.AppendLine($",   DATE_FORMAT(EX01.LimitDate, '%Y-%m-%d') AS LimitDate ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX01EstimaHeader AS EX01 ");
            sb.AppendLine($"INNER JOIN ");
            sb.AppendLine($"    EX02EstimaBody AS EX02 ");
            sb.AppendLine($"ON ");
            sb.AppendLine($"    EX01.EstimaID = EX02.EstimaID ");
            sb.AppendLine($"INNER JOIN ");
            sb.AppendLine($"    EX03EstimaDetail AS EX03 ");
            sb.AppendLine($"ON ");
            sb.AppendLine($"    EX01.EstimaID = EX03.EstimaID ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EX01.EstimaKbn = @estimaKbn ");
            sb.AppendLine($"{cmOutOrderNo       }AND EX01.OrderNo        LIKE @orderNo ");
            sb.AppendLine($"{cmOutEstimateNo    }AND EX01.EstimateNo     LIKE @estimateNo ");
            sb.AppendLine($"{cmOutProcessingNo  }AND EX01.ProcessingNo   LIKE @processingNo  ");
            sb.AppendLine($"{cmOutFieldName     }AND EX01.FieldName      LIKE @fieldName ");
            sb.AppendLine($"{cmOutAddressCompany}AND EX01.AddressCompany LIKE @addressCompany ");
            sb.AppendLine($"{cmOutAddressOffice }AND EX01.AddressOffice  LIKE @addressOffice ");
            sb.AppendLine($"{cmOutAddressTanto  }AND EX01.AddressTanto   LIKE @addressTanto ");
            sb.AppendLine($"{cmOutHakkoDateFrom }AND @hakkoDateFrom  <= EX01.HakkoDate ");
            sb.AppendLine($"{cmOutHakkoDateTo   }AND EX01.HakkoDate  <= @hakkoDateTo  ");
            sb.AppendLine($"{cmOutLimitDateFrom }AND @limitDateFrom  <= EX01.LimitDate ");
            sb.AppendLine($"{cmOutLimitDateTo   }AND EX01.LimitDate  <= @limitDateTo  ");
            sb.AppendLine($"GROUP BY ");
            sb.AppendLine($"    EX01.EstimaID ");
            sb.AppendLine($",   EX01.EstimaKbn ");
            sb.AppendLine($",   EX01.EstimateNo ");
            sb.AppendLine($",   EX01.OrderNo ");
            sb.AppendLine($",   EX01.ProcessingNo ");
            sb.AppendLine($",   EX01.FieldName ");
            sb.AppendLine($",   EX01.AddressCompany ");
            sb.AppendLine($",   EX01.AddressOffice ");
            sb.AppendLine($",   EX01.AddressTanto ");
            sb.AppendLine($",   DATE_FORMAT(EX01.HakkoDate, '%Y-%m-%d') ");
            sb.AppendLine($",   DATE_FORMAT(EX01.LimitDate, '%Y-%m-%d') ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    EX01.EstimaKbn ");
            sb.AppendLine($",   EX01.EstimateNo   DESC ");
            sb.AppendLine($",   EX01.OrderNo      DESC ");
            sb.AppendLine($",   EX01.ProcessingNo DESC ");
            sb.AppendLine($",   DATE_FORMAT(EX01.HakkoDate, '%Y-%m-%d') DESC ");
            sb.AppendLine($",   DATE_FORMAT(EX01.LimitDate, '%Y-%m-%d') DESC ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@estimateNo"    , "%" + con.EstimateNo + "%");
                    cmd.Parameters.AddWithValue("@orderNo"       , "%" + con.OrderNo + "%");
                    cmd.Parameters.AddWithValue("@processingNo"  , "%" + con.ProcessingNo + "%");
                    cmd.Parameters.AddWithValue("@fieldName"     , "%" + con.FieldName + "%");
                    cmd.Parameters.AddWithValue("@addressCompany", "%" + con.AddressCompany + "%");
                    cmd.Parameters.AddWithValue("@addressOffice" , "%" + con.AddressOffice + "%");
                    cmd.Parameters.AddWithValue("@addressTanto"  , "%" + con.AddressTanto + "%");
                    cmd.Parameters.Add(new MySqlParameter("@estimaKbn", con.EstimaKbn) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@hakkoDateFrom", con.HakkoDateFrom) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@hakkoDateTo"  , con.HakkoDateTo) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@limitDateFrom", con.LimitDateFrom) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@limitDateTo"  , con.LimitDateTo) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new EST0020_GridDto();
                            dto.EstimaID = CommonUtil.TostringNullForbid(dr[nameof(dto.EstimaID)]);
                            dto.EstimaKbn = CommonUtil.TostringNullForbid(dr[nameof(dto.EstimaKbn)]);
                            dto.EstimateNo = CommonUtil.TostringNullForbid(dr[nameof(dto.EstimateNo)]);
                            dto.OrderNo = CommonUtil.TostringNullForbid(dr[nameof(dto.OrderNo)]);
                            dto.ProcessingNo = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingNo)]);
                            dto.FieldName = CommonUtil.TostringNullForbid(dr[nameof(dto.FieldName)]);
                            dto.AddressCompany = CommonUtil.TostringNullForbid(dr[nameof(dto.AddressCompany)]);
                            dto.AddressOffice = CommonUtil.TostringNullForbid(dr[nameof(dto.AddressOffice)]);
                            dto.AddressTanto = CommonUtil.TostringNullForbid(dr[nameof(dto.AddressTanto)]);
                            dto.HakkoDate = CommonUtil.TostringNullForbid(dr[nameof(dto.HakkoDate)]);
                            dto.LimitDate = CommonUtil.TostringNullForbid(dr[nameof(dto.LimitDate)]);
                            dto.Status = Enums.EditStatus.Show;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 伝票データ削除
        /// </summary>
        /// <param name="estimaID"></param>
        internal void DelEstima(string estimaID)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine("DELETE FROM EX01EstimaHeader WHERE EstimaID = @estimaID; ");
            sb.AppendLine("DELETE FROM EX02EstimaBody   WHERE EstimaID = @estimaID; ");
            sb.AppendLine("DELETE FROM EX03EstimaDetail WHERE EstimaID = @estimaID; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
