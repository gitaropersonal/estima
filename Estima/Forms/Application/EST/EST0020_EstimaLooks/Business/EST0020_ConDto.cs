﻿using EstimaLib.Const;

namespace Estima.Forms.Application.EST.EST0020.Business
{
    public class EST0020_ConDto
    {
        public int EstimaKbn { get; set; }
        public string EstimateNo { get; set; }
        public string OrderNo { get; set; }
        public string ProcessingNo { get; set; }
        public string FieldName { get; set; }
        public string AddressCompany { get; set; }
        public string AddressOffice { get; set; }
        public string AddressTanto { get; set; }
        public string HakkoDateFrom { get; set; }
        public string HakkoDateTo{ get; set; }
        public string LimitDateFrom { get; set; }
        public string LimitDateTo { get; set; }
    }
}
