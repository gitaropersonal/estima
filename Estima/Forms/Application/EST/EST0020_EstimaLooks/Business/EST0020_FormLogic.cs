﻿using Estima.Forms.Application.EST.EST0010;
using Estima.Forms.Application.EST.EST0010.Business;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Report;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Application.EST.EST0020.Business
{
    internal class EST0020_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private EST0020_EstimaLooks Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private readonly LoginInfoDto LoginInfo = new LoginInfoDto();
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// 個別サービス
        /// </summary>
        private readonly EST0020_Service PersonalService = new EST0020_Service();
        /// <summary>
        /// 帳票作成ロジック
        /// </summary>
        private readonly CreateEstimaLogic CreateEstimaLogic = new CreateEstimaLogic();
        /// <summary>
        /// 列ヘッダdto
        /// </summary>
        private static EST0020_GridDto CellHeaderNameDto = new EST0020_GridDto();
        /// <summary>
        /// 初期化モード
        /// </summary>
        private enum InitMode
        {
            All = 0,
            Condition = 1,
        }
        /// <summary>
        /// 初期化モード
        /// </summary>
        private InitMode CurrInitMode = InitMode.All;
        /// <summary>
        /// 画面名リスト
        /// </summary>
        private readonly IEnumerable<KeyValuePair<Enums.ScreenModeEditEstima, string>> ScreenNameList = new Dictionary<Enums.ScreenModeEditEstima, string>
        {
              { Enums.ScreenModeEditEstima.Edit    , "(1) - 1.書類作成（編集モード）"}
            , { Enums.ScreenModeEditEstima.ReadOnly, "(1) - 1.書類作成（読取専用モード）"}
            , { Enums.ScreenModeEditEstima.Copy    , "(1) - 1.書類作成（複製モード）"}
        };
        /// <summary>
        /// エクスプローラのexe名
        /// </summary>
        private const string ExplorerExeName = "EXPLORER.EXE";
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        internal EST0020_FormLogic(
              EST0020_EstimaLooks body
            , LoginInfoDto lobinInfo
            )
        {
            this.Body = body;
            this.LoginInfo = lobinInfo;
            new InitControl(this.Body);
            this.Init();
            // 検索ボタン押下
            this.Body.btnSearch.Click += (s, e) =>
            {
                CommonUtil.EventWithValidateUpdExist(
                      new Action(() => this.BtnSearchClickEvent(s, e))
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 全選択ボタン押下
            this.Body.btnSelectAll.Click += (s, e) => { this.SelectOrReleaseAll(true); };
            // 全解除ボタン押下
            this.Body.btnRelease.Click += (s, e) => { this.SelectOrReleaseAll(false); };
            // 発行ボタン押下
            this.Body.btnHakko.Click += this.BtnHakkoClickEvent;
            // クリアボタン押下
            this.Body.btnClear.Click += this.BtnClearClickEvent;
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了イベント
            this.Body.FormClosing += this.ClosingEvent;
            // 形式切替
            this.Body.rdoEstima.CheckedChanged += (s, e) => { this.SwitchFormat(); };
            // 見積№と加工№
            this.Body.txtProcessingNum.Validated += CommonUtil.ZeroPaddingTextBox;
            // グリッドRowPostPaint
            this.Body.dgvLooks.RowPostPaint += (s, e) => { GridRowUtil<EST0020_GridDto>.SetRowNum(s, e); };
            // グリッドセルクリック
            this.Body.dgvLooks.CellClick += this.GridCellClick;
            // グリッドセルダブルクリック
            this.Body.dgvLooks.CellDoubleClick += this.GridCellDoubleClickEvent;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        this.Body.btnSearch.Focus();
                        this.Body.btnSearch.PerformClick();
                        break;
                    case Keys.N:
                        this.Body.btnSelectAll.Focus();
                        this.Body.btnSelectAll.PerformClick();
                        break;
                    case Keys.R:
                        this.Body.btnRelease.Focus();
                        this.Body.btnRelease.PerformClick();
                        break;
                    case Keys.H:
                        this.Body.btnHakko.Focus();
                        this.Body.btnHakko.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// クリアボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnClearClickEvent(
              object s
            , EventArgs e
            )
        {
            if (CommonUtil.ShowInfoMsgOKCancel(EST0020_Messages.MsgAskClear) != DialogResult.OK)
            {
                return;
            }
            this.Init();
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                if (CommonUtil.ShowInfoMsgOKCancel(EST0020_Messages.MsgAskFinish) != DialogResult.OK)
                {
                    cancel = true;
                    return;
                }
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                if (CommonUtil.ShowInfoMsgOKCancel(EST0020_Messages.MsgAskFinish) != DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSearchClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// 発行ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnHakkoClickEvent(
              object s
            , EventArgs e
            )
        {
            // 背景色初期化
            foreach (DataGridViewRow r in this.Body.dgvLooks.Rows)
            {
                foreach (DataGridViewCell c in r.Cells)
                {
                    c.Style.BackColor = Colors.BackColorEmpty;
                }
            }
            // Validate
            var selectedDatas = GridRowUtil<EST0020_GridDto>.GetAllRowsModel(this.Body.dgvLooks.DataSource).Where(n => n.ChkSelect).ToList();
            if (!this.ValidteHakko(selectedDatas))
            {
                return;
            }
            // 確認ダイアログ
            if (CommonUtil.ShowInfoMsgOKCancel(EST0020_Messages.MsgAskHakko) != DialogResult.OK)
            {
                return;
            }
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                // 帳票作成
                bool success = true;
                CommonUtil.ShowProgressDialog(() =>
                {
                    foreach (var rowDto in selectedDatas)
                    {
                        EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogHakko}：{rowDto.EstimaID}", Enums.LogStatusKbn.Start);
                        success = this.CreateEstimaLogic.Main(
                              rowDto.EstimaID
                            , SelectFolderUtil.SelectedFolderPath
                        );
                        EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogHakko}：{rowDto.EstimaID}", Enums.LogStatusKbn.End);
                    }
                });
                if (!success)
                {
                    return;
                }
                // 確認ダイアログ
                if (CommonUtil.ShowInfoMsgOKCancel(EST0020_Messages.MsgAskOpenFolder) != DialogResult.OK)
                {
                    return;
                }
                // フォルダオープン
                CommonUtil.ShowProgressDialog(() =>
                {
                    Process.Start(ExplorerExeName, SelectFolderUtil.SelectedFolderPath);
                });
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// グリッドセルクリック
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellClick(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            DataGridViewRow r = dgv.Rows[e.RowIndex];
            DataGridViewCell c = r.Cells[e.ColumnIndex];
            var rowDto = GridRowUtil<EST0020_GridDto>.GetRowModel(r);

            // ロック存在確認
            bool existLock = this.CommonService.JudgeExistLock(rowDto.EstimaID);
            // ロックエンティティ取得
            var lockEntity = this.CommonService.SelLock(rowDto.EstimaID);
            try
            {
                switch (c.OwningColumn.Name)
                {
                    // 表示ボタン押下
                    case nameof(CellHeaderNameDto.BtnEdit):
                        this.GridCellClickBtnShow(s, e, existLock, rowDto);
                        break;
                    // 複製ボタン押下
                    case nameof(CellHeaderNameDto.BtnCopy):
                        this.GridCellClickBtnCopy(s, e, rowDto);
                        break;
                    // 削除ボタン押下
                    case nameof(CellHeaderNameDto.BtnDel):
                        this.GridCellClickBtnDel(s, e, existLock, rowDto);
                        break;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                // ロック解除
                if (!existLock || lockEntity.AddIPAddress == CommonUtil.GetIpAddress())
                {
                    this.CommonService.DelLock(rowDto.EstimaID);
                }
            }
        }
        /// <summary>
        /// グリッドセルクリック（表示ボタン）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="existLock"></param>
        /// <param name="rowDto"></param>
        private void GridCellClickBtnShow(
              object s
            , EventArgs e
            , bool existLock
            , EST0020_GridDto rowDto
            )
        {
            var precursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                // ロック確認
                string screenTitle = this.ScreenNameList.First(n => n.Key == Enums.ScreenModeEditEstima.Edit).Value;
                if (!existLock)
                {
                
                }
                if (existLock)
                {
                    CommonUtil.ShowInfoExcramationOK(EST0020_Messages.MsgExOpenMeisaiReadOnly);
                    screenTitle = this.ScreenNameList.First(n => n.Key == Enums.ScreenModeEditEstima.ReadOnly).Value;
                }
                else
                {
                    try
                    {
                        // ロック追加
                        this.CommonService.AddLock(rowDto.EstimaID);
                    }
                    catch
                    {
                        existLock = true;
                        CommonUtil.ShowInfoExcramationOK(EST0020_Messages.MsgExOpenMeisaiReadOnly);
                        screenTitle = this.ScreenNameList.First(n => n.Key == Enums.ScreenModeEditEstima.ReadOnly).Value;
                    }
                }
                // 伝票表示
                var drEstima = new EST0010_EditEstima(
                      this.LoginInfo
                    , existLock ? Enums.ScreenModeEditEstima.ReadOnly : Enums.ScreenModeEditEstima.Edit
                    , rowDto.EstimaID
                );
                drEstima.Text = screenTitle;
                EstimaUtil.AddLogInfo(this.Body.Text, $"{drEstima.Text}：{rowDto.EstimaID}", Enums.LogStatusKbn.Start);
                drEstima.ShowDialog();
                EstimaUtil.AddLogInfo(this.Body.Text, $"{drEstima.Text}：{rowDto.EstimaID}", Enums.LogStatusKbn.End);

                // 再検索
                if (drEstima.Updated)
                {
                    this.BtnSearchClickEvent(s, e);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                Cursor.Current = precursor;
            }
        }
        /// <summary>
        /// グリッドセルクリック（複製ボタン）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="rowDto"></param>
        private void GridCellClickBtnCopy(
              object s
            , EventArgs e
            , EST0020_GridDto rowDto
            )
        {
            // 警告メッセージ
            if (CommonUtil.ShowInfoExcramationOKCancel(EST0020_Messages.MsgInfoStartCopy) != DialogResult.OK)
            {
                return;
            };
            var precursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            // 伝票ID発行
            string estimaIdOfCopy = EstimaUtil.CreateEstimaID();
            try
            {
                // 複製伝票のロック登録
                this.CommonService.AddLock(estimaIdOfCopy);

                // 伝票表示
                var drEstima = new EST0010_EditEstima(
                      this.LoginInfo
                    , Enums.ScreenModeEditEstima.Copy
                    , rowDto.EstimaID
                    , estimaIdOfCopy
                );
                drEstima.Text = this.ScreenNameList.First(n => n.Key == Enums.ScreenModeEditEstima.Copy).Value;
                EstimaUtil.AddLogInfo(this.Body.Text, $"{drEstima.Text}：{rowDto.EstimaID}", Enums.LogStatusKbn.Start);
                drEstima.ShowDialog();
                EstimaUtil.AddLogInfo(this.Body.Text, $"{drEstima.Text}：{rowDto.EstimaID}", Enums.LogStatusKbn.End);
                // 再検索
                if (drEstima.Updated)
                {
                    this.BtnSearchClickEvent(s, e);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // 複製伝票のロック削除
                this.CommonService.DelLock(estimaIdOfCopy);
                Cursor.Current = precursor;
            }
        }
        /// <summary>
        /// グリッドセルクリック（削除ボタン）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="existLock"></param>
        /// <param name="rowDto"></param>
        private void GridCellClickBtnDel(
              object s
            , EventArgs e
            , bool existLock
            , EST0020_GridDto rowDto
            )
        {
            // ロック確認
            if (existLock)
            {
                CommonUtil.ShowErrorMsg(EST0020_Messages.MsgErrorLocked);
                return;
            }
            // 確認ダイアログ
            if (CommonUtil.ShowInfoExcramationOKCancel(EST0020_Messages.MsgAskDelete) != DialogResult.OK)
            {
                return;
            }
            // データ削除
            CommonUtil.ShowProgressDialog(() =>
            {
                EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogDelEstimaData}：{rowDto.EstimaID}", Enums.LogStatusKbn.Start);
                this.PersonalService.DelEstima(rowDto.EstimaID);
                EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogDelEstimaData}：{rowDto.EstimaID}", Enums.LogStatusKbn.End);
            });
            // 完了メッセージ
            CommonUtil.ShowInfoMsgOK(EST0020_Messages.MsgInfoDelete);
            // 再検検索
            this.BtnSearchClickEvent(s, e);
        }
        /// <summary>
        /// グリッドCellDoubleClick
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellDoubleClickEvent(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            var dgv = ((DataGridView)s);
            dgv.Rows[e.RowIndex].Selected = true;
        }
        #endregion

        #region Business
        /// <summary>
        /// 全選択or全解除
        /// </summary>
        /// <param name="select"></param>
        private void SelectOrReleaseAll(bool select)
        {
            if (this.Body.dgvLooks.Rows == null || this.Body.dgvLooks.Rows.Count == 0)
            {
                CommonUtil.ShowErrorMsg(EST0020_Messages.MsgErrorNotExistDatas);
                return;
            }
            var allDatas = GridRowUtil<EST0020_GridDto>.GetAllRowsModel(this.Body.dgvLooks.DataSource);
            if (!allDatas.Any()) { return; }
            var newDatas = new BindingList<EST0020_GridDto>();
            foreach (var dto in allDatas)
            {
                dto.ChkSelect = select;
                newDatas.Add(dto);
            }
            this.Body.dgvLooks.DataSource = newDatas;
        }
        /// <summary>
        /// 形式区分に応じた切替
        /// </summary>
        /// <param name="afterSearch"></param>
        private void SwitchFormat(bool afterSearch = false)
        {
            bool isEstima = this.Body.rdoEstima.Checked;
            this.Body.lblOrderNum.Visible = !isEstima;
            this.Body.txtOrderNum.Visible = !isEstima;
            this.Body.lblProcessingNum.Text = isEstima ? "見積№" : "加工№";
            this.Body.lblEstimateNumPrefix.Text = isEstima ? "MK" : string.Empty;
            this.Body.lblEstimateNumPrefix.Visible = isEstima;
            this.Body.txtProcessingNum.MaxLength = isEstima ? 5 : 7;
            if (!afterSearch)
            {
                this.Body.txtProcessingNum.Text = string.Empty;
            }
            this.Body.btnSelectAll.Enabled = afterSearch;
            this.Body.btnRelease.Enabled = afterSearch;
            this.Body.btnHakko.Enabled = afterSearch;
            if (!this.Body.dgvLooks.Enabled)
            {
                return;
            }
            this.Body.dgvLooks.Columns[nameof(CellHeaderNameDto.EstimateNo)].Visible = isEstima;
            this.Body.dgvLooks.Columns[nameof(CellHeaderNameDto.ProcessingNo)].Visible = !isEstima;
            this.Body.dgvLooks.Columns[nameof(CellHeaderNameDto.OrderNo)].Visible = !isEstima;
        }
        /// <summary>
        /// 検索処理
        /// </summary>
        private void SearchLogic()
        {
            // グリッド初期化
            this.Body.dgvLooks.Rows.Clear();

            // 検索条件
            var con = new EST0020_ConDto()
            {
                EstimaKbn = this.Body.rdoEstima.Checked ? (int)Enums.EstimaKbn.Estimate : (int)Enums.EstimaKbn.Invoice,
                EstimateNo = this.Body.rdoEstima.Checked ? string.Concat(this.Body.lblEstimateNumPrefix.Text, this.Body.txtProcessingNum.Text) : string.Empty,
                OrderNo = this.Body.rdoSeikyu.Checked ? this.Body.txtOrderNum.Text : string.Empty,
                ProcessingNo = this.Body.rdoSeikyu.Checked ? this.Body.txtProcessingNum.Text : string.Empty,
                HakkoDateFrom = this.Body.dtpHakkoDateFrom.Value.ToString(Formats.YYYYMMDD_Slash),
                HakkoDateTo = this.Body.dtpHakkoDateTo.Value.ToString(Formats.YYYYMMDD_Slash),
                LimitDateFrom = this.Body.dtpLimitDateFrom.Value.ToString(Formats.YYYYMMDD_Slash),
                LimitDateTo = this.Body.dtpLimitDateTo.Value.ToString(Formats.YYYYMMDD_Slash),
                AddressCompany = this.Body.txtAddressCompany.Text,
                AddressOffice = this.Body.cmbAddressOffice.Text,
                AddressTanto = this.Body.txtAddressTanto.Text,
                FieldName = this.Body.txtFieldName.Text,
            };
            // グリッドデータ
            var bindingList = new BindingList<EST0020_GridDto>();
            CommonUtil.ShowProgressDialog((new Action(() =>
            {
                try
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.Start);
                    bindingList = this.PersonalService.GetGridData(con);
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.End);
                }
                catch (Exception ex)
                {
                    EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                    return;
                }
            })));
            if (!bindingList.Any())
            {
                CommonUtil.ShowErrorMsg(EST0020_Messages.MsgErrorNotExistDatas);
                return;
            }
            // グリッドデータセット
            this.Body.dgvLooks.DataSource = bindingList;
            this.Body.dgvLooks.AllowUserToAddRows = false;

            // 検索条件
            this.Body.grpCondition.Enabled = false;

            // 使用可否切替
            this.CurrInitMode = InitMode.Condition;
            this.Body.dgvLooks.Enabled = true;
            this.SwitchFormat(true);

            // フォーカス
            this.Body.dgvLooks.Focus();

        }
        #endregion

        #region Validate
        /// <summary>
        /// Validate（発行ボタン押下時）
        /// </summary>
        /// <param name="selectedDatas"></param>
        /// <returns></returns>
        private bool ValidteHakko(List<EST0020_GridDto> selectedDatas)
        {
            // Validate（選択行存在チェック）
            if (!selectedDatas.Any())
            {
                CommonUtil.ShowErrorMsg(EST0020_Messages.MsgErrorNotSelectedRow);
                return false;
            }
            // Validate（他ユーザ閲覧中チェック）
            var allDatas = GridRowUtil<EST0020_GridDto>.GetAllRowsModel(this.Body.dgvLooks.DataSource).ToList();
            bool lockExistFlg = false;
            foreach (var rowDto in selectedDatas)
            {
                if (this.CommonService.JudgeExistLock(rowDto.EstimaID))
                {
                    lockExistFlg = true;
                    int tgtIndex = allDatas.IndexOf(allDatas.First(n => n.EstimaID == rowDto.EstimaID));
                    DataGridViewRow r = this.Body.dgvLooks.Rows[tgtIndex];
                    foreach (DataGridViewCell c in r.Cells)
                    {
                        c.Style.BackColor = Colors.BackColorCellError;
                    }
                }
            }
            if (lockExistFlg)
            {
                CommonUtil.ShowErrorMsg(EST0020_Messages.MsgErrorLockedDataSelected);
                return false;
            }
            // 出力フォルダ選択
            if (!SelectFolderUtil.ShowDialog())
            {
                return false;
            }
            foreach (var rowDto in selectedDatas)
            {
                // Validate（ファイルが開かれているか？）
                string filePath = this.CreateEstimaLogic.GetFilePath(rowDto.EstimaID, SelectFolderUtil.SelectedFolderPath);
                if (!CommonUtil.ValidateFileOpened(filePath))
                {
                    CommonUtil.ShowErrorMsg(EST0010_Messages.MsgErrorFileAlreadyOpend);
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 更新されていない項目が存在する？
        /// </summary>
        /// <returns></returns>
        private bool ValidateUnUpdateDataExist()
        {
            foreach (DataGridViewRow r in this.Body.dgvLooks.Rows)
            {
                // 更新されていない行が含まれる場合
                var rowDto = GridRowUtil<EST0020_GridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.Insert || rowDto.Status == Enums.EditStatus.Update)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init()
        {
            // データ取得
            var mstAddressOfficeDatas = new List<MX06AddressOffice>();
            CommonUtil.ShowProgressDialog(() =>
            {
                try
                {
                    // 宛先（営業所）取得
                    mstAddressOfficeDatas = this.CommonService.SelMstAddressOffice();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            });
            switch (this.CurrInitMode)
            {
                case InitMode.All:
                    this.Body.cmbAddressOffice.Items.Clear();
                    this.Body.cmbAddressOffice.Text = string.Empty;
                    mstAddressOfficeDatas.ForEach(n => this.Body.cmbAddressOffice.Items.Add(n.OfficeName));
                    this.Body.txtAddressCompany.Text = string.Empty;
                    this.Body.txtAddressTanto.Text = string.Empty;
                    this.Body.txtOrderNum.Text = string.Empty;
                    this.Body.txtProcessingNum.Text = string.Empty;
                    this.Body.dtpHakkoDateFrom.Value = DateTime.Today.AddMonths(-1);
                    this.Body.dtpHakkoDateTo.Value = DateTime.Today.AddMonths(1);
                    this.Body.dtpLimitDateFrom.Value = this.Body.dtpHakkoDateFrom.Value.AddDays(30);
                    this.Body.dtpLimitDateTo.Value = this.Body.dtpHakkoDateTo.Value.AddDays(30);
                    this.Body.txtFieldName.Text = string.Empty;
                    this.Body.dgvLooks.Rows.Clear();
                    foreach (DataGridViewColumn c in this.Body.dgvLooks.Columns)
                    {
                        c.ReadOnly = (c.Name != nameof(CellHeaderNameDto.ChkSelect));
                    }
                    this.Body.dgvLooks.Enabled = true;
                    this.Body.grpCondition.Enabled = true;

                    // 使用可否切り替え
                    this.Body.rdoEstima.Checked = true;
                    this.SwitchFormat();

                    // フォーカス
                    this.Body.rdoEstima.Focus();
                    break;
                case InitMode.Condition:
                    this.Body.grpCondition.Enabled = true;
                    this.Body.dgvLooks.Enabled = false;
                    this.Body.btnSelectAll.Enabled = false;
                    this.Body.btnRelease.Enabled = false;
                    this.Body.btnHakko.Enabled = false;
                    this.CurrInitMode = InitMode.All;

                    // フォーカス
                    if (this.Body.rdoEstima.Checked)
                    {
                        this.Body.rdoEstima.Focus();
                    }
                    else
                    {
                        this.Body.rdoSeikyu.Focus();
                    }
                    break;
            }
        }
        #endregion
    }
}
