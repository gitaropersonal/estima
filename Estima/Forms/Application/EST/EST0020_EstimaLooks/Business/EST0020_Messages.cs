﻿namespace Estima.Forms.Application.EST.EST0020.Business
{
    internal static class EST0020_Messages
    {
        internal const string MsgErrorLockedDataSelected = "他ユーザが閲覧中のデータは発行できません";
        internal const string MsgErrorNotSelectedRow = "行を選択してください";
        internal const string MsgErrorNotExistDatas = "対象のデータがありません";
        internal const string MsgErrorNotExistMaster = "マスタ未登録の{0}が入力されています";
        internal const string MsgErrorIrregalZero = "{0}には0より大きい値を入力してください";
        internal const string MsgInfoDelete = "削除しました";
        internal const string MsgAskDelete = "削除しますか？";
        internal const string MsgAskHakko = "発行しますか？";
        internal const string MsgAskUpdate = "更新しますか？";
        internal const string MsgAskClear = "クリアしますか？";
        internal const string MsgAskFinish = "終了しますか？";
        internal const string MsgInfoFinishUpdate = "更新しました";
        internal const string MsgAskNotCommitedDataExist = "未更新のデータがありますが\r\nよろしいですか？";
        internal const string MsgErrorLocked = "他のユーザが開いているため、削除できません";
        internal const string MsgExOpenMeisaiReadOnly = "他のユーザが開いているため、\r\n読み取り専用で開きます";
        internal const string MsgInfoStartCopy = "このデータを複製します\r\n複製データを保存する場合は[更新]ボタンをクリックしてください";
        internal const string MsgAskOpenFolder = "作成しました\r\nフォルダを開きますか？";
    }
}
