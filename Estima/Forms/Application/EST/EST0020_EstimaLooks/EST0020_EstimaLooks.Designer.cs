﻿namespace Estima.Forms.Application.EST.EST0020
{
    partial class EST0020_EstimaLooks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlCondition = new System.Windows.Forms.Panel();
            this.grpCondition = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMarkSpan = new System.Windows.Forms.Label();
            this.dtpLimitDateTo = new System.Windows.Forms.DateTimePicker();
            this.dtpHakkoDateTo = new System.Windows.Forms.DateTimePicker();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtAddressTanto = new System.Windows.Forms.TextBox();
            this.txtAddressCompany = new System.Windows.Forms.TextBox();
            this.txtFieldName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbAddressOffice = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtProcessingNum = new System.Windows.Forms.TextBox();
            this.lblEstimateNumPrefix = new System.Windows.Forms.Label();
            this.dtpLimitDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblLimitDate = new System.Windows.Forms.Label();
            this.dtpHakkoDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblHakkoDate = new System.Windows.Forms.Label();
            this.lblProcessingNum = new System.Windows.Forms.Label();
            this.lblOrderNum = new System.Windows.Forms.Label();
            this.txtOrderNum = new System.Windows.Forms.TextBox();
            this.pnlFormat = new System.Windows.Forms.Panel();
            this.rdoSeikyu = new System.Windows.Forms.RadioButton();
            this.rdoEstima = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRelease = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnHakko = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvLooks = new System.Windows.Forms.DataGridView();
            this.ChkSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BtnEdit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnCopy = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnDel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.EstimaID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstimaKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstimateNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddressCompany = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddressOffice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddressTanto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HakkoDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LimitDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlCondition.SuspendLayout();
            this.grpCondition.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel13.SuspendLayout();
            this.pnlFormat.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLooks)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCondition
            // 
            this.pnlCondition.Controls.Add(this.grpCondition);
            this.pnlCondition.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCondition.Location = new System.Drawing.Point(0, 0);
            this.pnlCondition.Name = "pnlCondition";
            this.pnlCondition.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.pnlCondition.Size = new System.Drawing.Size(1584, 151);
            this.pnlCondition.TabIndex = 0;
            // 
            // grpCondition
            // 
            this.grpCondition.Controls.Add(this.label3);
            this.grpCondition.Controls.Add(this.lblMarkSpan);
            this.grpCondition.Controls.Add(this.dtpLimitDateTo);
            this.grpCondition.Controls.Add(this.dtpHakkoDateTo);
            this.grpCondition.Controls.Add(this.panel14);
            this.grpCondition.Controls.Add(this.txtAddressTanto);
            this.grpCondition.Controls.Add(this.txtAddressCompany);
            this.grpCondition.Controls.Add(this.txtFieldName);
            this.grpCondition.Controls.Add(this.label9);
            this.grpCondition.Controls.Add(this.label6);
            this.grpCondition.Controls.Add(this.cmbAddressOffice);
            this.grpCondition.Controls.Add(this.label5);
            this.grpCondition.Controls.Add(this.label2);
            this.grpCondition.Controls.Add(this.panel13);
            this.grpCondition.Controls.Add(this.dtpLimitDateFrom);
            this.grpCondition.Controls.Add(this.lblLimitDate);
            this.grpCondition.Controls.Add(this.dtpHakkoDateFrom);
            this.grpCondition.Controls.Add(this.lblHakkoDate);
            this.grpCondition.Controls.Add(this.lblProcessingNum);
            this.grpCondition.Controls.Add(this.lblOrderNum);
            this.grpCondition.Controls.Add(this.txtOrderNum);
            this.grpCondition.Controls.Add(this.pnlFormat);
            this.grpCondition.Controls.Add(this.label1);
            this.grpCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCondition.Location = new System.Drawing.Point(15, 10);
            this.grpCondition.Name = "grpCondition";
            this.grpCondition.Size = new System.Drawing.Size(1554, 131);
            this.grpCondition.TabIndex = 0;
            this.grpCondition.TabStop = false;
            this.grpCondition.Text = "検索条件";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1060, 89);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 20);
            this.label3.TabIndex = 59;
            this.label3.Text = "～";
            // 
            // lblMarkSpan
            // 
            this.lblMarkSpan.AutoSize = true;
            this.lblMarkSpan.Location = new System.Drawing.Point(1060, 60);
            this.lblMarkSpan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMarkSpan.Name = "lblMarkSpan";
            this.lblMarkSpan.Size = new System.Drawing.Size(25, 20);
            this.lblMarkSpan.TabIndex = 58;
            this.lblMarkSpan.Text = "～";
            // 
            // dtpLimitDateTo
            // 
            this.dtpLimitDateTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpLimitDateTo.Location = new System.Drawing.Point(1087, 86);
            this.dtpLimitDateTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpLimitDateTo.Name = "dtpLimitDateTo";
            this.dtpLimitDateTo.Size = new System.Drawing.Size(166, 28);
            this.dtpLimitDateTo.TabIndex = 10;
            // 
            // dtpHakkoDateTo
            // 
            this.dtpHakkoDateTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpHakkoDateTo.Location = new System.Drawing.Point(1087, 56);
            this.dtpHakkoDateTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpHakkoDateTo.Name = "dtpHakkoDateTo";
            this.dtpHakkoDateTo.Size = new System.Drawing.Size(166, 28);
            this.dtpHakkoDateTo.TabIndex = 8;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnSearch);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(1359, 24);
            this.panel14.Margin = new System.Windows.Forms.Padding(4);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(192, 104);
            this.panel14.TabIndex = 20;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(50, 38);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(138, 52);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "検索(S)";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // txtAddressTanto
            // 
            this.txtAddressTanto.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtAddressTanto.Location = new System.Drawing.Point(892, 28);
            this.txtAddressTanto.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddressTanto.MaxLength = 30;
            this.txtAddressTanto.Name = "txtAddressTanto";
            this.txtAddressTanto.Size = new System.Drawing.Size(361, 28);
            this.txtAddressTanto.TabIndex = 6;
            // 
            // txtAddressCompany
            // 
            this.txtAddressCompany.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtAddressCompany.Location = new System.Drawing.Point(489, 56);
            this.txtAddressCompany.Margin = new System.Windows.Forms.Padding(4);
            this.txtAddressCompany.MaxLength = 30;
            this.txtAddressCompany.Name = "txtAddressCompany";
            this.txtAddressCompany.Size = new System.Drawing.Size(262, 28);
            this.txtAddressCompany.TabIndex = 4;
            // 
            // txtFieldName
            // 
            this.txtFieldName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFieldName.Location = new System.Drawing.Point(489, 27);
            this.txtFieldName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFieldName.MaxLength = 30;
            this.txtFieldName.Name = "txtFieldName";
            this.txtFieldName.Size = new System.Drawing.Size(262, 28);
            this.txtFieldName.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(360, 31);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 20);
            this.label9.TabIndex = 55;
            this.label9.Text = "現場名";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(779, 31);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 20);
            this.label6.TabIndex = 54;
            this.label6.Text = "宛先（個人）";
            // 
            // cmbAddressOffice
            // 
            this.cmbAddressOffice.FormattingEnabled = true;
            this.cmbAddressOffice.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.cmbAddressOffice.Location = new System.Drawing.Point(489, 85);
            this.cmbAddressOffice.Margin = new System.Windows.Forms.Padding(4);
            this.cmbAddressOffice.MaxLength = 30;
            this.cmbAddressOffice.Name = "cmbAddressOffice";
            this.cmbAddressOffice.Size = new System.Drawing.Size(262, 28);
            this.cmbAddressOffice.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(360, 87);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 20);
            this.label5.TabIndex = 53;
            this.label5.Text = "宛先（営業所）";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(360, 59);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 20);
            this.label2.TabIndex = 52;
            this.label2.Text = "宛先（会社）";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.txtProcessingNum);
            this.panel13.Controls.Add(this.lblEstimateNumPrefix);
            this.panel13.Location = new System.Drawing.Point(110, 85);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(223, 29);
            this.panel13.TabIndex = 2;
            // 
            // txtProcessingNum
            // 
            this.txtProcessingNum.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtProcessingNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtProcessingNum.Location = new System.Drawing.Point(33, 0);
            this.txtProcessingNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtProcessingNum.MaxLength = 7;
            this.txtProcessingNum.Name = "txtProcessingNum";
            this.txtProcessingNum.Size = new System.Drawing.Size(159, 28);
            this.txtProcessingNum.TabIndex = 2;
            // 
            // lblEstimateNumPrefix
            // 
            this.lblEstimateNumPrefix.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblEstimateNumPrefix.Location = new System.Drawing.Point(0, 0);
            this.lblEstimateNumPrefix.Name = "lblEstimateNumPrefix";
            this.lblEstimateNumPrefix.Size = new System.Drawing.Size(33, 29);
            this.lblEstimateNumPrefix.TabIndex = 24;
            this.lblEstimateNumPrefix.Text = "MK";
            this.lblEstimateNumPrefix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpLimitDateFrom
            // 
            this.dtpLimitDateFrom.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpLimitDateFrom.Location = new System.Drawing.Point(892, 86);
            this.dtpLimitDateFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpLimitDateFrom.Name = "dtpLimitDateFrom";
            this.dtpLimitDateFrom.Size = new System.Drawing.Size(166, 28);
            this.dtpLimitDateFrom.TabIndex = 9;
            // 
            // lblLimitDate
            // 
            this.lblLimitDate.AutoSize = true;
            this.lblLimitDate.Location = new System.Drawing.Point(779, 89);
            this.lblLimitDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLimitDate.Name = "lblLimitDate";
            this.lblLimitDate.Size = new System.Drawing.Size(73, 20);
            this.lblLimitDate.TabIndex = 51;
            this.lblLimitDate.Text = "有効期限";
            // 
            // dtpHakkoDateFrom
            // 
            this.dtpHakkoDateFrom.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpHakkoDateFrom.Location = new System.Drawing.Point(892, 57);
            this.dtpHakkoDateFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpHakkoDateFrom.Name = "dtpHakkoDateFrom";
            this.dtpHakkoDateFrom.Size = new System.Drawing.Size(166, 28);
            this.dtpHakkoDateFrom.TabIndex = 7;
            // 
            // lblHakkoDate
            // 
            this.lblHakkoDate.AutoSize = true;
            this.lblHakkoDate.Location = new System.Drawing.Point(779, 60);
            this.lblHakkoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHakkoDate.Name = "lblHakkoDate";
            this.lblHakkoDate.Size = new System.Drawing.Size(57, 20);
            this.lblHakkoDate.TabIndex = 50;
            this.lblHakkoDate.Text = "発行日";
            // 
            // lblProcessingNum
            // 
            this.lblProcessingNum.AutoSize = true;
            this.lblProcessingNum.Location = new System.Drawing.Point(26, 88);
            this.lblProcessingNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProcessingNum.Name = "lblProcessingNum";
            this.lblProcessingNum.Size = new System.Drawing.Size(57, 20);
            this.lblProcessingNum.TabIndex = 49;
            this.lblProcessingNum.Text = "加工№";
            // 
            // lblOrderNum
            // 
            this.lblOrderNum.AutoSize = true;
            this.lblOrderNum.Location = new System.Drawing.Point(26, 58);
            this.lblOrderNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOrderNum.Name = "lblOrderNum";
            this.lblOrderNum.Size = new System.Drawing.Size(57, 20);
            this.lblOrderNum.TabIndex = 48;
            this.lblOrderNum.Text = "発注№";
            // 
            // txtOrderNum
            // 
            this.txtOrderNum.Location = new System.Drawing.Point(110, 56);
            this.txtOrderNum.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderNum.MaxLength = 12;
            this.txtOrderNum.Name = "txtOrderNum";
            this.txtOrderNum.Size = new System.Drawing.Size(192, 28);
            this.txtOrderNum.TabIndex = 1;
            // 
            // pnlFormat
            // 
            this.pnlFormat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlFormat.Controls.Add(this.rdoSeikyu);
            this.pnlFormat.Controls.Add(this.rdoEstima);
            this.pnlFormat.Location = new System.Drawing.Point(110, 27);
            this.pnlFormat.Margin = new System.Windows.Forms.Padding(4);
            this.pnlFormat.Name = "pnlFormat";
            this.pnlFormat.Size = new System.Drawing.Size(192, 28);
            this.pnlFormat.TabIndex = 0;
            // 
            // rdoSeikyu
            // 
            this.rdoSeikyu.AutoSize = true;
            this.rdoSeikyu.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoSeikyu.Location = new System.Drawing.Point(99, 0);
            this.rdoSeikyu.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSeikyu.Name = "rdoSeikyu";
            this.rdoSeikyu.Size = new System.Drawing.Size(75, 26);
            this.rdoSeikyu.TabIndex = 2;
            this.rdoSeikyu.TabStop = true;
            this.rdoSeikyu.Text = "請求書";
            this.rdoSeikyu.UseVisualStyleBackColor = true;
            // 
            // rdoEstima
            // 
            this.rdoEstima.AutoSize = true;
            this.rdoEstima.Dock = System.Windows.Forms.DockStyle.Left;
            this.rdoEstima.Location = new System.Drawing.Point(0, 0);
            this.rdoEstima.Margin = new System.Windows.Forms.Padding(4);
            this.rdoEstima.Name = "rdoEstima";
            this.rdoEstima.Padding = new System.Windows.Forms.Padding(12, 0, 12, 0);
            this.rdoEstima.Size = new System.Drawing.Size(99, 26);
            this.rdoEstima.TabIndex = 1;
            this.rdoEstima.TabStop = true;
            this.rdoEstima.Text = "見積書";
            this.rdoEstima.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 40;
            this.label1.Text = "フォーマット";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 791);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1584, 70);
            this.panel3.TabIndex = 102;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(1359, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(225, 70);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(69, 8);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnRelease);
            this.panel4.Controls.Add(this.btnSelectAll);
            this.panel4.Controls.Add(this.btnHakko);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(736, 70);
            this.panel4.TabIndex = 110;
            // 
            // btnRelease
            // 
            this.btnRelease.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRelease.Location = new System.Drawing.Point(291, 9);
            this.btnRelease.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnRelease.Name = "btnRelease";
            this.btnRelease.Size = new System.Drawing.Size(138, 52);
            this.btnRelease.TabIndex = 113;
            this.btnRelease.Text = "全解除（R）";
            this.btnRelease.UseVisualStyleBackColor = true;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectAll.Location = new System.Drawing.Point(153, 9);
            this.btnSelectAll.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(138, 52);
            this.btnSelectAll.TabIndex = 112;
            this.btnSelectAll.Text = "全選択（N）";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            // 
            // btnHakko
            // 
            this.btnHakko.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnHakko.Location = new System.Drawing.Point(429, 9);
            this.btnHakko.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnHakko.Name = "btnHakko";
            this.btnHakko.Size = new System.Drawing.Size(138, 52);
            this.btnHakko.TabIndex = 114;
            this.btnHakko.Text = "発行（H）";
            this.btnHakko.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(15, 9);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvLooks);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 151);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.panel2.Size = new System.Drawing.Size(1584, 640);
            this.panel2.TabIndex = 30;
            // 
            // dgvLooks
            // 
            this.dgvLooks.AllowUserToAddRows = false;
            this.dgvLooks.AllowUserToDeleteRows = false;
            this.dgvLooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChkSelect,
            this.BtnEdit,
            this.BtnCopy,
            this.BtnDel,
            this.EstimaID,
            this.EstimaKbn,
            this.EstimateNo,
            this.OrderNo,
            this.ProcessingNo,
            this.FieldName,
            this.AddressCompany,
            this.AddressOffice,
            this.AddressTanto,
            this.HakkoDate,
            this.LimitDate,
            this.Status});
            this.dgvLooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLooks.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.dgvLooks.Location = new System.Drawing.Point(15, 10);
            this.dgvLooks.Margin = new System.Windows.Forms.Padding(4);
            this.dgvLooks.Name = "dgvLooks";
            this.dgvLooks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLooks.RowTemplate.Height = 21;
            this.dgvLooks.Size = new System.Drawing.Size(1554, 620);
            this.dgvLooks.TabIndex = 31;
            // 
            // ChkSelect
            // 
            this.ChkSelect.DataPropertyName = "ChkSelect";
            this.ChkSelect.HeaderText = "選択";
            this.ChkSelect.Name = "ChkSelect";
            this.ChkSelect.Width = 50;
            // 
            // BtnEdit
            // 
            this.BtnEdit.DataPropertyName = "BtnEdit";
            this.BtnEdit.HeaderText = "";
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Width = 60;
            // 
            // BtnCopy
            // 
            this.BtnCopy.DataPropertyName = "BtnCopy";
            this.BtnCopy.HeaderText = "";
            this.BtnCopy.Name = "BtnCopy";
            this.BtnCopy.Width = 60;
            // 
            // BtnDel
            // 
            this.BtnDel.DataPropertyName = "BtnDel";
            this.BtnDel.HeaderText = "";
            this.BtnDel.Name = "BtnDel";
            this.BtnDel.Width = 60;
            // 
            // EstimaID
            // 
            this.EstimaID.DataPropertyName = "EstimaID";
            this.EstimaID.HeaderText = "EstimaID";
            this.EstimaID.Name = "EstimaID";
            this.EstimaID.Visible = false;
            // 
            // EstimaKbn
            // 
            this.EstimaKbn.DataPropertyName = "EstimaKbn";
            this.EstimaKbn.HeaderText = "EstimaKbn";
            this.EstimaKbn.Name = "EstimaKbn";
            this.EstimaKbn.Visible = false;
            // 
            // EstimateNo
            // 
            this.EstimateNo.DataPropertyName = "EstimateNo";
            this.EstimateNo.HeaderText = "見積№";
            this.EstimateNo.Name = "EstimateNo";
            // 
            // OrderNo
            // 
            this.OrderNo.DataPropertyName = "OrderNo";
            this.OrderNo.HeaderText = "発注№";
            this.OrderNo.MaxInputLength = 80;
            this.OrderNo.Name = "OrderNo";
            this.OrderNo.Width = 150;
            // 
            // ProcessingNo
            // 
            this.ProcessingNo.DataPropertyName = "ProcessingNo";
            this.ProcessingNo.HeaderText = "加工№";
            this.ProcessingNo.Name = "ProcessingNo";
            // 
            // FieldName
            // 
            this.FieldName.DataPropertyName = "FieldName";
            this.FieldName.HeaderText = "現場名";
            this.FieldName.Name = "FieldName";
            this.FieldName.Width = 200;
            // 
            // AddressCompany
            // 
            this.AddressCompany.DataPropertyName = "AddressCompany";
            this.AddressCompany.HeaderText = "宛先（会社）";
            this.AddressCompany.Name = "AddressCompany";
            this.AddressCompany.Width = 200;
            // 
            // AddressOffice
            // 
            this.AddressOffice.DataPropertyName = "AddressOffice";
            this.AddressOffice.HeaderText = "宛先（営業所）";
            this.AddressOffice.Name = "AddressOffice";
            this.AddressOffice.Width = 200;
            // 
            // AddressTanto
            // 
            this.AddressTanto.DataPropertyName = "AddressTanto";
            this.AddressTanto.HeaderText = "宛先（個人）";
            this.AddressTanto.Name = "AddressTanto";
            this.AddressTanto.Width = 200;
            // 
            // HakkoDate
            // 
            this.HakkoDate.DataPropertyName = "HakkoDate";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.HakkoDate.DefaultCellStyle = dataGridViewCellStyle1;
            this.HakkoDate.HeaderText = "発行日";
            this.HakkoDate.Name = "HakkoDate";
            this.HakkoDate.Width = 110;
            // 
            // LimitDate
            // 
            this.LimitDate.DataPropertyName = "LimitDate";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.LimitDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.LimitDate.HeaderText = "有効期限";
            this.LimitDate.Name = "LimitDate";
            this.LimitDate.Width = 110;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            // 
            // EST0020_EstimaLooks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 861);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnlCondition);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MinimumSize = new System.Drawing.Size(1000, 600);
            this.Name = "EST0020_EstimaLooks";
            this.Text = "伝票一覧";
            this.pnlCondition.ResumeLayout(false);
            this.grpCondition.ResumeLayout(false);
            this.grpCondition.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.pnlFormat.ResumeLayout(false);
            this.pnlFormat.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLooks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCondition;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.DataGridView dgvLooks;
        public System.Windows.Forms.GroupBox grpCondition;
        public System.Windows.Forms.Panel panel14;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtAddressTanto;
        public System.Windows.Forms.TextBox txtAddressCompany;
        public System.Windows.Forms.TextBox txtFieldName;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cmbAddressOffice;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel13;
        public System.Windows.Forms.TextBox txtProcessingNum;
        public System.Windows.Forms.Label lblEstimateNumPrefix;
        public System.Windows.Forms.DateTimePicker dtpLimitDateFrom;
        public System.Windows.Forms.Label lblLimitDate;
        public System.Windows.Forms.DateTimePicker dtpHakkoDateFrom;
        public System.Windows.Forms.Label lblHakkoDate;
        public System.Windows.Forms.Label lblProcessingNum;
        public System.Windows.Forms.Label lblOrderNum;
        public System.Windows.Forms.TextBox txtOrderNum;
        public System.Windows.Forms.Panel pnlFormat;
        public System.Windows.Forms.RadioButton rdoSeikyu;
        public System.Windows.Forms.RadioButton rdoEstima;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DateTimePicker dtpLimitDateTo;
        public System.Windows.Forms.DateTimePicker dtpHakkoDateTo;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label lblMarkSpan;
        public System.Windows.Forms.Button btnHakko;
        public System.Windows.Forms.Button btnSelectAll;
        public System.Windows.Forms.Button btnRelease;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ChkSelect;
        private System.Windows.Forms.DataGridViewButtonColumn BtnEdit;
        private System.Windows.Forms.DataGridViewButtonColumn BtnCopy;
        private System.Windows.Forms.DataGridViewButtonColumn BtnDel;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstimaID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstimaKbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstimateNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddressCompany;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddressOffice;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddressTanto;
        private System.Windows.Forms.DataGridViewTextBoxColumn HakkoDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn LimitDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}