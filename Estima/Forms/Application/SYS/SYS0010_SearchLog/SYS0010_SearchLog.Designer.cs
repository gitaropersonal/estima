﻿namespace Estima.Forms.Application.SYS.SYS0010
{
    partial class SYS0010_SearchLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHostName = new System.Windows.Forms.TextBox();
            this.lblMarkSpan = new System.Windows.Forms.Label();
            this.dtpAddDateTo = new System.Windows.Forms.DateTimePicker();
            this.dtpAddDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblHakkoDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.cmbLogType = new System.Windows.Forms.ComboBox();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtScreenName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dgvMaster = new System.Windows.Forms.DataGridView();
            this.LogType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LogTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ScreenName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Message = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnStackTrace = new System.Windows.Forms.DataGridViewButtonColumn();
            this.StackTrace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Version = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddHostName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaster)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(19, 11);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(1516, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(168, 75);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(11, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(344, 75);
            this.panel4.TabIndex = 110;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 886);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1684, 75);
            this.panel3.TabIndex = 106;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(19, 18, 0, 0);
            this.panel1.Size = new System.Drawing.Size(1684, 118);
            this.panel1.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.txtHostName);
            this.panel7.Controls.Add(this.lblMarkSpan);
            this.panel7.Controls.Add(this.dtpAddDateTo);
            this.panel7.Controls.Add(this.dtpAddDateFrom);
            this.panel7.Controls.Add(this.lblHakkoDate);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.txtVersion);
            this.panel7.Controls.Add(this.cmbLogType);
            this.panel7.Controls.Add(this.txtMessage);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.txtScreenName);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.lblName);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(19, 18);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.panel7.Size = new System.Drawing.Size(1153, 100);
            this.panel7.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(416, 35);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 20);
            this.label4.TabIndex = 64;
            this.label4.Text = "ホスト名";
            // 
            // txtHostName
            // 
            this.txtHostName.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHostName.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHostName.Location = new System.Drawing.Point(493, 32);
            this.txtHostName.Margin = new System.Windows.Forms.Padding(4);
            this.txtHostName.MaxLength = 25;
            this.txtHostName.Name = "txtHostName";
            this.txtHostName.Size = new System.Drawing.Size(167, 28);
            this.txtHostName.TabIndex = 4;
            // 
            // lblMarkSpan
            // 
            this.lblMarkSpan.AutoSize = true;
            this.lblMarkSpan.Location = new System.Drawing.Point(717, 65);
            this.lblMarkSpan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMarkSpan.Name = "lblMarkSpan";
            this.lblMarkSpan.Size = new System.Drawing.Size(25, 20);
            this.lblMarkSpan.TabIndex = 62;
            this.lblMarkSpan.Text = "～";
            // 
            // dtpAddDateTo
            // 
            this.dtpAddDateTo.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.dtpAddDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAddDateTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpAddDateTo.Location = new System.Drawing.Point(744, 61);
            this.dtpAddDateTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpAddDateTo.Name = "dtpAddDateTo";
            this.dtpAddDateTo.Size = new System.Drawing.Size(220, 28);
            this.dtpAddDateTo.TabIndex = 6;
            // 
            // dtpAddDateFrom
            // 
            this.dtpAddDateFrom.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.dtpAddDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAddDateFrom.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpAddDateFrom.Location = new System.Drawing.Point(494, 61);
            this.dtpAddDateFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpAddDateFrom.Name = "dtpAddDateFrom";
            this.dtpAddDateFrom.Size = new System.Drawing.Size(220, 28);
            this.dtpAddDateFrom.TabIndex = 5;
            // 
            // lblHakkoDate
            // 
            this.lblHakkoDate.AutoSize = true;
            this.lblHakkoDate.Location = new System.Drawing.Point(416, 64);
            this.lblHakkoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHakkoDate.Name = "lblHakkoDate";
            this.lblHakkoDate.Size = new System.Drawing.Size(73, 20);
            this.lblHakkoDate.TabIndex = 61;
            this.lblHakkoDate.Text = "記録日時";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(416, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "バージョン";
            // 
            // txtVersion
            // 
            this.txtVersion.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtVersion.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtVersion.Location = new System.Drawing.Point(493, 3);
            this.txtVersion.Margin = new System.Windows.Forms.Padding(4);
            this.txtVersion.MaxLength = 20;
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(167, 28);
            this.txtVersion.TabIndex = 3;
            // 
            // cmbLogType
            // 
            this.cmbLogType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLogType.FormattingEnabled = true;
            this.cmbLogType.Location = new System.Drawing.Point(112, 3);
            this.cmbLogType.Name = "cmbLogType";
            this.cmbLogType.Size = new System.Drawing.Size(122, 28);
            this.cmbLogType.TabIndex = 0;
            // 
            // txtMessage
            // 
            this.txtMessage.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMessage.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtMessage.Location = new System.Drawing.Point(112, 61);
            this.txtMessage.Margin = new System.Windows.Forms.Padding(4);
            this.txtMessage.MaxLength = 25;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(253, 28);
            this.txtMessage.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(4, 64);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "メッセージ";
            // 
            // txtScreenName
            // 
            this.txtScreenName.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtScreenName.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtScreenName.Location = new System.Drawing.Point(112, 32);
            this.txtScreenName.Margin = new System.Windows.Forms.Padding(4);
            this.txtScreenName.MaxLength = 25;
            this.txtScreenName.Name = "txtScreenName";
            this.txtScreenName.Size = new System.Drawing.Size(253, 28);
            this.txtScreenName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(4, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 13;
            this.label1.Text = "画面名";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblName.Location = new System.Drawing.Point(4, 6);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(65, 20);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "ログ種別";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1516, 18);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(168, 100);
            this.panel2.TabIndex = 23;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(11, 37);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(138, 52);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dgvMaster);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel6.Location = new System.Drawing.Point(0, 118);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(19, 12, 19, 6);
            this.panel6.Size = new System.Drawing.Size(1684, 768);
            this.panel6.TabIndex = 30;
            // 
            // dgvMaster
            // 
            this.dgvMaster.AllowUserToAddRows = false;
            this.dgvMaster.AllowUserToDeleteRows = false;
            this.dgvMaster.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvMaster.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMaster.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LogType,
            this.LogTypeName,
            this.ScreenName,
            this.Message,
            this.BtnStackTrace,
            this.StackTrace,
            this.Version,
            this.AddHostName,
            this.AddDate,
            this.Status});
            this.dgvMaster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMaster.Location = new System.Drawing.Point(19, 12);
            this.dgvMaster.Margin = new System.Windows.Forms.Padding(4);
            this.dgvMaster.Name = "dgvMaster";
            this.dgvMaster.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvMaster.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvMaster.RowTemplate.Height = 21;
            this.dgvMaster.Size = new System.Drawing.Size(1646, 750);
            this.dgvMaster.TabIndex = 31;
            // 
            // LogType
            // 
            this.LogType.DataPropertyName = "LogType";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LogType.DefaultCellStyle = dataGridViewCellStyle2;
            this.LogType.HeaderText = "LogType";
            this.LogType.MaxInputLength = 3;
            this.LogType.Name = "LogType";
            this.LogType.Visible = false;
            this.LogType.Width = 90;
            // 
            // LogTypeName
            // 
            this.LogTypeName.DataPropertyName = "LogTypeName";
            this.LogTypeName.HeaderText = "ログ種別";
            this.LogTypeName.Name = "LogTypeName";
            // 
            // ScreenName
            // 
            this.ScreenName.DataPropertyName = "ScreenName";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ScreenName.DefaultCellStyle = dataGridViewCellStyle3;
            this.ScreenName.HeaderText = "画面名";
            this.ScreenName.MaxInputLength = 25;
            this.ScreenName.Name = "ScreenName";
            this.ScreenName.Width = 250;
            // 
            // Message
            // 
            this.Message.DataPropertyName = "Message";
            this.Message.HeaderText = "メッセージ";
            this.Message.Name = "Message";
            this.Message.Width = 600;
            // 
            // BtnStackTrace
            // 
            this.BtnStackTrace.DataPropertyName = "BtnStackTrace";
            this.BtnStackTrace.HeaderText = "ｽﾀｯｸﾄﾚｰｽ";
            this.BtnStackTrace.Name = "BtnStackTrace";
            this.BtnStackTrace.Width = 85;
            // 
            // StackTrace
            // 
            this.StackTrace.DataPropertyName = "StackTrace";
            this.StackTrace.HeaderText = "スタックトレース";
            this.StackTrace.Name = "StackTrace";
            this.StackTrace.Visible = false;
            this.StackTrace.Width = 200;
            // 
            // Version
            // 
            this.Version.DataPropertyName = "Version";
            this.Version.HeaderText = "バージョン";
            this.Version.Name = "Version";
            this.Version.Width = 130;
            // 
            // AddHostName
            // 
            this.AddHostName.DataPropertyName = "AddHostName";
            this.AddHostName.HeaderText = "ホスト名";
            this.AddHostName.Name = "AddHostName";
            this.AddHostName.Width = 180;
            // 
            // AddDate
            // 
            this.AddDate.DataPropertyName = "AddDate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AddDate.DefaultCellStyle = dataGridViewCellStyle4;
            this.AddDate.HeaderText = "記録日時";
            this.AddDate.Name = "AddDate";
            this.AddDate.Width = 220;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Status.DefaultCellStyle = dataGridViewCellStyle5;
            this.Status.HeaderText = "ステータス";
            this.Status.Name = "Status";
            this.Status.Visible = false;
            this.Status.Width = 83;
            // 
            // SYS0010_SearchLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1684, 961);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MinimumSize = new System.Drawing.Size(1600, 1000);
            this.Name = "SYS0010_SearchLog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "親コード";
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMaster)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel7;
        public System.Windows.Forms.Label lblName;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.DataGridView dgvMaster;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtScreenName;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtMessage;
        public System.Windows.Forms.ComboBox cmbLogType;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtVersion;
        public System.Windows.Forms.Label lblMarkSpan;
        public System.Windows.Forms.DateTimePicker dtpAddDateTo;
        public System.Windows.Forms.DateTimePicker dtpAddDateFrom;
        public System.Windows.Forms.Label lblHakkoDate;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtHostName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LogType;
        private System.Windows.Forms.DataGridViewTextBoxColumn LogTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScreenName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Message;
        private System.Windows.Forms.DataGridViewButtonColumn BtnStackTrace;
        private System.Windows.Forms.DataGridViewTextBoxColumn StackTrace;
        private System.Windows.Forms.DataGridViewTextBoxColumn Version;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddHostName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}