﻿namespace Estima.Forms.Application.SYS.SYS0010.Business
{
    public class SYS0010_GridDto
    {
        public SYS0010_GridDto()
        {
            this.BtnStackTrace = "表示";
        }
        public int LogType { get; set; }
        public string LogTypeName { get; set; }
        public string ScreenName { get; set; }
        public string Message { get; set; }
        public string BtnStackTrace { get; set; }
        public string StackTrace { get; set; }
        public string Version { get; set; }
        public string AddHostName { get; set; }
        public string AddDate { get; set; }
    }
}
