﻿using System;

namespace Estima.Forms.Application.SYS.SYS0010.Business
{
    public class SYS0010_SelCondition
    {
        public int LogType { get; set; }
        public string ScreenName { get; set; }
        public string Message { get; set; }
        public string Version { get; set; }
        public string HostName { get; set; }
        public DateTime AddDateFrom { get; set; }
        public DateTime AddDateTo { get; set; }
    }
}
