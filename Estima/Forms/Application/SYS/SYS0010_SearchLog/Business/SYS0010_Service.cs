﻿using EstimaLib.Const;
using GadgetCommon.Const;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;

namespace Estima.Forms.Application.SYS.SYS0010.Business
{
    internal class SYS0010_Service
    {
        #region MX02ParentCD
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        internal BindingList<SYS0010_GridDto> GetGridDatas(SYS0010_SelCondition con)
        {
            string cmOutLogType = CommonUtil.GetCmout(con.LogType == -1);
            string cmOutScreenName = CommonUtil.GetCmout(string.IsNullOrEmpty(con.ScreenName));
            string cmOutMessage = CommonUtil.GetCmout(string.IsNullOrEmpty(con.Message));
            string cmOutVersion = CommonUtil.GetCmout(string.IsNullOrEmpty(con.Version));
            string cmOutHostName = CommonUtil.GetCmout(string.IsNullOrEmpty(con.HostName));

            var ret = new BindingList<SYS0010_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    LX01.LogType ");
            sb.AppendLine($",   LX01.ScreenName ");
            sb.AppendLine($",   LX01.Message ");
            sb.AppendLine($",   LX01.StackTrace ");
            sb.AppendLine($",   LX01.Version ");
            sb.AppendLine($",   LX01.AddHostName ");
            sb.AppendLine($",   LX01.AddDate ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    LX01LogInfo AS LX01 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutLogType   }AND LX01.LogType        = @logType ");
            sb.AppendLine($"{cmOutScreenName}AND LX01.ScreenName  LIKE @screenName ");
            sb.AppendLine($"{cmOutMessage   }AND LX01.Message     LIKE @message ");
            sb.AppendLine($"{cmOutVersion   }AND LX01.Version     LIKE @version ");
            sb.AppendLine($"{cmOutHostName  }AND LX01.AddHostName LIKE @hostName ");
            sb.AppendLine($"AND LX01.AddDate >= @addDateFrom ");
            sb.AppendLine($"AND LX01.AddDate <= @addDateTo ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    LX01.AddDate DESC ");
            sb.AppendLine($",   LX01.ScreenName ");
            sb.AppendLine($",   LX01.Message ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@screenName", "%" + con.ScreenName + "%");
                    cmd.Parameters.AddWithValue("@message"   , "%" + con.Message    + "%");
                    cmd.Parameters.AddWithValue("@version"   , "%" + con.Version    + "%");
                    cmd.Parameters.AddWithValue("@hostName"  , "%" + con.HostName   + "%");
                    cmd.Parameters.Add(new MySqlParameter("@logType", con.LogType) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@addDateFrom", con.AddDateFrom.ToString("yyyy/MM/dd HH:mm:ss")) { DbType = DbType.DateTime });
                    cmd.Parameters.Add(new MySqlParameter("@addDateTo", con.AddDateTo.ToString("yyyy/MM/dd HH:mm:ss")) { DbType = DbType.DateTime });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new SYS0010_GridDto();
                            dto.LogType = CommonUtil.ToInteger(dr[nameof(dto.LogType)]);
                            dto.LogTypeName = Enums.LogTypes.First(n => (int)n.Key == dto.LogType).Value;
                            dto.ScreenName = CommonUtil.TostringNullForbid(dr[nameof(dto.ScreenName)]);
                            dto.Message = CommonUtil.TostringNullForbid(dr[nameof(dto.Message)]);
                            dto.StackTrace = CommonUtil.TostringNullForbid(dr[nameof(dto.StackTrace)]);
                            dto.Version = CommonUtil.TostringNullForbid(dr[nameof(dto.Version)]);
                            dto.AddHostName = CommonUtil.TostringNullForbid(dr[nameof(dto.AddHostName)]);
                            dto.AddDate = CommonUtil.ToDateTime((object)dr[nameof(dto.AddDate)]).ToString(Formats.YYYYMMDDHHMMSSFFF_Slash);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}
