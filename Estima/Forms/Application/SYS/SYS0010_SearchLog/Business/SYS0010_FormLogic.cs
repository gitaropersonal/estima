﻿using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Forms;
using GadgetCommon.Forms.MessageBoxPlus;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Application.SYS.SYS0010.Business
{
    internal class SYS0010_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private SYS0010_SearchLog Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo;
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// 専用サービス
        /// </summary>
        private readonly SYS0010_Service PersonalService = new SYS0010_Service();
        /// <summary>
        /// 削除されたデータ
        /// </summary>
        private List<SYS0010_GridDto> DeletedDatas = new List<SYS0010_GridDto>();
        /// <summary>
        /// 列ヘッダdto
        /// </summary>
        private static SYS0010_GridDto CellHeaderNameDto = new SYS0010_GridDto();
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        internal SYS0010_FormLogic(
              SYS0010_SearchLog body
            , LoginInfoDto loginInfo
            )
        {
            this.Body = body;
            this.LoginInfo = loginInfo;
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += this.BtnClearClickEvent;
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了処理
            this.Body.FormClosing += this.ClosingEvent;
            // 検索ボタン押下
            this.Body.btnSearch.Click += this.BtnSearchClickEvent;
            // グリッドRowPostPaint
            this.Body.dgvMaster.RowPostPaint += this.GridRowPostPaintEvent;
            // グリッドセルクリック
            this.Body.dgvMaster.CellClick += this.GridCellClick;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        this.Body.btnSearch.Focus();
                        this.Body.btnSearch.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// クリアボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnClearClickEvent(
              object s
            , EventArgs e
            )
        {
            var drConfirm = CommonUtil.ShowInfoMsgOKCancel(SYS0010_Messages.MsgAskClear);
            if (drConfirm != DialogResult.OK)
            {
                return;
            }
            this.Init();
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                var drConfirm = CommonUtil.ShowInfoMsgOKCancel(SYS0010_Messages.MsgAskFinish);
                if (drConfirm != DialogResult.OK)
                {
                    cancel = true;
                    return;
                }
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                var drConfirm = CommonUtil.ShowInfoMsgOKCancel(SYS0010_Messages.MsgAskFinish);
                if (drConfirm != DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// グリッドRowPostPaint
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridRowPostPaintEvent(
              object s
            , DataGridViewRowPostPaintEventArgs e
            )
        {
            GridRowUtil<SYS0010_GridDto>.SetRowNum(s, e);
        }
        /// <summary>
        /// グリッドセルクリック
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellClick(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            DataGridViewRow r = dgv.Rows[e.RowIndex];
            DataGridViewCell c = r.Cells[e.ColumnIndex];
            var rowDto = GridRowUtil<SYS0010_GridDto>.GetRowModel(r);
            try
            {
                switch (c.OwningColumn.Name)
                {
                    // ｽﾀｯｸﾄﾚｰｽ表示ボタン押下
                    case nameof(CellHeaderNameDto.BtnStackTrace):
                        if (string.IsNullOrEmpty(rowDto.StackTrace))
                        {
                            CommonUtil.ShowErrorMsg(SYS0010_Messages.MsgErrorNotExistDatas);
                            return;
                        }
                        new MessageBoxPlus(
                              "エラー発生時のスタックトレース"
                            , "確認"
                            , MessageBoxButtons.OK
                            , MessageBoxIcon.Information
                            , rowDto.StackTrace
                        ).ShowDialog();
                        break;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSearchClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// <summary>
        /// 検索処理
        /// </summary>
        private void SearchLogic()
        {
            // グリッド初期化
            this.Body.dgvMaster.Rows.Clear();

            // Validate
            if (this.Body.dtpAddDateTo.Value < this.Body.dtpAddDateFrom.Value)
            {
                this.Body.dtpAddDateTo.Focus();
                CommonUtil.ShowErrorMsg(string.Format(Messages.MsgErrorLargeSmallCollision, "記録日時（終了）", "記録日時（開始）"));
                return;
            }
            // 検索条件
            int logType = -1;
            if (!string.IsNullOrEmpty(this.Body.cmbLogType.Text))
            {
                logType = (int)(Enums.LogTypes.First(n => n.Value == this.Body.cmbLogType.Text).Key);
            }
            var con = new SYS0010_SelCondition()
            {
                LogType = logType,
                ScreenName = this.Body.txtScreenName.Text,
                Message = this.Body.txtMessage.Text,
                Version = this.Body.txtVersion.Text,
                HostName = this.Body.txtHostName.Text,
                AddDateFrom = this.Body.dtpAddDateFrom.Value,
                AddDateTo = this.Body.dtpAddDateTo.Value,
            };
            // グリッドデータ
            var bindingList = new BindingList<SYS0010_GridDto>();
            CommonUtil.ShowProgressDialog((new Action(() =>
            {
                try
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.Start);
                    bindingList = this.PersonalService.GetGridDatas(con);
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.End);
                }
                catch (Exception ex)
                {
                    EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                    return;
                }
            })));
            if (!bindingList.Any())
            {
                CommonUtil.ShowErrorMsg(SYS0010_Messages.MsgErrorNotExistDatas);
                return;
            }
            // グリッドデータセット
            this.Body.dgvMaster.DataSource = bindingList;
            this.Body.dgvMaster.AllowUserToAddRows = false;

            // フォーカス
            this.Body.dgvMaster.Focus();
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            // 検索条件
            this.Body.cmbLogType.Items.Clear();
            this.Body.cmbLogType.Items.Add(string.Empty);
            Enums.LogTypes.ToList().ForEach(n => this.Body.cmbLogType.Items.Add(n.Value));
            this.Body.cmbLogType.Text = string.Empty;
            this.Body.txtScreenName.Text = string.Empty;
            this.Body.txtMessage.Text = string.Empty;
            this.Body.txtVersion.Text = CommonUtil.GetVersion();
            this.Body.dtpAddDateFrom.Value = DateTime.Today.AddDays(-7);
            this.Body.dtpAddDateTo.Value = DateTime.Now;

            // 削除データ
            this.DeletedDatas.Clear();

            // グリッド
            this.Body.dgvMaster.Rows.Clear();
            this.Body.dgvMaster.AllowUserToAddRows = false;
            this.Body.dgvMaster.ReadOnly = true;

            // フォーカス
            this.Body.cmbLogType.Focus();
        }
        #endregion
    }
}
