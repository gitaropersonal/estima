﻿using Estima.Forms.Application.SYS.SYS0010.Business;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Application.SYS.SYS0010
{
    public partial class SYS0010_SearchLog : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        public SYS0010_SearchLog(LoginInfoDto loginInfo)
        {
            this.InitializeComponent();
            try
            {
                new SYS0010_FormLogic(
                      this
                    , loginInfo
                );
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
