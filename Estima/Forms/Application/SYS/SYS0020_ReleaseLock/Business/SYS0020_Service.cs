﻿using EstimaLib.Const;
using GadgetCommon.Const;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.ComponentModel;
using System.Data;
using System.Text;

namespace Estima.Forms.Application.SYS.SYS0020.Business
{
    internal class SYS0020_Service
    {
        #region MX02ParentCD
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="con"></param>
        /// <returns></returns>
        internal BindingList<SYS0020_GridDto> GetGridDatas(SYS0020_SelCondition con)
        {
            string cmOutEstimaID = CommonUtil.GetCmout(string.IsNullOrEmpty(con.EstimaID));
            string cmOutEstimaNum = CommonUtil.GetCmout(string.IsNullOrEmpty(con.EstimaNo));
            string cmOutOrderNum = CommonUtil.GetCmout(string.IsNullOrEmpty(con.OrderNo));
            string cmOutProcessingNum = CommonUtil.GetCmout(string.IsNullOrEmpty(con.ProcessingNo));
            string cmOutIPAddress = CommonUtil.GetCmout(string.IsNullOrEmpty(con.IPAddress));
            string cmOutHostName = CommonUtil.GetCmout(string.IsNullOrEmpty(con.HostName));

            var ret = new BindingList<SYS0020_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    EX04.EstimaID ");
            sb.AppendLine($",   CASE WHEN EX01.EstimateNo   IS NULL THEN '' ELSE EX01.EstimateNo   END AS EstimaNum ");
            sb.AppendLine($",   CASE WHEN EX01.OrderNo      IS NULL THEN '' ELSE EX01.OrderNo      END AS OrderNum ");
            sb.AppendLine($",   CASE WHEN EX01.ProcessingNo IS NULL THEN '' ELSE EX01.ProcessingNo END AS ProcessingNum ");
            sb.AppendLine($",   EX04.AddIPAddress ");
            sb.AppendLine($",   EX04.AddHostName ");
            sb.AppendLine($",   EX04.AddDate ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX04EstimaLock AS EX04 ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    EX01EstimaHeader AS EX01 ");
            sb.AppendLine($"ON ");
            sb.AppendLine($"    EX04.EstimaID = EX01.EstimaID ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutEstimaID     }AND EX04.EstimaID     LIKE @estimaID ");
            sb.AppendLine($"{cmOutEstimaNum    }AND EX01.EstimateNo   LIKE @estimateNo ");
            sb.AppendLine($"{cmOutOrderNum     }AND EX01.OrderNo      LIKE @orderNo ");
            sb.AppendLine($"{cmOutProcessingNum}AND EX01.ProcessingNo LIKE @processingNo ");
            sb.AppendLine($"{cmOutIPAddress    }AND EX04.AddIPAddress LIKE @addIPAddress ");
            sb.AppendLine($"{cmOutHostName     }AND EX04.AddHostName  LIKE @addHostName ");
            sb.AppendLine($"AND EX04.AddDate >= @addDateFrom ");
            sb.AppendLine($"AND EX04.AddDate <= @addDateTo ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    EX04.AddDate DESC ");
            sb.AppendLine($",   EX04.EstimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@estimaID"    , "%" + con.EstimaID      + "%");
                    cmd.Parameters.AddWithValue("@estimateNo"  , "%" + con.EstimaNo     + "%");
                    cmd.Parameters.AddWithValue("@orderNo"     , "%" + con.OrderNo      + "%");
                    cmd.Parameters.AddWithValue("@processingNo", "%" + con.ProcessingNo + "%");
                    cmd.Parameters.AddWithValue("@addIPAddress", "%" + con.IPAddress     + "%");
                    cmd.Parameters.AddWithValue("@addHostName" , "%" + con.HostName      + "%");
                    cmd.Parameters.Add(new MySqlParameter("@addDateFrom", con.AddDateFrom.ToString("yyyy/MM/dd HH:mm:ss")) { DbType = DbType.DateTime });
                    cmd.Parameters.Add(new MySqlParameter("@addDateTo", con.AddDateTo.ToString("yyyy/MM/dd HH:mm:ss")) { DbType = DbType.DateTime });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new SYS0020_GridDto();
                            dto.EstimaID = CommonUtil.TostringNullForbid(dr[nameof(dto.EstimaID)]);
                            dto.EstimaNum = CommonUtil.TostringNullForbid(dr[nameof(dto.EstimaNum)]);
                            dto.OrderNum = CommonUtil.TostringNullForbid(dr[nameof(dto.OrderNum)]);
                            dto.ProcessingNum = CommonUtil.TostringNullForbid(dr[nameof(dto.ProcessingNum)]);
                            dto.AddIPAddress = CommonUtil.TostringNullForbid(dr[nameof(dto.AddIPAddress)]);
                            dto.AddHostName = CommonUtil.TostringNullForbid(dr[nameof(dto.AddHostName)]);
                            dto.AddDate = CommonUtil.ToDateTime((object)dr[nameof(dto.AddDate)]).ToString(Formats.YYYYMMDDHHMMSSFFF_Slash);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// ロックデータ削除
        /// </summary>
        /// <param name="estimaID"></param>
        internal void DelLockEntity(string estimaID)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine("DELETE FROM EX04EstimaLock WHERE EstimaID = @estimaID; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion
    }
}
