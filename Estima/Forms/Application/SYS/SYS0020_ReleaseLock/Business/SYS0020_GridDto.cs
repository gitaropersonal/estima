﻿namespace Estima.Forms.Application.SYS.SYS0020.Business
{
    public class SYS0020_GridDto
    {
        public SYS0020_GridDto()
        {
            this.ChkSelect = false;
        }
        public bool ChkSelect { get; set; }
        public string EstimaID { get; set; }
        public string EstimaNum { get; set; }
        public string OrderNum { get; set; }
        public string ProcessingNum { get; set; }
        public string AddIPAddress { get; set; }
        public string AddHostName { get; set; }
        public string AddDate { get; set; }
    }
}
