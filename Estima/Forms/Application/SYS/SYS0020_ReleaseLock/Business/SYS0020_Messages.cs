﻿namespace Estima.Forms.Application.SYS.SYS0020.Business
{
    internal class SYS0020_Messages
    {
        internal const string MsgAskClear            = "クリアしますか？";
        internal const string MsgAskFinish           = "終了しますか？";
        internal const string MsgErrorNotSelectedRow = "行を選択してください";
        internal const string MsgErrorNotExistDatas  = "対象のデータがありません";
        internal const string MsgAskReleaseLock      = "ロック解除しますか？";
        internal const string MsgInfoFinishRelease   = "ロック解除しました";
    }
}
