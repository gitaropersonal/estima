﻿using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Application.SYS.SYS0020.Business
{
    internal class SYS0020_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private SYS0020_ReleaseLock Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo;
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// 専用サービス
        /// </summary>
        private readonly SYS0020_Service PersonalService = new SYS0020_Service();
        /// <summary>
        /// 削除されたデータ
        /// </summary>
        private List<SYS0020_GridDto> DeletedDatas = new List<SYS0020_GridDto>();
        /// <summary>
        /// 列ヘッダdto
        /// </summary>
        private static SYS0020_GridDto CellHeaderNameDto = new SYS0020_GridDto();
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        internal SYS0020_FormLogic(
              SYS0020_ReleaseLock body
            , LoginInfoDto loginInfo
            )
        {
            this.Body = body;
            this.LoginInfo = loginInfo;
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += this.BtnClearClickEvent;
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了処理
            this.Body.FormClosing += this.ClosingEvent;
            // 検索ボタン押下
            this.Body.btnSearch.Click += this.BtnSearchClickEvent;
            // グリッドRowPostPaint
            this.Body.dgvLockDatas.RowPostPaint += this.GridRowPostPaintEvent;
            // 全選択ボタン押下
            this.Body.btnSelectAll.Click += (s, e) => { this.SelectOrReleaseAll(true); };
            // 全解除ボタン押下
            this.Body.btnRelease.Click += (s, e) => { this.SelectOrReleaseAll(false); };
            // ロック解除ボタン押下イベント
            this.Body.btnReleaseLock.Click += this.BtnReleaseLockClickEvent;
            // 見積№
            this.Body.txtEstimaNo.Validated += CommonUtil.ZeroPaddingTextBox;
            // 加工№
            this.Body.txtProcessingNo.Validated += CommonUtil.ZeroPaddingTextBox;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        this.Body.btnSearch.Focus();
                        this.Body.btnSearch.PerformClick();
                        break;
                    case Keys.N:
                        this.Body.btnSelectAll.Focus();
                        this.Body.btnSelectAll.PerformClick();
                        break;
                    case Keys.R:
                        this.Body.btnRelease.Focus();
                        this.Body.btnRelease.PerformClick();
                        break;
                    case Keys.D:
                        this.Body.btnReleaseLock.Focus();
                        this.Body.btnReleaseLock.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// クリアボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnClearClickEvent(
              object s
            , EventArgs e
            )
        {
            var drConfirm = CommonUtil.ShowInfoMsgOKCancel(SYS0020_Messages.MsgAskClear);
            if (drConfirm != DialogResult.OK)
            {
                return;
            }
            this.Init();
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                var drConfirm = CommonUtil.ShowInfoMsgOKCancel(SYS0020_Messages.MsgAskFinish);
                if (drConfirm != DialogResult.OK)
                {
                    cancel = true;
                    return;
                }
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                var drConfirm = CommonUtil.ShowInfoMsgOKCancel(SYS0020_Messages.MsgAskFinish);
                if (drConfirm != DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// グリッドRowPostPaint
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridRowPostPaintEvent(
              object s
            , DataGridViewRowPostPaintEventArgs e
            )
        {
            GridRowUtil<SYS0020_GridDto>.SetRowNum(s, e);
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSearchClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// ロック解除ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnReleaseLockClickEvent(
              object s
            , EventArgs e
            )
        {
            // Validate
            if (this.Body.dgvLockDatas.Rows == null || this.Body.dgvLockDatas.Rows.Count == 0)
            {
                CommonUtil.ShowErrorMsg(SYS0020_Messages.MsgErrorNotExistDatas);
                return;
            }
            var selectedDatas = GridRowUtil<SYS0020_GridDto>.GetAllRowsModel(this.Body.dgvLockDatas.DataSource).Where(n => n.ChkSelect).ToList();
            if (!this.ValidteDelete(selectedDatas))
            {
                return;
            }
            // 確認ダイアログ
            if (CommonUtil.ShowInfoMsgOKCancel(SYS0020_Messages.MsgAskReleaseLock) != DialogResult.OK)
            {
                return;
            }
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                
                bool success = false;
                CommonUtil.ShowProgressDialog(() =>
                {
                    foreach (var rowDto in selectedDatas)
                    {
                        EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogDelLockEntity}：{rowDto.EstimaID}", Enums.LogStatusKbn.Start);
                        this.PersonalService.DelLockEntity(rowDto.EstimaID);
                        EstimaUtil.AddLogInfo(this.Body.Text, $"{Messages.OpeLogDelLockEntity}：{rowDto.EstimaID}", Enums.LogStatusKbn.End);
                    }
                    success = true;
                });
                if (!success)
                {
                    return;
                }
                // 完了ダイアログ
                if (CommonUtil.ShowInfoMsgOKCancel(SYS0020_Messages.MsgInfoFinishRelease) != DialogResult.OK)
                {
                    return;
                }
                // 再検索
                this.BtnSearchClickEvent(s, e);
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// 全選択or全解除
        /// </summary>
        /// <param name="select"></param>
        private void SelectOrReleaseAll(bool select)
        {
            if (this.Body.dgvLockDatas.Rows == null || this.Body.dgvLockDatas.Rows.Count == 0)
            {
                CommonUtil.ShowErrorMsg(SYS0020_Messages.MsgErrorNotExistDatas);
                return;
            }
            var allDatas = GridRowUtil<SYS0020_GridDto>.GetAllRowsModel(this.Body.dgvLockDatas.DataSource);
            if (!allDatas.Any()) { return; }
            var newDatas = new BindingList<SYS0020_GridDto>();
            foreach (var dto in allDatas)
            {
                dto.ChkSelect = select;
                newDatas.Add(dto);
            }
            this.Body.dgvLockDatas.DataSource = newDatas;
        }
        /// <summary>
        /// <summary>
        /// 検索処理
        /// </summary>
        private void SearchLogic()
        {
            // グリッド初期化
            this.Body.dgvLockDatas.Rows.Clear();

            // Validate
            if (this.Body.dtpAddDateTo.Value < this.Body.dtpAddDateFrom.Value)
            {
                this.Body.dtpAddDateTo.Focus();
                CommonUtil.ShowErrorMsg(string.Format(Messages.MsgErrorLargeSmallCollision, "記録日時（終了）", "記録日時（開始）"));
                return;
            }
            // 検索条件
            string estimaNo = this.Body.txtEstimaNo.Text;
            if (!string.IsNullOrEmpty(this.Body.txtEstimaNo.Text))
            {
                estimaNo = $"{this.Body.lblEstimateNumPrefix.Text}{this.Body.txtEstimaNo.Text}";
            }
            var con = new SYS0020_SelCondition()
            {
                EstimaID = this.Body.txtEstimaID.Text,
                EstimaNo = estimaNo,
                OrderNo = this.Body.txtOrderNo.Text,
                ProcessingNo = this.Body.txtProcessingNo.Text,
                IPAddress = this.Body.txtIPAddress.Text,
                HostName = this.Body.txtHostName.Text,
                AddDateFrom = this.Body.dtpAddDateFrom.Value,
                AddDateTo = this.Body.dtpAddDateTo.Value,
            };
            // グリッドデータ
            var bindingList = new BindingList<SYS0020_GridDto>();
            CommonUtil.ShowProgressDialog((new Action(() =>
            {
                try
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.Start);
                    bindingList = this.PersonalService.GetGridDatas(con);
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.End);
                }
                catch (Exception ex)
                {
                    EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                    return;
                }
            })));
            if (!bindingList.Any())
            {
                CommonUtil.ShowErrorMsg(SYS0020_Messages.MsgErrorNotExistDatas);
                return;
            }
            // グリッドデータセット
            this.Body.dgvLockDatas.DataSource = bindingList;
            this.Body.dgvLockDatas.AllowUserToAddRows = false;

            // 使用可否切替切替
            this.SwitchEnabled(true);

            // フォーカス
            this.Body.dgvLockDatas.Focus();
        }
        /// <summary>
        /// 使用可否切替切替
        /// </summary>
        /// <param name="afterSearch"></param>
        private void SwitchEnabled(bool afterSearch = false)
        {
            this.Body.btnSelectAll.Enabled = afterSearch;
            this.Body.btnRelease.Enabled = afterSearch;
            this.Body.btnReleaseLock.Enabled = afterSearch;
        }
        #endregion

        #region Validate
        /// <summary>
        /// Validate（発行ボタン押下時）
        /// </summary>
        /// <param name="selectedDatas"></param>
        /// <returns></returns>
        private bool ValidteDelete(List<SYS0020_GridDto> selectedDatas)
        {
            // Validate（選択行存在チェック）
            if (!selectedDatas.Any())
            {
                CommonUtil.ShowErrorMsg(SYS0020_Messages.MsgErrorNotSelectedRow);
                return false;
            }
            return true;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            // 検索条件
            this.Body.txtEstimaID.Text = string.Empty;
            this.Body.txtProcessingNo.Text = string.Empty;
            this.Body.txtOrderNo.Text = string.Empty;
            this.Body.txtEstimaNo.Text = string.Empty;
            this.Body.txtIPAddress.Text = string.Empty;
            this.Body.dtpAddDateFrom.Value = DateTime.Today.AddDays(-7);
            this.Body.dtpAddDateTo.Value = DateTime.Now;

            // 削除データ
            this.DeletedDatas.Clear();

            // グリッド
            this.Body.dgvLockDatas.Rows.Clear();
            this.Body.dgvLockDatas.AllowUserToAddRows = false;

            // 使用可否切替切替
            this.SwitchEnabled();

            // フォーカス
            this.Body.txtEstimaID.Focus();
        }
        #endregion
    }
}
