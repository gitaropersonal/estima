﻿using System;

namespace Estima.Forms.Application.SYS.SYS0020.Business
{
    public class SYS0020_SelCondition
    {
        public string EstimaID { get; set; }
        public string EstimaNo { get; set; }

        public string OrderNo { get; set; }
        public string ProcessingNo { get; set; }
        public string IPAddress { get; set; }
        public string HostName { get; set; }
        public DateTime AddDateFrom { get; set; }
        public DateTime AddDateTo { get; set; }
    }
}
