﻿using Estima.Forms.Application.SYS.SYS0020.Business;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Application.SYS.SYS0020
{
    public partial class SYS0020_ReleaseLock : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="screenMode"></param>
        public SYS0020_ReleaseLock(LoginInfoDto loginInfo)
        {
            this.InitializeComponent();
            try
            {
                new SYS0020_FormLogic(
                      this
                    , loginInfo
                );
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
