﻿namespace Estima.Forms.Application.SYS.SYS0020
{
    partial class SYS0020_ReleaseLock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRelease = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnReleaseLock = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtEstimaNo = new System.Windows.Forms.TextBox();
            this.lblEstimateNumPrefix = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEstimaID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.lblOrderNum = new System.Windows.Forms.Label();
            this.txtProcessingNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHostName = new System.Windows.Forms.TextBox();
            this.lblMarkSpan = new System.Windows.Forms.Label();
            this.dtpAddDateTo = new System.Windows.Forms.DateTimePicker();
            this.dtpAddDateFrom = new System.Windows.Forms.DateTimePicker();
            this.lblHakkoDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dgvLockDatas = new System.Windows.Forms.DataGridView();
            this.ChkSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.LogTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstimaNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessingNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddIPAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddHostName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLockDatas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(19, 11);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(1216, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(168, 75);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(11, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnRelease);
            this.panel4.Controls.Add(this.btnSelectAll);
            this.panel4.Controls.Add(this.btnReleaseLock);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(617, 75);
            this.panel4.TabIndex = 110;
            // 
            // btnRelease
            // 
            this.btnRelease.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRelease.Location = new System.Drawing.Point(295, 11);
            this.btnRelease.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnRelease.Name = "btnRelease";
            this.btnRelease.Size = new System.Drawing.Size(138, 52);
            this.btnRelease.TabIndex = 113;
            this.btnRelease.Text = "全解除（R）";
            this.btnRelease.UseVisualStyleBackColor = true;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectAll.Location = new System.Drawing.Point(157, 11);
            this.btnSelectAll.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(138, 52);
            this.btnSelectAll.TabIndex = 112;
            this.btnSelectAll.Text = "全選択（N）";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            // 
            // btnReleaseLock
            // 
            this.btnReleaseLock.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnReleaseLock.Location = new System.Drawing.Point(433, 11);
            this.btnReleaseLock.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnReleaseLock.Name = "btnReleaseLock";
            this.btnReleaseLock.Size = new System.Drawing.Size(138, 52);
            this.btnReleaseLock.TabIndex = 114;
            this.btnReleaseLock.Text = "ロック解除(D)";
            this.btnReleaseLock.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 886);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1384, 75);
            this.panel3.TabIndex = 106;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(19, 18, 0, 0);
            this.panel1.Size = new System.Drawing.Size(1384, 151);
            this.panel1.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel13);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.txtEstimaID);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.txtOrderNo);
            this.panel7.Controls.Add(this.lblOrderNum);
            this.panel7.Controls.Add(this.txtProcessingNo);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.txtHostName);
            this.panel7.Controls.Add(this.lblMarkSpan);
            this.panel7.Controls.Add(this.dtpAddDateTo);
            this.panel7.Controls.Add(this.dtpAddDateFrom);
            this.panel7.Controls.Add(this.lblHakkoDate);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Controls.Add(this.txtIPAddress);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(19, 18);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.panel7.Size = new System.Drawing.Size(1059, 133);
            this.panel7.TabIndex = 0;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.txtEstimaNo);
            this.panel13.Controls.Add(this.lblEstimateNumPrefix);
            this.panel13.Location = new System.Drawing.Point(92, 32);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(193, 29);
            this.panel13.TabIndex = 1;
            // 
            // txtEstimaNo
            // 
            this.txtEstimaNo.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtEstimaNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtEstimaNo.Location = new System.Drawing.Point(33, 0);
            this.txtEstimaNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtEstimaNo.MaxLength = 5;
            this.txtEstimaNo.Name = "txtEstimaNo";
            this.txtEstimaNo.Size = new System.Drawing.Size(87, 28);
            this.txtEstimaNo.TabIndex = 25;
            // 
            // lblEstimateNumPrefix
            // 
            this.lblEstimateNumPrefix.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblEstimateNumPrefix.Location = new System.Drawing.Point(0, 0);
            this.lblEstimateNumPrefix.Name = "lblEstimateNumPrefix";
            this.lblEstimateNumPrefix.Size = new System.Drawing.Size(33, 29);
            this.lblEstimateNumPrefix.TabIndex = 24;
            this.lblEstimateNumPrefix.Text = "MK";
            this.lblEstimateNumPrefix.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 6);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 20);
            this.label5.TabIndex = 71;
            this.label5.Text = "ロックID";
            // 
            // txtEstimaID
            // 
            this.txtEstimaID.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtEstimaID.Location = new System.Drawing.Point(92, 3);
            this.txtEstimaID.Margin = new System.Windows.Forms.Padding(4);
            this.txtEstimaID.MaxLength = 40;
            this.txtEstimaID.Name = "txtEstimaID";
            this.txtEstimaID.Size = new System.Drawing.Size(283, 28);
            this.txtEstimaID.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 93);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 20);
            this.label2.TabIndex = 69;
            this.label2.Text = "加工№";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 67;
            this.label1.Text = "発注№";
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtOrderNo.Location = new System.Drawing.Point(92, 61);
            this.txtOrderNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderNo.MaxLength = 12;
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(223, 28);
            this.txtOrderNo.TabIndex = 2;
            // 
            // lblOrderNum
            // 
            this.lblOrderNum.AutoSize = true;
            this.lblOrderNum.Location = new System.Drawing.Point(19, 35);
            this.lblOrderNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOrderNum.Name = "lblOrderNum";
            this.lblOrderNum.Size = new System.Drawing.Size(57, 20);
            this.lblOrderNum.TabIndex = 65;
            this.lblOrderNum.Text = "見積№";
            // 
            // txtProcessingNo
            // 
            this.txtProcessingNo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtProcessingNo.Location = new System.Drawing.Point(92, 90);
            this.txtProcessingNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtProcessingNo.MaxLength = 7;
            this.txtProcessingNo.Name = "txtProcessingNo";
            this.txtProcessingNo.Size = new System.Drawing.Size(120, 28);
            this.txtProcessingNo.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(416, 35);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 20);
            this.label4.TabIndex = 64;
            this.label4.Text = "登録ホスト名";
            // 
            // txtHostName
            // 
            this.txtHostName.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHostName.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHostName.Location = new System.Drawing.Point(528, 32);
            this.txtHostName.Margin = new System.Windows.Forms.Padding(4);
            this.txtHostName.MaxLength = 25;
            this.txtHostName.Name = "txtHostName";
            this.txtHostName.Size = new System.Drawing.Size(221, 28);
            this.txtHostName.TabIndex = 5;
            // 
            // lblMarkSpan
            // 
            this.lblMarkSpan.AutoSize = true;
            this.lblMarkSpan.Location = new System.Drawing.Point(752, 65);
            this.lblMarkSpan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMarkSpan.Name = "lblMarkSpan";
            this.lblMarkSpan.Size = new System.Drawing.Size(25, 20);
            this.lblMarkSpan.TabIndex = 62;
            this.lblMarkSpan.Text = "～";
            // 
            // dtpAddDateTo
            // 
            this.dtpAddDateTo.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.dtpAddDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAddDateTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpAddDateTo.Location = new System.Drawing.Point(779, 61);
            this.dtpAddDateTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtpAddDateTo.Name = "dtpAddDateTo";
            this.dtpAddDateTo.Size = new System.Drawing.Size(220, 28);
            this.dtpAddDateTo.TabIndex = 7;
            // 
            // dtpAddDateFrom
            // 
            this.dtpAddDateFrom.CustomFormat = "yyyy/MM/dd HH:mm:ss";
            this.dtpAddDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpAddDateFrom.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dtpAddDateFrom.Location = new System.Drawing.Point(529, 61);
            this.dtpAddDateFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtpAddDateFrom.Name = "dtpAddDateFrom";
            this.dtpAddDateFrom.Size = new System.Drawing.Size(220, 28);
            this.dtpAddDateFrom.TabIndex = 6;
            // 
            // lblHakkoDate
            // 
            this.lblHakkoDate.AutoSize = true;
            this.lblHakkoDate.Location = new System.Drawing.Point(416, 64);
            this.lblHakkoDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHakkoDate.Name = "lblHakkoDate";
            this.lblHakkoDate.Size = new System.Drawing.Size(73, 20);
            this.lblHakkoDate.TabIndex = 61;
            this.lblHakkoDate.Text = "登録日時";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(416, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 20);
            this.label3.TabIndex = 16;
            this.label3.Text = "登録IPアドレス";
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtIPAddress.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtIPAddress.Location = new System.Drawing.Point(528, 3);
            this.txtIPAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtIPAddress.MaxLength = 20;
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(221, 28);
            this.txtIPAddress.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnSearch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(1216, 18);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(168, 133);
            this.panel2.TabIndex = 23;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(17, 66);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(138, 52);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dgvLockDatas);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.panel6.Location = new System.Drawing.Point(0, 151);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(19, 12, 19, 6);
            this.panel6.Size = new System.Drawing.Size(1384, 735);
            this.panel6.TabIndex = 30;
            // 
            // dgvLockDatas
            // 
            this.dgvLockDatas.AllowUserToAddRows = false;
            this.dgvLockDatas.AllowUserToDeleteRows = false;
            this.dgvLockDatas.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvLockDatas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvLockDatas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLockDatas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChkSelect,
            this.LogTypeName,
            this.EstimaNum,
            this.OrderNum,
            this.ProcessingNum,
            this.AddIPAddress,
            this.AddHostName,
            this.AddDate});
            this.dgvLockDatas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLockDatas.Location = new System.Drawing.Point(19, 12);
            this.dgvLockDatas.Margin = new System.Windows.Forms.Padding(4);
            this.dgvLockDatas.Name = "dgvLockDatas";
            this.dgvLockDatas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dgvLockDatas.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvLockDatas.RowTemplate.Height = 21;
            this.dgvLockDatas.Size = new System.Drawing.Size(1346, 717);
            this.dgvLockDatas.TabIndex = 31;
            // 
            // ChkSelect
            // 
            this.ChkSelect.DataPropertyName = "ChkSelect";
            this.ChkSelect.HeaderText = "選択";
            this.ChkSelect.Name = "ChkSelect";
            this.ChkSelect.Width = 60;
            // 
            // LogTypeName
            // 
            this.LogTypeName.DataPropertyName = "EstimaID";
            this.LogTypeName.HeaderText = "ロックID";
            this.LogTypeName.Name = "LogTypeName";
            this.LogTypeName.ReadOnly = true;
            this.LogTypeName.Width = 280;
            // 
            // EstimaNum
            // 
            this.EstimaNum.DataPropertyName = "EstimaNum";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.EstimaNum.DefaultCellStyle = dataGridViewCellStyle6;
            this.EstimaNum.HeaderText = "見積書№";
            this.EstimaNum.MaxInputLength = 25;
            this.EstimaNum.Name = "EstimaNum";
            this.EstimaNum.ReadOnly = true;
            // 
            // OrderNum
            // 
            this.OrderNum.DataPropertyName = "OrderNum";
            this.OrderNum.HeaderText = "発注№";
            this.OrderNum.Name = "OrderNum";
            this.OrderNum.ReadOnly = true;
            this.OrderNum.Width = 200;
            // 
            // ProcessingNum
            // 
            this.ProcessingNum.DataPropertyName = "ProcessingNum";
            this.ProcessingNum.HeaderText = "加工№";
            this.ProcessingNum.Name = "ProcessingNum";
            this.ProcessingNum.ReadOnly = true;
            // 
            // AddIPAddress
            // 
            this.AddIPAddress.DataPropertyName = "AddIPAddress";
            this.AddIPAddress.HeaderText = "登録IPアドレス";
            this.AddIPAddress.Name = "AddIPAddress";
            this.AddIPAddress.ReadOnly = true;
            this.AddIPAddress.Width = 150;
            // 
            // AddHostName
            // 
            this.AddHostName.DataPropertyName = "AddHostName";
            this.AddHostName.HeaderText = "登録ホスト名";
            this.AddHostName.Name = "AddHostName";
            this.AddHostName.ReadOnly = true;
            this.AddHostName.Width = 180;
            // 
            // AddDate
            // 
            this.AddDate.DataPropertyName = "AddDate";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.AddDate.DefaultCellStyle = dataGridViewCellStyle7;
            this.AddDate.HeaderText = "登録日時";
            this.AddDate.Name = "AddDate";
            this.AddDate.ReadOnly = true;
            this.AddDate.Width = 220;
            // 
            // SYS0020_ReleaseLock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 961);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MinimumSize = new System.Drawing.Size(1400, 1000);
            this.Name = "SYS0020_ReleaseLock";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "親コード";
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLockDatas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel7;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.DataGridView dgvLockDatas;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtIPAddress;
        public System.Windows.Forms.Label lblMarkSpan;
        public System.Windows.Forms.DateTimePicker dtpAddDateTo;
        public System.Windows.Forms.DateTimePicker dtpAddDateFrom;
        public System.Windows.Forms.Label lblHakkoDate;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtHostName;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtEstimaID;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtOrderNo;
        public System.Windows.Forms.Label lblOrderNum;
        public System.Windows.Forms.TextBox txtProcessingNo;
        private System.Windows.Forms.Panel panel13;
        public System.Windows.Forms.TextBox txtEstimaNo;
        public System.Windows.Forms.Label lblEstimateNumPrefix;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ChkSelect;
        private System.Windows.Forms.DataGridViewTextBoxColumn LogTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstimaNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessingNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddIPAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddHostName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddDate;
        public System.Windows.Forms.Button btnRelease;
        public System.Windows.Forms.Button btnSelectAll;
        public System.Windows.Forms.Button btnReleaseLock;
    }
}