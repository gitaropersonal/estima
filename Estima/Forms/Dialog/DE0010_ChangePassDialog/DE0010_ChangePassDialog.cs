﻿using Estima.Forms.Dialog.DE0010.Business;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0010
{
    public partial class DE0010_ChangePassDialog : Form
    {
        /// <summary>
        /// ログイン情報
        /// </summary>
        private static LoginInfoDto LoginInfo = new LoginInfoDto();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DE0010_ChangePassDialog()
        {
            this.InitializeComponent();
            try
            {
                new DE0010_FormLogic(this);
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
