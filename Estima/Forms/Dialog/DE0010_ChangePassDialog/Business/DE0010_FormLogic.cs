﻿using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System.Drawing;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0010.Business
{
    internal class DE0010_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private DE0010_ChangePassDialog Body { get; set; }
        ///// <summary>
        ///// DB接続文字列
        ///// </summary>
        //private static  = ConfigUtil.Config.DbConnString();
        /// <summary>
        /// ログイン情報
        /// </summary>
        private static LoginInfoDto LoginInfo = new LoginInfoDto();
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        internal DE0010_FormLogic(DE0010_ChangePassDialog body)
        {
            this.Body = body;
            this.Body.txtOldPassWord.PasswordChar = '*';
            this.Body.txtNewPassWord.PasswordChar = '*';
            this.Body.txtNewPassWordRe.PasswordChar = '*';

            // コントロール背景色の初期化
            new InitControl(this.Body);

            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.O:
                        this.Body.btnOK.Focus();
                        this.Body.btnOK.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnCancel.Focus();
                        this.Body.btnCancel.PerformClick();
                        break;
                }
            };
            // OKボタン押下イベント
            this.Body.btnOK.Click += (s, e) => {

                // OKボタン押下時Validate
                if (ValidateBtnOK())
                {
                    // 確認ダイアログ
                    if (CommonUtil.ShowInfoMsgOKCancel(Messages.MsgAskUpdate) != DialogResult.OK)
                    {
                        return;
                    }
                    //// パスワードの更新
                    //new LoginService().AddLoginInfo(DbConnString, this.Body.txtTantoId.Text, LoginInfo.Seq + 1, this.Body.txtNewPassWord.Text);

                    // 更新完了メッセージ
                    CommonUtil.ShowInfoMsgOK(Messages.MsgInfoFinishUpdate);
                    this.Body.Close();
                }
            };
            // キャンセルボタン押下イベント
            this.Body.btnCancel.Click += (s, e) => {

                this.Body.Close();

            };
        }
        #endregion

        #region Validate
        /// <summary>
        /// OKボタン押下時Validate
        /// </summary>
        private bool ValidateBtnOK()
        {
            this.Body.txtTantoId.BackColor = Color.Empty;
            this.Body.txtOldPassWord.BackColor = Color.Empty;
            this.Body.txtNewPassWord.BackColor = Color.Empty;
            this.Body.txtNewPassWordRe.BackColor = Color.Empty;

            // 空文字チェック（Proxy）
            if (!ValidateEmptyProxy())
            {
                return false;
            }
            // パスワードの新旧チェック
            if (this.Body.txtOldPassWord.Text == this.Body.txtNewPassWord.Text)
            {
                this.Body.txtNewPassWord.Focus();
                this.Body.txtNewPassWord.BackColor = Colors.BackColorCellError;
                CommonUtil.ShowErrorMsg(Messages.MsgErrorNotChangedPassword);
                return false;
            }
            // 新しいパスワードのマッチングチェック
            if (this.Body.txtNewPassWord.Text != this.Body.txtNewPassWordRe.Text)
            {
                this.Body.txtNewPassWord.Focus();
                this.Body.txtNewPassWord.BackColor = Colors.BackColorCellError;
                this.Body.txtNewPassWordRe.BackColor = Colors.BackColorCellError;
                CommonUtil.ShowErrorMsg(Messages.MsgErrorUnmatchedPassword);
                return false;
            }
            //// ログイン情報取得
            //var service = new LoginService();
            //LoginInfo = service.SelLoginInfo(DbConnString, this.Body.txtTantoId.Text, this.Body.txtOldPassWord.Text);

            //// 照合処理
            //if (string.IsNullOrEmpty(LoginInfo.TantoID))
            //{
            //    this.Body.txtTantoId.Focus();
            //    this.Body.txtTantoId.BackColor = Colors.BackColorCellError;
            //    this.Body.txtOldPassWord.BackColor = Colors.BackColorCellError;
            //    CommonUtil.ShowErroMsg(Messages.MsgErrorWrongPassword);
            //    return false;
            //}
            //// 過去のパスワード使いまわしチェック
            //if (!service.ValidateUsedPass(DbConnString, this.Body.txtTantoId.Text, this.Body.txtNewPassWord.Text))
            //{
            //    this.Body.txtNewPassWord.Focus();
            //    this.Body.txtNewPassWord.BackColor = Colors.BackColorCellError;
            //    CommonUtil.ShowErroMsg(Messages.MsgErrorUsedPassword);
            //    return false;
            //}
            return true;
        }
        /// <summary>
        /// 空文字チェック（Proxy）
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmptyProxy()
        {
            if (!ValidateEmpty(this.Body.txtTantoId, this.Body.lblTantoID))
            {
                return false;
            }
            if (!ValidateEmpty(this.Body.txtOldPassWord, this.Body.lblPassWord))
            {
                return false;
            }
            if (!ValidateEmpty(this.Body.txtNewPassWord, this.Body.lblNewPassWord))
            {
                return false;
            }
            if (!ValidateEmpty(this.Body.txtNewPassWordRe, this.Body.lblNewPassWordRe))
            {
                return false;
            }
            if (!ValidateEmpty(this.Body.txtTantoId, this.Body.lblTantoID))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 空文字チェック
        /// </summary>
        /// <param name="txtBox"></param>
        /// <param name="lbl"></param>
        /// <returns></returns>
        private bool ValidateEmpty(TextBox txtBox, Label lbl)
        {
            if (string.IsNullOrEmpty(txtBox.Text))
            {
                txtBox.Focus();
                txtBox.BackColor = Colors.BackColorCellError;
                CommonUtil.ShowErrorMsg(string.Format(Messages.MsgErrorMissEmpty, lbl.Text));
                return false;
            }
            return true;
        }
        #endregion
    }
}
