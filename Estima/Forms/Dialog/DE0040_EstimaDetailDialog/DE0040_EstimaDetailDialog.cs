﻿using Estima.Forms.Dialog.DE0040.Business;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0040
{
    public partial class DE0040_EstimaDetailDialog : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="tempEstimaID"></param>
        /// <param name="meisaiNo"></param>
        /// <param name="meisaiType"></param>
        /// <param name="consumptionRate"></param>
        /// <param name="inspectionRate"></param>
        /// <param name="expenceRate"></param>
        /// <param name="readOnly"></param>
        /// <param name="taishoYM"></param>
        public DE0040_EstimaDetailDialog(
              LoginInfoDto loginInfo
            , string tempEstimaID
            , int meisaiNo
            , int meisaiType
            , float consumptionRate
            , float inspectionRate
            , float expenceRate
            , bool readOnly
            , string taishoYM
            )
        {
            this.InitializeComponent();
            try
            {
                new DE0040_FormLogic(
                      this
                    , loginInfo
                    , tempEstimaID
                    , meisaiNo
                    , meisaiType
                    , consumptionRate
                    , inspectionRate
                    , expenceRate
                    , readOnly
                    , taishoYM
                );
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
                this.Close();
            }
        }
    }
}
