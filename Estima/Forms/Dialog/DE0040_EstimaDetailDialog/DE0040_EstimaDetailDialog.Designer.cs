﻿namespace Estima.Forms.Dialog.DE0040
{
    partial class DE0040_EstimaDetailDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tabMeisai = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.dgvPlumbing = new System.Windows.Forms.DataGridView();
            this.ShohinID_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Seq_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnSearch_Plb = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnClear_Plb = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ShohinCD_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentCDName_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShohinSizeName_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShohinName_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Suryo_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit_Plb = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Tanka_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BDCount_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kingaku_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status_Plb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.dgvHousing = new System.Windows.Forms.DataGridView();
            this.ShohinID_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Seq_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnSearch_Hsg = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnClear_Hsg = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ShohinCD_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentCDName_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShohinSizeName_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShohinName_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Suryo_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit_Hsg = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Tanka_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BDCount_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kingaku_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status_Hsg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.dgvProcessing = new System.Windows.Forms.DataGridView();
            this.ShohinID_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Seq_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtnSearch_Prc = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnClear_Prc = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ShohinCD_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParentCDName_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShohinSizeName_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShohinName_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Suryo_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit_Prc = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Tanka_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BDCount_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kingaku_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DetailKbn_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddFromOtherGrid_Prc = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Status_Prc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.lblRateInspection = new System.Windows.Forms.Label();
            this.numRateInspection = new System.Windows.Forms.NumericUpDown();
            this.lblRateConsumption = new System.Windows.Forms.Label();
            this.numRateConsumption = new System.Windows.Forms.NumericUpDown();
            this.lblRateExpence = new System.Windows.Forms.Label();
            this.numRateExpence = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSumKingaku = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSumSuryo = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.btnCalcProcessing = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabMeisai.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlumbing)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHousing)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessing)).BeginInit();
            this.panel7.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRateInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRateConsumption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRateExpence)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 641);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1584, 70);
            this.panel3.TabIndex = 102;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(1359, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(225, 70);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(60, 9);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(548, 70);
            this.panel4.TabIndex = 110;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(156, 8);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(138, 52);
            this.btnUpdate.TabIndex = 112;
            this.btnUpdate.Text = "仮登録（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(19, 8);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(19, 12, 19, 0);
            this.panel1.Size = new System.Drawing.Size(1584, 641);
            this.panel1.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.tabMeisai);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(19, 89);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1546, 484);
            this.panel6.TabIndex = 12;
            // 
            // tabMeisai
            // 
            this.tabMeisai.Controls.Add(this.tabPage2);
            this.tabMeisai.Controls.Add(this.tabPage3);
            this.tabMeisai.Controls.Add(this.tabPage4);
            this.tabMeisai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMeisai.Location = new System.Drawing.Point(0, 0);
            this.tabMeisai.Margin = new System.Windows.Forms.Padding(4);
            this.tabMeisai.Multiline = true;
            this.tabMeisai.Name = "tabMeisai";
            this.tabMeisai.SelectedIndex = 0;
            this.tabMeisai.Size = new System.Drawing.Size(1546, 484);
            this.tabMeisai.TabIndex = 3;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1538, 451);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "①配管・継手";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.dgvPlumbing);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(4, 4);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1530, 443);
            this.panel8.TabIndex = 0;
            // 
            // dgvPlumbing
            // 
            this.dgvPlumbing.AllowUserToResizeRows = false;
            this.dgvPlumbing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPlumbing.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShohinID_Plb,
            this.Seq_Plb,
            this.BtnSearch_Plb,
            this.BtnClear_Plb,
            this.ShohinCD_Plb,
            this.ParentCDName_Plb,
            this.ShohinSizeName_Plb,
            this.ShohinName_Plb,
            this.Suryo_Plb,
            this.Unit_Plb,
            this.Tanka_Plb,
            this.BDCount_Plb,
            this.Kingaku_Plb,
            this.Status_Plb});
            this.dgvPlumbing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPlumbing.Location = new System.Drawing.Point(0, 0);
            this.dgvPlumbing.Margin = new System.Windows.Forms.Padding(4);
            this.dgvPlumbing.Name = "dgvPlumbing";
            this.dgvPlumbing.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPlumbing.RowTemplate.Height = 21;
            this.dgvPlumbing.Size = new System.Drawing.Size(1530, 443);
            this.dgvPlumbing.TabIndex = 53;
            // 
            // ShohinID_Plb
            // 
            this.ShohinID_Plb.DataPropertyName = "ShohinID_Plb";
            this.ShohinID_Plb.HeaderText = "ShohinID";
            this.ShohinID_Plb.Name = "ShohinID_Plb";
            this.ShohinID_Plb.Visible = false;
            // 
            // Seq_Plb
            // 
            this.Seq_Plb.DataPropertyName = "Seq_Plb";
            this.Seq_Plb.HeaderText = "Seq_Plb";
            this.Seq_Plb.Name = "Seq_Plb";
            this.Seq_Plb.Visible = false;
            // 
            // BtnSearch_Plb
            // 
            this.BtnSearch_Plb.DataPropertyName = "BtnSearch_Plb";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnSearch_Plb.DefaultCellStyle = dataGridViewCellStyle1;
            this.BtnSearch_Plb.HeaderText = "";
            this.BtnSearch_Plb.Name = "BtnSearch_Plb";
            this.BtnSearch_Plb.Width = 60;
            // 
            // BtnClear_Plb
            // 
            this.BtnClear_Plb.DataPropertyName = "BtnClear_Plb";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnClear_Plb.DefaultCellStyle = dataGridViewCellStyle2;
            this.BtnClear_Plb.HeaderText = "";
            this.BtnClear_Plb.Name = "BtnClear_Plb";
            this.BtnClear_Plb.Width = 60;
            // 
            // ShohinCD_Plb
            // 
            this.ShohinCD_Plb.DataPropertyName = "ShohinCD_Plb";
            this.ShohinCD_Plb.HeaderText = "商品コード";
            this.ShohinCD_Plb.MaxInputLength = 40;
            this.ShohinCD_Plb.Name = "ShohinCD_Plb";
            this.ShohinCD_Plb.Width = 250;
            // 
            // ParentCDName_Plb
            // 
            this.ParentCDName_Plb.DataPropertyName = "ParentCDName_Plb";
            this.ParentCDName_Plb.HeaderText = "種別";
            this.ParentCDName_Plb.MaxInputLength = 25;
            this.ParentCDName_Plb.Name = "ParentCDName_Plb";
            this.ParentCDName_Plb.Width = 220;
            // 
            // ShohinSizeName_Plb
            // 
            this.ShohinSizeName_Plb.DataPropertyName = "ShohinSizeName_Plb";
            this.ShohinSizeName_Plb.HeaderText = "サイズ";
            this.ShohinSizeName_Plb.Name = "ShohinSizeName_Plb";
            this.ShohinSizeName_Plb.Width = 150;
            // 
            // ShohinName_Plb
            // 
            this.ShohinName_Plb.DataPropertyName = "ShohinName_Plb";
            this.ShohinName_Plb.HeaderText = "品名";
            this.ShohinName_Plb.Name = "ShohinName_Plb";
            this.ShohinName_Plb.Width = 250;
            // 
            // Suryo_Plb
            // 
            this.Suryo_Plb.DataPropertyName = "Suryo_Plb";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Suryo_Plb.DefaultCellStyle = dataGridViewCellStyle3;
            this.Suryo_Plb.HeaderText = "数量";
            this.Suryo_Plb.MaxInputLength = 3;
            this.Suryo_Plb.Name = "Suryo_Plb";
            // 
            // Unit_Plb
            // 
            this.Unit_Plb.DataPropertyName = "Unit_Plb";
            this.Unit_Plb.HeaderText = "単位";
            this.Unit_Plb.Name = "Unit_Plb";
            this.Unit_Plb.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Unit_Plb.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Unit_Plb.Width = 80;
            // 
            // Tanka_Plb
            // 
            this.Tanka_Plb.DataPropertyName = "Tanka_Plb";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Tanka_Plb.DefaultCellStyle = dataGridViewCellStyle4;
            this.Tanka_Plb.HeaderText = "単価";
            this.Tanka_Plb.MaxInputLength = 10;
            this.Tanka_Plb.Name = "Tanka_Plb";
            // 
            // BDCount_Plb
            // 
            this.BDCount_Plb.DataPropertyName = "BDCount_Plb";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BDCount_Plb.DefaultCellStyle = dataGridViewCellStyle5;
            this.BDCount_Plb.HeaderText = "BD数";
            this.BDCount_Plb.MaxInputLength = 3;
            this.BDCount_Plb.Name = "BDCount_Plb";
            // 
            // Kingaku_Plb
            // 
            this.Kingaku_Plb.DataPropertyName = "Kingaku_Plb";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGray;
            this.Kingaku_Plb.DefaultCellStyle = dataGridViewCellStyle6;
            this.Kingaku_Plb.HeaderText = "金額";
            this.Kingaku_Plb.Name = "Kingaku_Plb";
            this.Kingaku_Plb.ReadOnly = true;
            // 
            // Status_Plb
            // 
            this.Status_Plb.DataPropertyName = "Status_Plb";
            this.Status_Plb.HeaderText = "ステータス";
            this.Status_Plb.Name = "Status_Plb";
            this.Status_Plb.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel9);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage3.Size = new System.Drawing.Size(1538, 451);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "②ハウジング";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.dgvHousing);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(4, 4);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1530, 443);
            this.panel9.TabIndex = 1;
            // 
            // dgvHousing
            // 
            this.dgvHousing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHousing.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShohinID_Hsg,
            this.Seq_Hsg,
            this.BtnSearch_Hsg,
            this.BtnClear_Hsg,
            this.ShohinCD_Hsg,
            this.ParentCDName_Hsg,
            this.ShohinSizeName_Hsg,
            this.ShohinName_Hsg,
            this.Suryo_Hsg,
            this.Unit_Hsg,
            this.Tanka_Hsg,
            this.BDCount_Hsg,
            this.Kingaku_Hsg,
            this.Status_Hsg});
            this.dgvHousing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvHousing.Location = new System.Drawing.Point(0, 0);
            this.dgvHousing.Margin = new System.Windows.Forms.Padding(4);
            this.dgvHousing.Name = "dgvHousing";
            this.dgvHousing.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvHousing.RowTemplate.Height = 21;
            this.dgvHousing.Size = new System.Drawing.Size(1530, 443);
            this.dgvHousing.TabIndex = 54;
            // 
            // ShohinID_Hsg
            // 
            this.ShohinID_Hsg.DataPropertyName = "ShohinID_Hsg";
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ShohinID_Hsg.DefaultCellStyle = dataGridViewCellStyle7;
            this.ShohinID_Hsg.HeaderText = "ShohinID";
            this.ShohinID_Hsg.Name = "ShohinID_Hsg";
            this.ShohinID_Hsg.Visible = false;
            // 
            // Seq_Hsg
            // 
            this.Seq_Hsg.DataPropertyName = "Seq_Hsg";
            this.Seq_Hsg.HeaderText = "Seq_Hsg";
            this.Seq_Hsg.Name = "Seq_Hsg";
            this.Seq_Hsg.Visible = false;
            // 
            // BtnSearch_Hsg
            // 
            this.BtnSearch_Hsg.DataPropertyName = "BtnSearch_Hsg";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnSearch_Hsg.DefaultCellStyle = dataGridViewCellStyle8;
            this.BtnSearch_Hsg.HeaderText = "";
            this.BtnSearch_Hsg.Name = "BtnSearch_Hsg";
            this.BtnSearch_Hsg.Width = 60;
            // 
            // BtnClear_Hsg
            // 
            this.BtnClear_Hsg.DataPropertyName = "BtnClear_Hsg";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnClear_Hsg.DefaultCellStyle = dataGridViewCellStyle9;
            this.BtnClear_Hsg.HeaderText = "";
            this.BtnClear_Hsg.Name = "BtnClear_Hsg";
            this.BtnClear_Hsg.Width = 60;
            // 
            // ShohinCD_Hsg
            // 
            this.ShohinCD_Hsg.DataPropertyName = "ShohinCD_Hsg";
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ShohinCD_Hsg.DefaultCellStyle = dataGridViewCellStyle10;
            this.ShohinCD_Hsg.HeaderText = "商品コード";
            this.ShohinCD_Hsg.MaxInputLength = 40;
            this.ShohinCD_Hsg.Name = "ShohinCD_Hsg";
            this.ShohinCD_Hsg.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ShohinCD_Hsg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ShohinCD_Hsg.Width = 250;
            // 
            // ParentCDName_Hsg
            // 
            this.ParentCDName_Hsg.DataPropertyName = "ParentCDName_Hsg";
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ParentCDName_Hsg.DefaultCellStyle = dataGridViewCellStyle11;
            this.ParentCDName_Hsg.HeaderText = "種別";
            this.ParentCDName_Hsg.Name = "ParentCDName_Hsg";
            this.ParentCDName_Hsg.Width = 220;
            // 
            // ShohinSizeName_Hsg
            // 
            this.ShohinSizeName_Hsg.DataPropertyName = "ShohinSizeName_Hsg";
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ShohinSizeName_Hsg.DefaultCellStyle = dataGridViewCellStyle12;
            this.ShohinSizeName_Hsg.HeaderText = "サイズ";
            this.ShohinSizeName_Hsg.Name = "ShohinSizeName_Hsg";
            this.ShohinSizeName_Hsg.Width = 150;
            // 
            // ShohinName_Hsg
            // 
            this.ShohinName_Hsg.DataPropertyName = "ShohinName_Hsg";
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ShohinName_Hsg.DefaultCellStyle = dataGridViewCellStyle13;
            this.ShohinName_Hsg.HeaderText = "品名";
            this.ShohinName_Hsg.Name = "ShohinName_Hsg";
            this.ShohinName_Hsg.Width = 250;
            // 
            // Suryo_Hsg
            // 
            this.Suryo_Hsg.DataPropertyName = "Suryo_Hsg";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Suryo_Hsg.DefaultCellStyle = dataGridViewCellStyle14;
            this.Suryo_Hsg.HeaderText = "数量";
            this.Suryo_Hsg.Name = "Suryo_Hsg";
            this.Suryo_Hsg.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Unit_Hsg
            // 
            this.Unit_Hsg.DataPropertyName = "Unit_Hsg";
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Unit_Hsg.DefaultCellStyle = dataGridViewCellStyle15;
            this.Unit_Hsg.HeaderText = "単位";
            this.Unit_Hsg.Name = "Unit_Hsg";
            this.Unit_Hsg.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Unit_Hsg.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Unit_Hsg.Width = 80;
            // 
            // Tanka_Hsg
            // 
            this.Tanka_Hsg.DataPropertyName = "Tanka_Hsg";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Tanka_Hsg.DefaultCellStyle = dataGridViewCellStyle16;
            this.Tanka_Hsg.HeaderText = "単価";
            this.Tanka_Hsg.Name = "Tanka_Hsg";
            // 
            // BDCount_Hsg
            // 
            this.BDCount_Hsg.DataPropertyName = "BDCount_Hsg";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BDCount_Hsg.DefaultCellStyle = dataGridViewCellStyle17;
            this.BDCount_Hsg.HeaderText = "BD数";
            this.BDCount_Hsg.MaxInputLength = 3;
            this.BDCount_Hsg.Name = "BDCount_Hsg";
            // 
            // Kingaku_Hsg
            // 
            this.Kingaku_Hsg.DataPropertyName = "Kingaku_Hsg";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Kingaku_Hsg.DefaultCellStyle = dataGridViewCellStyle18;
            this.Kingaku_Hsg.HeaderText = "金額";
            this.Kingaku_Hsg.Name = "Kingaku_Hsg";
            this.Kingaku_Hsg.ReadOnly = true;
            // 
            // Status_Hsg
            // 
            this.Status_Hsg.DataPropertyName = "Status_Hsg";
            this.Status_Hsg.HeaderText = "ステータス";
            this.Status_Hsg.Name = "Status_Hsg";
            this.Status_Hsg.Visible = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel10);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage4.Size = new System.Drawing.Size(1538, 451);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "③ 加工費";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.dgvProcessing);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(4, 4);
            this.panel10.Margin = new System.Windows.Forms.Padding(4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(1530, 443);
            this.panel10.TabIndex = 0;
            // 
            // dgvProcessing
            // 
            this.dgvProcessing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProcessing.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShohinID_Prc,
            this.Seq_Prc,
            this.BtnSearch_Prc,
            this.BtnClear_Prc,
            this.ShohinCD_Prc,
            this.ParentCDName_Prc,
            this.ShohinSizeName_Prc,
            this.ShohinName_Prc,
            this.Suryo_Prc,
            this.Unit_Prc,
            this.Tanka_Prc,
            this.BDCount_Prc,
            this.Kingaku_Prc,
            this.DetailKbn_Prc,
            this.AddFromOtherGrid_Prc,
            this.Status_Prc});
            this.dgvProcessing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProcessing.Location = new System.Drawing.Point(0, 0);
            this.dgvProcessing.Margin = new System.Windows.Forms.Padding(4);
            this.dgvProcessing.Name = "dgvProcessing";
            this.dgvProcessing.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvProcessing.RowTemplate.Height = 21;
            this.dgvProcessing.Size = new System.Drawing.Size(1530, 443);
            this.dgvProcessing.TabIndex = 54;
            // 
            // ShohinID_Prc
            // 
            this.ShohinID_Prc.DataPropertyName = "ShohinID_Prc";
            this.ShohinID_Prc.HeaderText = "ShohinID";
            this.ShohinID_Prc.Name = "ShohinID_Prc";
            this.ShohinID_Prc.Visible = false;
            // 
            // Seq_Prc
            // 
            this.Seq_Prc.DataPropertyName = "Seq_Prc";
            this.Seq_Prc.HeaderText = "Seq_Prc";
            this.Seq_Prc.Name = "Seq_Prc";
            this.Seq_Prc.Visible = false;
            // 
            // BtnSearch_Prc
            // 
            this.BtnSearch_Prc.DataPropertyName = "BtnSearch_Prc";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnSearch_Prc.DefaultCellStyle = dataGridViewCellStyle19;
            this.BtnSearch_Prc.HeaderText = "";
            this.BtnSearch_Prc.Name = "BtnSearch_Prc";
            this.BtnSearch_Prc.Width = 60;
            // 
            // BtnClear_Prc
            // 
            this.BtnClear_Prc.DataPropertyName = "BtnClear_Prc";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.BtnClear_Prc.DefaultCellStyle = dataGridViewCellStyle20;
            this.BtnClear_Prc.HeaderText = "";
            this.BtnClear_Prc.Name = "BtnClear_Prc";
            this.BtnClear_Prc.Width = 60;
            // 
            // ShohinCD_Prc
            // 
            this.ShohinCD_Prc.DataPropertyName = "ShohinCD_Prc";
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ShohinCD_Prc.DefaultCellStyle = dataGridViewCellStyle21;
            this.ShohinCD_Prc.HeaderText = "商品コード";
            this.ShohinCD_Prc.MaxInputLength = 40;
            this.ShohinCD_Prc.Name = "ShohinCD_Prc";
            this.ShohinCD_Prc.Width = 250;
            // 
            // ParentCDName_Prc
            // 
            this.ParentCDName_Prc.DataPropertyName = "ParentCDName_Prc";
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ParentCDName_Prc.DefaultCellStyle = dataGridViewCellStyle22;
            this.ParentCDName_Prc.HeaderText = "種別";
            this.ParentCDName_Prc.Name = "ParentCDName_Prc";
            this.ParentCDName_Prc.Width = 220;
            // 
            // ShohinSizeName_Prc
            // 
            this.ShohinSizeName_Prc.DataPropertyName = "ShohinSizeName_Prc";
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ShohinSizeName_Prc.DefaultCellStyle = dataGridViewCellStyle23;
            this.ShohinSizeName_Prc.HeaderText = "サイズ";
            this.ShohinSizeName_Prc.Name = "ShohinSizeName_Prc";
            this.ShohinSizeName_Prc.Width = 150;
            // 
            // ShohinName_Prc
            // 
            this.ShohinName_Prc.DataPropertyName = "ShohinName_Prc";
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ShohinName_Prc.DefaultCellStyle = dataGridViewCellStyle24;
            this.ShohinName_Prc.HeaderText = "品名";
            this.ShohinName_Prc.Name = "ShohinName_Prc";
            this.ShohinName_Prc.Width = 250;
            // 
            // Suryo_Prc
            // 
            this.Suryo_Prc.DataPropertyName = "Suryo_Prc";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Suryo_Prc.DefaultCellStyle = dataGridViewCellStyle25;
            this.Suryo_Prc.HeaderText = "数量";
            this.Suryo_Prc.Name = "Suryo_Prc";
            // 
            // Unit_Prc
            // 
            this.Unit_Prc.DataPropertyName = "Unit_Prc";
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Unit_Prc.DefaultCellStyle = dataGridViewCellStyle26;
            this.Unit_Prc.HeaderText = "単位";
            this.Unit_Prc.Name = "Unit_Prc";
            this.Unit_Prc.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Unit_Prc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Unit_Prc.Width = 80;
            // 
            // Tanka_Prc
            // 
            this.Tanka_Prc.DataPropertyName = "Tanka_Prc";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Tanka_Prc.DefaultCellStyle = dataGridViewCellStyle27;
            this.Tanka_Prc.HeaderText = "単価";
            this.Tanka_Prc.Name = "Tanka_Prc";
            // 
            // BDCount_Prc
            // 
            this.BDCount_Prc.DataPropertyName = "BDCount_Prc";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.BDCount_Prc.DefaultCellStyle = dataGridViewCellStyle28;
            this.BDCount_Prc.HeaderText = "BD数";
            this.BDCount_Prc.MaxInputLength = 3;
            this.BDCount_Prc.Name = "BDCount_Prc";
            // 
            // Kingaku_Prc
            // 
            this.Kingaku_Prc.DataPropertyName = "Kingaku_Prc";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Kingaku_Prc.DefaultCellStyle = dataGridViewCellStyle29;
            this.Kingaku_Prc.HeaderText = "金額";
            this.Kingaku_Prc.Name = "Kingaku_Prc";
            this.Kingaku_Prc.ReadOnly = true;
            // 
            // DetailKbn_Prc
            // 
            this.DetailKbn_Prc.DataPropertyName = "DetailKbn_Prc";
            this.DetailKbn_Prc.HeaderText = "DetailKbn_Prc";
            this.DetailKbn_Prc.Name = "DetailKbn_Prc";
            this.DetailKbn_Prc.Visible = false;
            // 
            // AddFromOtherGrid_Prc
            // 
            this.AddFromOtherGrid_Prc.DataPropertyName = "AddFromOtherGrid_Prc";
            this.AddFromOtherGrid_Prc.HeaderText = "他グリッドから追加？";
            this.AddFromOtherGrid_Prc.Name = "AddFromOtherGrid_Prc";
            this.AddFromOtherGrid_Prc.Visible = false;
            // 
            // Status_Prc
            // 
            this.Status_Prc.DataPropertyName = "Status_Prc";
            this.Status_Prc.HeaderText = "ステータス";
            this.Status_Prc.Name = "Status_Prc";
            this.Status_Prc.Visible = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel11);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(19, 573);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1546, 68);
            this.panel7.TabIndex = 30;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.lblRateInspection);
            this.panel11.Controls.Add(this.numRateInspection);
            this.panel11.Controls.Add(this.lblRateConsumption);
            this.panel11.Controls.Add(this.numRateConsumption);
            this.panel11.Controls.Add(this.lblRateExpence);
            this.panel11.Controls.Add(this.numRateExpence);
            this.panel11.Controls.Add(this.label2);
            this.panel11.Controls.Add(this.txtSumKingaku);
            this.panel11.Controls.Add(this.label1);
            this.panel11.Controls.Add(this.txtSumSuryo);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(312, 0);
            this.panel11.Margin = new System.Windows.Forms.Padding(4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1234, 68);
            this.panel11.TabIndex = 0;
            // 
            // lblRateInspection
            // 
            this.lblRateInspection.AutoSize = true;
            this.lblRateInspection.Location = new System.Drawing.Point(409, 22);
            this.lblRateInspection.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRateInspection.Name = "lblRateInspection";
            this.lblRateInspection.Size = new System.Drawing.Size(89, 20);
            this.lblRateInspection.TabIndex = 25;
            this.lblRateInspection.Text = "検査費掛率";
            // 
            // numRateInspection
            // 
            this.numRateInspection.DecimalPlaces = 2;
            this.numRateInspection.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numRateInspection.Location = new System.Drawing.Point(506, 18);
            this.numRateInspection.Margin = new System.Windows.Forms.Padding(4);
            this.numRateInspection.Name = "numRateInspection";
            this.numRateInspection.Size = new System.Drawing.Size(119, 28);
            this.numRateInspection.TabIndex = 23;
            this.numRateInspection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblRateConsumption
            // 
            this.lblRateConsumption.AutoSize = true;
            this.lblRateConsumption.Location = new System.Drawing.Point(155, 22);
            this.lblRateConsumption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRateConsumption.Name = "lblRateConsumption";
            this.lblRateConsumption.Size = new System.Drawing.Size(121, 20);
            this.lblRateConsumption.TabIndex = 23;
            this.lblRateConsumption.Text = "消耗品雑材掛率";
            // 
            // numRateConsumption
            // 
            this.numRateConsumption.DecimalPlaces = 2;
            this.numRateConsumption.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numRateConsumption.Location = new System.Drawing.Point(284, 18);
            this.numRateConsumption.Margin = new System.Windows.Forms.Padding(4);
            this.numRateConsumption.Name = "numRateConsumption";
            this.numRateConsumption.Size = new System.Drawing.Size(119, 28);
            this.numRateConsumption.TabIndex = 22;
            this.numRateConsumption.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblRateExpence
            // 
            this.lblRateExpence.AutoSize = true;
            this.lblRateExpence.Location = new System.Drawing.Point(633, 21);
            this.lblRateExpence.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRateExpence.Name = "lblRateExpence";
            this.lblRateExpence.Size = new System.Drawing.Size(73, 20);
            this.lblRateExpence.TabIndex = 21;
            this.lblRateExpence.Text = "経費掛率";
            // 
            // numRateExpence
            // 
            this.numRateExpence.DecimalPlaces = 2;
            this.numRateExpence.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numRateExpence.Location = new System.Drawing.Point(714, 19);
            this.numRateExpence.Margin = new System.Windows.Forms.Padding(4);
            this.numRateExpence.Name = "numRateExpence";
            this.numRateExpence.Size = new System.Drawing.Size(119, 28);
            this.numRateExpence.TabIndex = 24;
            this.numRateExpence.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1036, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "金額";
            // 
            // txtSumKingaku
            // 
            this.txtSumKingaku.Location = new System.Drawing.Point(1086, 18);
            this.txtSumKingaku.Margin = new System.Windows.Forms.Padding(4);
            this.txtSumKingaku.Name = "txtSumKingaku";
            this.txtSumKingaku.ReadOnly = true;
            this.txtSumKingaku.Size = new System.Drawing.Size(139, 28);
            this.txtSumKingaku.TabIndex = 6;
            this.txtSumKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(874, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "数量";
            // 
            // txtSumSuryo
            // 
            this.txtSumSuryo.Location = new System.Drawing.Point(924, 18);
            this.txtSumSuryo.Margin = new System.Windows.Forms.Padding(4);
            this.txtSumSuryo.Name = "txtSumSuryo";
            this.txtSumSuryo.ReadOnly = true;
            this.txtSumSuryo.Size = new System.Drawing.Size(96, 28);
            this.txtSumSuryo.TabIndex = 4;
            this.txtSumSuryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCalcProcessing);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(19, 12);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1546, 77);
            this.panel2.TabIndex = 0;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAdd.Location = new System.Drawing.Point(9, 14);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(138, 52);
            this.btnAdd.TabIndex = 52;
            this.btnAdd.Text = "一括選択(I)";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            // 
            // btnCalcProcessing
            // 
            this.btnCalcProcessing.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCalcProcessing.Location = new System.Drawing.Point(147, 14);
            this.btnCalcProcessing.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalcProcessing.Name = "btnCalcProcessing";
            this.btnCalcProcessing.Size = new System.Drawing.Size(138, 52);
            this.btnCalcProcessing.TabIndex = 53;
            this.btnCalcProcessing.Text = "加工費計算(C)";
            this.btnCalcProcessing.UseVisualStyleBackColor = true;
            // 
            // DE0040_EstimaDetailDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 711);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MinimumSize = new System.Drawing.Size(1600, 650);
            this.Name = "DE0040_EstimaDetailDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DE0040 - 明細内訳";
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.tabMeisai.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlumbing)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHousing)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProcessing)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRateInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRateConsumption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRateExpence)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.TabControl tabMeisai;
        public System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.Panel panel8;
        public System.Windows.Forms.DataGridView dgvPlumbing;
        public System.Windows.Forms.TabPage tabPage3;
        public System.Windows.Forms.Panel panel9;
        public System.Windows.Forms.DataGridView dgvHousing;
        public System.Windows.Forms.TabPage tabPage4;
        public System.Windows.Forms.Panel panel10;
        public System.Windows.Forms.DataGridView dgvProcessing;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtSumKingaku;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtSumSuryo;
        public System.Windows.Forms.Label lblRateExpence;
        public System.Windows.Forms.NumericUpDown numRateExpence;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinID_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Seq_Plb;
        private System.Windows.Forms.DataGridViewButtonColumn BtnSearch_Plb;
        private System.Windows.Forms.DataGridViewButtonColumn BtnClear_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinCD_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentCDName_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinSizeName_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinName_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Suryo_Plb;
        private System.Windows.Forms.DataGridViewComboBoxColumn Unit_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tanka_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn BDCount_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kingaku_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status_Plb;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinID_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Seq_Hsg;
        private System.Windows.Forms.DataGridViewButtonColumn BtnSearch_Hsg;
        private System.Windows.Forms.DataGridViewButtonColumn BtnClear_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinCD_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentCDName_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinSizeName_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinName_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Suryo_Hsg;
        private System.Windows.Forms.DataGridViewComboBoxColumn Unit_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tanka_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn BDCount_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kingaku_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status_Hsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinID_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Seq_Prc;
        private System.Windows.Forms.DataGridViewButtonColumn BtnSearch_Prc;
        private System.Windows.Forms.DataGridViewButtonColumn BtnClear_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinCD_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParentCDName_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinSizeName_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShohinName_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Suryo_Prc;
        private System.Windows.Forms.DataGridViewComboBoxColumn Unit_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tanka_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn BDCount_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kingaku_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn DetailKbn_Prc;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AddFromOtherGrid_Prc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status_Prc;
        public System.Windows.Forms.Label lblRateInspection;
        public System.Windows.Forms.NumericUpDown numRateInspection;
        public System.Windows.Forms.Label lblRateConsumption;
        public System.Windows.Forms.NumericUpDown numRateConsumption;
        public System.Windows.Forms.Button btnCalcProcessing;
    }
}