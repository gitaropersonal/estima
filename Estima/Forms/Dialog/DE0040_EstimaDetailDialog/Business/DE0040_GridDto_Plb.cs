﻿using EstimaLib.Const;

namespace Estima.Forms.Dialog.DE0040.Business
{
    public class DE0040_GridDto_Plb
    {
        public DE0040_GridDto_Plb()
        {
            this.BtnSearch_Plb = "選択";
            this.BtnClear_Plb = "クリア";
            this.Status_Plb = Enums.EditStatus.None;
        }
        public int ShohinID_Plb { get; set; }
        public int Seq_Plb { get; set; }
        public string BtnSearch_Plb { get; set; }
        public string BtnClear_Plb { get; set; }
        public string ShohinCD_Plb { get; set; }
        public string ParentCDName_Plb { get; set; }
        public string ShohinSizeName_Plb { get; set; }
        public string ShohinName_Plb { get; set; }
        public string Suryo_Plb { get; set; }
        public string Tanka_Plb { get; set; }
        public string Unit_Plb { get; set; }
        public string Kingaku_Plb { get; set; }
        public string BDCount_Plb { get; set; }
        public Enums.EditStatus Status_Plb { get; set; }
    }
}
