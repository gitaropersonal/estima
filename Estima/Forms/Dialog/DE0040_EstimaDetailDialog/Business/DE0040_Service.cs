﻿using EstimaLib.Const;
using EstimaLib.Entity;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Estima.Forms.Dialog.DE0040.Business
{
    internal class DE0040_Service
    {
        #region 商品データ
        /// <summary>
        /// 商品データ取得
        /// </summary>
        /// <param name="shohinID"></param>
        /// <returns></returns>
        internal DE0040_ShohinInfoDto SelShohinData(int shohinID)
        {
            var ret = new DE0040_ShohinInfoDto();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VMX01.ShohinID ");
            sb.AppendLine($",   VMX01.ShohinCD ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($",   VMX01.ParentCDName ");
            sb.AppendLine($",   VMX01.ChildCDName AS ShohinName ");
            sb.AppendLine($",   VMX01.ShohinSize AS ShohinSizeName ");
            sb.AppendLine($",   VMX01.Price AS Tanka ");
            sb.AppendLine($",   VMX01.InchDia ");
            sb.AppendLine($",   VMX01.BDCount ");
            sb.AppendLine($",   VMX01.DetailKbn ");
            sb.AppendLine($",   VMX01.ProcessingChildCD1 ");
            sb.AppendLine($",   VMX01.ProcessingChildCD2 ");
            sb.AppendLine($",   VMX01.ProcessingChildCD3 ");
            sb.AppendLine($",   VMX01.ProcessingChildCD4 ");
            sb.AppendLine($",   VMX01.ProcessingChildCD5 ");
            sb.AppendLine($",   VMX01.ProcessingSeq1 ");
            sb.AppendLine($",   VMX01.ProcessingSeq2 ");
            sb.AppendLine($",   VMX01.ProcessingSeq3 ");
            sb.AppendLine($",   VMX01.ProcessingSeq4 ");
            sb.AppendLine($",   VMX01.ProcessingSeq5 ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VMX01Shohin AS VMX01 ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    VMX01.ShohinID = @shohinID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@shohinID", shohinID) { DbType = DbType.Int32 });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.ShohinID = CommonUtil.ToInteger(dr[nameof(ret.ShohinID)]);
                            ret.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinCD)]);
                            ret.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinName)]);
                            ret.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinSizeName)]);
                            ret.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(ret.ParentCDName)]);
                            ret.Tanka = CommonUtil.ToInteger(dr[nameof(ret.Tanka)]).ToString("#,0");
                            ret.BDCount = CommonUtil.ToInteger(dr[nameof(ret.BDCount)]).ToString("#,0");
                            ret.DetailKbn = CommonUtil.ToInteger(dr[nameof(ret.DetailKbn)]);
                            ret.ProcessingChildCD1 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingChildCD1)]);
                            ret.ProcessingChildCD2 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingChildCD2)]);
                            ret.ProcessingChildCD3 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingChildCD3)]);
                            ret.ProcessingChildCD4 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingChildCD4)]);
                            ret.ProcessingChildCD5 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingChildCD5)]);
                            ret.ProcessingSeq1 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingSeq1)]);
                            ret.ProcessingSeq2 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingSeq2)]);
                            ret.ProcessingSeq3 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingSeq3)]);
                            ret.ProcessingSeq4 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingSeq4)]);
                            ret.ProcessingSeq5 = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingSeq5)]);
                            ret.Status = Enums.EditStatus.Show;
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 商品データ取得
        /// </summary>
        /// <param name="shohinCD"></param>
        /// <param name="detailKbn"></param>
        /// <returns></returns>
        internal DE0040_ShohinInfoDto SelShohinData(
               string shohinCD
            , Enums.DetailKbn detailKbn
            )
        {
            var ret = new DE0040_ShohinInfoDto();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VMX01.ShohinID ");
            sb.AppendLine($",   VMX01.ShohinCD ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($",   VMX01.ParentCDName ");
            sb.AppendLine($",   VMX01.ChildCDName AS ShohinName ");
            sb.AppendLine($",   VMX01.ShohinSize AS ShohinSizeName ");
            sb.AppendLine($",   VMX01.Price AS Tanka ");
            sb.AppendLine($",   VMX01.InchDia ");
            sb.AppendLine($",   VMX01.BDCount ");
            sb.AppendLine($",   VMX01.DetailKbn ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VMX01Shohin AS VMX01 ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    VMX01.ShohinCD = @shohinCD ");
            switch (detailKbn)
            {
                case Enums.DetailKbn.TabPlumbing:
                    sb.AppendLine($" AND VMX01.DetailKbn IN(  {(int)Enums.DetailKbn.Plumbing} ");
                    sb.AppendLine($"                        , {(int)Enums.DetailKbn.Joint} ");
                    sb.AppendLine($") ");
                    break;
                case Enums.DetailKbn.TabHousing:
                    sb.AppendLine($" AND VMX01.DetailKbn IN(  {(int)Enums.DetailKbn.Housing} ");
                    sb.AppendLine($") ");
                    break;
                case Enums.DetailKbn.TabProcessing:
                    sb.AppendLine($" AND VMX01.DetailKbn IN(  {(int)Enums.DetailKbn.ProcessingCost} ");
                    sb.AppendLine($") ");
                    break;
            }
            sb.AppendLine($"ORDER BY");
            sb.AppendLine($"    VMX01.TaishoYM DESC LIMIT 1 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@shohinCD", shohinCD) { DbType = DbType.String });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.ShohinID = CommonUtil.ToInteger(dr[nameof(ret.ShohinID)]);
                            ret.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinCD)]);
                            ret.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinName)]);
                            ret.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinSizeName)]);
                            ret.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(ret.ParentCDName)]);
                            ret.Tanka = CommonUtil.ToInteger(dr[nameof(ret.Tanka)]).ToString("#,0");
                            ret.BDCount = CommonUtil.ToInteger(dr[nameof(ret.BDCount)]).ToString("#,0");
                            ret.DetailKbn = CommonUtil.ToInteger(dr[nameof(ret.DetailKbn)]);
                            ret.Status = Enums.EditStatus.Show;
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// マスタデータ存在チェック
        /// </summary>
        /// <param name="shohinCD"></param>
        /// <returns></returns>
        internal bool ValidateExistMaster(string shohinCD)
        {
            int count = 0;
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    COUNT(*) ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX03Shohin AS MX03 ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    MX03.ShohinCD = @shohinCD ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@shohinCD", shohinCD) { DbType = DbType.String });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            count = CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return 0 < count;
        }
        /// <summary>
        /// 加工費マスタデータ存在チェック
        /// </summary>
        /// <param name="childCD"></param>
        /// <param name="childSeq"></param>
        /// <param name="shohinSize"></param>
        /// <returns></returns>
        internal bool ValidateExistProcessing(
              string childCD
            , string childSeq
            , string shohinSize
            )
        {
            if (string.IsNullOrEmpty(childCD))
            {
                return true;
            }
            int count = 0;
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    COUNT(*) ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VMX01Shohin AS VMX01 ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    VMX01.ChildCD    = @childCD ");
            sb.AppendLine($"AND VMX01.ChildSeq   = @childSeq ");
            sb.AppendLine($"AND VMX01.ShohinSize = @shohinSize ");
            sb.AppendLine($"AND VMX01.DetailKbn  = {(int)Enums.DetailKbn.ProcessingCost} ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@childCD", childCD) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@childSeq", childSeq) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@shohinSize", shohinSize) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            count = CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return 0 < count;
        }
        #endregion

        #region 加工費
        /// <summary>
        /// 配管加工費情報一覧取得
        /// </summary>
        /// <param name="processingCD1"></param>
        /// <param name="processingCD2"></param>
        /// <param name="processingCD3"></param>
        /// <param name="processingCD4"></param>
        /// <param name="processingCD5"></param>
        /// <param name="processingSeq1"></param>
        /// <param name="processingSeq2"></param>
        /// <param name="processingSeq3"></param>
        /// <param name="processingSeq4"></param>
        /// <param name="processingSeq5"></param>
        /// <param name="sizeList"></param>
        /// <param name="taishoYM"></param>
        /// <returns></returns>
        internal List<DE0040_ShohinInfoDto> GetShohniInfosProcessing(
              string processingCD1
            , string processingCD2
            , string processingCD3
            , string processingCD4
            , string processingCD5
            , string processingSeq1
            , string processingSeq2
            , string processingSeq3
            , string processingSeq4
            , string processingSeq5
            , List<string> sizeList
            , string taishoYM
            )
        {
            var ret = new List<DE0040_ShohinInfoDto>();

            // コメントアウト
            string cmOutProcessingCD1 = CommonUtil.GetCmout(string.IsNullOrEmpty(processingCD1));
            string cmOutProcessingCD2 = CommonUtil.GetCmout(string.IsNullOrEmpty(processingCD2));
            string cmOutProcessingCD3 = CommonUtil.GetCmout(string.IsNullOrEmpty(processingCD3));
            string cmOutProcessingCD4 = CommonUtil.GetCmout(string.IsNullOrEmpty(processingCD4));
            string cmOutProcessingCD5 = CommonUtil.GetCmout(string.IsNullOrEmpty(processingCD5));

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VMX01.ShohinID ");
            sb.AppendLine($",   VMX01.ShohinCD ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($",   VMX01.ParentCDName ");
            sb.AppendLine($",   VMX01.ChildCDName AS ShohinName ");
            sb.AppendLine($",   VMX01.ShohinSize AS ShohinSizeName ");
            sb.AppendLine($",   VMX01.Price AS Tanka ");
            sb.AppendLine($",   VMX01.InchDia ");
            sb.AppendLine($",   VMX01.BDCount ");
            sb.AppendLine($",   VMX01.DetailKbn ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VMX01Shohin AS VMX01 ");
            sb.AppendLine($"INNER JOIN( ");
            sb.AppendLine($"    SELECT ");
            sb.AppendLine($"        V01.ShohinCD ");
            sb.AppendLine($"    ,   MAX(V01.TaishoYM) AS TaishoYM ");
            sb.AppendLine($"    FROM ");
            sb.AppendLine($"        VMX01Shohin AS V01 ");
            sb.AppendLine($"    WHERE ");
            sb.AppendLine($"        V01.TaishoYM <= @taishoYM ");
            sb.AppendLine($"    GROUP BY ");
            sb.AppendLine($"        V01.ShohinCD ");
            sb.AppendLine($") AS UNIT");
            sb.AppendLine($"ON");
            sb.AppendLine($"    VMX01.ShohinCD = Unit.ShohinCD ");
            sb.AppendLine($"AND VMX01.TaishoYM = Unit.TaishoYM ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    VMX01.DetailKbn = @detailKbn ");
            sb.AppendLine($"AND( ");
            sb.AppendLine($"    0 <> 0 ");
            sb.AppendLine($"{cmOutProcessingCD1}OR (VMX01.ChildCD = @processingChildCD1 AND VMX01.ChildSeq = @processingSeq1) ");
            sb.AppendLine($"{cmOutProcessingCD2}OR (VMX01.ChildCD = @processingChildCD2 AND VMX01.ChildSeq = @processingSeq2) ");
            sb.AppendLine($"{cmOutProcessingCD3}OR (VMX01.ChildCD = @processingChildCD3 AND VMX01.ChildSeq = @processingSeq3) ");
            sb.AppendLine($"{cmOutProcessingCD4}OR (VMX01.ChildCD = @processingChildCD4 AND VMX01.ChildSeq = @processingSeq4) ");
            sb.AppendLine($"{cmOutProcessingCD5}OR (VMX01.ChildCD = @processingChildCD5 AND VMX01.ChildSeq = @processingSeq5) ");
            sb.AppendLine($") ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@detailKbn", (int)Enums.DetailKbn.ProcessingCost) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD1", processingCD1) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD2", processingCD2) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD3", processingCD3) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD4", processingCD4) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@processingChildCD5", processingCD5) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq1", processingSeq1) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq2", processingSeq2) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq3", processingSeq3) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq4", processingSeq4) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@processingSeq5", processingSeq5) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@taishoYM", taishoYM) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new DE0040_ShohinInfoDto();
                            dto.ShohinID = CommonUtil.ToInteger(dr[nameof(dto.ShohinID)]);
                            dto.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinCD)]);
                            dto.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinName)]);
                            dto.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinSizeName)]);
                            dto.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentCDName)]);
                            dto.Tanka = CommonUtil.ToInteger(dr[nameof(dto.Tanka)]).ToString("#,0");
                            dto.BDCount = CommonUtil.ToInteger(dr[nameof(dto.BDCount)]).ToString("#,0");
                            dto.DetailKbn = CommonUtil.ToInteger(dr[nameof(dto.DetailKbn)]);
                            dto.Status = Enums.EditStatus.Show;
                            if (!sizeList.Contains(dto.ShohinSizeName))
                            {
                                continue;
                            }
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret.OrderBy(n => CommonUtil.ToInteger(n.ShohinCD.Substring(0, 3)))
                      .ThenBy(n => (n.ShohinCD + "-ZZZ-ZZZ").Split('-')[1])
                      .ThenBy(n => CommonUtil.CutAInteger((n.ShohinCD + "-ZZZ-ZZZ").Split('-')[2]))
                      .ToList();
        }
        /// <summary>
        ///  商品データ取得（消耗品雑材）
        /// </summary>
        /// <returns></returns>
        internal DE0040_ShohinInfoDto GetShohniInfoConsumable()
        {
            var ret = new DE0040_ShohinInfoDto();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VMX01.ShohinID ");
            sb.AppendLine($",   VMX01.ShohinCD ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($",   VMX01.ParentCDName ");
            sb.AppendLine($",   VMX01.ChildCDName AS ShohinName ");
            sb.AppendLine($",   VMX01.ShohinSize AS ShohinSizeName ");
            sb.AppendLine($",   VMX01.Price AS Tanka ");
            sb.AppendLine($",   VMX01.InchDia ");
            sb.AppendLine($",   VMX01.BDCount ");
            sb.AppendLine($",   VMX01.DetailKbn ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VMX01Shohin AS VMX01 ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    VMX01.DetailKbn = @detailKbn ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    LEFT(VMX01.ShohinCD,3) ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($"LIMIT 1 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@detailKbn", (int)Enums.DetailKbn.Consumable) { DbType = DbType.Int32 });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.ShohinID = CommonUtil.ToInteger(dr[nameof(ret.ShohinID)]);
                            ret.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinCD)]);
                            ret.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinName)]);
                            ret.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinSizeName)]);
                            ret.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(ret.ParentCDName)]);
                            ret.Tanka = CommonUtil.ToInteger(dr[nameof(ret.Tanka)]).ToString("#,0");
                            ret.BDCount = CommonUtil.ToInteger(dr[nameof(ret.BDCount)]).ToString("#,0");
                            ret.DetailKbn = CommonUtil.ToInteger(dr[nameof(ret.DetailKbn)]);
                            ret.Status = Enums.EditStatus.Show;
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        ///  商品データ取得（検査費）
        /// </summary>
        /// <returns></returns>
        internal DE0040_ShohinInfoDto GetShohniInfoInspection()
        {
            var ret = new DE0040_ShohinInfoDto();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VMX01.ShohinID ");
            sb.AppendLine($",   VMX01.ShohinCD ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($",   VMX01.ParentCDName ");
            sb.AppendLine($",   VMX01.ChildCDName AS ShohinName ");
            sb.AppendLine($",   VMX01.ShohinSize AS ShohinSizeName ");
            sb.AppendLine($",   VMX01.Price AS Tanka ");
            sb.AppendLine($",   VMX01.InchDia ");
            sb.AppendLine($",   VMX01.BDCount ");
            sb.AppendLine($",   VMX01.DetailKbn ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VMX01Shohin AS VMX01 ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    VMX01.DetailKbn = @detailKbn ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    LEFT(VMX01.ShohinCD,3) ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($"LIMIT 1 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@detailKbn", (int)Enums.DetailKbn.Inspection) { DbType = DbType.Int32 });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.ShohinID = CommonUtil.ToInteger(dr[nameof(ret.ShohinID)]);
                            ret.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinCD)]);
                            ret.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinName)]);
                            ret.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinSizeName)]);
                            ret.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(ret.ParentCDName)]);
                            ret.Tanka = CommonUtil.ToInteger(dr[nameof(ret.Tanka)]).ToString("#,0");
                            ret.BDCount = CommonUtil.ToInteger(dr[nameof(ret.BDCount)]).ToString("#,0");
                            ret.DetailKbn = CommonUtil.ToInteger(dr[nameof(ret.DetailKbn)]);
                            ret.Status = Enums.EditStatus.Show;
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        ///  商品データ取得（諸経費）
        /// </summary>
        /// <returns></returns>
        internal DE0040_ShohinInfoDto GetShohniInfoExpence()
        {
            var ret = new DE0040_ShohinInfoDto();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VMX01.ShohinID ");
            sb.AppendLine($",   VMX01.ShohinCD ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($",   VMX01.ParentCDName ");
            sb.AppendLine($",   VMX01.ChildCDName AS ShohinName ");
            sb.AppendLine($",   VMX01.ShohinSize AS ShohinSizeName ");
            sb.AppendLine($",   VMX01.Price AS Tanka ");
            sb.AppendLine($",   VMX01.InchDia ");
            sb.AppendLine($",   VMX01.BDCount ");
            sb.AppendLine($",   VMX01.DetailKbn ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VMX01Shohin AS VMX01 ");
            sb.AppendLine($"WHERE");
            sb.AppendLine($"    VMX01.DetailKbn = @detailKbn ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    LEFT(VMX01.ShohinCD,3) ");
            sb.AppendLine($",   VMX01.TaishoYM ");
            sb.AppendLine($"LIMIT 1 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@detailKbn", (int)Enums.DetailKbn.Expense) { DbType = DbType.Int32 });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.ShohinID = CommonUtil.ToInteger(dr[nameof(ret.ShohinID)]);
                            ret.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinCD)]);
                            ret.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinName)]);
                            ret.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(ret.ShohinSizeName)]);
                            ret.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(ret.ParentCDName)]);
                            ret.Tanka = CommonUtil.ToInteger(dr[nameof(ret.Tanka)]).ToString("#,0");
                            ret.BDCount = CommonUtil.ToInteger(dr[nameof(ret.BDCount)]).ToString("#,0");
                            ret.DetailKbn = CommonUtil.ToInteger(dr[nameof(ret.DetailKbn)]);
                            ret.Status = Enums.EditStatus.Show;
                        }
                    }
                }
            }
            return ret;
        }
        #endregion

        #region ワークテーブル
        /// <summary>
        /// ワークテーブル抽出
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <param name="meisaiNo"></param>
        /// <returns></returns>
        internal List<DE0040_ShohinInfoDto> SelWorkTable(
              string tempEstimaID
            , int meisaiNo
            )
        {
            var ret = new List<DE0040_ShohinInfoDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    WX01.TempEstimaID ");
            sb.AppendLine($",   WX01.MeisaiType ");
            sb.AppendLine($",   WX01.Seq ");
            sb.AppendLine($",   WX01.ShohinID ");
            sb.AppendLine($",   WX01.ShohinCD ");
            sb.AppendLine($",   WX01.ParentCDName ");
            sb.AppendLine($",   '' AS ZaishitsuName ");
            sb.AppendLine($",   WX01.ShohinSizeName ");
            sb.AppendLine($",   WX01.ShohinName ");
            sb.AppendLine($",   WX01.Suryo ");
            sb.AppendLine($",   WX01.Tanka ");
            sb.AppendLine($",   CASE ");
            sb.AppendLine($"         WHEN WX01.MeisaiType = 3 ");
            sb.AppendLine($"             THEN WX01.Tanka * WX01.Suryo * WX01.BDCount ");
            sb.AppendLine($"             ELSE WX01.Tanka * WX01.Suryo ");
            sb.AppendLine($"    END AS Kingaku ");
            sb.AppendLine($",   WX01.Unit ");
            sb.AppendLine($",   WX01.BDCount ");
            sb.AppendLine($",   MX02.DetailKbn ");
            sb.AppendLine($",   WX01.ManualFlg ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    WX01TempEstimaDetail AS WX01 ");
            sb.AppendLine($"INNER JOIN ");
            sb.AppendLine($"    MX03Shohin AS MX03 ");
            sb.AppendLine($"ON ");
            sb.AppendLine($"    MX03.ShohinID = WX01.ShohinID ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    MX02ParentCD AS MX02 ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    MX02.ParentCD = LEFT(WX01.ShohinCD, 3) ");
            sb.AppendLine($"AND MX02.ParentSeq = MX03.ParentSeq");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    WX01.TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"AND WX01.MeisaiNo     = @meisaiNo ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    WX01.TempEstimaID ");
            sb.AppendLine($",   WX01.MeisaiType ");
            sb.AppendLine($",   MX02.DetailKbn ");
            sb.AppendLine($",   MX02.ParentCD ");
            sb.AppendLine($",   WX01.ShohinCD ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo", meisaiNo) { DbType = DbType.Int32 });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new DE0040_ShohinInfoDto();
                            dto.MeisaiType = CommonUtil.ToInteger(dr[nameof(dto.MeisaiType)]);
                            dto.ShohinID = CommonUtil.ToInteger(dr[nameof(dto.ShohinID)]);
                            dto.ShohinCD = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinCD)]);
                            dto.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinName)]);
                            dto.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinSizeName)]);
                            dto.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentCDName)]);
                            dto.Tanka = CommonUtil.ToInteger(dr[nameof(dto.Tanka)]).ToString("#,0");
                            dto.Suryo = CommonUtil.ToInteger(dr[nameof(dto.Suryo)]).ToString("#,0");
                            dto.Unit = CommonUtil.TostringNullForbid(dr[nameof(dto.Unit)]);
                            dto.BDCount = CommonUtil.ToInteger(dr[nameof(dto.BDCount)]).ToString("#,0");
                            dto.Kingaku = CommonUtil.ToInteger(dr[nameof(dto.Kingaku)]).ToString("#,0");
                            dto.DetailKbn = CommonUtil.ToInteger(dr[nameof(dto.DetailKbn)]);
                            dto.ManualFlg = CommonUtil.ToInteger(dr[nameof(dto.ManualFlg)]);
                            dto.Status = Enums.EditStatus.Show;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret.OrderBy(n => CommonUtil.ToInteger(n.ShohinCD.Substring(0, 3)))
                      .ThenBy(n => CommonUtil.TostringNullForbid((n.ShohinCD + "-ZZZ-ZZZ").Split('-')[1]))
                      .ThenBy(n => CommonUtil.CutAInteger((n.ShohinCD + "-ZZZ-ZZZ").Split('-')[2]))
                      .ToList();
        }
        /// <summary>
        /// ワークテーブル登録
        /// </summary>
        /// <param name="addEntity"></param>
        /// <returns></returns>
        internal void AddWorkTable(WX01TempEstimaDetail addEntity)
        {
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO WX01TempEstimaDetail( ");
            sb.AppendLine($"    TempEstimaID ");
            sb.AppendLine($",   MeisaiNo ");
            sb.AppendLine($",   MeisaiType ");
            sb.AppendLine($",   Seq ");
            sb.AppendLine($",   ShohinID ");
            sb.AppendLine($",   ShohinCD ");
            sb.AppendLine($",   ParentCDName ");
            sb.AppendLine($",   ShohinSizeName ");
            sb.AppendLine($",   ShohinName ");
            sb.AppendLine($",   Tanka ");
            sb.AppendLine($",   Suryo ");
            sb.AppendLine($",   Unit ");
            sb.AppendLine($",   BDCount ");
            sb.AppendLine($",   ManualFlg ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @tempEstimaID ");
            sb.AppendLine($",   @meisaiNo ");
            sb.AppendLine($",   @meisaiType ");
            sb.AppendLine($",   @seq ");
            sb.AppendLine($",   @shohinID ");
            sb.AppendLine($",   @shohinCD ");
            sb.AppendLine($",   @parentCDName ");
            sb.AppendLine($",   @shohinSizeName ");
            sb.AppendLine($",   @shohinName ");
            sb.AppendLine($",   @tanka ");
            sb.AppendLine($",   @suryo ");
            sb.AppendLine($",   @unit ");
            sb.AppendLine($",   @bdCount ");
            sb.AppendLine($",   @manualFlg ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($"); ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID"    , addEntity.TempEstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo"        , addEntity.MeisaiNo) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiType"      , addEntity.MeisaiType) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@seq"             , addEntity.Seq) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@shohinID"        , addEntity.ShohinID) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@shohinCD"        , addEntity.ShohinCD) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@parentCDName"    , addEntity.ParentCDName) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@shohinSizeName"  , addEntity.ShohinSizeName) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@shohinName"      , addEntity.ShohinName) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@tanka"           , addEntity.Tanka) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@suryo"           , addEntity.Suryo) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@unit"            , addEntity.Unit) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@bdCount"         , addEntity.BDCount) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@manualFlg"       , addEntity.ManualFlg) { DbType = DbType.Int32 });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// ワークテーブル登録（経費）
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <param name="meisaiNo"></param>
        /// <param name="wtEntity"></param>
        internal void AddWorkTableExpence(
              string tempEstimaID
            , int meisaiNo
            , WX02TempEstimaRate wtEntity
            )
        {
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO WX02TempEstimaRate( ");
            sb.AppendLine($"    TempEstimaID ");
            sb.AppendLine($",   MeisaiNo ");
            sb.AppendLine($",   ConsumptionRate ");
            sb.AppendLine($",   InspectionRate ");
            sb.AppendLine($",   ExpenceRate ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @tempEstimaID ");
            sb.AppendLine($",   @meisaiNo ");
            sb.AppendLine($",   @consumptionRate ");
            sb.AppendLine($",   @inspectionRate ");
            sb.AppendLine($",   @expenceRate ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($"); ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo", meisaiNo) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@consumptionRate", wtEntity.ConsumptionRate) { DbType = DbType.Decimal });
                    cmd.Parameters.Add(new MySqlParameter("@inspectionRate", wtEntity.InspectionRate) { DbType = DbType.Decimal });
                    cmd.Parameters.Add(new MySqlParameter("@expenceRate", wtEntity.ExpenceRate) { DbType = DbType.Decimal });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// ワークテーブル削除
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <param name="meisaiNo"></param>
        /// <param name="meisaiType"></param>
        /// <param name="seq"></param>
        internal void DelWorkTableEstimaDetail(
              string tempEstimaID
            , int meisaiNo = -1
            , int meisaiType = -1
            , int seq = -1
            )
        {
            // コメントアウト
            string cmOutMeisaiNo = CommonUtil.GetCmout(meisaiNo == -1);
            string cmOutMeisaiType = CommonUtil.GetCmout(meisaiType == -1);
            string cmOutSeq = CommonUtil.GetCmout(seq == -1);

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM WX01TempEstimaDetail ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"{cmOutMeisaiNo  }AND MeisaiNo     = @meisaiNo ");
            sb.AppendLine($"{cmOutMeisaiType}AND MeisaiType   = @meisaiType ");
            sb.AppendLine($"{cmOutSeq       }AND Seq          = @seq ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo", meisaiNo) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiType", meisaiType) { DbType = DbType.Int32 });
                    cmd.Parameters.Add(new MySqlParameter("@seq", seq) { DbType = DbType.Int32 });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// ワークテーブル削除（経費）
        /// </summary>
        /// <param name="tempEstimaID"></param>
        /// <param name="meisaiNo"></param>
        internal void DelWorkTableExpence(
              string tempEstimaID
            , int meisaiNo
            )
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM WX02TempEstimaRate ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    TempEstimaID = @tempEstimaID ");
            sb.AppendLine($"AND MeisaiNo     = @meisaiNo ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@tempEstimaID", tempEstimaID) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@meisaiNo", meisaiNo) { DbType = DbType.Int32 });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion
    }
}
