﻿using EstimaLib.Const;

namespace Estima.Forms.Dialog.DE0040.Business
{
    public class DE0040_GridDto_Hsg
    {
        public DE0040_GridDto_Hsg()
        {
            this.BtnSearch_Hsg = "選択";
            this.BtnClear_Hsg = "クリア";
            this.Status_Hsg = Enums.EditStatus.None;
        }
        public int ShohinID_Hsg { get; set; }
        public int Seq_Hsg { get; set; }
        public string BtnSearch_Hsg { get; set; }
        public string BtnClear_Hsg { get; set; }
        public string ShohinCD_Hsg { get; set; }
        public string ParentCDName_Hsg { get; set; }
        public string ShohinSizeName_Hsg { get; set; }
        public string ShohinName_Hsg { get; set; }
        public string Suryo_Hsg { get; set; }
        public string Tanka_Hsg { get; set; }
        public string Unit_Hsg { get; set; }
        public string Kingaku_Hsg { get; set; }
        public string BDCount_Hsg { get; set; }
        public Enums.EditStatus Status_Hsg { get; set; }
    }
}
