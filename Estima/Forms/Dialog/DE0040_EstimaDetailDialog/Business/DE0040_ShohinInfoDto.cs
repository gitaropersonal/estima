﻿using EstimaLib.Const;
using System.Collections.Generic;

namespace Estima.Forms.Dialog.DE0040.Business
{
    public class DE0040_ShohinInfoDto
    {
        public DE0040_ShohinInfoDto()
        {
            this.Status = Enums.EditStatus.None;
        }
        public int ShohinID { get; set; }
        public int Seq { get; set; }
        public int MeisaiType { get; set; }
        public string BtnSearch { get; set; }
        public string BtnClear { get; set; }
        public string ShohinCD { get; set; }
        public string ParentCDName { get; set; }
        public string ShohinSizeName { get; set; }
        public string ShohinName { get; set; }
        public string Suryo { get; set; }
        public string Tanka { get; set; }
        public string Unit { get; set; }
        public string Kingaku { get; set; }
        public string BDCount { get; set; }
        public int ManualFlg { get; set; }
        public int DetailKbn { get; set; }
        public string ProcessingChildCD1 { get; set; }
        public string ProcessingChildCD2 { get; set; }
        public string ProcessingChildCD3 { get; set; }
        public string ProcessingChildCD4 { get; set; }
        public string ProcessingChildCD5 { get; set; }
        public string ProcessingSeq1 { get; set; }
        public string ProcessingSeq2 { get; set; }
        public string ProcessingSeq3 { get; set; }
        public string ProcessingSeq4 { get; set; }
        public string ProcessingSeq5 { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
