﻿using EstimaLib.Entity;

namespace Estima.Forms.Dialog.DE0040.Business
{
    internal class DE0040_ProcessingDetailDto
    {
        internal DE0040_ProcessingDetailDto(
              int shohinID
            , int suryo
            , int bdCount
            ) 
        {
            this.ShihinInfo = new MX03Shohin() { ShohinID = shohinID };
            this.Suryo = suryo;
            this.ShihinInfo.BDCount = bdCount;
        }
        internal MX03Shohin ShihinInfo {get; set;}
        internal int Suryo { get; set; }
    }
}
