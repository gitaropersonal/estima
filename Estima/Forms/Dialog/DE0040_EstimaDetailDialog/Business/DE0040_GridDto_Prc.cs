﻿using EstimaLib.Const;

namespace Estima.Forms.Dialog.DE0040.Business
{
    public class DE0040_GridDto_Prc
    {
        public DE0040_GridDto_Prc()
        {
            this.BtnSearch_Prc = "選択";
            this.BtnClear_Prc = "クリア";
            this.AddFromOtherGrid_Prc = false;
            this.Status_Prc = Enums.EditStatus.None;
        }
        public int ShohinID_Prc { get; set; }
        public int Seq_Prc { get; set; }
        public string BtnSearch_Prc { get; set; }
        public string BtnClear_Prc { get; set; }
        public string ShohinCD_Prc { get; set; }
        public string ParentCDName_Prc { get; set; }
        public string ShohinSizeName_Prc { get; set; }
        public string ShohinName_Prc { get; set; }
        public string Suryo_Prc { get; set; }
        public string Tanka_Prc { get; set; }
        public string Unit_Prc { get; set; }
        public string Kingaku_Prc { get; set; }
        public string BDCount_Prc { get; set; }
        public int DetailKbn_Prc { get; set; }
        public bool AddFromOtherGrid_Prc { get; set; }
        public Enums.EditStatus Status_Prc { get; set; }
    }
}
