﻿namespace Estima.Forms.Dialog.DE0040.Business
{
    internal static class DE0040_Messages
    {
        internal const string MsgErrorNotExistDatas = "対象のデータがありません";
        internal const string MsgErrorNotExistMaster = "マスタ未登録の{0}が入力されています";
        internal const string MsgErrorIrregalZero = "{0}には0より大きい値を入力してください";
        internal const string MsgAskUpdate = "仮登録しますか？";
        internal const string MsgAskClear = "行クリアしますか？";
        internal const string MsgAskFinish = "終了しますか？";
        internal const string MsgInfoFinishUpdate = "仮登録しました\r\n親画面で[更新]すると本登録します";
        internal const string MsgAskNotCommitedDataExist = "仮登録されていないデータがありますが\r\nよろしいですか？";
        internal const string MsgExNotExistProcessingInfo = "加工費がマスタ登録されていない商品があります\r\nマスタ画面から加工費の商品情報を登録してください";
    }
}
