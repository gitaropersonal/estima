﻿using Estima.Forms.Application.MST.MST0010;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0040.Business
{
    internal class DE0040_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private DE0040_EstimaDetailDialog Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private readonly LoginInfoDto LoginInfo;
        /// <summary>
        /// 個別サービス
        /// </summary>
        private readonly DE0040_Service PersonalService = new DE0040_Service();
        /// <summary>
        /// 読み取り専用フラグ
        /// </summary>
        private bool ReadOnly { get; set; }
        /// <summary>
        /// 編集フラグ
        /// </summary>
        private bool EditedFlg = false;
        /// <summary>
        /// 対象年月
        /// </summary>
        private string TaishoYM = string.Empty;
        /// <summary>
        /// グリッドセル
        /// </summary>
        private static DE0040_ShohinInfoDto CellHeaderNameDto = new DE0040_ShohinInfoDto();
        private static DE0040_GridDto_Plb CellHeaderNameDtoPlb = new DE0040_GridDto_Plb();
        private static DE0040_GridDto_Hsg CellHeaderNameDtoHsg = new DE0040_GridDto_Hsg();
        private static DE0040_GridDto_Prc CellHeaderNameDtoPrc = new DE0040_GridDto_Prc();
        private readonly string[] ForbidUpdateGridCells = new string[]
        {
              nameof(CellHeaderNameDtoPlb.ShohinID_Plb)
            , nameof(CellHeaderNameDtoPlb.ShohinName_Plb)
            , nameof(CellHeaderNameDtoPlb.ParentCDName_Plb)
            , nameof(CellHeaderNameDtoPlb.ShohinSizeName_Plb)
            , nameof(CellHeaderNameDtoPlb.Tanka_Plb)
            , nameof(CellHeaderNameDtoPlb.Kingaku_Plb)
        };
        private readonly string[] ForbidColoredGridCells = new string[]
        {
              nameof(CellHeaderNameDto.Kingaku)
            , nameof(CellHeaderNameDto.Status)
        };
        /// <summary>
        /// 選択された列名末尾
        /// </summary>
        private string SelectedCollAdd = string.Empty;

        /// <summary>
        /// 加工費明細タブインデックス
        /// </summary>
        private const int TabIndexPlb = 0;
        /// <summary>
        /// 加工費明細タブインデックス
        /// </summary>
        private const int TabIndexHsg = 1;
        /// <summary>
        /// 加工費明細タブインデックス
        /// </summary>
        private const int TabIndexPrc = 2;

        /// <summary>
        /// グリッドで選択されている行インデックス
        /// </summary>
        private int CurrRowIdx = 0;
        /// <summary>
        /// グリッドで選択されている列インデックス
        /// </summary>
        private int CurrColIdx = 0;

        /// <summary>
        /// 画面終了時に返すダイアログリザルト
        /// </summary>
        private DialogResult CurrDialogResult = DialogResult.Cancel;
        /// <summary>
        /// 最大の明細行数
        /// </summary>
        private const int MaxRowCount = 300;
        /// <summary>
        /// 更新時エラーメッセージ
        /// </summary>
        private string UpdErrMessage = string.Empty;
        /// <summary>
        /// 仮帳票ID
        /// </summary>
        private string TempEstimaID = string.Empty;
        /// <summary>
        /// 明細№
        /// </summary>
        private int MeisaiNo = 0;
        /// <summary>
        /// 明細タイプ
        /// </summary>
        private int MeisaiType = 0;
        /// <summary>
        /// 消耗品雑材率（クリア用）
        /// </summary>
        private float OriginConsumptionRate = 0;
        /// <summary>
        /// 検査費率（クリア用）
        /// </summary>
        private float OriginInspectionRate = 0;
        /// <summary>
        /// 経費率（クリア用）
        /// </summary>
        private float OriginExpenceRate = 0;
        /// <summary>
        /// 消耗品雑材率
        /// </summary>
        private float ConsumptionRate = 0;
        /// <summary>
        /// 検査費率
        /// </summary>
        private float InspectionRate = 0;
        /// <summary>
        /// 経費率
        /// </summary>
        private float ExpenceRate = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        /// <param name="tempEstimaID"></param>
        /// <param name="meisaiNo"></param>
        /// <param name="meisaiType"></param>
        /// <param name="consumptionRate"></param>
        /// <param name="inspectionRate"></param>
        /// <param name="expenceRate"></param>
        /// <param name="readOnly"></param>
        /// <param name="taishoYM"></param>
        internal DE0040_FormLogic(
              DE0040_EstimaDetailDialog body
            , LoginInfoDto loginInfo
            , string tempEstimaID
            , int meisaiNo
            , int meisaiType
            , float consumptionRate
            , float inspectionRate
            , float expenceRate
            , bool readOnly
            , string taishoYM
            )
        {
            this.Body = body;
            this.LoginInfo = loginInfo;
            this.TempEstimaID = tempEstimaID;
            this.MeisaiNo = meisaiNo;
            this.MeisaiType = meisaiType;
            this.ReadOnly = readOnly;
            this.OriginConsumptionRate = consumptionRate;
            this.OriginInspectionRate = inspectionRate;
            this.OriginExpenceRate = expenceRate;
            this.ConsumptionRate = consumptionRate;
            this.InspectionRate = inspectionRate;
            this.ExpenceRate = expenceRate;
            this.TaishoYM = taishoYM;
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += (s, e) =>
            {
                CommonUtil.BtnClearClickEvent(
                      new Action(() => this.Init())
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了処理
            this.Body.FormClosing += this.ClosingEvent;
            // 追加ボタン押下
            this.Body.btnAdd.Click += this.BtnAddClickEvent;
            // 加工費計算ボタン押下
            this.Body.btnCalcProcessing.Click += this.BtnCalcProcessingClickEvent;
            // 更新ボタン押下
            this.Body.btnUpdate.Click += this.BtnUpdClickEvent;
            // グリッドRowPostPaint
            this.Body.dgvHousing.RowPostPaint += this.GridRowPostPaintEvent;
            this.Body.dgvPlumbing.RowPostPaint += this.GridRowPostPaintEvent;
            this.Body.dgvProcessing.RowPostPaint += this.GridRowPostPaintEvent;
            // グリッドCellEnter
            this.Body.dgvHousing.CellEnter += this.GridCelEnter;
            this.Body.dgvPlumbing.CellEnter += this.GridCelEnter;
            this.Body.dgvProcessing.CellEnter += this.GridCelEnter;
            // グリッドEditingControlShowing
            this.Body.dgvHousing.EditingControlShowing += this.GridKeyPress;
            this.Body.dgvPlumbing.EditingControlShowing += this.GridKeyPress;
            this.Body.dgvProcessing.EditingControlShowing += this.GridKeyPress;
            // グリッドCellPainting
            this.Body.dgvHousing.CellPainting += (s, e) =>
            {
                var dgv = (DataGridView)s;
                dgv.ReadOnly = this.ReadOnly;
                dgv.Columns[nameof(CellHeaderNameDtoHsg.Kingaku_Hsg)].ReadOnly = true;
            };
            this.Body.dgvPlumbing.CellPainting += (s, e) =>
            {
                var dgv = (DataGridView)s;
                dgv.ReadOnly = this.ReadOnly;
                dgv.Columns[nameof(CellHeaderNameDtoPlb.Kingaku_Plb)].ReadOnly = true;
            };
            this.Body.dgvProcessing.CellPainting += (s, e) =>
            {
                var dgv = (DataGridView)s;
                dgv.ReadOnly = this.ReadOnly;
                dgv.Columns[nameof(CellHeaderNameDtoPrc.Kingaku_Prc)].ReadOnly = true;
            };
            // グリッドCellイベント管理
            this.ManageGridCellEvent(false);
            // タブSelectedIndexChanged
            this.Body.tabMeisai.SelectedIndexChanged += this.TabSelectedIndexChanged;
            // 掛率
            this.Body.numRateConsumption.ValueChanged += this.RateValueChanged;
            this.Body.numRateInspection.ValueChanged += this.RateValueChanged;
            this.Body.numRateExpence.ValueChanged += this.RateValueChanged;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.I:
                        this.Body.btnAdd.Focus();
                        this.Body.btnAdd.PerformClick();
                        break;
                    case Keys.C:
                        this.Body.btnCalcProcessing.Focus();
                        this.Body.btnCalcProcessing.PerformClick();
                        break;
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.O:
                        this.Body.btnUpdate.Focus();
                        this.Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// 掛率ValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void RateValueChanged(
              object s
            , EventArgs e
            )
        {
            this.ManageGridCellEvent();
            try
            {
                // 掛率セット
                this.ConsumptionRate = (float)this.Body.numRateConsumption.Value;
                this.InspectionRate = (float)this.Body.numRateInspection.Value;
                this.ExpenceRate = (float)this.Body.numRateExpence.Value;
                // グリッドデータ存在チェック
                if (!this.ValidateExistGridData(false))
                {
                    return;
                }
                // 商品情報作成
                var shohinInfos = this.GetShohinInfosFromOtherGrid();
                // 自動追加行データ編集
                this.EditAutoAdditionalRow();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                this.ManageGridCellEvent(false);
            }
        }
        /// <summary>
        /// タブ選択変更時イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void TabSelectedIndexChanged(
              object s
            , EventArgs e
            )
        {
            switch (this.Body.tabMeisai.SelectedIndex)
            {
                case 0:
                    this.SelectedCollAdd = "_Plb";
                    this.Body.numRateConsumption.Visible = false;
                    this.Body.lblRateConsumption.Visible = false;
                    this.Body.numRateInspection.Visible = false;
                    this.Body.lblRateInspection.Visible = false;
                    this.Body.numRateExpence.Visible = false;
                    this.Body.lblRateExpence.Visible = false;
                    break;
                case 1:
                    this.SelectedCollAdd = "_Hsg";
                    this.Body.numRateConsumption.Visible = false;
                    this.Body.lblRateConsumption.Visible = false;
                    this.Body.numRateInspection.Visible = false;
                    this.Body.lblRateInspection.Visible = false;
                    this.Body.numRateExpence.Visible = false;
                    this.Body.lblRateExpence.Visible = false;
                    break;
                case 2:
                    this.SelectedCollAdd =  "_Prc";
                    this.Body.numRateConsumption.Visible = true;
                    this.Body.lblRateConsumption.Visible = true;
                    this.Body.numRateInspection.Visible = true;
                    this.Body.lblRateInspection.Visible = true;
                    this.Body.numRateExpence.Visible = true;
                    this.Body.lblRateExpence.Visible = true;
                    break;
            }
            // 小計計算
            this.CalcSummary(this.Body.tabMeisai.SelectedIndex);
        }
        /// <summary>
        /// グリッドRowPostPaint
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridRowPostPaintEvent(
              object s
            , DataGridViewRowPostPaintEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            GridRowUtil<DE0040_GridDto_Hsg>.SetRowNum(dgv, e);
            if (this.ReadOnly)
            {
                return;
            }
            this.EditGridCellStyle(dgv, this.SelectedCollAdd);
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCelEnter(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            this.CurrRowIdx = e.RowIndex;
            this.CurrColIdx = e.ColumnIndex;
            var dgv = (DataGridView)s;
            var tgtCol = dgv.Columns[e.ColumnIndex];
            if (tgtCol == null)
            {
                return;
            }
            // IMEモードを設定
            if (tgtCol.Name.Contains(nameof(CellHeaderNameDto.ParentCDName)) || tgtCol.Name.Contains(nameof(CellHeaderNameDto.ShohinName)))
            {
                dgv.ImeMode = ImeMode.Hiragana;
                return;
            }
            dgv.ImeMode = ImeMode.Disable;
        }
        /// <summary>
        /// グリッドセルクリック
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellClick(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var dgv = (DataGridView)s;
                if (e.RowIndex < 0 || e.ColumnIndex < 0)
                {
                    return;
                }
                this.CurrRowIdx = e.RowIndex;
                this.CurrColIdx = e.ColumnIndex;
                DataGridViewRow r = dgv.Rows[this.CurrRowIdx];
                DataGridViewCell c = r.Cells[this.CurrColIdx];
                int tabIndex = this.Body.tabMeisai.SelectedIndex;
                if (c.OwningColumn.Name.Contains(nameof(CellHeaderNameDto.BtnSearch)))
                {
                    // GridCellValueChangeイベントの管理
                    this.ManageGridCellEvent();
                    // 行追加処理
                    bool rowAdded = this.AddRowLogic(r, tabIndex);
                    // グリッドセル背景色セット
                    this.EditGridCellStyle(dgv, this.SelectedCollAdd);
                    // 加工費存在チェック
                    int shohinID = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{this.SelectedCollAdd}"].Value);
                    if (rowAdded && !this.ValidateExistProcessingChildCds(shohinID))
                    {
                        r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellError;
                        CommonUtil.ShowInfoExcramationOK(DE0040_Messages.MsgExNotExistProcessingInfo);
                    }
                    // GridCellValueChangeイベントの管理
                    this.ManageGridCellEvent(false);
                    return;
                }
                if (c.OwningColumn.Name.Contains(nameof(CellHeaderNameDto.BtnClear)))
                {
                    // GridCellValueChangeイベントの管理
                    this.ManageGridCellEvent();
                    // 行クリア処理
                    this.ClearRowLogic(r, tabIndex);
                    // グリッドセル背景色セット
                    this.EditGridCellStyle(dgv, this.SelectedCollAdd);
                    // GridCellValueChangeイベントの管理
                    this.ManageGridCellEvent(false);
                    return;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// グリッドセルキー入力制御
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridKeyPress(
              object s
            , DataGridViewEditingControlShowingEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            if (!dgv.Focused)
            {
                return;
            }
            //表示されているコントロールがDataGridViewTextBoxEditingControlか調べる
            if (!(e.Control is DataGridViewTextBoxEditingControl))
            {
                return;
            }
            //編集のために表示されているコントロールを取得
            var tb = (DataGridViewTextBoxEditingControl)e.Control;

            //イベントハンドラ編集
            tb.KeyPress -= new KeyPressEventHandler(CommonUtil.GridCellKeyPressInteger);
            tb.KeyPress -= new KeyPressEventHandler(CommonUtil.GridCellKeyPressUpperInteger);
            string cellName = dgv.CurrentCell.OwningColumn.Name;
            if (cellName.Contains(nameof(CellHeaderNameDto.Suryo)) || cellName.Contains(nameof(CellHeaderNameDto.BDCount)))
            {
                tb.KeyPress += new KeyPressEventHandler(CommonUtil.GridCellKeyPressInteger);
                return;
            }
            if (cellName.Contains(nameof(CellHeaderNameDto.Tanka)))
            {
                tb.KeyPress += new KeyPressEventHandler(CommonUtil.GridCellKeyPressUpperInteger);
                return;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            this.CurrRowIdx = e.RowIndex;
            this.CurrColIdx = e.ColumnIndex;

            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // GridCellValueChangeイベントの管理
            this.ManageGridCellEvent();

            var dgv = (DataGridView)s;
            DataGridViewRow r = dgv.Rows[this.CurrRowIdx];
            DataGridViewCell c = r.Cells[this.CurrColIdx];
            var tgtCellVal = c.Value;
            int tabIndex = this.Body.tabMeisai.SelectedIndex;
            var selectedDetailKbn = this.GetTargetDetailKbn(tabIndex);
            bool calcProcessing = true;

            // 列ごとに処理分岐
            string tgtCellName = c.OwningColumn.Name
                                  .Replace("_Plb", string.Empty)
                                  .Replace("_Hsg", string.Empty)
                                  .Replace("_Prc", string.Empty);
            try
            {
                switch (tgtCellName)
                {
                    // 商品コード
                    case nameof(CellHeaderNameDto.ShohinCD):
                        var shohinCD = CommonUtil.TostringNullForbid(r.Cells[$"{tgtCellName}{this.SelectedCollAdd}"].Value);
                        var shohinInfo = this.PersonalService.SelShohinData(shohinCD, selectedDetailKbn);
                        if (!string.IsNullOrEmpty(shohinInfo.ShohinCD))
                        {
                            // 行データセット
                            this.SetRowValue(r, shohinInfo, tabIndex);
                        }
                        else
                        {
                            c.Value = string.Empty;
                            var oldShohinID = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{this.SelectedCollAdd}"].Value);
                            if (oldShohinID <= 0)
                            {
                                calcProcessing = false;
                                r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{this.SelectedCollAdd}"].Value = -1;
                            }
                        }
                        break;
                    // 数量
                    case nameof(CellHeaderNameDto.Suryo):
                    // 単価
                    case nameof(CellHeaderNameDto.Tanka):
                    // BD数
                    case nameof(CellHeaderNameDto.BDCount):
                        c.Value = CommonUtil.ToInteger(tgtCellVal).ToString("#,0");
                        // 金額計算
                        this.CalcRowKingaku(r, tabIndex);
                        // 小計計算
                        this.CalcSummary(tabIndex);
                        break;
                }
                if (calcProcessing)
                {
                    // ステータス更新
                    this.UpdateRowStatus(r);
                }
                // セル背景色
                this.EditCellBackColorGridCellValueChanged(r, c, tgtCellName, tabIndex, dgv, calcProcessing);
                // 編集フラグ
                this.EditedFlg = calcProcessing;
                // 使用可否切り替え（更新ボタン）
                this.SwitchEnabled();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                // GridCellValueChangeイベントの管理
                this.ManageGridCellEvent(false);
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// グリッドセル編集時の背景色編集
        /// </summary>
        /// <param name="r"></param>
        /// <param name="c"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="tabIndex"></param>
        /// <param name="dgv"></param>
        /// <param name="calcProcessing"></param>
        private void EditCellBackColorGridCellValueChanged(
              DataGridViewRow r
            , DataGridViewCell c
            , string tgtCellName
            , int tabIndex
            , DataGridView dgv
            , bool calcProcessing
            )
        {
            if (calcProcessing && !this.ForbidColoredGridCells.Contains(tgtCellName))
            {
                c.Style.BackColor = Colors.BackColorCellEdited;
            }
            if (!tgtCellName.Contains(nameof(CellHeaderNameDto.Unit)))
            {
                if (tabIndex == TabIndexPrc)
                {
                    if (calcProcessing)
                    {
                        // 自動追加行データ編集
                        this.EditAutoAdditionalRow();
                    }
                }
                else
                {
                    if (calcProcessing)
                    {
                        // 商品情報作成
                        var shohinInfos = this.GetShohinInfosFromOtherGrid();
                        // 加工費明細の行セット（Proxy）
                        this.SetProcessingRowDataProxy(shohinInfos, tabIndex);
                    }
                    // グリッドセル背景色セット
                    this.EditGridCellStyle(dgv, this.SelectedCollAdd);
                }
            }
            if (!calcProcessing)
            {
                return;
            }
            if (tgtCellName.Contains(nameof(CellHeaderNameDto.ShohinCD)))
            {
                int shohinID = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{this.SelectedCollAdd}"].Value);
                if (!this.ValidateExistProcessingChildCds(shohinID))
                {
                    c.Style.BackColor = Colors.BackColorCellError;
                    CommonUtil.ShowInfoExcramationOK(DE0040_Messages.MsgExNotExistProcessingInfo);
                }
            }
        }
        /// <summary>
        /// 自動追加行データ編集
        /// </summary>
        private void EditAutoAdditionalRow()
        {
            // グリッドデータ取得
            var dataSourceProcessing = GridRowUtil<DE0040_GridDto_Prc>.GetAllRowsModel(this.Body.dgvProcessing.DataSource);
            // 自動で行追加される行の金額計算
            var kingakuArray = this.CalcAutoAdditionalRowKingaku(dataSourceProcessing);
            // 消耗品雑材の行データ編集
            this.EditAutoAdditionalRowValue(dataSourceProcessing, Enums.DetailKbn.Consumable, kingakuArray[0]);
            // 検査費の行データ編集
            this.EditAutoAdditionalRowValue(dataSourceProcessing, Enums.DetailKbn.Inspection, kingakuArray[1]);
            // 経費の行データ編集
            this.EditAutoAdditionalRowValue(dataSourceProcessing, Enums.DetailKbn.Expense, kingakuArray[2]);
            // 小計計算
            this.CalcSummary(this.Body.tabMeisai.SelectedIndex);
            // 編集フラグ
            this.EditedFlg = true;
            // 使用可否切り替え（更新ボタン）
            this.SwitchEnabled();
        }
        /// <summary>
        /// 自動追加行データの値編集
        /// </summary>
        /// <param name="dataSourceProcessing"></param>
        /// <param name="detailKbn"></param>
        /// <param name="kingaku"></param>
        private void EditAutoAdditionalRowValue(
              List<DE0040_GridDto_Prc> dataSourceProcessing
            , Enums.DetailKbn detailKbn
            , int kingaku
            )
        {
            var rowDto = dataSourceProcessing.FirstOrDefault(n => n.DetailKbn_Prc == (int)detailKbn);
            if (rowDto != null)
            {
                int rowIndex = dataSourceProcessing.IndexOf(rowDto);
                DataGridViewRow r = this.Body.dgvProcessing.Rows[rowIndex];
                r.Cells[nameof(CellHeaderNameDtoPrc.Tanka_Prc)].Value = kingaku.ToString("#,0");
                r.Cells[nameof(CellHeaderNameDtoPrc.Tanka_Prc)].Style.BackColor = Colors.BackColorCellEdited;
                r.Cells[nameof(CellHeaderNameDtoPrc.Kingaku_Prc)].Value = kingaku.ToString("#,0");
                string status = r.Cells[nameof(CellHeaderNameDtoPrc.Status_Prc)].Value.ToString();
                if (status == Enums.EditStatus.None.ToString() || status == Enums.EditStatus.Insert.ToString())
                {
                    r.Cells[nameof(CellHeaderNameDtoPrc.Status_Prc)].Value = Enums.EditStatus.Insert;
                }
                else
                {
                    r.Cells[nameof(CellHeaderNameDtoPrc.Status_Prc)].Value = Enums.EditStatus.Update;
                }
            }
        }
        /// <summary>
        /// グリッドのセル背景色編集
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="colAdd"></param>
        private void EditGridCellStyle(
              DataGridView dgv
            , string colAdd
            )
        {
            foreach (DataGridViewRow r in dgv.Rows)
            {
                string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{colAdd}"].Value.ToString();
                if (status == Enums.EditStatus.None.ToString())
                {
                    continue;
                }
                int shohinID = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{colAdd}"].Value);
                string shohinCD = r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{colAdd}"].Value.ToString();
                var shohinCDSplit = shohinCD.Split('-');
                if (shohinID == -1 || shohinCDSplit.Length == 2)
                {
                    foreach (DataGridViewCell c in r.Cells)
                    {
                        c.Style.BackColor = this.GetBackColor(c);
                        c.ReadOnly = false;
                    }
                    continue;
                }
                r.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{colAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;
                r.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{colAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;
                r.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{colAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;

                r.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{colAdd}"].ReadOnly = true;
                r.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{colAdd}"].ReadOnly = true;
                r.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{colAdd}"].ReadOnly = true;
                switch (colAdd)
                {
                    case "_Prc":
                        this.EditGridCellStyleProcessing(r, colAdd);
                        break;
                    default:
                        r.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{colAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;
                        r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{colAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;
                        r.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{colAdd}"].ReadOnly = true;
                        r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{colAdd}"].ReadOnly = true;
                        break;
                }
            }
        }
        /// <summary>
        /// グリッドのセル背景色編集（加工費）
        /// </summary>
        /// <param name="r"></param>
        /// <param name="colAdd"></param>
        private void EditGridCellStyleProcessing(
              DataGridViewRow r
            , string colAdd
            )
        {
            int detailKbn = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.DetailKbn)}{colAdd}"].Value);
            DataGridViewCell cellTanka = r.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{colAdd}"];
            DataGridViewCell cellSuryo = r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{colAdd}"];
            DataGridViewCell cellBDCount = r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{colAdd}"];
            switch (detailKbn)
            {
                case (int)Enums.DetailKbn.Consumable:
                case (int)Enums.DetailKbn.Inspection:
                case (int)Enums.DetailKbn.Expense:
                    cellTanka.ReadOnly = false;
                    cellTanka.Style.BackColor = this.GetBackColor(cellTanka);
                    cellSuryo.ReadOnly = true;
                    cellSuryo.Style.BackColor = Colors.BackColorBtnReadOnly;
                    cellBDCount.ReadOnly = true;
                    cellBDCount.Style.BackColor = Colors.BackColorBtnReadOnly;
                    break;
                case (int)Enums.DetailKbn.ProcessingCost:
                    cellTanka.ReadOnly = true;
                    cellTanka.Style.BackColor = this.GetBackColor(cellTanka);
                    cellSuryo.ReadOnly = false;
                    cellSuryo.Style.BackColor = this.GetBackColor(cellTanka);
                    cellBDCount.ReadOnly = false;
                    cellBDCount.Style.BackColor = this.GetBackColor(cellTanka);
                    break;
                default:
                    cellTanka.Style.BackColor = Colors.BackColorBtnReadOnly;
                    cellTanka.ReadOnly = true;
                    cellBDCount.ReadOnly = false;
                    cellBDCount.Style.BackColor = this.GetBackColor(cellTanka);
                    break;
            }
        }
        /// <summary>
        /// セル背景色取得
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private Color GetBackColor(DataGridViewCell c)
        {
            if (c.Style.BackColor == Colors.BackColorCellError)
            {
                return Colors.BackColorCellError;
            }
            if (c.Style.BackColor == Colors.BackColorCellEdited)
            {
                return Colors.BackColorCellEdited;
            }
            return Color.Empty;
        }
        /// <summary>
        /// 一括選択ボタン押下イベント
        /// </summary>
        /// <param name="r"></param>
        private void BtnAddClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // GridCellValueChangeイベントの管理
            this.ManageGridCellEvent();

            int tabIndex = this.Body.tabMeisai.SelectedIndex;
            int gridRowEmptyCount = 0;
            var dgv = this.GetTargetGrid(this.Body.tabMeisai.SelectedIndex);
            var selectedDetailKbn = this.GetTargetDetailKbn(tabIndex);
            try
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value.ToString();
                    if (status == Enums.EditStatus.None.ToString())
                    {
                        gridRowEmptyCount++;
                    }
                }
                // 商品選択ダイアログ表示
                var dtShohin = new MST0010_MstShohin(
                      this.LoginInfo
                    , tabIndex == TabIndexPrc? Enums.ScreenModeMstShohin.DialogMultiProcessing : Enums.ScreenModeMstShohin.DialogMulti
                    , selectedDetailKbn
                    , gridRowEmptyCount
                );
                EstimaUtil.AddLogInfo(this.Body.Text, dtShohin.Text, Enums.LogStatusKbn.Start);
                var dr = dtShohin.ShowDialog();
                EstimaUtil.AddLogInfo(this.Body.Text, dtShohin.Text, Enums.LogStatusKbn.End);
                if (dr != DialogResult.OK)
                {
                    return;
                }
                // 選択した商品マスタ情報を1つずつループ
                bool success = true;
                CommonUtil.ShowProgressDialog(() =>
                {
                    foreach (var selectedInfo in dtShohin.SelectedShohinInfos)
                    {
                        var shohinInfo = this.PersonalService.SelShohinData(selectedInfo.ShohinID);
                        foreach (DataGridViewRow r in dgv.Rows)
                        {
                            string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value.ToString();
                            if (status != Enums.EditStatus.None.ToString())
                            {
                                continue;
                            }
                            // 行データセット
                            this.SetRowValue(r, shohinInfo, tabIndex);
                            // 加工費存在チェック
                            int shohinID = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{this.SelectedCollAdd}"].Value);
                            if (!this.ValidateExistProcessingChildCds(shohinID))
                            {
                                r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellError;
                                success = false;
                            }
                            break;
                        }
                    }
                });
                if (!success)
                {
                    CommonUtil.ShowInfoExcramationOK(DE0040_Messages.MsgExNotExistProcessingInfo);
                }
                // 小計計算
                this.CalcSummary(this.Body.tabMeisai.SelectedIndex);

                // 編集フラグ
                this.EditedFlg = true;
                this.SwitchEnabled();
            }
            catch
            {
                throw;
            }
            finally
            {
                // GridCellValueChangeイベントの管理
                this.ManageGridCellEvent(false);
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// 加工費再計算ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCalcProcessingClickEvent(
              object s
            , EventArgs e
            )
        {
            // グリッドデータ存在チェック
            if (!this.ValidateExistGridData())
            {
                return;
            }
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // GridCellValueChangeイベントの管理
            this.ManageGridCellEvent();
            try
            {
                // 各タブで加工費子商品コードが存在するか？
                bool existProcessingChildCD = this.ValidateEachTabExsistProcessingCDs();
                // タブ③へフォーカス
                this.Body.tabMeisai.SelectedIndex = TabIndexPrc;
                // 掛率セット
                this.ConsumptionRate = (float)this.Body.numRateConsumption.Value;
                this.InspectionRate = (float)this.Body.numRateInspection.Value;
                this.ExpenceRate = (float)this.Body.numRateExpence.Value;
                // 商品情報作成
                var shohinInfos = this.GetShohinInfosFromOtherGrid();
                // ③以外のタブで作成した商品から、加工費用のデータを作成
                var dataPrcList = new List<DE0040_GridDto_Prc>();
                CommonUtil.ShowProgressDialog(() => { dataPrcList = this.CreateProcessingDatas(shohinInfos); });
                // グリッド③クリア
                this.Body.dgvProcessing.Rows.Clear();
                var dataSource = new BindingList<DE0040_GridDto_Prc>();
                for (int i = 0; i < MaxRowCount; i++) { dataSource.Add(new DE0040_GridDto_Prc()); }
                this.Body.dgvProcessing.DataSource = dataSource;
                // 加工費明細の行セット
                this.SetProcessingRowData(dataPrcList);
                // 小計計算
                this.CalcSummary(this.Body.tabMeisai.SelectedIndex);
                // 編集フラグ
                this.EditedFlg = true;
                this.SwitchEnabled();
                if (!existProcessingChildCD)
                {
                    CommonUtil.ShowInfoExcramationOK(DE0040_Messages.MsgExNotExistProcessingInfo);
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                this.ManageGridCellEvent(false);
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnUpdClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                // 更新時Validate
                bool result = true;
                CommonUtil.ShowProgressDialog(() =>
                {
                    result = this.ValidateUpdate();
                });
                if (!result)
                {
                    CommonUtil.ShowErrorMsg(this.UpdErrMessage);
                    return;
                }
                if (CommonUtil.ShowInfoMsgOKCancel(DE0040_Messages.MsgAskUpdate) != DialogResult.OK)
                {
                    return;
                }
                // 更新処理
                result = true;
                CommonUtil.ShowProgressDialog(() =>
                {
                    try
                    {
                        EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.Start);
                        this.UpdLogic();
                        EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.End);
                    }
                    catch (Exception ex)
                    {
                        EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                        result = false;
                    }
                });
                if (!result)
                {
                    return;
                }
                // 完了メッセージ
                CommonUtil.ShowInfoMsgOK(DE0040_Messages.MsgInfoFinishUpdate);

                // ワークテーブルからの該当データ取得処理
                this.SearchFromWorkTable();

                // ダイアログリザルト
                if (result)
                {
                    this.CurrDialogResult = DialogResult.OK;
                }
                this.Body.Close();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                if (!this.ValidateUnUpdateDataExist())
                {
                    var drConfirm = CommonUtil.ShowInfoExcramationOKCancel(DE0040_Messages.MsgAskNotCommitedDataExist);
                    if (drConfirm != DialogResult.OK)
                    {
                        cancel = true;
                        return;
                    }
                }
                this.Body.DialogResult = this.CurrDialogResult;
                this.Body.Close();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                if (!this.ValidateUnUpdateDataExist())
                {
                    var drConfirm = CommonUtil.ShowInfoExcramationOKCancel(DE0040_Messages.MsgAskNotCommitedDataExist);
                    if (drConfirm != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                this.Body.DialogResult = this.CurrDialogResult;
                this.Body.Close();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        #endregion

        #region TabFunc
        /// <summary>
        /// 小計計算
        /// </summary>
        /// <param name="tabIndex"></param>
        private void CalcSummary(int tabIndex)
        {
            int sumSuryo = 0;
            int sumKingaku = 0;
            var dgv = this.GetTargetGrid(tabIndex);
            foreach (DataGridViewRow r in dgv.Rows)
            {
                string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value.ToString();
                if (status == Enums.EditStatus.None.ToString())
                {
                    continue;
                }
                sumSuryo += CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Value);
                string cellName = $"{nameof(CellHeaderNameDto.Kingaku)}{this.SelectedCollAdd}";
                sumKingaku += CommonUtil.ToInteger(CommonUtil.TostringNullForbid(r.Cells[cellName].Value));
            }
            this.Body.txtSumSuryo.Text = sumSuryo.ToString("#,0");
            this.Body.txtSumKingaku.Text = sumKingaku.ToString("#,0");
        }
        /// <summary>
        /// 対象のグリッド取得
        /// </summary>
        /// <returns></returns>
        private DataGridView GetTargetGrid(int tabIndex)
        {
            switch (tabIndex)
            {
                case TabIndexPlb:
                    return this.Body.dgvPlumbing;
                case TabIndexHsg:
                    return this.Body.dgvHousing;
                case TabIndexPrc:
                    return this.Body.dgvProcessing;
            }
            return new DataGridView();
        }
        /// <summary>
        /// 対象の明細内訳区分取得
        /// </summary>
        /// <returns></returns>
        private Enums.DetailKbn GetTargetDetailKbn(int tabIndex)
        {
            switch (tabIndex)
            {
                case TabIndexPlb:
                    return Enums.DetailKbn.TabPlumbing;
                case TabIndexHsg:
                    return Enums.DetailKbn.TabHousing;
                case TabIndexPrc:
                    return Enums.DetailKbn.TabProcessing;
            }
            return Enums.DetailKbn.All;
        }
        #endregion

        #region GridRowFunc
        /// <summary>
        /// 行データの金額計算
        /// </summary>
        /// <param name="r"></param>
        /// <param name="selectedTabIndex"></param>
        private void CalcRowKingaku(
              DataGridViewRow r
            , int selectedTabIndex
            )
        {
            int suryo = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Value);
            int bdCount = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Value);
            string cellName = $"{nameof(CellHeaderNameDto.Tanka)}{this.SelectedCollAdd}";
            int tanka = CommonUtil.ToInteger(CommonUtil.TostringNullForbid(r.Cells[cellName].Value));
            switch (selectedTabIndex)
            {
                case TabIndexPrc:
                    r.Cells[$"{nameof(CellHeaderNameDto.Kingaku)}{this.SelectedCollAdd}"].Value = (suryo * bdCount * tanka).ToString("#,0");
                    break;
                default:
                    r.Cells[$"{nameof(CellHeaderNameDto.Kingaku)}{this.SelectedCollAdd}"].Value = (suryo * tanka).ToString("#,0");
                    break;
            }
        }
        /// <summary>
        /// 行ステータス更新
        /// </summary>
        /// <param name="r"></param>
        private void UpdateRowStatus(DataGridViewRow r)
        {
            string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value.ToString();
            if ((status == Enums.EditStatus.Show.ToString() || status == Enums.EditStatus.Update.ToString()))
            {
                r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value = Enums.EditStatus.Update;
                return;
            }
            r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value = Enums.EditStatus.Insert;
        }
        /// <summary>
        /// 行データセット
        /// </summary>
        /// <param name="r"></param>
        /// <param name="shohinInfo"></param>
        /// <param name="tabIndex"></param>
        private void SetRowValue(
              DataGridViewRow r
            , DE0040_ShohinInfoDto shohinInfo
            , int tabIndex
            )
        {
            int seq = this.GetTargetGrid(tabIndex).Rows.IndexOf(r);
            r.Cells[$"{nameof(CellHeaderNameDto.Seq)}{this.SelectedCollAdd}"].Value = seq;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{this.SelectedCollAdd}"].Value = shohinInfo.ShohinID;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Value = shohinInfo.ShohinCD;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].Value = shohinInfo.ShohinName;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].Value = shohinInfo.ShohinSizeName;
            r.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].Value = shohinInfo.ParentCDName;
            r.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{this.SelectedCollAdd}"].Value = shohinInfo.Tanka;
            r.Cells[$"{nameof(CellHeaderNameDto.Unit)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Value = "0";
            r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Value = shohinInfo.BDCount;
            r.Cells[$"{nameof(CellHeaderNameDto.Kingaku)}{this.SelectedCollAdd}"].Value = "0";
            r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value = Enums.EditStatus.Insert;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[$"{nameof(CellHeaderNameDto.Unit)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
            r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
            if (tabIndex == TabIndexPrc)
            {
                r.Cells[$"{nameof(CellHeaderNameDto.DetailKbn)}{this.SelectedCollAdd}"].Value = shohinInfo.DetailKbn;
                r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Value = 1;
            }
        }
        /// <summary>
        /// 行データ追加処理
        /// </summary>
        /// <param name="tgtRow"></param>
        /// <param name="tabIndex"></param>
        private bool AddRowLogic(
              DataGridViewRow tgtRow
            , int tabIndex
            )
        {
            // タブインデックスからグリッドを取得
            var dgv = this.GetTargetGrid(tabIndex);
            var selectedDetailKbn = this.GetTargetDetailKbn(tabIndex);
            int gridRowEmptyCount = 0;
            foreach (DataGridViewRow r in dgv.Rows)
            {
                if (r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value.ToString() == Enums.EditStatus.None.ToString())
                {
                    gridRowEmptyCount++;
                }
            }
            // 商品選択ダイアログ
            string searchShohinCD = CommonUtil.TostringNullForbid(tgtRow.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Value);
            var dtShohin = new MST0010_MstShohin(
                  this.LoginInfo
                , tabIndex == TabIndexPrc ? Enums.ScreenModeMstShohin.DialogSingleProcessing : Enums.ScreenModeMstShohin.DialogSingle
                , selectedDetailKbn
                , gridRowEmptyCount
                , searchShohinCD
            );
            EstimaUtil.AddLogInfo(this.Body.Text, dtShohin.Text, Enums.LogStatusKbn.Start);
            var dr = dtShohin.ShowDialog();
            EstimaUtil.AddLogInfo(this.Body.Text, dtShohin.Text, Enums.LogStatusKbn.End);
            if (dr != DialogResult.OK)
            {
                return false;
            }
            CommonUtil.ShowProgressDialog(() =>
            {
                // 商品情報取得
                var shohinInfo = this.PersonalService.SelShohinData(dtShohin.SelectedShohinInfos[0].ShohinID);
                string status = tgtRow.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value.ToString();
                // 行データセット
                this.SetRowValue(tgtRow, shohinInfo, tabIndex);
            });
            if (tabIndex == TabIndexPrc)
            {
                // 自動追加行データ編集
                this.EditAutoAdditionalRow();
            }
            return true;
        }
        /// <summary>
        /// 行クリア処理
        /// </summary>
        /// <param name="r"></param>
        /// <param name="tabIndex"></param>
        private void ClearRowLogic(
              DataGridViewRow r
            , int tabIndex
            )
        {
            string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value.ToString();
            if (status == Enums.EditStatus.None.ToString())
            {
                return;
            }
            if (CommonUtil.ShowInfoMsgOKCancel(DE0040_Messages.MsgAskClear) != DialogResult.OK)
            {
                return;
            }
            // セットする値を取得
            int seq = this.GetTargetGrid(tabIndex).Rows.IndexOf(r);
            CommonUtil.ShowProgressDialog(() =>
            {
                // クリアする行の編集
                this.ClearRowEdit(r, seq);
            });
            switch (tabIndex)
            {
                case TabIndexPlb:
                case TabIndexHsg:
                    // 商品情報作成
                    var shohinInfos = this.GetShohinInfosFromOtherGrid();
                    // 加工費明細の行セット（Proxy）
                    this.SetProcessingRowDataProxy(shohinInfos, tabIndex);
                    break;
                case TabIndexPrc:
                    int detailKbn = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.DetailKbn)}{this.SelectedCollAdd}"].Value);
                    switch (detailKbn)
                    {
                        case (int)Enums.DetailKbn.Consumable:
                        case (int)Enums.DetailKbn.Inspection:
                        case (int)Enums.DetailKbn.Expense:
                            // 小計計算
                            this.CalcSummary(this.Body.tabMeisai.SelectedIndex);
                            // 編集フラグ
                            this.EditedFlg = true;
                            // 使用可否切り替え（更新ボタン）
                            this.SwitchEnabled();
                            break;
                        default:
                            // 自動追加行データ編集
                            this.EditAutoAdditionalRow();
                            break;
                    }
                    break;
            }
        }
        /// <summary>
        /// クリアする行の編集
        /// </summary>
        /// <param name="r"></param>
        /// <param name="seq"></param>
        private void ClearRowEdit(
              DataGridViewRow r
            , int seq
            )
        {
            // セル値
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{this.SelectedCollAdd}"].Value = 0;
            r.Cells[$"{nameof(CellHeaderNameDto.Seq)}{this.SelectedCollAdd}"].Value = seq;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.Unit)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Value = string.Empty;
            r.Cells[$"{nameof(CellHeaderNameDto.Kingaku)}{this.SelectedCollAdd}"].Value = string.Empty;

            string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value.ToString();
            switch (status)
            {
                case "Insert":
                    // ステータス
                    r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value = Enums.EditStatus.None;

                    // 背景色
                    r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Style.BackColor = Color.Empty;
                    r.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].Style.BackColor = Color.Empty;
                    r.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].Style.BackColor = Color.Empty;
                    r.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].Style.BackColor = Color.Empty;
                    r.Cells[$"{nameof(CellHeaderNameDto.Unit)}{this.SelectedCollAdd}"].Style.BackColor = Color.Empty;
                    r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Style.BackColor = Color.Empty;
                    r.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{this.SelectedCollAdd}"].Style.BackColor = Color.Empty;
                    r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Style.BackColor = Color.Empty;
                    break;
                case "Show":
                case "Update":
                    // ステータス
                    r.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value = Enums.EditStatus.Update;

                    // 背景色
                    r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    r.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    r.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    r.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    r.Cells[$"{nameof(CellHeaderNameDto.Unit)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    r.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    r.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    break;
            }
        }
        #endregion

        #region ProcessingDetail
        /// <summary>
        /// 他のグリッドから商品情報取得
        /// </summary>
        /// <returns></returns>
        private List<DE0040_ProcessingDetailDto> GetShohinInfosFromOtherGrid()
        {
            var ret = new List<DE0040_ProcessingDetailDto>();
            foreach (DataGridViewRow rowPrc in this.Body.dgvPlumbing.Rows)
            {
                var rowDtoPlb = GridRowUtil<DE0040_GridDto_Plb>.GetRowModel(rowPrc);
                if (rowDtoPlb.Status_Plb == Enums.EditStatus.None)
                {
                    continue;
                }
                ret.Add(new DE0040_ProcessingDetailDto(
                      rowDtoPlb.ShohinID_Plb
                    , CommonUtil.ToInteger(rowDtoPlb.Suryo_Plb)
                    , CommonUtil.ToInteger(rowDtoPlb.BDCount_Plb)
                ));
            }
            foreach (DataGridViewRow rowHsg in this.Body.dgvHousing.Rows)
            {
                var rowDtoHsg = GridRowUtil<DE0040_GridDto_Hsg>.GetRowModel(rowHsg);
                if (rowDtoHsg.Status_Hsg == Enums.EditStatus.None)
                {
                    continue;
                }
                ret.Add(new DE0040_ProcessingDetailDto(
                      rowDtoHsg.ShohinID_Hsg
                    , CommonUtil.ToInteger(rowDtoHsg.Suryo_Hsg)
                    , CommonUtil.ToInteger(rowDtoHsg.BDCount_Hsg)
                ));
            }
            return ret;
        }
        /// <summary>
        /// 加工費明細の行セット（Proxy）
        /// </summary>
        /// <param name="processingDetailDtoList"></param>
        /// <param name="tabIndex"></param>
        private void SetProcessingRowDataProxy(
              List<DE0040_ProcessingDetailDto> processingDetailDtoList
            , int tabIndex
            )
        {
            // タブ③へフォーカス
            this.Body.tabMeisai.SelectedIndex = TabIndexPrc;

            // ③以外のタブで作成した商品から、加工費用のデータを作成
            var dataPrcList = new List<DE0040_GridDto_Prc>();
            CommonUtil.ShowProgressDialog(() => { dataPrcList = this.CreateProcessingDatas(processingDetailDtoList); });
            // グリッド③クリア
            this.Body.dgvProcessing.Rows.Clear();
            var dataSourcePrc = new BindingList<DE0040_GridDto_Prc>();
            for (int i = 0; i < MaxRowCount; i++) { dataSourcePrc.Add(new DE0040_GridDto_Prc()); }
            this.Body.dgvProcessing.DataSource = dataSourcePrc;

            // 加工費明細の行セット
            this.SetProcessingRowData(dataPrcList);

            // 元のタブへフォーカス
            this.Body.tabMeisai.SelectedIndex = tabIndex;
            switch (this.Body.tabMeisai.SelectedIndex)
            {
                case TabIndexPlb:
                    this.Body.dgvPlumbing.Focus();
                    break;
                case TabIndexHsg:
                    this.Body.dgvHousing.Focus();
                    break;
                case TabIndexPrc:
                    this.Body.dgvProcessing.Focus();
                    break;
            }
            // 小計計算
            this.CalcSummary(this.Body.tabMeisai.SelectedIndex);

            // 編集フラグ
            this.EditedFlg = true;
            this.SwitchEnabled();
        }
        /// <summary>
        /// 加工費明細の行データ作成
        /// </summary>
        /// <param name="processingDetailDtoList"></param>
        /// <returns></returns>
        private List<DE0040_GridDto_Prc> CreateProcessingDatas(List<DE0040_ProcessingDetailDto> processingDetailDtoList)
        {
            var dataPrcList = new List<DE0040_GridDto_Prc>();
            foreach (var processingDetailDto in processingDetailDtoList)
            {
                // 材料費の商品情報を取得
                var shohinInfoMaterial = this.PersonalService.SelShohinData(processingDetailDto.ShihinInfo.ShohinID);
                if (shohinInfoMaterial == null || string.IsNullOrEmpty(shohinInfoMaterial.ShohinCD))
                {
                    continue;
                }
                // 配管加工費
                switch (shohinInfoMaterial.DetailKbn)
                {
                    case (int)Enums.DetailKbn.Plumbing:
                    case (int)Enums.DetailKbn.Joint:
                    case (int)Enums.DetailKbn.Housing:
                        dataPrcList.AddRange(this.CreateRowDatasProcessing(
                              shohinInfoMaterial
                            , processingDetailDto
                        ));
                        break;
                }
            }
            // グリッド③に登録済みのデータを取得
            var dataListPrcAdded = GridRowUtil<DE0040_GridDto_Prc>.GetAllRowsModel(this.Body.dgvProcessing.DataSource);
            dataListPrcAdded = dataListPrcAdded.Where(n => n.Status_Prc != Enums.EditStatus.None && !n.AddFromOtherGrid_Prc).ToList();
            dataPrcList.AddRange(dataListPrcAdded);
            // 数量・金額を計算
            var ret = this.SummaryProcessingGridDtoList(dataPrcList);
            // 消耗品雑材
            ret.Add(this.CreateRowDatasConsumable(ret));
            // 検査費
            ret.Add(this.CreateRowDatasInspection(ret));
            // 諸経費
            ret.Add(this.CreateRowDatasExpence(ret));
            // ソート
            ret = ret.OrderBy(n => CommonUtil.ToInteger(n.ShohinCD_Prc.Substring(0, 3)))
                     .ThenBy(n => CommonUtil.TostringNullForbid((n.ShohinCD_Prc + "-ZZZ-ZZZ").Split('-')[1]))
                     .ThenBy(n => CommonUtil.CutAInteger((n.ShohinCD_Prc + "-ZZZ-ZZZ").Split('-')[2]))
                     .ToList();
            return ret;
        }
        /// <summary>
        /// 配管加工費の行データ作成
        /// </summary>
        /// <param name="shohinInfoMaterial"></param>
        /// <param name="processingDetailDto"></param>
        /// <returns></returns>
        private List<DE0040_GridDto_Prc> CreateRowDatasProcessing(
              DE0040_ShohinInfoDto shohinInfoMaterial
            , DE0040_ProcessingDetailDto processingDetailDto
            )
        {
            var ret = new List<DE0040_GridDto_Prc>();

            // 材料費のサイズをリスト化
            var sizeList = CommonUtil.CutSpace(shohinInfoMaterial.ShohinSizeName).Replace("x", "X").Split('X').ToList();
            // 配管加工費情報一覧取得
            var shohinInfosProcessing = this.PersonalService.GetShohniInfosProcessing(
                  shohinInfoMaterial.ProcessingChildCD1
                , shohinInfoMaterial.ProcessingChildCD2
                , shohinInfoMaterial.ProcessingChildCD3
                , shohinInfoMaterial.ProcessingChildCD4
                , shohinInfoMaterial.ProcessingChildCD5
                , shohinInfoMaterial.ProcessingSeq1
                , shohinInfoMaterial.ProcessingSeq2
                , shohinInfoMaterial.ProcessingSeq3
                , shohinInfoMaterial.ProcessingSeq4
                , shohinInfoMaterial.ProcessingSeq5
                , sizeList
                , this.TaishoYM
            );
            // リスト化したサイズを１つずつループ
            foreach (string materialSize in sizeList)
            {
                // 加工費の商品情報一覧を1行ずつループ
                foreach (var shohinInfoProcessing in shohinInfosProcessing)
                {
                    int kingaku = CommonUtil.ToInteger(shohinInfoProcessing.Tanka);
                    kingaku *= processingDetailDto.Suryo;
                    kingaku *= CommonUtil.ToInteger(shohinInfoMaterial.BDCount);
                    var dtoPrc = new DE0040_GridDto_Prc()
                    {
                        ShohinID_Prc = shohinInfoProcessing.ShohinID,
                        ShohinCD_Prc = shohinInfoProcessing.ShohinCD,
                        ShohinName_Prc = shohinInfoProcessing.ShohinName,
                        ShohinSizeName_Prc = materialSize,
                        ParentCDName_Prc = shohinInfoProcessing.ParentCDName,
                        Suryo_Prc = processingDetailDto.Suryo.ToString("#,0"),
                        Unit_Prc = string.Empty,
                        Tanka_Prc = shohinInfoProcessing.Tanka,
                        BDCount_Prc = processingDetailDto.ShihinInfo.BDCount.Value.ToString("#,0"),
                        Kingaku_Prc = kingaku.ToString("#,0"),
                        DetailKbn_Prc = shohinInfoProcessing.DetailKbn,
                        AddFromOtherGrid_Prc = true,
                        Status_Prc = Enums.EditStatus.Insert,
                    };
                    ret.Add(dtoPrc);
                }
            }
            return ret;
        }
        /// <summary>
        /// 消耗品雑材の行データ作成
        /// </summary>
        /// <param name="dataSourceProcessing"></param>
        /// <returns></returns>
        private DE0040_GridDto_Prc CreateRowDatasConsumable(List<DE0040_GridDto_Prc> dataSourceProcessing)
        {
            var consumableShohinInfo = this.PersonalService.GetShohniInfoConsumable();
            int kingaku = this.CalcConsumptionKingaku(dataSourceProcessing);
            var ret = new DE0040_GridDto_Prc()
            {
                ShohinID_Prc = consumableShohinInfo.ShohinID,
                ShohinCD_Prc = consumableShohinInfo.ShohinCD,
                ShohinName_Prc = consumableShohinInfo.ShohinName,
                ShohinSizeName_Prc = consumableShohinInfo.ShohinSizeName,
                ParentCDName_Prc = consumableShohinInfo.ParentCDName,
                Suryo_Prc = "1",
                Unit_Prc = string.Empty,
                Tanka_Prc = kingaku.ToString("#,0"),
                BDCount_Prc = "1",
                Kingaku_Prc = kingaku.ToString("#,0"),
                DetailKbn_Prc = (int)Enums.DetailKbn.Consumable,
                AddFromOtherGrid_Prc = true,
                Status_Prc = Enums.EditStatus.Insert,
            };
            return ret;
        }
        /// <summary>
        /// 検査費の行データ作成
        /// </summary>
        /// <param name="dataSourceProcessing"></param>
        /// <returns></returns>
        private DE0040_GridDto_Prc CreateRowDatasInspection(List<DE0040_GridDto_Prc> dataSourceProcessing)
        {
            var inspectionShohinInfo = this.PersonalService.GetShohniInfoInspection();
            int kingaku = this.CalcInspectionKingaku(dataSourceProcessing);
            var ret = new DE0040_GridDto_Prc()
            {
                ShohinID_Prc = inspectionShohinInfo.ShohinID,
                ShohinCD_Prc = inspectionShohinInfo.ShohinCD,
                ShohinName_Prc = inspectionShohinInfo.ShohinName,
                ShohinSizeName_Prc = inspectionShohinInfo.ShohinSizeName,
                ParentCDName_Prc = inspectionShohinInfo.ParentCDName,
                Suryo_Prc = "1",
                Unit_Prc = string.Empty,
                Tanka_Prc = kingaku.ToString("#,0"),
                BDCount_Prc = "1",
                Kingaku_Prc = kingaku.ToString("#,0"),
                DetailKbn_Prc = (int)Enums.DetailKbn.Inspection,
                AddFromOtherGrid_Prc = true,
                Status_Prc = Enums.EditStatus.Insert,
            };
            return ret;
        }
        /// <summary>
        /// 諸経費の行データ作成
        /// </summary>
        /// <param name="dataSourceProcessing"></param>
        /// <returns></returns>
        private DE0040_GridDto_Prc CreateRowDatasExpence(List<DE0040_GridDto_Prc> dataSourceProcessing)
        {
            var expenceShohinInfo = this.PersonalService.GetShohniInfoExpence();
            int expenceKingaku = this.CalcExpenceKingaku(dataSourceProcessing);

            var ret = new DE0040_GridDto_Prc()
            {
                ShohinID_Prc = expenceShohinInfo.ShohinID,
                ShohinCD_Prc = expenceShohinInfo.ShohinCD,
                ShohinName_Prc = expenceShohinInfo.ShohinName,
                ShohinSizeName_Prc = expenceShohinInfo.ShohinSizeName,
                ParentCDName_Prc = expenceShohinInfo.ParentCDName,
                Suryo_Prc = "1",
                Unit_Prc = string.Empty,
                Tanka_Prc = expenceKingaku.ToString("#,0"),
                BDCount_Prc = "1",
                Kingaku_Prc = expenceKingaku.ToString("#,0"),
                DetailKbn_Prc = (int)Enums.DetailKbn.Expense,
                AddFromOtherGrid_Prc = true,
                Status_Prc = Enums.EditStatus.Insert,
            };
            return ret;
        }
        /// <summary>
        /// 加工費明細の数量・金額を集計
        /// </summary>
        /// <param name="dataPrcList"></param>
        /// <returns></returns>
        private List<DE0040_GridDto_Prc> SummaryProcessingGridDtoList(List<DE0040_GridDto_Prc> dataPrcList)
        {
            var newList = new List<DE0040_GridDto_Prc>();
            foreach (var dtpPrc in dataPrcList)
            {
                var tgtData = newList.FirstOrDefault(n => n.ShohinCD_Prc == dtpPrc.ShohinCD_Prc
                && n.ShohinSizeName_Prc == dtpPrc.ShohinSizeName_Prc
                && n.BDCount_Prc == dtpPrc.BDCount_Prc);
                if (tgtData != null)
                {
                    int suryo = CommonUtil.ToInteger(dtpPrc.Suryo_Prc);
                    int tanka = CommonUtil.ToInteger(dtpPrc.Tanka_Prc);
                    int bdCount = CommonUtil.ToInteger(dtpPrc.BDCount_Prc);
                    int sumSuryo = CommonUtil.ToInteger(tgtData.Suryo_Prc);
                    int sumKingaku = CommonUtil.ToInteger(tgtData.Kingaku_Prc);
                    sumSuryo += suryo;
                    sumKingaku += (tanka * suryo * bdCount);
                    tgtData.Suryo_Prc = sumSuryo.ToString("#,0");
                    tgtData.BDCount_Prc = bdCount.ToString("#,0");
                    tgtData.Kingaku_Prc = sumKingaku.ToString("#,0");
                }
                else
                {
                    int suryo = CommonUtil.ToInteger(dtpPrc.Suryo_Prc);
                    int tanka = CommonUtil.ToInteger(dtpPrc.Tanka_Prc);
                    int bdCount = CommonUtil.ToInteger(dtpPrc.BDCount_Prc);
                    int sumSuryo = suryo;
                    int sumKingaku = (tanka * suryo * bdCount);
                    dtpPrc.Suryo_Prc = sumSuryo.ToString("#,0");
                    dtpPrc.BDCount_Prc = bdCount.ToString("#,0");
                    dtpPrc.Kingaku_Prc = sumKingaku.ToString("#,0");
                    newList.Add(dtpPrc);
                }
            }
            return newList.Where(n => n.Suryo_Prc != "0")
                          .OrderBy(n => CommonUtil.ToInteger(n.ShohinCD_Prc.Substring(0, 3)))
                          .ThenBy(n => CommonUtil.TostringNullForbid((n.ShohinCD_Prc + "-ZZZ-ZZZ").Split('-')[1]))
                          .ThenBy(n => CommonUtil.CutAInteger((n.ShohinCD_Prc + "-ZZZ-ZZZ").Split('-')[2]))
                          .ToList();
        }
        /// <summary>
        /// 加工費明細の行セット
        /// </summary>
        /// <param name="dataPrcList"></param>
        private void SetProcessingRowData(List<DE0040_GridDto_Prc> dataPrcList)
        {
            // グリッド③に登録するデータを1件ずつループ
            foreach (var dataPrc in dataPrcList)
            {
                foreach (DataGridViewRow rowPrc in this.Body.dgvProcessing.Rows)
                {
                    // グリッド③の行を先頭から1件ずつループ
                    string status = rowPrc.Cells[nameof(CellHeaderNameDtoPrc.Status_Prc)].Value.ToString();
                    if (status != Enums.EditStatus.None.ToString())
                    {
                        // 既に行データが存在する場合はスルー
                        continue;
                    }
                    // 値セット
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Seq)}{this.SelectedCollAdd}"].Value = 0;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{this.SelectedCollAdd}"].Value = dataPrc.ShohinID_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Value = dataPrc.ShohinCD_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].Value = dataPrc.ShohinName_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].Value = dataPrc.ShohinSizeName_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].Value = dataPrc.ParentCDName_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{this.SelectedCollAdd}"].Value = dataPrc.Tanka_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Unit)}{this.SelectedCollAdd}"].Value = string.Empty;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Value = dataPrc.Suryo_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Value = dataPrc.BDCount_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Kingaku)}{this.SelectedCollAdd}"].Value = dataPrc.Kingaku_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDtoPrc.DetailKbn_Prc)}"].Value = dataPrc.DetailKbn_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDtoPrc.AddFromOtherGrid_Prc)}"].Value = dataPrc.AddFromOtherGrid_Prc;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Status)}{this.SelectedCollAdd}"].Value = Enums.EditStatus.Insert;

                    // 背景色セット
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Unit)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                    rowPrc.Cells[$"{nameof(CellHeaderNameDto.Tanka)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;

                    var shohinCD = CommonUtil.TostringNullForbid(rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{this.SelectedCollAdd}"].Value);
                    var shohinCDSplit = shohinCD.Split('-');
                    if (shohinCDSplit.Length == 2)
                    {
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorCellEdited;

                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].ReadOnly = false;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].ReadOnly = false;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].ReadOnly = false;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].ReadOnly = false;
                    }
                    else
                    {
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].Style.BackColor = Colors.BackColorBtnReadOnly;

                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ParentCDName)}{this.SelectedCollAdd}"].ReadOnly = true;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinName)}{this.SelectedCollAdd}"].ReadOnly = true;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.ShohinSizeName)}{this.SelectedCollAdd}"].ReadOnly = true;
                        rowPrc.Cells[$"{nameof(CellHeaderNameDto.BDCount)}{this.SelectedCollAdd}"].ReadOnly = true;
                    }
                    // 値と背景色をセットしたら、グリッド③の行ループを抜ける
                    break;
                }
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// 自動で行追加される行の金額計算
        /// </summary>
        /// <param name="dataSourceProcessing"></param>
        /// <returns></returns>
        private int[] CalcAutoAdditionalRowKingaku(List<DE0040_GridDto_Prc> dataSourceProcessing)
        {
            int sumKingakuPlumbing = GridRowUtil<DE0040_GridDto_Plb>.GetAllRowsModel(this.Body.dgvPlumbing.DataSource).Sum(n => CommonUtil.ToInteger(n.Kingaku_Plb));
            int sumKingakuHousing = GridRowUtil<DE0040_GridDto_Hsg>.GetAllRowsModel(this.Body.dgvHousing.DataSource).Sum(n => CommonUtil.ToInteger(n.Kingaku_Hsg));
            int sumKingakuProcessing = dataSourceProcessing.Where(n => n.DetailKbn_Prc == (int)Enums.DetailKbn.ProcessingCost).Sum(n => CommonUtil.ToInteger(n.Kingaku_Prc));
            int sumKingaku = sumKingakuPlumbing + sumKingakuHousing + sumKingakuProcessing;

            // 消耗品雑材金額
            int consumptioneKingaku = (int)Math.Round(sumKingaku * this.ConsumptionRate, 0);
            sumKingaku += consumptioneKingaku;
            // 検査費金額
            int inspectionKingaku = (int)Math.Round(sumKingakuProcessing * this.InspectionRate, 0);
            sumKingaku += inspectionKingaku;
            // 経費金額
            int expenceKingaku = (int)Math.Round(sumKingaku * this.ExpenceRate, 0);
            expenceKingaku = expenceKingaku + 10000 - CommonUtil.ToInteger(CommonUtil.SubstringRight("0000" + (sumKingaku + expenceKingaku).ToString(), 4));
            return new int[] { consumptioneKingaku, inspectionKingaku, expenceKingaku };
        }
        /// <summary>
        /// 消耗品雑材金額計算
        /// </summary>
        /// <param name="dataSourceProcessing"></param>
        /// <returns></returns>
        private int CalcConsumptionKingaku(List<DE0040_GridDto_Prc> dataSourceProcessing)
        {
            int sumKingakuPlumbing = GridRowUtil<DE0040_GridDto_Plb>.GetAllRowsModel(this.Body.dgvPlumbing.DataSource).Sum(n => CommonUtil.ToInteger(n.Kingaku_Plb));
            int sumKingakuProcessing = dataSourceProcessing.Where(n => n.DetailKbn_Prc == (int)Enums.DetailKbn.ProcessingCost).Sum(n => CommonUtil.ToInteger(n.Kingaku_Prc));
            int sumKingaku = sumKingakuPlumbing + sumKingakuProcessing;
            int consumptioneKingaku = (int)Math.Round(sumKingaku * this.ConsumptionRate, 0);
            return consumptioneKingaku;
        }
        /// <summary>
        /// 検査費金額計算
        /// </summary>
        /// <param name="dataSourceProcessing"></param>
        /// <returns></returns>
        private int CalcInspectionKingaku(List<DE0040_GridDto_Prc> dataSourceProcessing)
        {
            int sumKingakuProcessing = dataSourceProcessing.Where(n => n.DetailKbn_Prc == (int)Enums.DetailKbn.ProcessingCost).Sum(n => CommonUtil.ToInteger(n.Kingaku_Prc));
            int inspectionKingaku = (int)Math.Round(sumKingakuProcessing * this.InspectionRate, 0);
            return inspectionKingaku;
        }
        /// <summary>
        /// 経費金額計算
        /// </summary>
        /// <param name="dataSourceProcessing"></param>
        /// <returns></returns>
        private int CalcExpenceKingaku(List<DE0040_GridDto_Prc> dataSourceProcessing)
        {
            int sumKingakuPlumbing = GridRowUtil<DE0040_GridDto_Plb>.GetAllRowsModel(this.Body.dgvPlumbing.DataSource).Sum(n => CommonUtil.ToInteger(n.Kingaku_Plb));
            int sumKingakuHousing = GridRowUtil<DE0040_GridDto_Hsg>.GetAllRowsModel(this.Body.dgvHousing.DataSource).Sum(n => CommonUtil.ToInteger(n.Kingaku_Hsg));
            int sumKingakuProcessing = dataSourceProcessing.Where(n => n.DetailKbn_Prc != (int)Enums.DetailKbn.Expense).Sum(n => CommonUtil.ToInteger(n.Kingaku_Prc));
            int sumKingaku = sumKingakuPlumbing + sumKingakuHousing + sumKingakuProcessing;
            int expenceKingaku = (int)Math.Round(sumKingaku * this.ExpenceRate, 0);
            expenceKingaku = expenceKingaku + 10000 - CommonUtil.ToInteger(CommonUtil.SubstringRight("0000" + (sumKingaku + expenceKingaku).ToString(), 4));
            return expenceKingaku;
        }
        /// <summary>
        /// 使用可否切替
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void SwitchEnabled()
        {
            this.Body.btnUpdate.Enabled = this.EditedFlg;
        }
        /// <summary>
        /// GridCellValueChangeイベントの管理
        /// </summary>
        /// <param name="delete"></param>
        private void ManageGridCellEvent(bool delete = true)
        {
            // グリッド使用可否
            this.Body.dgvHousing.ReadOnly = delete;
            this.Body.dgvPlumbing.ReadOnly = delete;
            this.Body.dgvProcessing.ReadOnly = delete;

            if (delete)
            {
                // グリッドセルクリック
                this.Body.dgvHousing.CellClick -= this.GridCellClick;
                this.Body.dgvPlumbing.CellClick -= this.GridCellClick;
                this.Body.dgvProcessing.CellClick -= this.GridCellClick;

                // グリッドセル値変更
                this.Body.dgvPlumbing.CellValueChanged -= this.GridCellValueChanged;
                this.Body.dgvHousing.CellValueChanged -= this.GridCellValueChanged;
                this.Body.dgvProcessing.CellValueChanged -= this.GridCellValueChanged;

                // グリッド行番号描画
                this.Body.dgvHousing.RowPostPaint -= this.GridRowPostPaintEvent;
                this.Body.dgvPlumbing.RowPostPaint -= this.GridRowPostPaintEvent;
                this.Body.dgvProcessing.RowPostPaint -= this.GridRowPostPaintEvent;
                return;
            }
            // グリッドセルクリック
            this.Body.dgvHousing.CellClick += this.GridCellClick;
            this.Body.dgvPlumbing.CellClick += this.GridCellClick;
            this.Body.dgvProcessing.CellClick += this.GridCellClick;

            // グリッドセル値変更
            this.Body.dgvPlumbing.CellValueChanged += this.GridCellValueChanged;
            this.Body.dgvHousing.CellValueChanged += this.GridCellValueChanged;
            this.Body.dgvProcessing.CellValueChanged += this.GridCellValueChanged;

            // グリッド行番号描画
            this.Body.dgvHousing.RowPostPaint += this.GridRowPostPaintEvent;
            this.Body.dgvPlumbing.RowPostPaint += this.GridRowPostPaintEvent;
            this.Body.dgvProcessing.RowPostPaint += this.GridRowPostPaintEvent;
        }
        /// <summary>
        /// 更新処理
        /// </summary>
        private void UpdLogic()
        {
            var datasPlb = GridRowUtil<DE0040_GridDto_Plb>.GetAllRowsModel(this.Body.dgvPlumbing.DataSource).Where(n => n.Status_Plb != Enums.EditStatus.None);
            var datasHsg = GridRowUtil<DE0040_GridDto_Hsg>.GetAllRowsModel(this.Body.dgvHousing.DataSource).Where(n => n.Status_Hsg != Enums.EditStatus.None);
            var datasPrc = GridRowUtil<DE0040_GridDto_Prc>.GetAllRowsModel(this.Body.dgvProcessing.DataSource).Where(n => n.Status_Prc != Enums.EditStatus.None);

            // 登録データ作成
            var addDatas = new List<WX01TempEstimaDetail>();
            var delDatas = new List<WX01TempEstimaDetail>();
            int seq = 1;
            foreach (var plb in datasPlb)
            {
                if (string.IsNullOrEmpty(plb.ShohinCD_Plb))
                {
                    continue;
                }
                var entityPlb = new WX01TempEstimaDetail()
                {
                    TempEstimaID = this.TempEstimaID,
                    MeisaiNo = this.MeisaiNo,
                    MeisaiType = TabIndexPlb + 1,
                    Seq = seq,
                    ShohinID = plb.ShohinID_Plb,
                    ShohinCD = plb.ShohinCD_Plb,
                    ParentCDName = plb.ParentCDName_Plb,
                    ShohinSizeName = plb.ShohinSizeName_Plb,
                    ShohinName = plb.ShohinName_Plb,
                    Tanka = CommonUtil.ToInteger(plb.Tanka_Plb),
                    Suryo = CommonUtil.ToInteger(plb.Suryo_Plb),
                    Unit = CommonUtil.TostringNullForbid(plb.Unit_Plb),
                    BDCount = CommonUtil.ToInteger(plb.BDCount_Plb),
                    ManualFlg = 1,
                };
                addDatas.Add(entityPlb);
                seq++;
            }
            seq = 1;
            foreach (var hsg in datasHsg)
            {
                if (string.IsNullOrEmpty(hsg.ShohinCD_Hsg))
                {
                    continue;
                }
                var entityHsg = new WX01TempEstimaDetail()
                {
                    TempEstimaID = this.TempEstimaID,
                    MeisaiNo = this.MeisaiNo,
                    MeisaiType = TabIndexHsg + 1,
                    Seq = seq,
                    ShohinID = hsg.ShohinID_Hsg,
                    ShohinCD = hsg.ShohinCD_Hsg,
                    ParentCDName = hsg.ParentCDName_Hsg,
                    ShohinSizeName = hsg.ShohinSizeName_Hsg,
                    ShohinName = hsg.ShohinName_Hsg,
                    Tanka = CommonUtil.ToInteger(hsg.Tanka_Hsg),
                    Suryo = CommonUtil.ToInteger(hsg.Suryo_Hsg),
                    Unit = CommonUtil.TostringNullForbid(hsg.Unit_Hsg),
                    BDCount = CommonUtil.ToInteger(hsg.BDCount_Hsg),
                    ManualFlg = 1,
                };
                addDatas.Add(entityHsg);
                seq++;
            }
            seq = 1;
            foreach (var prc in datasPrc)
            {
                if (string.IsNullOrEmpty(prc.ShohinCD_Prc))
                {
                    continue;
                }
                var entityPrc = new WX01TempEstimaDetail()
                {
                    TempEstimaID = this.TempEstimaID,
                    MeisaiNo = this.MeisaiNo,
                    MeisaiType = TabIndexPrc + 1,
                    Seq = seq,
                    ShohinID = prc.ShohinID_Prc,
                    ShohinCD = prc.ShohinCD_Prc,
                    ParentCDName = prc.ParentCDName_Prc,
                    ShohinSizeName = prc.ShohinSizeName_Prc,
                    ShohinName = prc.ShohinName_Prc,
                    Tanka = CommonUtil.ToInteger(prc.Tanka_Prc),
                    Suryo = CommonUtil.ToInteger(prc.Suryo_Prc),
                    Unit = CommonUtil.TostringNullForbid(prc.Unit_Prc),
                    BDCount = CommonUtil.ToInteger(prc.BDCount_Prc),
                    ManualFlg = CommonUtil.ConvertFlgToVal(!prc.AddFromOtherGrid_Prc),
                };
                addDatas.Add(entityPrc);
                seq++;
            }
            // 既存ワークテーブル削除
            this.PersonalService.DelWorkTableEstimaDetail(
                  this.TempEstimaID
                , this.MeisaiNo
            );
            // 既存ワークテーブル削除（経費）
            this.PersonalService.DelWorkTableExpence(
                  this.TempEstimaID
                , this.MeisaiNo
            );
            foreach (var addEntity in addDatas)
            {
                // ワークテーブル登録
                this.PersonalService.AddWorkTable(addEntity);
            }
            // ワークテーブル登録（経費）
            var wtEntity = new WX02TempEstimaRate()
            {
                ConsumptionRate = this.ConsumptionRate,
                InspectionRate = this.InspectionRate,
                ExpenceRate = this.ExpenceRate,
            };
            this.PersonalService.AddWorkTableExpence(
                  this.TempEstimaID
                , this.MeisaiNo
                , wtEntity
            );
        }
        /// <summary>
        /// ワークテーブルからの該当データ取得処理
        /// </summary>
        private void SearchFromWorkTable()
        {
            try
            {
                var wtDatas = this.PersonalService.SelWorkTable(this.TempEstimaID, this.MeisaiNo);
                var dataSourcePlb = new BindingList<DE0040_GridDto_Plb>();
                var dataSourceHsg = new BindingList<DE0040_GridDto_Hsg>();
                var dataSourcePrc = new BindingList<DE0040_GridDto_Prc>();
                bool result = true;
                CommonUtil.ShowProgressDialog(() =>
                {
                    try
                    {
                        // ワークテーブルからグリッドデータソース取得
                        this.AddDataSourceFromWorkTable(wtDatas, ref dataSourcePlb, ref dataSourceHsg, ref dataSourcePrc);
                    }
                    catch (Exception ex)
                    {
                        EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                        result = false;
                    }
                });
                if (!result) { return; }

                // 明細の上限に達するまで空の明細を追加
                int dataCountPlb = MaxRowCount - dataSourcePlb.Count;
                for (int i = dataCountPlb; 0 < i; i--) { dataSourcePlb.Add(new DE0040_GridDto_Plb()); }
                int dataCountHsg = MaxRowCount - dataSourceHsg.Count;
                for (int i = dataCountHsg; 0 < i; i--) { dataSourceHsg.Add(new DE0040_GridDto_Hsg()); }
                int dataCountPrc = MaxRowCount - dataSourcePrc.Count;
                for (int i = dataCountPrc; 0 < i; i--) { dataSourcePrc.Add(new DE0040_GridDto_Prc()); }

                // グリッド表示
                this.Body.dgvPlumbing.DataSource = dataSourcePlb;
                this.Body.dgvHousing.DataSource = dataSourceHsg;
                this.Body.dgvProcessing.DataSource = dataSourcePrc;
                this.Body.dgvPlumbing.Show();
                this.Body.dgvHousing.Show();
                this.Body.dgvProcessing.Show();

                // 使用可否切替
                this.EditedFlg = false;
                this.SwitchEnabled();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// ワークテーブルからグリッドデータソース取得
        /// </summary>
        /// <param name="wtDatas"></param>
        /// <param name="dataSourcePlb"></param>
        /// <param name="dataSourceHsg"></param>
        /// <param name="dataSourcePrc"></param>
        private void AddDataSourceFromWorkTable(
              List<DE0040_ShohinInfoDto> wtDatas
            , ref BindingList<DE0040_GridDto_Plb> dataSourcePlb
            , ref BindingList<DE0040_GridDto_Hsg> dataSourceHsg
            , ref BindingList<DE0040_GridDto_Prc> dataSourcePrc
            )
        {
            foreach (var wtData in wtDatas)
            {
                switch (wtData.MeisaiType)
                {
                    case 1:
                        var plb = new DE0040_GridDto_Plb()
                        {
                            ShohinID_Plb = wtData.ShohinID,
                            ShohinCD_Plb = wtData.ShohinCD,
                            ShohinName_Plb = wtData.ShohinName,
                            ShohinSizeName_Plb = wtData.ShohinSizeName,
                            ParentCDName_Plb = wtData.ParentCDName,
                            Suryo_Plb = wtData.Suryo,
                            Tanka_Plb = wtData.Tanka,
                            Unit_Plb = wtData.Unit,
                            Kingaku_Plb = wtData.Kingaku,
                            BDCount_Plb = wtData.BDCount,
                            Status_Plb = Enums.EditStatus.Show,
                        };
                        dataSourcePlb.Add(plb);
                        break;
                    case 2:
                        var Hsg = new DE0040_GridDto_Hsg()
                        {
                            ShohinID_Hsg = wtData.ShohinID,
                            ShohinCD_Hsg = wtData.ShohinCD,
                            ShohinName_Hsg = wtData.ShohinName,
                            ShohinSizeName_Hsg = wtData.ShohinSizeName,
                            ParentCDName_Hsg = wtData.ParentCDName,
                            Suryo_Hsg = wtData.Suryo,
                            Tanka_Hsg = wtData.Tanka,
                            Unit_Hsg = wtData.Unit,
                            Kingaku_Hsg = wtData.Kingaku,
                            BDCount_Hsg = wtData.BDCount,
                            Status_Hsg = Enums.EditStatus.Show,
                        };
                        dataSourceHsg.Add(Hsg);
                        break;
                    case 3:
                        var Prc = new DE0040_GridDto_Prc()
                        {
                            ShohinID_Prc = wtData.ShohinID,
                            ShohinCD_Prc = wtData.ShohinCD,
                            ShohinName_Prc = wtData.ShohinName,
                            ShohinSizeName_Prc = wtData.ShohinSizeName,
                            ParentCDName_Prc = wtData.ParentCDName,
                            Suryo_Prc = wtData.Suryo,
                            Tanka_Prc = wtData.Tanka,
                            Unit_Prc = wtData.Unit,
                            Kingaku_Prc = wtData.Kingaku,
                            BDCount_Prc = wtData.BDCount,
                            DetailKbn_Prc = wtData.DetailKbn,
                            AddFromOtherGrid_Prc = wtData.ManualFlg == 0,
                            Status_Prc = Enums.EditStatus.Show,
                        };
                        dataSourcePrc.Add(Prc);
                        break;
                }
            }
        }
        #endregion

        #region Validate
        /// <summary>
        /// グリッドデータが存在するか？
        /// </summary>
        /// <param name="showErrMsg"></param>
        /// <returns></returns>
        private bool ValidateExistGridData(bool showErrMsg = true)
        {
            var dataSourcePlb = GridRowUtil<DE0040_GridDto_Plb>.GetAllRowsModel(this.Body.dgvPlumbing.DataSource);
            dataSourcePlb = dataSourcePlb.Where(n => n.Status_Plb != Enums.EditStatus.None).ToList();
            var dataSourceHsg = GridRowUtil<DE0040_GridDto_Hsg>.GetAllRowsModel(this.Body.dgvHousing.DataSource);
            dataSourceHsg = dataSourceHsg.Where(n => n.Status_Hsg != Enums.EditStatus.None).ToList();
            var dataSourcePrc = GridRowUtil<DE0040_GridDto_Prc>.GetAllRowsModel(this.Body.dgvProcessing.DataSource);
            dataSourcePrc = dataSourcePrc.Where(n => n.Status_Prc != Enums.EditStatus.None).ToList();
            if (!dataSourcePlb.Any() && !dataSourceHsg.Any() && !dataSourcePrc.Any())
            {
                CommonUtil.ShowErrorMsg(DE0040_Messages.MsgErrorNotExistDatas);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 各タブで加工費子商品コードが存在するか？
        /// </summary>
        /// <returns></returns>
        private bool ValidateEachTabExsistProcessingCDs()
        {
            bool existProcessingChildCD = true;

            // タブ①へフォーカス
            this.Body.tabMeisai.SelectedIndex = TabIndexPlb;
            foreach (DataGridViewRow r in this.Body.dgvPlumbing.Rows)
            {
                var rowDto = GridRowUtil<DE0040_GridDto_Plb>.GetRowModel(r);
                if (rowDto.Status_Plb == Enums.EditStatus.None)
                {
                    continue;
                }
                if (!this.ValidateExistProcessingChildCds(rowDto.ShohinID_Plb))
                {
                    r.Cells[nameof(CellHeaderNameDtoPlb.ShohinCD_Plb)].Style.BackColor = Colors.BackColorCellError;
                    existProcessingChildCD = false;
                }
            }
            // タブ②へフォーカス
            this.Body.tabMeisai.SelectedIndex = TabIndexHsg;
            foreach (DataGridViewRow r in this.Body.dgvHousing.Rows)
            {
                var rowDto = GridRowUtil<DE0040_GridDto_Hsg>.GetRowModel(r);
                if (rowDto.Status_Hsg == Enums.EditStatus.None)
                {
                    continue;
                }
                if (!this.ValidateExistProcessingChildCds(rowDto.ShohinID_Hsg))
                {
                    r.Cells[nameof(CellHeaderNameDtoHsg.ShohinID_Hsg)].Style.BackColor = Colors.BackColorCellError;
                    existProcessingChildCD = false;
                }
            }
            return existProcessingChildCD;
        }
        /// <summary>
        /// 更新されていない項目が存在する？
        /// </summary>
        /// <returns></returns>
        private bool ValidateUnUpdateDataExist()
        {
            var dataSourcePlb = GridRowUtil<DE0040_GridDto_Plb>.GetAllRowsModel(this.Body.dgvPlumbing.DataSource);
            if (dataSourcePlb.Any(n => (int)Enums.EditStatus.Insert <= (int)n.Status_Plb)) { return false; }
            var dataSourceHsg = GridRowUtil<DE0040_GridDto_Hsg>.GetAllRowsModel(this.Body.dgvHousing.DataSource);
            if (dataSourceHsg.Any(n => (int)Enums.EditStatus.Insert <= (int)n.Status_Hsg)) { return false; }
            var dataSourcePrc = GridRowUtil<DE0040_GridDto_Prc>.GetAllRowsModel(this.Body.dgvProcessing.DataSource);
            if (dataSourcePrc.Any(n => (int)Enums.EditStatus.Insert <= (int)n.Status_Prc)) { return false; }
            return true;
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <returns></returns>
        private bool ValidateUpdate()
        {
            // 件数チェック
            if (!this.ValidateExistUpdDatas()) { return false; }

            // マスタ存在チェック
            bool result = true;
            if (!this.ValidateUpdExistMaster(this.Body.dgvPlumbing, "_Plb")) { result = false; }
            if (!this.ValidateUpdExistMaster(this.Body.dgvHousing, "_Hsg")) { result = false; }
            if (!this.ValidateUpdExistMaster(this.Body.dgvProcessing, "_Prc")) { result = false; }
            if (!result) { return false; }

            // 数量チェック
            if (!this.ValidateUpdSuryo(this.Body.dgvPlumbing, "_Plb")) { result = false; }
            if (!this.ValidateUpdSuryo(this.Body.dgvHousing, "_Hsg")) { result = false; }
            if (!this.ValidateUpdSuryo(this.Body.dgvProcessing, "_Prc")) { result = false; }
            if (!result) { return false; }
            return true;
        }
        /// <summary>
        /// 件数チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateExistUpdDatas()
        {
            var datasPlb = GridRowUtil<DE0040_GridDto_Plb>.GetAllRowsModel(this.Body.dgvPlumbing.DataSource).Where(n => (int)Enums.EditStatus.Insert <= (int)n.Status_Plb);
            var datasHsg = GridRowUtil<DE0040_GridDto_Hsg>.GetAllRowsModel(this.Body.dgvHousing.DataSource).Where(n => (int)Enums.EditStatus.Insert <= (int)n.Status_Hsg);
            var datasPrc = GridRowUtil<DE0040_GridDto_Prc>.GetAllRowsModel(this.Body.dgvProcessing.DataSource).Where(n => (int)Enums.EditStatus.Insert <= (int)n.Status_Prc);
            if (!datasPlb.Any() && !datasHsg.Any() && !datasPrc.Any())
            {
                this.UpdErrMessage = DE0040_Messages.MsgErrorNotExistDatas;
                return false;
            }
            return true;
        }
        /// <summary>
        /// マスタ存在チェック
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="colAdd"></param>
        /// <returns></returns>
        private bool ValidateUpdExistMaster(
              DataGridView dgv
            , string colAdd
            )
        {
            bool resutlt = true;
            foreach (DataGridViewRow r in dgv.Rows)
            {
                string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{colAdd}"].Value.ToString();
                if (status == Enums.EditStatus.None.ToString() || status == Enums.EditStatus.Show.ToString())
                {
                    continue;
                }
                int shohinID = CommonUtil.ToInteger(r.Cells[$"{nameof(CellHeaderNameDto.ShohinID)}{colAdd}"].Value);
                string shohinCD = CommonUtil.TostringNullForbid(r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{colAdd}"].Value);
                if (string.IsNullOrEmpty(shohinCD) && shohinID == 0 && status == Enums.EditStatus.Update.ToString())
                {
                    continue;
                }
                if (!this.PersonalService.ValidateExistMaster(shohinCD))
                {
                    r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{colAdd}"].Style.BackColor = Color.Red;
                    resutlt = false;
                }
            }
            if (!resutlt)
            {
                string colName = dgv.Columns[$"{nameof(CellHeaderNameDto.ShohinCD)}{colAdd}"].HeaderText;
                this.UpdErrMessage = string.Format(DE0040_Messages.MsgErrorNotExistMaster, colName);
            }
            return resutlt;
        }
        /// <summary>
        /// 数量チェック
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="colAdd"></param>
        /// <returns></returns>
        private bool ValidateUpdSuryo(
              DataGridView dgv
            , string colAdd
            )
        {
            bool resutlt = true;
            foreach (DataGridViewRow r in dgv.Rows)
            {
                string status = r.Cells[$"{nameof(CellHeaderNameDto.Status)}{colAdd}"].Value.ToString();
                if (status == Enums.EditStatus.None.ToString() || status == Enums.EditStatus.Show.ToString())
                {
                    continue;
                }
                string shohinCD = CommonUtil.TostringNullForbid(r.Cells[$"{nameof(CellHeaderNameDto.ShohinCD)}{colAdd}"].Value);
                string suryo = CommonUtil.TostringNullForbid(r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{colAdd}"].Value);
                if (!string.IsNullOrEmpty(shohinCD) && CommonUtil.ToInteger(suryo) == 0)
                {
                    r.Cells[$"{nameof(CellHeaderNameDto.Suryo)}{colAdd}"].Style.BackColor = Color.Red;
                    resutlt = false;
                }
            }
            if (!resutlt)
            {
                string colName = dgv.Columns[$"{nameof(CellHeaderNameDto.Suryo)}{colAdd}"].HeaderText;
                this.UpdErrMessage = string.Format(DE0040_Messages.MsgErrorIrregalZero, colName);
            }
            return resutlt;
        }
        /// <summary>
        /// 加工費情報存在チェック
        /// </summary>
        /// <param name="shohinID"></param>
        /// <returns></returns>
        private bool ValidateExistProcessingChildCds(int shohinID)
        {
            var shohinInfo = this.PersonalService.SelShohinData(shohinID);
            // 材料費のサイズをリスト化
            var sizeList = CommonUtil.CutSpace(shohinInfo.ShohinSizeName).Replace("x", "X").Split('X').ToList();
            // リスト化したサイズを１つずつループ
            foreach (string materialSize in sizeList)
            {
                if (!this.PersonalService.ValidateExistProcessing(shohinInfo.ProcessingChildCD1, shohinInfo.ProcessingSeq1, materialSize))
                {
                    return false;
                }
                if (!this.PersonalService.ValidateExistProcessing(shohinInfo.ProcessingChildCD2, shohinInfo.ProcessingSeq2, materialSize))
                {
                    return false;
                }
                if (!this.PersonalService.ValidateExistProcessing(shohinInfo.ProcessingChildCD3, shohinInfo.ProcessingSeq3, materialSize))
                {
                    return false;
                }
                if (!this.PersonalService.ValidateExistProcessing(shohinInfo.ProcessingChildCD4, shohinInfo.ProcessingSeq4, materialSize))
                {
                    return false;
                }
                if (!this.PersonalService.ValidateExistProcessing(shohinInfo.ProcessingChildCD5, shohinInfo.ProcessingSeq5, materialSize))
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            // グリッド初期化
            this.InitGrid();

            // グリッドコンボ初期化
            this.InitGridCombo();

            // 使用可否切り替え
            this.Body.tabMeisai.SelectTab(0);

            // 編集フラグ
            this.EditedFlg = false;

            // 変数初期化
            this.Body.tabMeisai.SelectedIndex = this.MeisaiType - 1;
            this.Body.numRateConsumption.Value = (decimal)this.OriginConsumptionRate;
            this.Body.numRateInspection.Value = (decimal)this.OriginInspectionRate;
            this.Body.numRateExpence.Value = (decimal)this.OriginExpenceRate;
            switch (this.Body.tabMeisai.SelectedIndex)
            {
                case TabIndexPlb:
                    this.SelectedCollAdd = "_Plb";
                    this.Body.numRateConsumption.Visible = false;
                    this.Body.lblRateConsumption.Visible = false;
                    this.Body.numRateInspection.Visible = false;
                    this.Body.lblRateInspection.Visible = false;
                    this.Body.numRateExpence.Visible = false;
                    this.Body.lblRateExpence.Visible = false;
                    break;
                case TabIndexHsg:
                    this.SelectedCollAdd = "_Hsg";
                    this.Body.numRateConsumption.Visible = false;
                    this.Body.lblRateConsumption.Visible = false;
                    this.Body.numRateInspection.Visible = false;
                    this.Body.lblRateInspection.Visible = false;
                    this.Body.numRateExpence.Visible = false;
                    this.Body.lblRateExpence.Visible = false;
                    break;
                case TabIndexPrc:
                    this.SelectedCollAdd = "_Prc";
                    this.Body.numRateConsumption.Visible = true;
                    this.Body.lblRateConsumption.Visible = true;
                    this.Body.numRateInspection.Visible = true;
                    this.Body.lblRateInspection.Visible = true;
                    this.Body.numRateExpence.Visible = true;
                    this.Body.lblRateExpence.Visible = true;
                    break;
            }
            // ワークテーブルからの該当データ取得処理
            this.SearchFromWorkTable();
            // 金額計算
            this.CalcSummary(this.Body.tabMeisai.SelectedIndex);
            // フォーカス
            this.Body.dgvPlumbing.Focus();
        }
        /// <summary>
        /// 初期化（グリッド）
        /// </summary>
        private void InitGrid()
        {
            this.Body.dgvPlumbing.Rows.Clear();
            this.Body.dgvPlumbing.AllowUserToAddRows = false;
            this.Body.dgvHousing.Rows.Clear();
            this.Body.dgvHousing.AllowUserToAddRows = false;
            this.Body.dgvProcessing.Rows.Clear();
            this.Body.dgvProcessing.AllowUserToAddRows = false;

            this.Body.dgvPlumbing.Columns[nameof(CellHeaderNameDtoPlb.ShohinID_Plb)].Visible = false;
            this.Body.dgvHousing.Columns[nameof(CellHeaderNameDtoHsg.ShohinID_Hsg)].Visible = false;
            this.Body.dgvProcessing.Columns[nameof(CellHeaderNameDtoPrc.ShohinID_Prc)].Visible = false;

            // 読み取り専用
            if (this.ReadOnly)
            {
                this.Body.btnAdd.Enabled = false;
                this.Body.btnUpdate.Enabled = false;
                this.Body.dgvPlumbing.Columns[nameof(CellHeaderNameDtoPlb.BtnSearch_Plb)].Visible = false;
                this.Body.dgvPlumbing.Columns[nameof(CellHeaderNameDtoPlb.BtnClear_Plb)].Visible = false;
                foreach (DataGridViewColumn c in this.Body.dgvPlumbing.Columns)
                {
                    c.DefaultCellStyle.BackColor = Colors.BackColorBtnReadOnly;
                }
                this.Body.dgvHousing.Columns[nameof(CellHeaderNameDtoHsg.BtnSearch_Hsg)].Visible = false;
                this.Body.dgvHousing.Columns[nameof(CellHeaderNameDtoHsg.BtnClear_Hsg)].Visible = false;
                foreach (DataGridViewColumn c in this.Body.dgvHousing.Columns)
                {
                    c.DefaultCellStyle.BackColor = Colors.BackColorBtnReadOnly;
                }
                this.Body.dgvProcessing.Columns[nameof(CellHeaderNameDtoPrc.BtnSearch_Prc)].Visible = false;
                this.Body.dgvProcessing.Columns[nameof(CellHeaderNameDtoPrc.BtnClear_Prc)].Visible = false;
                foreach (DataGridViewColumn c in this.Body.dgvProcessing.Columns)
                {
                    c.DefaultCellStyle.BackColor = Colors.BackColorBtnReadOnly;
                }
            }
        }
        /// <summary>
        /// グリッドコンボ初期化
        /// </summary>
        /// <param name="tabIndex"></param>
        private void InitGridCombo()
        {
            var cmbUnitPlb = (DataGridViewComboBoxColumn)this.Body.dgvPlumbing.Columns[nameof(CellHeaderNameDtoPlb.Unit_Plb)];
            var cmbUnitHsg = (DataGridViewComboBoxColumn)this.Body.dgvHousing.Columns[nameof(CellHeaderNameDtoHsg.Unit_Hsg)];
            var cmbUnitPrc = (DataGridViewComboBoxColumn)this.Body.dgvProcessing.Columns[nameof(CellHeaderNameDtoPrc.Unit_Prc)];

            cmbUnitPlb.Items.Clear();
            cmbUnitPlb.Items.Add(string.Empty);
            cmbUnitHsg.Items.Clear();
            cmbUnitHsg.Items.Add(string.Empty);
            cmbUnitPrc.Items.Clear();
            cmbUnitPrc.Items.Add(string.Empty);

            foreach (var pair in Enums.PlumbingUnits)
            {
                cmbUnitPlb.Items.Add(pair.Value);
                cmbUnitHsg.Items.Add(pair.Value);
                cmbUnitPrc.Items.Add(pair.Value);
            }
        }
        #endregion
    }
}
