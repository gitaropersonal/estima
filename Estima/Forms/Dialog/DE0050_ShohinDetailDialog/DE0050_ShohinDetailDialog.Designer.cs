﻿namespace Estima.Forms.Dialog.DE0050
{
    partial class DE0050_ShohinDetailDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.pnlContent = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtTaishoYM = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSelectProcessingCD5 = new System.Windows.Forms.Button();
            this.txtProcessingChildCD5 = new System.Windows.Forms.TextBox();
            this.txtProcessingChildCDName5 = new System.Windows.Forms.TextBox();
            this.btnSelectProcessingCD4 = new System.Windows.Forms.Button();
            this.txtProcessingChildCD4 = new System.Windows.Forms.TextBox();
            this.txtProcessingChildCDName4 = new System.Windows.Forms.TextBox();
            this.btnSelectProcessingCD3 = new System.Windows.Forms.Button();
            this.txtProcessingChildCD3 = new System.Windows.Forms.TextBox();
            this.txtProcessingChildCDName3 = new System.Windows.Forms.TextBox();
            this.btnSelectProcessingCD2 = new System.Windows.Forms.Button();
            this.txtProcessingChildCD2 = new System.Windows.Forms.TextBox();
            this.txtProcessingChildCDName2 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.btnSelectProcessingCD1 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.txtProcessingChildCD1 = new System.Windows.Forms.TextBox();
            this.txtProcessingChildCDName1 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.numBDCount = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.numInchDia = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.numGBCount = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.numGBSize = new System.Windows.Forms.NumericUpDown();
            this.label31 = new System.Windows.Forms.Label();
            this.numSaddleCount = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.numSaddleSize = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.numScrewCount3 = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.numScrewSize3 = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.numScrewCount2 = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.numScrewSize2 = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.numScrewCount1 = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.numScrewSize1 = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.numWeldingCount2 = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numWeldingSize2 = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numWeldingCount1 = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numWeldingSize1 = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.numPrice = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numShikiri = new System.Windows.Forms.NumericUpDown();
            this.numShikiriRate = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.numRiekiRate = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbSize1 = new System.Windows.Forms.ComboBox();
            this.cmbSize2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbSize3 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbSize4 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSelectParentCD = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtParentCD = new System.Windows.Forms.TextBox();
            this.txtParentCDName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtChildCD = new System.Windows.Forms.TextBox();
            this.btnSelectChildCD = new System.Windows.Forms.Button();
            this.txtChildCDName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtShohinCD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSizeCD = new System.Windows.Forms.TextBox();
            this.grpHidden = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtParentSeq = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtChildSeq = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtProcessingSeq1 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtProcessingSeq2 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtProcessingSeq3 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtProcessingSeq4 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtProcessingSeq5 = new System.Windows.Forms.TextBox();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBDCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numInchDia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGBCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGBSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSaddleCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSaddleSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewCount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewSize3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewCount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewSize2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewSize1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeldingCount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeldingSize2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeldingCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeldingSize1)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numShikiri)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numShikiriRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRiekiRate)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grpHidden.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 691);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1034, 70);
            this.panel3.TabIndex = 103;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnClose);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(809, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(225, 70);
            this.panel5.TabIndex = 120;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(68, 8);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 52);
            this.btnClose.TabIndex = 121;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClear);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(511, 70);
            this.panel4.TabIndex = 110;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(156, 8);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(138, 52);
            this.btnUpdate.TabIndex = 112;
            this.btnUpdate.Text = "仮登録（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(19, 8);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(138, 52);
            this.btnClear.TabIndex = 111;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.grpHidden);
            this.pnlContent.Controls.Add(this.groupBox6);
            this.pnlContent.Controls.Add(this.groupBox1);
            this.pnlContent.Controls.Add(this.groupBox5);
            this.pnlContent.Controls.Add(this.groupBox4);
            this.pnlContent.Controls.Add(this.groupBox3);
            this.pnlContent.Controls.Add(this.groupBox2);
            this.pnlContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlContent.Location = new System.Drawing.Point(0, 0);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Padding = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.pnlContent.Size = new System.Drawing.Size(1034, 691);
            this.pnlContent.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtTaishoYM);
            this.groupBox6.Location = new System.Drawing.Point(18, 274);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(493, 78);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "開始年月（YYYYMM）";
            // 
            // txtTaishoYM
            // 
            this.txtTaishoYM.Location = new System.Drawing.Point(28, 32);
            this.txtTaishoYM.MaxLength = 6;
            this.txtTaishoYM.Name = "txtTaishoYM";
            this.txtTaishoYM.Size = new System.Drawing.Size(83, 28);
            this.txtTaishoYM.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSelectProcessingCD5);
            this.groupBox1.Controls.Add(this.txtProcessingChildCD5);
            this.groupBox1.Controls.Add(this.txtProcessingChildCDName5);
            this.groupBox1.Controls.Add(this.btnSelectProcessingCD4);
            this.groupBox1.Controls.Add(this.txtProcessingChildCD4);
            this.groupBox1.Controls.Add(this.txtProcessingChildCDName4);
            this.groupBox1.Controls.Add(this.btnSelectProcessingCD3);
            this.groupBox1.Controls.Add(this.txtProcessingChildCD3);
            this.groupBox1.Controls.Add(this.txtProcessingChildCDName3);
            this.groupBox1.Controls.Add(this.btnSelectProcessingCD2);
            this.groupBox1.Controls.Add(this.txtProcessingChildCD2);
            this.groupBox1.Controls.Add(this.txtProcessingChildCDName2);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.btnSelectProcessingCD1);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.txtProcessingChildCD1);
            this.groupBox1.Controls.Add(this.txtProcessingChildCDName1);
            this.groupBox1.Location = new System.Drawing.Point(517, 311);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(498, 353);
            this.groupBox1.TabIndex = 70;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "子商品コード（加工費）";
            // 
            // btnSelectProcessingCD5
            // 
            this.btnSelectProcessingCD5.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectProcessingCD5.Location = new System.Drawing.Point(62, 269);
            this.btnSelectProcessingCD5.Name = "btnSelectProcessingCD5";
            this.btnSelectProcessingCD5.Size = new System.Drawing.Size(60, 28);
            this.btnSelectProcessingCD5.TabIndex = 82;
            this.btnSelectProcessingCD5.Text = "検索";
            this.btnSelectProcessingCD5.UseVisualStyleBackColor = true;
            // 
            // txtProcessingChildCD5
            // 
            this.txtProcessingChildCD5.Location = new System.Drawing.Point(122, 269);
            this.txtProcessingChildCD5.MaxLength = 20;
            this.txtProcessingChildCD5.Name = "txtProcessingChildCD5";
            this.txtProcessingChildCD5.Size = new System.Drawing.Size(204, 28);
            this.txtProcessingChildCD5.TabIndex = 83;
            // 
            // txtProcessingChildCDName5
            // 
            this.txtProcessingChildCDName5.Location = new System.Drawing.Point(62, 298);
            this.txtProcessingChildCDName5.Name = "txtProcessingChildCDName5";
            this.txtProcessingChildCDName5.ReadOnly = true;
            this.txtProcessingChildCDName5.Size = new System.Drawing.Size(348, 28);
            this.txtProcessingChildCDName5.TabIndex = 84;
            this.txtProcessingChildCDName5.TabStop = false;
            // 
            // btnSelectProcessingCD4
            // 
            this.btnSelectProcessingCD4.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectProcessingCD4.Location = new System.Drawing.Point(62, 211);
            this.btnSelectProcessingCD4.Name = "btnSelectProcessingCD4";
            this.btnSelectProcessingCD4.Size = new System.Drawing.Size(60, 28);
            this.btnSelectProcessingCD4.TabIndex = 79;
            this.btnSelectProcessingCD4.Text = "検索";
            this.btnSelectProcessingCD4.UseVisualStyleBackColor = true;
            // 
            // txtProcessingChildCD4
            // 
            this.txtProcessingChildCD4.Location = new System.Drawing.Point(122, 211);
            this.txtProcessingChildCD4.MaxLength = 20;
            this.txtProcessingChildCD4.Name = "txtProcessingChildCD4";
            this.txtProcessingChildCD4.Size = new System.Drawing.Size(204, 28);
            this.txtProcessingChildCD4.TabIndex = 80;
            // 
            // txtProcessingChildCDName4
            // 
            this.txtProcessingChildCDName4.Location = new System.Drawing.Point(62, 240);
            this.txtProcessingChildCDName4.Name = "txtProcessingChildCDName4";
            this.txtProcessingChildCDName4.ReadOnly = true;
            this.txtProcessingChildCDName4.Size = new System.Drawing.Size(348, 28);
            this.txtProcessingChildCDName4.TabIndex = 81;
            this.txtProcessingChildCDName4.TabStop = false;
            // 
            // btnSelectProcessingCD3
            // 
            this.btnSelectProcessingCD3.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectProcessingCD3.Location = new System.Drawing.Point(62, 153);
            this.btnSelectProcessingCD3.Name = "btnSelectProcessingCD3";
            this.btnSelectProcessingCD3.Size = new System.Drawing.Size(60, 28);
            this.btnSelectProcessingCD3.TabIndex = 76;
            this.btnSelectProcessingCD3.Text = "検索";
            this.btnSelectProcessingCD3.UseVisualStyleBackColor = true;
            // 
            // txtProcessingChildCD3
            // 
            this.txtProcessingChildCD3.Location = new System.Drawing.Point(122, 153);
            this.txtProcessingChildCD3.MaxLength = 20;
            this.txtProcessingChildCD3.Name = "txtProcessingChildCD3";
            this.txtProcessingChildCD3.Size = new System.Drawing.Size(204, 28);
            this.txtProcessingChildCD3.TabIndex = 77;
            // 
            // txtProcessingChildCDName3
            // 
            this.txtProcessingChildCDName3.Location = new System.Drawing.Point(62, 182);
            this.txtProcessingChildCDName3.Name = "txtProcessingChildCDName3";
            this.txtProcessingChildCDName3.ReadOnly = true;
            this.txtProcessingChildCDName3.Size = new System.Drawing.Size(348, 28);
            this.txtProcessingChildCDName3.TabIndex = 78;
            this.txtProcessingChildCDName3.TabStop = false;
            // 
            // btnSelectProcessingCD2
            // 
            this.btnSelectProcessingCD2.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectProcessingCD2.Location = new System.Drawing.Point(62, 95);
            this.btnSelectProcessingCD2.Name = "btnSelectProcessingCD2";
            this.btnSelectProcessingCD2.Size = new System.Drawing.Size(60, 28);
            this.btnSelectProcessingCD2.TabIndex = 73;
            this.btnSelectProcessingCD2.Text = "検索";
            this.btnSelectProcessingCD2.UseVisualStyleBackColor = true;
            // 
            // txtProcessingChildCD2
            // 
            this.txtProcessingChildCD2.Location = new System.Drawing.Point(122, 95);
            this.txtProcessingChildCD2.MaxLength = 20;
            this.txtProcessingChildCD2.Name = "txtProcessingChildCD2";
            this.txtProcessingChildCD2.Size = new System.Drawing.Size(204, 28);
            this.txtProcessingChildCD2.TabIndex = 74;
            // 
            // txtProcessingChildCDName2
            // 
            this.txtProcessingChildCDName2.Location = new System.Drawing.Point(62, 124);
            this.txtProcessingChildCDName2.Name = "txtProcessingChildCDName2";
            this.txtProcessingChildCDName2.ReadOnly = true;
            this.txtProcessingChildCDName2.Size = new System.Drawing.Size(348, 28);
            this.txtProcessingChildCDName2.TabIndex = 75;
            this.txtProcessingChildCDName2.TabStop = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(24, 272);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(25, 20);
            this.label41.TabIndex = 135;
            this.label41.Text = "⑤";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(23, 214);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(25, 20);
            this.label39.TabIndex = 131;
            this.label39.Text = "④";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(24, 156);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(25, 20);
            this.label40.TabIndex = 127;
            this.label40.Text = "③";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(24, 98);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(25, 20);
            this.label38.TabIndex = 123;
            this.label38.Text = "②";
            // 
            // btnSelectProcessingCD1
            // 
            this.btnSelectProcessingCD1.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectProcessingCD1.Location = new System.Drawing.Point(62, 37);
            this.btnSelectProcessingCD1.Name = "btnSelectProcessingCD1";
            this.btnSelectProcessingCD1.Size = new System.Drawing.Size(60, 28);
            this.btnSelectProcessingCD1.TabIndex = 70;
            this.btnSelectProcessingCD1.Text = "検索";
            this.btnSelectProcessingCD1.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(24, 40);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(25, 20);
            this.label37.TabIndex = 119;
            this.label37.Text = "①";
            // 
            // txtProcessingChildCD1
            // 
            this.txtProcessingChildCD1.Location = new System.Drawing.Point(122, 37);
            this.txtProcessingChildCD1.MaxLength = 20;
            this.txtProcessingChildCD1.Name = "txtProcessingChildCD1";
            this.txtProcessingChildCD1.Size = new System.Drawing.Size(204, 28);
            this.txtProcessingChildCD1.TabIndex = 71;
            // 
            // txtProcessingChildCDName1
            // 
            this.txtProcessingChildCDName1.Location = new System.Drawing.Point(62, 66);
            this.txtProcessingChildCDName1.Name = "txtProcessingChildCDName1";
            this.txtProcessingChildCDName1.ReadOnly = true;
            this.txtProcessingChildCDName1.Size = new System.Drawing.Size(348, 28);
            this.txtProcessingChildCDName1.TabIndex = 72;
            this.txtProcessingChildCDName1.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.numBDCount);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.numInchDia);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.numGBCount);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.numGBSize);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.numSaddleCount);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.numSaddleSize);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.numScrewCount3);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.numScrewSize3);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.numScrewCount2);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.numScrewSize2);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.numScrewCount1);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.numScrewSize1);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.numWeldingCount2);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.numWeldingSize2);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.numWeldingCount1);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.numWeldingSize1);
            this.groupBox5.Location = new System.Drawing.Point(517, 13);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(498, 297);
            this.groupBox5.TabIndex = 50;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "BD関連";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(238, 236);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(48, 20);
            this.label34.TabIndex = 184;
            this.label34.Text = "箇所:";
            // 
            // numBDCount
            // 
            this.numBDCount.Enabled = false;
            this.numBDCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numBDCount.Location = new System.Drawing.Point(292, 233);
            this.numBDCount.Name = "numBDCount";
            this.numBDCount.ReadOnly = true;
            this.numBDCount.Size = new System.Drawing.Size(79, 28);
            this.numBDCount.TabIndex = 65;
            this.numBDCount.TabStop = false;
            this.numBDCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(68, 236);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(60, 20);
            this.label35.TabIndex = 182;
            this.label35.Text = "/1ヶ所:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(23, 236);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(32, 20);
            this.label36.TabIndex = 180;
            this.label36.Text = "BD";
            // 
            // numInchDia
            // 
            this.numInchDia.DecimalPlaces = 2;
            this.numInchDia.Enabled = false;
            this.numInchDia.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numInchDia.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numInchDia.Location = new System.Drawing.Point(130, 233);
            this.numInchDia.Name = "numInchDia";
            this.numInchDia.ReadOnly = true;
            this.numInchDia.Size = new System.Drawing.Size(102, 28);
            this.numInchDia.TabIndex = 64;
            this.numInchDia.TabStop = false;
            this.numInchDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(254, 207);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 20);
            this.label19.TabIndex = 179;
            this.label19.Text = "数:";
            // 
            // numGBCount
            // 
            this.numGBCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numGBCount.Location = new System.Drawing.Point(292, 204);
            this.numGBCount.Name = "numGBCount";
            this.numGBCount.Size = new System.Drawing.Size(79, 28);
            this.numGBCount.TabIndex = 63;
            this.numGBCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(80, 207);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(48, 20);
            this.label20.TabIndex = 177;
            this.label20.Text = "ｻｲｽﾞ:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(23, 207);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 20);
            this.label21.TabIndex = 175;
            this.label21.Text = "GB";
            // 
            // numGBSize
            // 
            this.numGBSize.DecimalPlaces = 2;
            this.numGBSize.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numGBSize.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numGBSize.Location = new System.Drawing.Point(130, 204);
            this.numGBSize.Name = "numGBSize";
            this.numGBSize.Size = new System.Drawing.Size(102, 28);
            this.numGBSize.TabIndex = 62;
            this.numGBSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(254, 178);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 20);
            this.label31.TabIndex = 174;
            this.label31.Text = "数:";
            // 
            // numSaddleCount
            // 
            this.numSaddleCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numSaddleCount.Location = new System.Drawing.Point(292, 175);
            this.numSaddleCount.Name = "numSaddleCount";
            this.numSaddleCount.Size = new System.Drawing.Size(79, 28);
            this.numSaddleCount.TabIndex = 61;
            this.numSaddleCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(80, 178);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(48, 20);
            this.label32.TabIndex = 172;
            this.label32.Text = "ｻｲｽﾞ:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(23, 178);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 20);
            this.label33.TabIndex = 170;
            this.label33.Text = "サドル";
            // 
            // numSaddleSize
            // 
            this.numSaddleSize.DecimalPlaces = 2;
            this.numSaddleSize.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numSaddleSize.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numSaddleSize.Location = new System.Drawing.Point(130, 175);
            this.numSaddleSize.Name = "numSaddleSize";
            this.numSaddleSize.Size = new System.Drawing.Size(102, 28);
            this.numSaddleSize.TabIndex = 60;
            this.numSaddleSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(254, 149);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 20);
            this.label22.TabIndex = 169;
            this.label22.Text = "数:";
            // 
            // numScrewCount3
            // 
            this.numScrewCount3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numScrewCount3.Location = new System.Drawing.Point(292, 146);
            this.numScrewCount3.Name = "numScrewCount3";
            this.numScrewCount3.Size = new System.Drawing.Size(79, 28);
            this.numScrewCount3.TabIndex = 59;
            this.numScrewCount3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(80, 149);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 20);
            this.label23.TabIndex = 167;
            this.label23.Text = "ｻｲｽﾞ:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(23, 149);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 20);
            this.label24.TabIndex = 165;
            this.label24.Text = "ネジ3";
            // 
            // numScrewSize3
            // 
            this.numScrewSize3.DecimalPlaces = 2;
            this.numScrewSize3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numScrewSize3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numScrewSize3.Location = new System.Drawing.Point(130, 146);
            this.numScrewSize3.Name = "numScrewSize3";
            this.numScrewSize3.Size = new System.Drawing.Size(102, 28);
            this.numScrewSize3.TabIndex = 58;
            this.numScrewSize3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(254, 120);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 20);
            this.label25.TabIndex = 164;
            this.label25.Text = "数:";
            // 
            // numScrewCount2
            // 
            this.numScrewCount2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numScrewCount2.Location = new System.Drawing.Point(292, 117);
            this.numScrewCount2.Name = "numScrewCount2";
            this.numScrewCount2.Size = new System.Drawing.Size(79, 28);
            this.numScrewCount2.TabIndex = 57;
            this.numScrewCount2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(80, 120);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(48, 20);
            this.label26.TabIndex = 162;
            this.label26.Text = "ｻｲｽﾞ:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(23, 120);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 20);
            this.label27.TabIndex = 160;
            this.label27.Text = "ネジ2";
            // 
            // numScrewSize2
            // 
            this.numScrewSize2.DecimalPlaces = 2;
            this.numScrewSize2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numScrewSize2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numScrewSize2.Location = new System.Drawing.Point(130, 117);
            this.numScrewSize2.Name = "numScrewSize2";
            this.numScrewSize2.Size = new System.Drawing.Size(102, 28);
            this.numScrewSize2.TabIndex = 56;
            this.numScrewSize2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(254, 91);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(32, 20);
            this.label28.TabIndex = 159;
            this.label28.Text = "数:";
            // 
            // numScrewCount1
            // 
            this.numScrewCount1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numScrewCount1.Location = new System.Drawing.Point(292, 88);
            this.numScrewCount1.Name = "numScrewCount1";
            this.numScrewCount1.Size = new System.Drawing.Size(79, 28);
            this.numScrewCount1.TabIndex = 55;
            this.numScrewCount1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(80, 91);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(48, 20);
            this.label29.TabIndex = 157;
            this.label29.Text = "ｻｲｽﾞ:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(23, 91);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(45, 20);
            this.label30.TabIndex = 155;
            this.label30.Text = "ネジ1";
            // 
            // numScrewSize1
            // 
            this.numScrewSize1.DecimalPlaces = 2;
            this.numScrewSize1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numScrewSize1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numScrewSize1.Location = new System.Drawing.Point(130, 88);
            this.numScrewSize1.Name = "numScrewSize1";
            this.numScrewSize1.Size = new System.Drawing.Size(102, 28);
            this.numScrewSize1.TabIndex = 54;
            this.numScrewSize1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(254, 62);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 20);
            this.label16.TabIndex = 149;
            this.label16.Text = "数:";
            // 
            // numWeldingCount2
            // 
            this.numWeldingCount2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numWeldingCount2.Location = new System.Drawing.Point(292, 59);
            this.numWeldingCount2.Name = "numWeldingCount2";
            this.numWeldingCount2.Size = new System.Drawing.Size(79, 28);
            this.numWeldingCount2.TabIndex = 53;
            this.numWeldingCount2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(80, 62);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 20);
            this.label17.TabIndex = 147;
            this.label17.Text = "ｻｲｽﾞ:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(23, 62);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(51, 20);
            this.label18.TabIndex = 145;
            this.label18.Text = "溶接2";
            // 
            // numWeldingSize2
            // 
            this.numWeldingSize2.DecimalPlaces = 2;
            this.numWeldingSize2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numWeldingSize2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numWeldingSize2.Location = new System.Drawing.Point(130, 59);
            this.numWeldingSize2.Name = "numWeldingSize2";
            this.numWeldingSize2.Size = new System.Drawing.Size(102, 28);
            this.numWeldingSize2.TabIndex = 52;
            this.numWeldingSize2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(254, 33);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 20);
            this.label15.TabIndex = 144;
            this.label15.Text = "数:";
            // 
            // numWeldingCount1
            // 
            this.numWeldingCount1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numWeldingCount1.Location = new System.Drawing.Point(292, 30);
            this.numWeldingCount1.Name = "numWeldingCount1";
            this.numWeldingCount1.Size = new System.Drawing.Size(79, 28);
            this.numWeldingCount1.TabIndex = 51;
            this.numWeldingCount1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(80, 33);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 20);
            this.label14.TabIndex = 142;
            this.label14.Text = "ｻｲｽﾞ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 140;
            this.label6.Text = "溶接1";
            // 
            // numWeldingSize1
            // 
            this.numWeldingSize1.DecimalPlaces = 2;
            this.numWeldingSize1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numWeldingSize1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numWeldingSize1.Location = new System.Drawing.Point(130, 30);
            this.numWeldingSize1.Name = "numWeldingSize1";
            this.numWeldingSize1.Size = new System.Drawing.Size(102, 28);
            this.numWeldingSize1.TabIndex = 50;
            this.numWeldingSize1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.numPrice);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.numShikiri);
            this.groupBox4.Controls.Add(this.numShikiriRate);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.numRiekiRate);
            this.groupBox4.Location = new System.Drawing.Point(18, 352);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(493, 162);
            this.groupBox4.TabIndex = 30;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "定価関連";
            // 
            // numPrice
            // 
            this.numPrice.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numPrice.Location = new System.Drawing.Point(122, 32);
            this.numPrice.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numPrice.Name = "numPrice";
            this.numPrice.Size = new System.Drawing.Size(123, 28);
            this.numPrice.TabIndex = 30;
            this.numPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numPrice.ThousandsSeparator = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 20);
            this.label9.TabIndex = 136;
            this.label9.Text = "定価";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 20);
            this.label10.TabIndex = 138;
            this.label10.Text = "仕切率(%)";
            // 
            // numShikiri
            // 
            this.numShikiri.Enabled = false;
            this.numShikiri.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numShikiri.Location = new System.Drawing.Point(122, 119);
            this.numShikiri.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numShikiri.Name = "numShikiri";
            this.numShikiri.ReadOnly = true;
            this.numShikiri.Size = new System.Drawing.Size(123, 28);
            this.numShikiri.TabIndex = 33;
            this.numShikiri.TabStop = false;
            this.numShikiri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numShikiri.ThousandsSeparator = true;
            // 
            // numShikiriRate
            // 
            this.numShikiriRate.DecimalPlaces = 1;
            this.numShikiriRate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numShikiriRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numShikiriRate.Location = new System.Drawing.Point(166, 61);
            this.numShikiriRate.Name = "numShikiriRate";
            this.numShikiriRate.Size = new System.Drawing.Size(79, 28);
            this.numShikiriRate.TabIndex = 31;
            this.numShikiriRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 121);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 20);
            this.label13.TabIndex = 145;
            this.label13.Text = "仕切";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 20);
            this.label11.TabIndex = 140;
            this.label11.Text = "利益率(%)";
            // 
            // numRiekiRate
            // 
            this.numRiekiRate.DecimalPlaces = 1;
            this.numRiekiRate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.numRiekiRate.Location = new System.Drawing.Point(166, 90);
            this.numRiekiRate.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            65536});
            this.numRiekiRate.Name = "numRiekiRate";
            this.numRiekiRate.Size = new System.Drawing.Size(79, 28);
            this.numRiekiRate.TabIndex = 32;
            this.numRiekiRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbSize1);
            this.groupBox3.Controls.Add(this.cmbSize2);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.cmbSize3);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.cmbSize4);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(18, 201);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(492, 73);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "サイズ";
            // 
            // cmbSize1
            // 
            this.cmbSize1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSize1.FormattingEnabled = true;
            this.cmbSize1.Location = new System.Drawing.Point(27, 31);
            this.cmbSize1.Name = "cmbSize1";
            this.cmbSize1.Size = new System.Drawing.Size(79, 28);
            this.cmbSize1.TabIndex = 10;
            // 
            // cmbSize2
            // 
            this.cmbSize2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSize2.FormattingEnabled = true;
            this.cmbSize2.Location = new System.Drawing.Point(128, 31);
            this.cmbSize2.Name = "cmbSize2";
            this.cmbSize2.Size = new System.Drawing.Size(79, 28);
            this.cmbSize2.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(108, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 20);
            this.label5.TabIndex = 131;
            this.label5.Text = "x";
            // 
            // cmbSize3
            // 
            this.cmbSize3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSize3.FormattingEnabled = true;
            this.cmbSize3.Location = new System.Drawing.Point(229, 31);
            this.cmbSize3.Name = "cmbSize3";
            this.cmbSize3.Size = new System.Drawing.Size(79, 28);
            this.cmbSize3.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(209, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 20);
            this.label7.TabIndex = 133;
            this.label7.Text = "x";
            // 
            // cmbSize4
            // 
            this.cmbSize4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSize4.FormattingEnabled = true;
            this.cmbSize4.Location = new System.Drawing.Point(330, 31);
            this.cmbSize4.Name = "cmbSize4";
            this.cmbSize4.Size = new System.Drawing.Size(79, 28);
            this.cmbSize4.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(310, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 20);
            this.label8.TabIndex = 135;
            this.label8.Text = "x";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSelectParentCD);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtParentCD);
            this.groupBox2.Controls.Add(this.txtParentCDName);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtChildCD);
            this.groupBox2.Controls.Add(this.btnSelectChildCD);
            this.groupBox2.Controls.Add(this.txtChildCDName);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtShohinCD);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtSizeCD);
            this.groupBox2.Location = new System.Drawing.Point(18, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(493, 188);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "商品コード関連";
            // 
            // btnSelectParentCD
            // 
            this.btnSelectParentCD.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectParentCD.Location = new System.Drawing.Point(122, 33);
            this.btnSelectParentCD.Name = "btnSelectParentCD";
            this.btnSelectParentCD.Size = new System.Drawing.Size(60, 28);
            this.btnSelectParentCD.TabIndex = 0;
            this.btnSelectParentCD.Text = "検索";
            this.btnSelectParentCD.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 20);
            this.label1.TabIndex = 119;
            this.label1.Text = "親コード";
            // 
            // txtParentCD
            // 
            this.txtParentCD.Location = new System.Drawing.Point(182, 33);
            this.txtParentCD.MaxLength = 3;
            this.txtParentCD.Name = "txtParentCD";
            this.txtParentCD.Size = new System.Drawing.Size(46, 28);
            this.txtParentCD.TabIndex = 1;
            // 
            // txtParentCDName
            // 
            this.txtParentCDName.Location = new System.Drawing.Point(229, 33);
            this.txtParentCDName.Name = "txtParentCDName";
            this.txtParentCDName.ReadOnly = true;
            this.txtParentCDName.Size = new System.Drawing.Size(240, 28);
            this.txtParentCDName.TabIndex = 2;
            this.txtParentCDName.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 20);
            this.label2.TabIndex = 123;
            this.label2.Text = "子商品コード";
            // 
            // txtChildCD
            // 
            this.txtChildCD.Location = new System.Drawing.Point(182, 62);
            this.txtChildCD.MaxLength = 20;
            this.txtChildCD.Name = "txtChildCD";
            this.txtChildCD.Size = new System.Drawing.Size(222, 28);
            this.txtChildCD.TabIndex = 4;
            // 
            // btnSelectChildCD
            // 
            this.btnSelectChildCD.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSelectChildCD.Location = new System.Drawing.Point(122, 62);
            this.btnSelectChildCD.Name = "btnSelectChildCD";
            this.btnSelectChildCD.Size = new System.Drawing.Size(60, 28);
            this.btnSelectChildCD.TabIndex = 3;
            this.btnSelectChildCD.Text = "検索";
            this.btnSelectChildCD.UseVisualStyleBackColor = true;
            // 
            // txtChildCDName
            // 
            this.txtChildCDName.Location = new System.Drawing.Point(122, 90);
            this.txtChildCDName.Name = "txtChildCDName";
            this.txtChildCDName.ReadOnly = true;
            this.txtChildCDName.Size = new System.Drawing.Size(348, 28);
            this.txtChildCDName.TabIndex = 5;
            this.txtChildCDName.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 20);
            this.label4.TabIndex = 117;
            this.label4.Text = "商品コード";
            // 
            // txtShohinCD
            // 
            this.txtShohinCD.Location = new System.Drawing.Point(122, 148);
            this.txtShohinCD.Name = "txtShohinCD";
            this.txtShohinCD.ReadOnly = true;
            this.txtShohinCD.Size = new System.Drawing.Size(348, 28);
            this.txtShohinCD.TabIndex = 7;
            this.txtShohinCD.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 125;
            this.label3.Text = "サイズコード";
            // 
            // txtSizeCD
            // 
            this.txtSizeCD.Location = new System.Drawing.Point(122, 119);
            this.txtSizeCD.MaxLength = 15;
            this.txtSizeCD.Name = "txtSizeCD";
            this.txtSizeCD.Size = new System.Drawing.Size(106, 28);
            this.txtSizeCD.TabIndex = 6;
            // 
            // grpHidden
            // 
            this.grpHidden.Controls.Add(this.label47);
            this.grpHidden.Controls.Add(this.txtProcessingSeq5);
            this.grpHidden.Controls.Add(this.label45);
            this.grpHidden.Controls.Add(this.txtProcessingSeq3);
            this.grpHidden.Controls.Add(this.label46);
            this.grpHidden.Controls.Add(this.txtProcessingSeq4);
            this.grpHidden.Controls.Add(this.label43);
            this.grpHidden.Controls.Add(this.txtProcessingSeq1);
            this.grpHidden.Controls.Add(this.label44);
            this.grpHidden.Controls.Add(this.txtProcessingSeq2);
            this.grpHidden.Controls.Add(this.label12);
            this.grpHidden.Controls.Add(this.txtParentSeq);
            this.grpHidden.Controls.Add(this.label42);
            this.grpHidden.Controls.Add(this.txtChildSeq);
            this.grpHidden.Location = new System.Drawing.Point(17, 514);
            this.grpHidden.Name = "grpHidden";
            this.grpHidden.Size = new System.Drawing.Size(493, 164);
            this.grpHidden.TabIndex = 71;
            this.grpHidden.TabStop = false;
            this.grpHidden.Text = "非表示コントロール群";
            this.grpHidden.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 20);
            this.label12.TabIndex = 119;
            this.label12.Text = "親SEQ";
            // 
            // txtParentSeq
            // 
            this.txtParentSeq.Location = new System.Drawing.Point(88, 31);
            this.txtParentSeq.MaxLength = 3;
            this.txtParentSeq.Name = "txtParentSeq";
            this.txtParentSeq.Size = new System.Drawing.Size(46, 28);
            this.txtParentSeq.TabIndex = 1;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(25, 63);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(57, 20);
            this.label42.TabIndex = 123;
            this.label42.Text = "子SEQ";
            // 
            // txtChildSeq
            // 
            this.txtChildSeq.Location = new System.Drawing.Point(88, 60);
            this.txtChildSeq.MaxLength = 20;
            this.txtChildSeq.Name = "txtChildSeq";
            this.txtChildSeq.Size = new System.Drawing.Size(46, 28);
            this.txtChildSeq.TabIndex = 4;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(140, 33);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(67, 20);
            this.label43.TabIndex = 126;
            this.label43.Text = "加SEQ1";
            // 
            // txtProcessingSeq1
            // 
            this.txtProcessingSeq1.Location = new System.Drawing.Point(213, 31);
            this.txtProcessingSeq1.MaxLength = 3;
            this.txtProcessingSeq1.Name = "txtProcessingSeq1";
            this.txtProcessingSeq1.Size = new System.Drawing.Size(46, 28);
            this.txtProcessingSeq1.TabIndex = 124;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(140, 63);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 20);
            this.label44.TabIndex = 127;
            this.label44.Text = "加SEQ2";
            // 
            // txtProcessingSeq2
            // 
            this.txtProcessingSeq2.Location = new System.Drawing.Point(213, 60);
            this.txtProcessingSeq2.MaxLength = 20;
            this.txtProcessingSeq2.Name = "txtProcessingSeq2";
            this.txtProcessingSeq2.Size = new System.Drawing.Size(46, 28);
            this.txtProcessingSeq2.TabIndex = 125;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(140, 91);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 20);
            this.label45.TabIndex = 130;
            this.label45.Text = "加SEQ3";
            // 
            // txtProcessingSeq3
            // 
            this.txtProcessingSeq3.Location = new System.Drawing.Point(213, 89);
            this.txtProcessingSeq3.MaxLength = 3;
            this.txtProcessingSeq3.Name = "txtProcessingSeq3";
            this.txtProcessingSeq3.Size = new System.Drawing.Size(46, 28);
            this.txtProcessingSeq3.TabIndex = 128;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(140, 121);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(67, 20);
            this.label46.TabIndex = 131;
            this.label46.Text = "加SEQ4";
            // 
            // txtProcessingSeq4
            // 
            this.txtProcessingSeq4.Location = new System.Drawing.Point(213, 118);
            this.txtProcessingSeq4.MaxLength = 20;
            this.txtProcessingSeq4.Name = "txtProcessingSeq4";
            this.txtProcessingSeq4.Size = new System.Drawing.Size(46, 28);
            this.txtProcessingSeq4.TabIndex = 129;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(269, 33);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(67, 20);
            this.label47.TabIndex = 133;
            this.label47.Text = "加SEQ5";
            // 
            // txtProcessingSeq5
            // 
            this.txtProcessingSeq5.Location = new System.Drawing.Point(342, 30);
            this.txtProcessingSeq5.MaxLength = 20;
            this.txtProcessingSeq5.Name = "txtProcessingSeq5";
            this.txtProcessingSeq5.Size = new System.Drawing.Size(46, 28);
            this.txtProcessingSeq5.TabIndex = 132;
            // 
            // DE0050_ShohinDetailDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 761);
            this.Controls.Add(this.pnlContent);
            this.Controls.Add(this.panel3);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.Name = "DE0050_ShohinDetailDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DE0050 - 商品詳細";
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.pnlContent.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBDCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numInchDia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGBCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGBSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSaddleCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSaddleSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewCount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewSize3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewCount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewSize2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numScrewSize1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeldingCount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeldingSize2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeldingCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeldingSize1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numShikiri)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numShikiriRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRiekiRate)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grpHidden.ResumeLayout(false);
            this.grpHidden.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel pnlContent;
        public System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.NumericUpDown numPrice;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.NumericUpDown numShikiri;
        public System.Windows.Forms.NumericUpDown numShikiriRate;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.NumericUpDown numRiekiRate;
        public System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.ComboBox cmbSize1;
        public System.Windows.Forms.ComboBox cmbSize2;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox cmbSize3;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cmbSize4;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Button btnSelectParentCD;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtParentCD;
        public System.Windows.Forms.TextBox txtParentCDName;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtChildCD;
        public System.Windows.Forms.Button btnSelectChildCD;
        public System.Windows.Forms.TextBox txtChildCDName;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtShohinCD;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtSizeCD;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.NumericUpDown numWeldingCount2;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.NumericUpDown numWeldingSize2;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.NumericUpDown numWeldingCount1;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.NumericUpDown numWeldingSize1;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.NumericUpDown numBDCount;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.NumericUpDown numInchDia;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.NumericUpDown numGBCount;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.NumericUpDown numGBSize;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.NumericUpDown numSaddleCount;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.NumericUpDown numSaddleSize;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.NumericUpDown numScrewCount3;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.NumericUpDown numScrewSize3;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.NumericUpDown numScrewCount2;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.NumericUpDown numScrewSize2;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.NumericUpDown numScrewCount1;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.NumericUpDown numScrewSize1;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Button btnSelectProcessingCD1;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.TextBox txtProcessingChildCD1;
        public System.Windows.Forms.TextBox txtProcessingChildCDName1;
        public System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.Label label40;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.Button btnSelectProcessingCD5;
        public System.Windows.Forms.TextBox txtProcessingChildCD5;
        public System.Windows.Forms.TextBox txtProcessingChildCDName5;
        public System.Windows.Forms.Button btnSelectProcessingCD4;
        public System.Windows.Forms.TextBox txtProcessingChildCD4;
        public System.Windows.Forms.TextBox txtProcessingChildCDName4;
        public System.Windows.Forms.Button btnSelectProcessingCD3;
        public System.Windows.Forms.TextBox txtProcessingChildCD3;
        public System.Windows.Forms.TextBox txtProcessingChildCDName3;
        public System.Windows.Forms.Button btnSelectProcessingCD2;
        public System.Windows.Forms.TextBox txtProcessingChildCD2;
        public System.Windows.Forms.TextBox txtProcessingChildCDName2;
        public System.Windows.Forms.TextBox txtTaishoYM;
        public System.Windows.Forms.GroupBox grpHidden;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox txtParentSeq;
        public System.Windows.Forms.Label label42;
        public System.Windows.Forms.TextBox txtChildSeq;
        public System.Windows.Forms.Label label47;
        public System.Windows.Forms.TextBox txtProcessingSeq5;
        public System.Windows.Forms.Label label45;
        public System.Windows.Forms.TextBox txtProcessingSeq3;
        public System.Windows.Forms.Label label46;
        public System.Windows.Forms.TextBox txtProcessingSeq4;
        public System.Windows.Forms.Label label43;
        public System.Windows.Forms.TextBox txtProcessingSeq1;
        public System.Windows.Forms.Label label44;
        public System.Windows.Forms.TextBox txtProcessingSeq2;
    }
}