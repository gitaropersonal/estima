﻿using Estima.Forms.Dialog.DE0050.Business;
using EstimaLib.Dto;
using EstimaLib.Util;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0050
{
    public partial class DE0050_ShohinDetailDialog : Form
    {
        #region Property
        /// <summary>
        /// 商品コード
        /// </summary>
        public string ShohinCD
        {
            get
            {
                return this.txtShohinCD.Text;
            }
        }
        /// <summary>
        /// 対象年月
        /// </summary>
        public string TaishoYM
        {
            get
            {
                return this.txtTaishoYM.Text;
            }
        }
        /// <summary>
        /// 定価
        /// </summary>
        public int? Price
        {
            get
            {
                return this.ToInteger(this.numPrice.Text);
            }
        }
        /// <summary>
        /// 親コード
        /// </summary>
        public string ParentCD
        {
            get
            {
                return this.txtParentCD.Text;
            }
        }
        /// <summary>
        /// 親コードSEQ
        /// </summary>
        public string ParentSeq
        {
            get
            {
                return this.txtParentSeq.Text;
            }
        }
        /// <summary>
        /// 親コード名
        /// </summary>
        public string ParentCDName
        {
            get
            {
                return this.txtParentCDName.Text;
            }
        }
        /// <summary>
        /// 子商品コード
        /// </summary>
        public string ChildCD
        {
            get
            {
                return this.txtChildCD.Text;
            }
        }
        /// <summary>
        /// 子商品コードSEQ
        /// </summary>
        public string ChildSeq
        {
            get
            {
                return this.txtChildSeq.Text;
            }
        }
        /// <summary>
        /// 子商品コード名
        /// </summary>
        public string ChildCDName
        {
            get
            {
                return this.txtChildCDName.Text;
            }
        }
        /// <summary>
        /// サイズコード
        /// </summary>
        public string SizeCD
        {
            get
            {
                return this.txtSizeCD.Text;
            }
        }
        /// <summary>
        /// サイズ1
        /// </summary>
        public string Size1
        {
            get
            {
                return this.cmbSize1.Text;
            }
        }
        /// <summary>
        /// サイズ2
        /// </summary>
        public string Size2
        {
            get
            {
                return this.cmbSize2.Text;
            }
        }
        /// <summary>
        /// サイズ3
        /// </summary>
        public string Size3
        {
            get
            {
                return this.cmbSize3.Text;
            }
        }
        /// <summary>
        /// サイズ4
        /// </summary>
        public string Size4
        {
            get
            {
                return this.cmbSize4.Text;
            }
        }
        /// <summary>
        /// サイズ
        /// </summary>
        public string ShohinSize
        {
            get
            {
                var sizeList = new List<string>();
                if (!string.IsNullOrEmpty(this.Size1)) { sizeList.Add(this.Size1); }
                if (!string.IsNullOrEmpty(this.Size2)) { sizeList.Add(this.Size2); }
                if (!string.IsNullOrEmpty(this.Size3)) { sizeList.Add(this.Size3); }
                if (!string.IsNullOrEmpty(this.Size4)) { sizeList.Add(this.Size4); }
                return string.Join("x", sizeList);
            }
        }
        /// <summary>
        /// 仕切率
        /// </summary>
        public float? ShikiriRate
        {
            get
            {
                return this.ToFloat(this.numShikiriRate.Text);
            }
        }
        /// <summary>
        /// 利益率
        /// </summary>
        public float? RiekiRate
        {
            get
            {
                return this.ToFloat(this.numRiekiRate.Text);
            }
        }
        /// <summary>
        /// 仕切
        /// </summary>
        public int? Shikiri
        {
            get
            {
                return CommonUtil.ToInteger(this.numShikiri.Text);
            }
        }
        /// <summary>
        /// 溶接サイズ１
        /// </summary>
        public float? WeldingSize1
        {
            get
            {
                return this.ToFloat(this.numWeldingSize1.Text);
            }
        }
        /// <summary>
        /// 溶接サイズ２
        /// </summary>
        public float? WeldingSize2
        {
            get
            {
                return this.ToFloat(this.numWeldingSize2.Text);
            }
        }
        /// <summary>
        /// ネジサイズ１
        /// </summary>
        public float? ScrewSize1
        {
            get
            {
                return this.ToFloat(this.numScrewSize1.Text);
            }
        }
        /// <summary>
        /// ネジサイズ２
        /// </summary>
        public float? ScrewSize2
        {
            get
            {
                return this.ToFloat(this.numScrewSize2.Text);
            }
        }
        /// <summary>
        /// ネジサイズ３
        /// </summary>
        public float? ScrewSize3
        {
            get
            {
                return this.ToFloat(this.numScrewSize3.Text);
            }
        }
        /// <summary>
        /// サドルサイズ
        /// </summary>
        public float? SaddleSize
        {
            get
            {
                return this.ToFloat(this.numSaddleSize.Text);
            }
        }
        /// <summary>
        /// GBサイズ
        /// </summary>
        public float? GBSize
        {
            get
            {
                return this.ToFloat(this.numGBSize.Text);
            }
        }
        /// <summary>
        /// 溶接数１
        /// </summary>
        public int? WeldingCount1
        {
            get
            {
                return this.ToInteger(this.numWeldingCount1.Text);
            }
        }
        /// <summary>
        /// 溶接数２
        /// </summary>
        public int? WeldingCount2
        {
            get
            {
                return this.ToInteger(this.numWeldingCount2.Text);
            }
        }
        /// <summary>
        /// ネジ数１
        /// </summary>
        public int? ScrewCount1
        {
            get
            {
                return this.ToInteger(this.numScrewCount1.Text);
            }
        }
        /// <summary>
        /// ネジ数２
        /// </summary>
        public int? ScrewCount2
        {
            get
            {
                return this.ToInteger(this.numScrewCount2.Text);
            }
        }
        /// <summary>
        /// ネジ数３
        /// </summary>
        public int? ScrewCount3
        {
            get
            {
                return this.ToInteger(this.numScrewCount3.Text);
            }
        }
        /// <summary>
        /// サドル数
        /// </summary>
        public int? SaddleCount
        {
            get
            {
                return this.ToInteger(this.numSaddleCount.Text);
            }
        }
        /// <summary>
        /// GB数
        /// </summary>
        public int? GBCount
        {
            get
            {
                return this.ToInteger(this.numGBCount.Text);
            }
        }
        /// <summary>
        /// インチダイア
        /// </summary>
        public float? InchDia
        {
            get
            {
                return this.ToFloat(this.numInchDia.Text);
            }
        }
        /// <summary>
        /// BD数
        /// </summary>
        public int? BDCount
        {
            get
            {
                return this.ToInteger(this.numBDCount.Text);
            }
        }
        /// <summary>
        /// 加工費子商品コード１
        /// </summary>
        public string ProcessingChildCD1
        {
            get
            {
                return this.txtProcessingChildCD1.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コード２
        /// </summary>
        public string ProcessingChildCD2
        {
            get
            {
                return this.txtProcessingChildCD2.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コード３
        /// </summary>
        public string ProcessingChildCD3
        {
            get
            {
                return this.txtProcessingChildCD3.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コード４
        /// </summary>
        public string ProcessingChildCD4
        {
            get
            {
                return this.txtProcessingChildCD4.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コード５
        /// </summary>
        public string ProcessingChildCD5
        {
            get
            {
                return this.txtProcessingChildCD5.Text;
            }
        }

        /// <summary>
        /// 加工費子商品コードSEQ１
        /// </summary>
        public string ProcessingSeq1
        {
            get
            {
                return this.txtProcessingSeq1.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コードSEQ２
        /// </summary>
        public string ProcessingSeq2
        {
            get
            {
                return this.txtProcessingSeq2.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コードSEQ３
        /// </summary>
        public string ProcessingSeq3
        {
            get
            {
                return this.txtProcessingSeq3.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コードSEQ４
        /// </summary>
        public string ProcessingSeq4
        {
            get
            {
                return this.txtProcessingSeq4.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コードSEQ５
        /// </summary>
        public string ProcessingSeq5
        {
            get
            {
                return this.txtProcessingSeq5.Text;
            }
        }

        /// <summary>
        /// 加工費子商品コードサマリ
        /// </summary>
        public string ProcessingChildCDs
        {
            get
            {
                var processingCDList = new List<string>();
                if (!string.IsNullOrEmpty(this.ProcessingChildCD1)) { processingCDList.Add(this.ProcessingChildCD1); }
                if (!string.IsNullOrEmpty(this.ProcessingChildCD2)) { processingCDList.Add(this.ProcessingChildCD2); }
                if (!string.IsNullOrEmpty(this.ProcessingChildCD3)) { processingCDList.Add(this.ProcessingChildCD3); }
                if (!string.IsNullOrEmpty(this.ProcessingChildCD4)) { processingCDList.Add(this.ProcessingChildCD4); }
                if (!string.IsNullOrEmpty(this.ProcessingChildCD5)) { processingCDList.Add(this.ProcessingChildCD5); }
                if (processingCDList.Any())
                {
                    string cds = processingCDList.First();
                    if (1 < processingCDList.Count)
                    {
                        cds = $"{cds} 他";
                    }
                    return cds;
                }
                else
                {
                    return "なし";
                }
            }
        }
        /// <summary>
        /// 加工費子商品コード名１
        /// </summary>
        public string ProcessingChildCDName1
        {
            get
            {
                return this.txtProcessingChildCDName1.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コード名２
        /// </summary>
        public string ProcessingChildCDName2
        {
            get
            {
                return this.txtProcessingChildCDName2.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コード名３
        /// </summary>
        public string ProcessingChildCDName3
        {
            get
            {
                return this.txtProcessingChildCDName3.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コード名４
        /// </summary>
        public string ProcessingChildCDName4
        {
            get
            {
                return this.txtProcessingChildCDName4.Text;
            }
        }
        /// <summary>
        /// 加工費子商品コード名５
        /// </summary>
        public string ProcessingChildCDName5
        {
            get
            {
                return this.txtProcessingChildCDName5.Text;
            }
        }
        /// <summary>
        /// 見積書・請求書で使用されているか？
        /// </summary>
        public bool UsedByEstima { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="contentsDto"></param>
        /// <param name="readOnly"></param>
        public DE0050_ShohinDetailDialog(
              LoginInfoDto loginInfo
            , DE0050_ContentsDto contentsDto
            , bool readOnly
            )
        {
            this.InitializeComponent();
            try
            {
                this.UsedByEstima = contentsDto.UsedByEstima;
                new DE0050_FormLogic(
                      this
                    , loginInfo
                    , contentsDto
                    , readOnly
                );
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
                this.Close();
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// 数値変換（int）
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private int? ToInteger(object o)
        {
            if (o == null)
            {
                return null;
            }
            if (string.IsNullOrEmpty(o.ToString()))
            {
                return null;
            }
            return CommonUtil.ToInteger(o);
        }
        /// <summary>
        /// 数値変換（float）
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private float? ToFloat(object o)
        {
            if (o == null)
            {
                return null;
            }
            if (string.IsNullOrEmpty(o.ToString()))
            {
                return null;
            }
            return CommonUtil.ToFloat(o);
        }
        #endregion
    }
}
