﻿using Estima.Forms.Application.MST.MST0020;
using Estima.Forms.Application.MST.MST0030;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0050.Business
{
    public class DE0050_FormLogic
    {
        #region Member
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// ボディパネル
        /// </summary>
        private DE0050_ShohinDetailDialog Body;
        /// <summary>
        /// ログイン情報
        /// </summary>
        private LoginInfoDto LoginInfo;
        /// <summary>
        /// 画面終了時に返すダイアログリザルト
        /// </summary>
        private DialogResult CurrDialogResult = DialogResult.Cancel;
        /// <summary>
        /// コンテンツ（初期化用）
        /// </summary>
        public DE0050_ContentsDto InitContentsDto = new DE0050_ContentsDto();
        /// <summary>
        /// 編集フラグ
        /// </summary>
        private bool EditedFlg = false;
        /// <summary>
        /// 読取専用フラグ
        /// </summary>
        private bool ReadOnly = false;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="loginInfo"></param>
        /// <param name="contentsDto"></param>
        /// <param name="readOnly"></param>
        public DE0050_FormLogic(
              DE0050_ShohinDetailDialog body
            , LoginInfoDto loginInfo
            , DE0050_ContentsDto contentsDto
            , bool readOnly
            )
        {
            this.Body = body;
            this.LoginInfo = loginInfo;
            this.ReadOnly = readOnly;
            this.InitContensMember(contentsDto);
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += (s, e) =>
            {
                CommonUtil.BtnClearClickEvent(
                      new Action(() => this.Init())
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了処理
            this.Body.FormClosing += this.ClosingEvent;
            // 更新ボタン押下
            this.Body.btnUpdate.Click += this.BtnUpdClickEvent;
            // 親コード検索ボタン押下
            this.Body.btnSelectParentCD.Click += this.BtnSelectParentCDClickEvent;
            // 子商品コード検索ボタン押下
            this.Body.btnSelectChildCD.Click += this.BtnSelectChildCDClickEvent;
            // 子商品コード（加工費）検索ボタン押下
            this.Body.btnSelectProcessingCD1.Click += (s, e) => this.BtnSelectProcessingChildCDClickEvent(this.Body.txtProcessingChildCD1, this.Body.txtProcessingChildCDName1, this.Body.txtProcessingSeq1);
            this.Body.btnSelectProcessingCD2.Click += (s, e) => this.BtnSelectProcessingChildCDClickEvent(this.Body.txtProcessingChildCD2, this.Body.txtProcessingChildCDName2, this.Body.txtProcessingSeq2);
            this.Body.btnSelectProcessingCD3.Click += (s, e) => this.BtnSelectProcessingChildCDClickEvent(this.Body.txtProcessingChildCD3, this.Body.txtProcessingChildCDName3, this.Body.txtProcessingSeq3);
            this.Body.btnSelectProcessingCD4.Click += (s, e) => this.BtnSelectProcessingChildCDClickEvent(this.Body.txtProcessingChildCD4, this.Body.txtProcessingChildCDName4, this.Body.txtProcessingSeq4);
            this.Body.btnSelectProcessingCD5.Click += (s, e) => this.BtnSelectProcessingChildCDClickEvent(this.Body.txtProcessingChildCD5, this.Body.txtProcessingChildCDName5, this.Body.txtProcessingSeq5);
            // 親コードValidate
            this.Body.txtParentCD.Validated += this.TxtParentCDValidated;
            // 子商品コードValidate
            this.Body.txtChildCD.Validated += this.TxtChildCDValidated;
            // 価格Validate
            this.Body.numPrice.ValueChanged += (s, e) => this.CalcShikiri();
            this.Body.numShikiriRate.ValueChanged += (s, e) => this.CalcShikiri();
            this.Body.numRiekiRate.ValueChanged += (s, e) => this.CalcShikiri();
            // サイズValidate
            this.Body.numWeldingSize1.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numWeldingSize2.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numScrewSize1.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numScrewSize2.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numScrewSize3.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numSaddleSize.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numGBSize.ValueChanged += (s, e) => this.CalcBD();
            // 数Validate
            this.Body.numWeldingCount1.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numWeldingCount2.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numScrewCount1.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numScrewCount2.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numScrewCount3.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numSaddleCount.ValueChanged += (s, e) => this.CalcBD();
            this.Body.numGBCount.ValueChanged += (s, e) => this.CalcBD();
            // 子商品コード（加工費）Validate
            this.Body.txtProcessingChildCD1.Validated += (s, e) => this.TxtProcessingChildCDValidated((TextBox)s, this.Body.txtProcessingChildCDName1);
            this.Body.txtProcessingChildCD2.Validated += (s, e) => this.TxtProcessingChildCDValidated((TextBox)s, this.Body.txtProcessingChildCDName2);
            this.Body.txtProcessingChildCD3.Validated += (s, e) => this.TxtProcessingChildCDValidated((TextBox)s, this.Body.txtProcessingChildCDName3);
            this.Body.txtProcessingChildCD4.Validated += (s, e) => this.TxtProcessingChildCDValidated((TextBox)s, this.Body.txtProcessingChildCDName4);
            this.Body.txtProcessingChildCD5.Validated += (s, e) => this.TxtProcessingChildCDValidated((TextBox)s, this.Body.txtProcessingChildCDName5);
            // サイズコードValidate
            this.Body.txtSizeCD.Validated += this.TxtSizeCDValidated;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.O:
                        this.Body.btnUpdate.Focus();
                        this.Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// 親コードValidate
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void TxtParentCDValidated(
              object s
            , EventArgs e
            )
        {
            this.Body.txtParentCD.Validated -= this.TxtParentCDValidated;
            try
            {
                var selectedParentCD = this.CommonService.SelEntityParentCD(this.Body.txtParentCD.Text);
                if (string.IsNullOrEmpty(selectedParentCD.ParentCD))
                {
                    this.Body.txtParentCD.Text = string.Empty;
                    this.Body.txtParentCDName.Text = string.Empty;
                }
                else
                {
                    this.Body.txtParentCDName.Text = selectedParentCD.ParentCDName;
                }
                // 商品コードに結合
                this.JoinShohinSize();
            }
            finally
            {
                this.Body.txtParentCD.Validated += this.TxtParentCDValidated;
            }
        }
        /// <summary>
        /// 子商品コードValidate
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void TxtChildCDValidated(
              object s
            , EventArgs e
            )
        {
            this.Body.txtChildCD.Validated -= this.TxtChildCDValidated;
            try
            {
                var selectedChildCD = this.CommonService.SelEntityChildCD(this.Body.txtChildCD.Text);
                if (string.IsNullOrEmpty(selectedChildCD.ChildCD))
                {
                    this.Body.txtChildCD.Text = string.Empty;
                    this.Body.txtChildCDName.Text = string.Empty;
                }
                else
                {
                    this.Body.txtChildCDName.Text = selectedChildCD.ChildCDName;
                }
                // 商品コードに結合
                this.JoinShohinSize();
            }
            finally
            {
                this.Body.txtChildCD.Validated += this.TxtChildCDValidated;
            }
        }
        /// <summary>
        /// 子商品コード（加工費）Validate
        /// </summary>
        /// <param name="txtCD"></param>
        /// <param name="txtName"></param>
        private void TxtProcessingChildCDValidated(
              TextBox txtCD
            , TextBox txtName
            )
        {
            txtCD.Validated -= (s, e) => this.TxtProcessingChildCDValidated(txtCD, txtName);
            try
            {
                var selectedChildCD = this.CommonService.SelEntityChildCD(txtCD.Text);
                if (string.IsNullOrEmpty(selectedChildCD.ChildCD))
                {
                    txtCD.Text = string.Empty;
                    txtName.Text = string.Empty;
                }
                else
                {
                    txtName.Text = selectedChildCD.ChildCDName;
                }
            }
            finally
            {
                txtCD.Validated += (s, e) => this.TxtProcessingChildCDValidated(txtCD, txtName);
            }
        }
        /// <summary>
        /// サイズコードValidate
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void TxtSizeCDValidated(
              object s
            , EventArgs e
            )
        {
            this.Body.txtSizeCD.Validated -= this.TxtSizeCDValidated;
            try
            {
                // 商品コードに結合
                this.JoinShohinSize();
            }
            finally
            {
                this.Body.txtSizeCD.Validated += this.TxtSizeCDValidated;
            }
        }
        /// <summary>
        /// 親コード検索ボタン押下
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSelectParentCDClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                // 親コード画面表示
                var drMeisai = new MST0020_MstParentCD(
                      this.LoginInfo
                    , Enums.ScreenModeMstShohin.DialogSingleProcessing
                );
                if (drMeisai.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                this.Body.txtParentCD.Text = drMeisai.SelectedDtoList[0].ParentCD;
                this.Body.txtParentSeq.Text = drMeisai.SelectedDtoList[0].ParentSeq;
                this.Body.txtParentCDName.Text = drMeisai.SelectedDtoList[0].ParentCDName;

                // 商品コードに結合
                this.JoinShohinSize();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 子商品コード検索ボタン押下
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSelectChildCDClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                var drMeisai = new MST0030_MstChildCD(
                  this.LoginInfo
                , Enums.ScreenModeMstShohin.DialogSingleProcessing
            );
                if (drMeisai.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                this.Body.txtChildCD.Text = drMeisai.SelectedDtoList[0].ChildCD;
                this.Body.txtChildSeq.Text = drMeisai.SelectedDtoList[0].ChildSeq;
                this.Body.txtChildCDName.Text = drMeisai.SelectedDtoList[0].ChildCDName;

                // 商品コードに結合
                this.JoinShohinSize();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 子商品コード（加工費）検索ボタン押下
        /// </summary>
        /// <param name="txtCD"></param>
        /// <param name="txtName"></param>
        /// <param name="txtChildSeq"></param>
        private void BtnSelectProcessingChildCDClickEvent(
              TextBox txtCD
            , TextBox txtName
            , TextBox txtChildSeq
            )
        {
            try
            {
                var drMeisai = new MST0030_MstChildCD(
                      this.LoginInfo
                    , Enums.ScreenModeMstShohin.DialogSingleProcessing
                );
                if (drMeisai.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                txtCD.Text = drMeisai.SelectedDtoList[0].ChildCD;
                txtChildSeq.Text = drMeisai.SelectedDtoList[0].ChildSeq;
                txtName.Text = drMeisai.SelectedDtoList[0].ChildCDName;
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnUpdClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                // 更新時Validate
                if (!this.UpdValidate())
                {
                    return;
                }
                if (CommonUtil.ShowInfoMsgOKCancel(DE0050_Messages.MsgAskUpdate) != DialogResult.OK)
                {
                    return;
                }
                // 完了メッセージ
                CommonUtil.ShowInfoMsgOK(DE0050_Messages.MsgInfoFinishUpdate);
                // ダイアログリザルト
                this.CurrDialogResult = DialogResult.OK;
                // 画面を閉じる
                this.Body.Close();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                if (!this.ValidateUnUpdateDataExist())
                {
                    var drConfirm = CommonUtil.ShowInfoExcramationOKCancel(DE0050_Messages.MsgAskNotCommitedDataExist);
                    if (drConfirm != DialogResult.OK)
                    {
                        cancel = true;
                        return;
                    }
                }
                this.Body.DialogResult = this.CurrDialogResult;
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            if (this.CurrDialogResult == DialogResult.OK)
            {
                this.Body.DialogResult = this.CurrDialogResult;
                return;
            }
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                if (!this.ValidateUnUpdateDataExist())
                {
                    var drConfirm = CommonUtil.ShowInfoExcramationOKCancel(DE0050_Messages.MsgAskNotCommitedDataExist);
                    if (drConfirm != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                this.Body.DialogResult = this.CurrDialogResult;
                this.Body.Close();
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// 初期化用コンテンツに値をセット
        /// </summary>
        /// <param name="contentsDto"></param>
        private void InitContensMember(DE0050_ContentsDto contentsDto)
        {
            this.InitContentsDto = new DE0050_ContentsDto()
            {
                ShohinID = contentsDto.ShohinID,
                ParentCD = contentsDto.ParentCD,
                ParentCDName = contentsDto.ParentCDName,
                ChildCD = contentsDto.ChildCD,
                ChildCDName = contentsDto.ChildCDName,
                SizeCD = contentsDto.SizeCD,
                ShohinCD = contentsDto.ShohinCD,
                Size1 = contentsDto.Size1,
                Size2 = contentsDto.Size2,
                Size3 = contentsDto.Size3,
                Size4 = contentsDto.Size4,
                TaishoYM = contentsDto.TaishoYM,
                Price = contentsDto.Price,
                Shikiri = contentsDto.Shikiri,
                ShikiriRate = contentsDto.ShikiriRate,
                RiekiRate = contentsDto.RiekiRate,
                WeldingSize1 = contentsDto.WeldingSize1,
                WeldingSize2 = contentsDto.WeldingSize2,
                ScrewSize1 = contentsDto.ScrewSize1,
                ScrewSize2 = contentsDto.ScrewSize2,
                ScrewSize3 = contentsDto.ScrewSize3,
                SaddleSize = contentsDto.SaddleSize,
                GBSize = contentsDto.GBSize,
                WeldingCount1 = contentsDto.WeldingCount1,
                WeldingCount2 = contentsDto.WeldingCount2,
                ScrewCount1 = contentsDto.ScrewCount1,
                ScrewCount2 = contentsDto.ScrewCount2,
                ScrewCount3 = contentsDto.ScrewCount3,
                SaddleCount = contentsDto.SaddleCount,
                GBCount = contentsDto.GBCount,
                InchDia = contentsDto.InchDia,
                BDCount = contentsDto.BDCount,
                ProcessingChildCD1 = contentsDto.ProcessingChildCD1,
                ProcessingChildCD2 = contentsDto.ProcessingChildCD2,
                ProcessingChildCD3 = contentsDto.ProcessingChildCD3,
                ProcessingChildCD4 = contentsDto.ProcessingChildCD4,
                ProcessingChildCD5 = contentsDto.ProcessingChildCD5,
                ProcessingChildCDName1 = contentsDto.ProcessingChildCDName1,
                ProcessingChildCDName2 = contentsDto.ProcessingChildCDName2,
                ProcessingChildCDName3 = contentsDto.ProcessingChildCDName3,
                ProcessingChildCDName4 = contentsDto.ProcessingChildCDName4,
                ProcessingChildCDName5 = contentsDto.ProcessingChildCDName5,
                UsedByEstima = contentsDto.UsedByEstima,
            };
        }
        /// <summary>
        /// 商品コードに結合
        /// </summary>
        private void JoinShohinSize()
        {
            string shohinCD = string.Join("-", this.Body.ParentCD, this.Body.ChildCD);
            this.Body.txtShohinCD.Text = string.IsNullOrEmpty(this.Body.SizeCD)? shohinCD : $"{shohinCD}-{this.Body.SizeCD}";
        }
        /// <summary>
        /// 仕切率計算
        /// </summary>
        private void CalcShikiri()
        {
            var shikiri = EstimaUtil.CalcShikiri(
                  (int)this.Body.numPrice.Value
                , (float)this.Body.numShikiriRate.Value
                , (float)this.Body.numRiekiRate.Value
            );
            this.Body.numShikiri.Value = CommonUtil.ToInteger(shikiri);
            this.EditedFlg = true;
        }
        /// <summary>
        /// BD数計算
        /// </summary>
        private void CalcBD()
        {
            int weldingCount1 = CommonUtil.ToInteger(this.Body.numWeldingCount1.Value);
            int weldingCount2 = CommonUtil.ToInteger(this.Body.numWeldingCount2.Value);
            int screwCount1 = CommonUtil.ToInteger(this.Body.numScrewCount1.Value);
            int screwCount2 = CommonUtil.ToInteger(this.Body.numScrewCount2.Value);
            int screwCount3 = CommonUtil.ToInteger(this.Body.numScrewCount3.Value);
            int saddleCount = CommonUtil.ToInteger(this.Body.numSaddleCount.Value);
            int gbCount = CommonUtil.ToInteger(this.Body.numGBCount.Value);
            var bdCount = weldingCount1 + weldingCount2 + screwCount1 + screwCount2 + screwCount3 + saddleCount + gbCount;
            this.SetNumInteger(this.Body.numBDCount, bdCount);

            float weldingSize1 = CommonUtil.ToFloat(this.Body.numWeldingSize1.Value);
            float weldingSize2 = CommonUtil.ToFloat(this.Body.numWeldingSize2.Value);
            float screwSize1 = CommonUtil.ToFloat(this.Body.numScrewSize1.Value);
            float screwSize2 = CommonUtil.ToFloat(this.Body.numScrewSize2.Value);
            float screwSize3 = CommonUtil.ToFloat(this.Body.numScrewSize3.Value);
            float saddleSize = CommonUtil.ToFloat(this.Body.numSaddleSize.Value);
            float gbSize = CommonUtil.ToFloat(this.Body.numGBSize.Value);
            float inchDia = 0;
            inchDia += weldingSize1 * weldingCount1;
            inchDia += weldingSize2 * weldingCount2;
            inchDia += screwSize1 * screwCount1;
            inchDia += screwSize2 * screwCount2;
            inchDia += screwSize3 * screwCount3;
            inchDia += saddleSize * saddleCount;
            inchDia += gbSize * gbCount;
            this.SetNumFloat(this.Body.numInchDia, inchDia);
            this.EditedFlg = true;
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <returns></returns>
        private bool UpdValidate()
        {
            // 親コード
            if (!this.ValidateTxtEmpty(this.Body.txtParentCD, "親コード"))
            {
                return false;
            }
            if (!this.ValidateTxtExistMaster(this.Body.txtParentCD, this.Body.txtParentCDName, "親コード"))
            {
                return false;
            }
            // 子商品コード
            if (!this.ValidateTxtEmpty(this.Body.txtChildCD, "子商品コード"))
            {
                return false;
            }
            if (!this.ValidateTxtExistMaster(this.Body.txtChildCD, this.Body.txtChildCDName, "子商品コード"))
            {
                return false;
            }
            // サイズ
            if (this.Body.txtSizeCD.Text.Contains("-"))
            {
                this.Body.txtSizeCD.Focus();
                CommonUtil.ShowErrorMsg(DE0050_Messages.MsgErrorSizeCDContainsHyphen);
                return false;
            }
            // 対象年月
            if (!this.ValidateTxtEmpty(this.Body.txtTaishoYM, "対象年月"))
            {
                return false;
            }
            if (CommonUtil.ToDateTime(this.Body.txtTaishoYM.Text) == DateTime.MinValue)
            {
                this.Body.txtTaishoYM.Focus();
                CommonUtil.ShowErrorMsg(string.Format(DE0050_Messages.MsgErrorMissEmpty, "対象年月"));
                return false;
            }
            // 加工費子商品コードがマスタに存在するか？
            if (!this.ValidateTxtExistMasterProcessingChildCDs())
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// テキストボックスValidate（空文字）
        /// </summary>
        /// <param name="txt"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        private bool ValidateTxtEmpty(
              TextBox txt
            , string controlName
            )
        {
            if (string.IsNullOrEmpty(txt.Text))
            {
                txt.Focus();
                CommonUtil.ShowErrorMsg(string.Format(DE0050_Messages.MsgErrorMissEmpty, controlName));
                return false;
            }
            return true;
        }
        /// <summary>
        /// 加工費子商品コードがマスタに存在するか？
        /// </summary>
        /// <returns></returns>
        private bool ValidateTxtExistMasterProcessingChildCDs()
        {
            const string controlName = "子商品コード（加工費）";
            if (!this.ValidateTxtExistMaster(this.Body.txtProcessingChildCD1, this.Body.txtProcessingChildCDName1, $"{controlName}1"))
            {
                return false;
            }
            if (!this.ValidateTxtExistMaster(this.Body.txtProcessingChildCD2, this.Body.txtProcessingChildCDName2, $"{controlName}2"))
            {
                return false;
            }
            if (!this.ValidateTxtExistMaster(this.Body.txtProcessingChildCD3, this.Body.txtProcessingChildCDName3, $"{controlName}3"))
            {
                return false;
            }
            if (!this.ValidateTxtExistMaster(this.Body.txtProcessingChildCD4, this.Body.txtProcessingChildCDName4, $"{controlName}4"))
            {
                return false;
            }
            if (!this.ValidateTxtExistMaster(this.Body.txtProcessingChildCD5, this.Body.txtProcessingChildCDName5, $"{controlName}5"))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// テキストボックスValidate（マスタ存在）
        /// </summary>
        /// <param name="txt"></param>
        /// <param name="controlName"></param>
        /// <returns></returns>
        private bool ValidateTxtExistMaster(
              TextBox txtCD
            , TextBox txtName
            , string controlName
            )
        {
            if (string.IsNullOrEmpty(txtCD.Text))
            {
                return true;
            }
            if (string.IsNullOrEmpty(txtName.Text))
            {
                txtCD.Focus();
                CommonUtil.ShowErrorMsg(string.Format(DE0050_Messages.MsgErrorNotExistMaster, controlName));
                return false;
            }
            return true;
        }
        /// <summary>
        /// 更新されていない項目が存在する？
        /// </summary>
        /// <returns></returns>
        private bool ValidateUnUpdateDataExist()
        {
            return !this.EditedFlg;
        }
        /// <summary>
        /// 数値コントロールに値セット（小数）
        /// </summary>
        /// <param name="num"></param>
        /// <param name="val"></param>
        private void SetNumFloat(
              NumericUpDown num
            , float? val
            )
        {
            if (val == null || val.Value == 0)
            {
                num.Value = 0;
                num.Text = string.Empty;
                return;
            }
            num.Value = (decimal)val.Value;
        }
        /// <summary>
        /// 数値コントロールに値セット（整数）
        /// </summary>
        /// <param name="num"></param>
        /// <param name="val"></param>
        private void SetNumInteger(
              NumericUpDown num
            , int? val
            )
        {
            if (val == null || val.Value == 0)
            {
                num.Value = 0;
                num.Text = string.Empty;
                return;
            }
            num.Value = (decimal)val.Value;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init()
        {
            // 商品コード
            this.Body.txtParentCD.Text = this.InitContentsDto.ParentCD;
            this.Body.txtParentCDName.Text = this.InitContentsDto.ParentCDName;
            this.Body.txtChildCD.Text = this.InitContentsDto.ChildCD;
            this.Body.txtChildCDName.Text = this.InitContentsDto.ChildCDName;
            this.Body.txtSizeCD.Text = this.InitContentsDto.SizeCD;
            this.Body.txtShohinCD.Text = this.InitContentsDto.ShohinCD;
            // サイズ
            this.Body.cmbSize1.Items.Add(string.Empty);
            this.Body.cmbSize2.Items.Add(string.Empty);
            this.Body.cmbSize3.Items.Add(string.Empty);
            this.Body.cmbSize4.Items.Add(string.Empty);
            this.CommonService.SelMstSize().ForEach(n =>
            {
                if (!n.ShohinSizeName.Contains("X") && !n.ShohinSizeName.Contains("x"))
                {
                    this.Body.cmbSize1.Items.Add(n.ShohinSizeName);
                    this.Body.cmbSize2.Items.Add(n.ShohinSizeName);
                    this.Body.cmbSize3.Items.Add(n.ShohinSizeName);
                    this.Body.cmbSize4.Items.Add(n.ShohinSizeName);
                }
            });
            this.Body.cmbSize1.Text = this.InitContentsDto.Size1;
            this.Body.cmbSize2.Text = this.InitContentsDto.Size2;
            this.Body.cmbSize3.Text = this.InitContentsDto.Size3;
            this.Body.cmbSize4.Text = this.InitContentsDto.Size4;
            if (this.InitContentsDto.UsedByEstima)
            {
                this.Body.txtParentCD.Enabled = false;
                this.Body.btnSelectParentCD.Enabled = false;
                this.Body.txtChildCD.Enabled = false;
                this.Body.btnSelectChildCD.Enabled = false;
                this.Body.txtSizeCD.Enabled = false;
                this.Body.cmbSize1.Enabled = false;
                this.Body.cmbSize2.Enabled = false;
                this.Body.cmbSize3.Enabled = false;
                this.Body.cmbSize4.Enabled = false;
            }
            // 対象年月
            this.Body.txtTaishoYM.Text = this.InitContentsDto.TaishoYM;
            // 定価
            this.Body.numPrice.Value = CommonUtil.ToInteger(this.InitContentsDto.Price);
            this.Body.numShikiriRate.Value = (decimal)CommonUtil.ToFloat(this.InitContentsDto.ShikiriRate);
            this.Body.numRiekiRate.Value = (decimal)CommonUtil.ToFloat(this.InitContentsDto.RiekiRate);
            this.Body.numShikiri.Value = (decimal)CommonUtil.ToFloat(this.InitContentsDto.Shikiri);
            // BD
            this.SetNumFloat(this.Body.numWeldingSize1, this.InitContentsDto.WeldingSize1);
            this.SetNumFloat(this.Body.numWeldingSize2, this.InitContentsDto.WeldingSize2);
            this.SetNumFloat(this.Body.numScrewSize1, this.InitContentsDto.ScrewSize1);
            this.SetNumFloat(this.Body.numScrewSize2, this.InitContentsDto.ScrewSize2);
            this.SetNumFloat(this.Body.numScrewSize3, this.InitContentsDto.ScrewSize3);
            this.SetNumFloat(this.Body.numSaddleSize, this.InitContentsDto.SaddleSize);
            this.SetNumFloat(this.Body.numGBSize, this.InitContentsDto.GBSize);

            this.SetNumInteger(this.Body.numWeldingCount1, this.InitContentsDto.WeldingCount1);
            this.SetNumInteger(this.Body.numWeldingCount2, this.InitContentsDto.WeldingCount2);
            this.SetNumInteger(this.Body.numScrewCount1, this.InitContentsDto.ScrewCount1);
            this.SetNumInteger(this.Body.numScrewCount2, this.InitContentsDto.ScrewCount2);
            this.SetNumInteger(this.Body.numScrewCount3, this.InitContentsDto.ScrewCount3);
            this.SetNumInteger(this.Body.numSaddleCount, this.InitContentsDto.SaddleCount);
            this.SetNumInteger(this.Body.numGBCount, this.InitContentsDto.GBCount);
            this.CalcBD();
            // 加工費
            this.Body.txtProcessingChildCD1.Text = this.InitContentsDto.ProcessingChildCD1;
            this.Body.txtProcessingChildCD2.Text = this.InitContentsDto.ProcessingChildCD2;
            this.Body.txtProcessingChildCD3.Text = this.InitContentsDto.ProcessingChildCD3;
            this.Body.txtProcessingChildCD4.Text = this.InitContentsDto.ProcessingChildCD4;
            this.Body.txtProcessingChildCD5.Text = this.InitContentsDto.ProcessingChildCD5;

            this.Body.txtProcessingChildCDName1.Text = this.InitContentsDto.ProcessingChildCDName1;
            this.Body.txtProcessingChildCDName2.Text = this.InitContentsDto.ProcessingChildCDName2;
            this.Body.txtProcessingChildCDName3.Text = this.InitContentsDto.ProcessingChildCDName3;
            this.Body.txtProcessingChildCDName4.Text = this.InitContentsDto.ProcessingChildCDName4;
            this.Body.txtProcessingChildCDName5.Text = this.InitContentsDto.ProcessingChildCDName5;
            // フラグ
            this.EditedFlg = false;
            // 読取専用
            if (this.ReadOnly)
            {
                this.Body.pnlContent.Enabled = false;
                this.Body.btnClear.Enabled = false;
                this.Body.btnUpdate.Enabled = false;
            }
            // フォーカス
            this.Body.btnSelectParentCD.Focus();
        }
        #endregion
    }
}
