﻿namespace Estima.Forms.Dialog.DE0050.Business
{
    public class DE0050_ContentsDto
    {
        public string ShohinID { get; set; }
        public bool ChkSelect { get; set; }
        public string BtnShowDetail { get; set; }
        public string ShohinCD { get; set; }
        public string TaishoYM { get; set; }
        public int? Price { get; set; }
        public string ParentCD { get; set; }
        public string ParentSeq { get; set; }
        public string ParentCDName { get; set; }
        public string ChildCD { get; set; }
        public string ChildSeq { get; set; }
        public string ChildCDName { get; set; }
        public string SizeCD { get; set; }
        public string Size1 { get; set; }
        public string Size2 { get; set; }
        public string Size3 { get; set; }
        public string Size4 { get; set; }
        public string ShohinSize { get; set; }
        public float? ShikiriRate { get; set; }
        public float? RiekiRate { get; set; }
        public int? Shikiri { get; set; }
        public float? WeldingSize1 { get; set; }
        public float? WeldingSize2 { get; set; }
        public float? ScrewSize1 { get; set; }
        public float? ScrewSize2 { get; set; }
        public float? ScrewSize3 { get; set; }
        public float? SaddleSize { get; set; }
        public float? GBSize { get; set; }
        public int? WeldingCount1 { get; set; }
        public int? WeldingCount2 { get; set; }
        public int? ScrewCount1 { get; set; }
        public int? ScrewCount2 { get; set; }
        public int? ScrewCount3 { get; set; }
        public int? SaddleCount { get; set; }
        public int? GBCount { get; set; }
        public float? InchDia { get; set; }
        public int? BDCount { get; set; }
        public string ProcessingChildCD1 { get; set; }
        public string ProcessingChildCD2 { get; set; }
        public string ProcessingChildCD3 { get; set; }
        public string ProcessingChildCD4 { get; set; }
        public string ProcessingChildCD5 { get; set; }
        public string ProcessingSeq1 { get; set; }
        public string ProcessingSeq2 { get; set; }
        public string ProcessingSeq3 { get; set; }
        public string ProcessingSeq4 { get; set; }
        public string ProcessingSeq5 { get; set; }
        public string ProcessingChildCDName1 { get; set; }
        public string ProcessingChildCDName2 { get; set; }
        public string ProcessingChildCDName3 { get; set; }
        public string ProcessingChildCDName4 { get; set; }
        public string ProcessingChildCDName5 { get; set; }
        public bool UsedByEstima { get; set; }
    }
}
