﻿using Estima.Forms.Dialog.DE0000.Business;
using EstimaLib.Dto;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0000
{
    public partial class DE0000_LoginDialog : Form
    {
        /// <summary>
        /// ログイン情報
        /// </summary>
        public LoginInfoDto LoginInfo { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public DE0000_LoginDialog()
        {
            this.InitializeComponent();
            try
            {
                new DE0000_FormLogic(this);
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
