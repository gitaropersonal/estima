﻿using Estima.Forms.Dialog.DE0010;
using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0000.Business
{
    internal class DE0000_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private DE0000_LoginDialog Body { get; set; }
        /// <summary>
        /// ログイン情報
        /// </summary>
        public LoginInfoDto LoginInfo { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        internal DE0000_FormLogic(DE0000_LoginDialog body)
        {
            this.Body = body;
            this.Body.DialogResult = DialogResult.None;
            this.Body.txtPassWord.PasswordChar = '*';

            // コントロール背景色の初期化
            new InitControl(this.Body);

            // ログインボタン押下イベント
            this.Body.btnLogin.Click += this.BtnLoginClickEvent;
            // リンクラベル押下イベント
            this.Body.lnkChangePass.Click += this.LinkLabelEnter;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.P:
                        this.Body.lnkChangePass.Focus();
                        this.LinkLabelEnter(s, e);
                        break;
                    case Keys.O:
                        this.Body.btnLogin.Focus();
                        this.Body.btnLogin.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// ログインボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnLoginClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.txtTantoId.BackColor = Color.Empty;
            this.Body.txtPassWord.BackColor = Color.Empty;

            if (string.IsNullOrEmpty(this.Body.txtTantoId.Text))
            {
                this.Body.txtTantoId.Focus();
                this.Body.txtTantoId.BackColor = Colors.BackColorCellError;
                CommonUtil.ShowErrorMsg(string.Format(Messages.MsgErrorMissEmpty, this.Body.lblTantoID.Text));
                return;
            }
            if (string.IsNullOrEmpty(this.Body.txtPassWord.Text))
            {
                this.Body.txtPassWord.Focus();
                this.Body.txtPassWord.BackColor = Colors.BackColorCellError;
                CommonUtil.ShowErrorMsg(string.Format(Messages.MsgErrorMissEmpty, this.Body.lblPassWord.Text));
                return;
            }
            try
            {
                // ログイン情報取得
                //var loginInfo = new LoginService().SelLoginInfo(_DB_CONN_STRING, this.Body.txtTantoId.Text, this.Body.txtPassWord.Text);
                this.LoginInfo = this.GetDemoData();
                this.Body.LoginInfo = this.LoginInfo;

                // 照合処理
                if (string.IsNullOrEmpty(this.LoginInfo.TantoID))
                {
                    this.Body.txtTantoId.Focus();
                    this.Body.txtTantoId.BackColor = Colors.BackColorCellError;
                    this.Body.txtPassWord.BackColor = Colors.BackColorCellError;
                    CommonUtil.ShowErrorMsg(Messages.MsgErrorWrongPassword);
                    return;
                }
                // 照合処理がOKなら、画面を閉じる
                this.Body.DialogResult = DialogResult.OK;
                this.Body.Close();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// リンクラベル押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void LinkLabelEnter(
              object s
            , EventArgs e
            )
        {
            var dialog = new DE0010_ChangePassDialog();
            this.Body.Visible = false;
            new DE0010_ChangePassDialog().ShowDialog();
            this.Body.Visible = true;
        }
        #endregion

        #region Business
        /// <summary>
        /// デモデータ取得
        /// </summary>
        /// <returns></returns>
        private LoginInfoDto GetDemoData()
        {
            return new LoginInfoDto()
            {
                TantoID = "1",
                KengenKbn = 10,
                Seq = 1,
                TantoName = "担当者001",
                BushoCD = "001001"
            };
        }
        #endregion
    }
}
