﻿namespace Estima.Forms.Dialog.DE0000
{
    partial class DE0000_LoginDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlExecute = new System.Windows.Forms.Panel();
            this.txtPassWord = new System.Windows.Forms.TextBox();
            this.txtTantoId = new System.Windows.Forms.TextBox();
            this.lblPassWord = new System.Windows.Forms.Label();
            this.pnlFootor = new System.Windows.Forms.Panel();
            this.lnkChangePass = new System.Windows.Forms.LinkLabel();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblTantoID = new System.Windows.Forms.Label();
            this.pnlExecute.SuspendLayout();
            this.pnlFootor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExecute
            // 
            this.pnlExecute.Controls.Add(this.txtPassWord);
            this.pnlExecute.Controls.Add(this.txtTantoId);
            this.pnlExecute.Controls.Add(this.lblPassWord);
            this.pnlExecute.Controls.Add(this.pnlFootor);
            this.pnlExecute.Controls.Add(this.lblTantoID);
            this.pnlExecute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlExecute.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlExecute.Location = new System.Drawing.Point(0, 0);
            this.pnlExecute.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlExecute.Name = "pnlExecute";
            this.pnlExecute.Size = new System.Drawing.Size(530, 207);
            this.pnlExecute.TabIndex = 1;
            // 
            // txtPassWord
            // 
            this.txtPassWord.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPassWord.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtPassWord.Location = new System.Drawing.Point(145, 68);
            this.txtPassWord.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPassWord.MaxLength = 16;
            this.txtPassWord.Name = "txtPassWord";
            this.txtPassWord.Size = new System.Drawing.Size(266, 28);
            this.txtPassWord.TabIndex = 1;
            // 
            // txtTantoId
            // 
            this.txtTantoId.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoId.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTantoId.Location = new System.Drawing.Point(145, 39);
            this.txtTantoId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTantoId.MaxLength = 6;
            this.txtTantoId.Name = "txtTantoId";
            this.txtTantoId.Size = new System.Drawing.Size(266, 28);
            this.txtTantoId.TabIndex = 0;
            // 
            // lblPassWord
            // 
            this.lblPassWord.AutoSize = true;
            this.lblPassWord.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPassWord.Location = new System.Drawing.Point(55, 72);
            this.lblPassWord.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPassWord.Name = "lblPassWord";
            this.lblPassWord.Size = new System.Drawing.Size(71, 20);
            this.lblPassWord.TabIndex = 234;
            this.lblPassWord.Text = "パスワード";
            // 
            // pnlFootor
            // 
            this.pnlFootor.Controls.Add(this.lnkChangePass);
            this.pnlFootor.Controls.Add(this.btnLogin);
            this.pnlFootor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFootor.Location = new System.Drawing.Point(0, 131);
            this.pnlFootor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlFootor.Name = "pnlFootor";
            this.pnlFootor.Size = new System.Drawing.Size(530, 76);
            this.pnlFootor.TabIndex = 200;
            // 
            // lnkChangePass
            // 
            this.lnkChangePass.AutoSize = true;
            this.lnkChangePass.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lnkChangePass.Location = new System.Drawing.Point(55, 13);
            this.lnkChangePass.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lnkChangePass.Name = "lnkChangePass";
            this.lnkChangePass.Size = new System.Drawing.Size(145, 20);
            this.lnkChangePass.TabIndex = 100;
            this.lnkChangePass.TabStop = true;
            this.lnkChangePass.Text = "パスワード変更（P）";
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnLogin.Location = new System.Drawing.Point(339, 16);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(138, 47);
            this.btnLogin.TabIndex = 10;
            this.btnLogin.Text = "ログイン（O）";
            this.btnLogin.UseVisualStyleBackColor = true;
            // 
            // lblTantoID
            // 
            this.lblTantoID.AutoSize = true;
            this.lblTantoID.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoID.Location = new System.Drawing.Point(55, 43);
            this.lblTantoID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTantoID.Name = "lblTantoID";
            this.lblTantoID.Size = new System.Drawing.Size(75, 20);
            this.lblTantoID.TabIndex = 233;
            this.lblTantoID.Text = "担当者ID";
            // 
            // DE0000_LoginDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 207);
            this.Controls.Add(this.pnlExecute);
            this.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "DE0000_LoginDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ログイン";
            this.pnlExecute.ResumeLayout(false);
            this.pnlExecute.PerformLayout();
            this.pnlFootor.ResumeLayout(false);
            this.pnlFootor.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel pnlExecute;
        public System.Windows.Forms.TextBox txtPassWord;
        public System.Windows.Forms.TextBox txtTantoId;
        public System.Windows.Forms.Label lblPassWord;
        public System.Windows.Forms.Panel pnlFootor;
        public System.Windows.Forms.LinkLabel lnkChangePass;
        public System.Windows.Forms.Button btnLogin;
        public System.Windows.Forms.Label lblTantoID;
    }
}