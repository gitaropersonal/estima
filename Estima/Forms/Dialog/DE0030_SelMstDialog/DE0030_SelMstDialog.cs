﻿using Estima.Forms.Dialog.DE0030.Business;
using EstimaLib.Const;
using EstimaLib.Util;
using GadgetCommon.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0030
{
    public partial class DE0030_SelMstDialog : Form
    {
        /// <summary>
        /// 選択されたレコード
        /// </summary>
        public DE0030_GridDto SelectedRecord { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="mstType"></param>
        /// <param name="imeMoede"></param>
        /// <param name="onlySingleType"></param>
        public DE0030_SelMstDialog(
              Enums.MstType mstType
            , ImeMode imeMoede
            , bool onlySingleType = false
            )
        {
            this.InitializeComponent();
            try
            {
                new DE0030_FormLogic(this, mstType, imeMoede, onlySingleType);
            }
            catch (Exception ex)
            {
                string msg = ex.Message + Environment.NewLine + ex.StackTrace;
                CommonUtil.ShowErrorMsg(msg);
                EstimaUtil.AddLogError(ex, this.Text);
            }
        }
    }
}
