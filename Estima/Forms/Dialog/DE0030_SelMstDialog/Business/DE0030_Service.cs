﻿using EstimaLib.Const;
using EstimaLib.Util;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Estima.Forms.Dialog.DE0030.Business
{
    internal class DE0030_Service
    {
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="name"></param>
        /// <param name="onlySingleSize"></param>
        /// <returns></returns>
        internal BindingList<DE0030_GridDto> GetGridDatas_MX05(
              string name
            , bool onlySingleSize
            )
        {
            string cmOutName = EstimaUtil.GetCmout(string.IsNullOrEmpty(name));
            string cmOutSingleSize = EstimaUtil.GetCmout(!onlySingleSize);
            var ret = new BindingList<DE0030_GridDto>();
            var list = new List<DE0030_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX05.ShohinSizeID   AS ID ");
            sb.AppendLine($",   MX05.ShohinSizeName AS ColName ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX05ShohinSize AS MX05 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutName}AND MX05.ShohinSizeName LIKE @name ");
            sb.AppendLine($"{cmOutSingleSize}AND MX05.ShohinSizeName NOT LIKE '%x%' ");
            sb.AppendLine($"{cmOutSingleSize}AND MX05.ShohinSizeName NOT LIKE '%X%' ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    MX05.ShohinSizeName ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@name", "%" + name + "%");
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new DE0030_GridDto();
                            dto.ID = CommonUtil.TostringNullForbid(dr[nameof(dto.ID)]);
                            dto.ColName = CommonUtil.TostringNullForbid(dr[nameof(dto.ColName)]);
                            dto.Status = Enums.EditStatus.Show;
                            list.Add(dto);
                        }
                    }
                }
            }
            list = list.OrderBy(n => n.ColName.Replace("X", "x").Count(x => x == 'x'))
                       .ThenBy(n => CommonUtil.CutAnAlpha(n.ColName.Replace("X", "x") + "xZZZxZZZ").Split('x')[0])
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ColName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[0]))
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ColName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[1]))
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ColName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[2]))
                       .ToList();
            list.ForEach(n => ret.Add(n));
            return ret;
        }
    }
}
