﻿using EstimaLib.Const;

namespace Estima.Forms.Dialog.DE0030.Business
{
    public class DE0030_GridDto
    {
        public DE0030_GridDto()
        {
            this.Status = Enums.EditStatus.None;
        }
        public string ID { get; set; }
        public string ColName { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
