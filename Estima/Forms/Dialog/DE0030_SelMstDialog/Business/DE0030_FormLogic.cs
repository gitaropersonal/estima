﻿using EstimaLib.Const;
using EstimaLib.Util;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0030.Business
{
    internal class DE0030_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private DE0030_SelMstDialog Body { get; set; }
        /// <summary>
        /// マスタ種別
        /// </summary>
        private Enums.MstType MstType = Enums.MstType.None;
        /// <summary>
        /// IMEモード
        /// </summary>
        private ImeMode IME { get; set; }
        /// <summary>
        /// 個別サービス
        /// </summary>
        private readonly DE0030_Service PersonalService = new DE0030_Service();
        /// <summary>
        /// 選択されたレコード
        /// </summary>
        public DE0030_GridDto SelectedRecord { get; set; }
        /// <summary>
        /// 列ヘッダーDto
        /// </summary>
        private static DE0030_GridDto CellHeaderDto = new DE0030_GridDto();
        /// <summary>
        /// 単一サイズのみ？
        /// </summary>
        private bool OnlySingleType = false;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="mstType"></param>
        /// <param name="imeMode"></param>
        internal DE0030_FormLogic(
              DE0030_SelMstDialog body
            , Enums.MstType mstType
            , ImeMode imeMode
            , bool onlySingleType
            )
        {
            this.Body = body;
            this.MstType = mstType;
            this.IME = imeMode;
            this.OnlySingleType = onlySingleType;
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += (s, e) => { CommonUtil.BtnClearClickEvent((new Action(() => this.Init()))); };
            // 終了ボタン押下
            this.Body.btnClose.Click += (s, e) => { this.Body.Close(); };
            // 検索ボタン押下
            this.Body.btnSearch.Click += this.BtnSearchClickEvent;
            // OKボタン押下
            this.Body.btnOK.Click += (s, e) =>
            {
                if (this.Body.dgvMaster.SelectedRows == null || this.Body.dgvMaster.SelectedRows.Count == 0)
                {
                    CommonUtil.ShowErrorMsg(DialogMessages.MsgErrorNotSelectedRow);
                    return;
                }
                this.Body.SelectedRecord = GridRowUtil<DE0030_GridDto>.GetRowModel(this.Body.dgvMaster.SelectedRows[0]);
                this.Body.DialogResult = DialogResult.OK;
                this.Body.Close();
            };
            // グリッドRowPostPaint
            this.Body.dgvMaster.RowPostPaint += this.GridRowPostPaintEvent;
            // グリッドCellDoubleClick
            this.Body.dgvMaster.CellDoubleClick += this.GridCellDoubleClickEvent;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        this.Body.btnSearch.Focus();
                        this.Body.btnSearch.PerformClick();
                        break;
                    case Keys.O:
                        this.Body.btnOK.Focus();
                        this.Body.btnOK.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// グリッドRowPostPaint
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridRowPostPaintEvent(
              object s
            , DataGridViewRowPostPaintEventArgs e
            )
        {
            GridRowUtil<DE0030_GridDto>.SetRowNum(s, e);
        }
        /// <summary>
        /// グリッドCellDoubleClick
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellDoubleClickEvent(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            ((DataGridView)s).Rows[e.RowIndex].Selected = true;
            this.Body.btnOK.Focus();
            this.Body.btnOK.PerformClick();
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSearchClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// 検索処理
        /// </summary>
        private void SearchLogic()
        {
            // グリッド初期化
            this.Body.dgvMaster.Rows.Clear();

            // グリッドデータ
            var bindingList = new BindingList<DE0030_GridDto>();
            CommonUtil.ShowProgressDialog((new Action(() =>
            {
                try
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.Start);
                    switch (this.MstType)
                    {
                        case Enums.MstType.ShohinSize:
                            bindingList = this.PersonalService.GetGridDatas_MX05(this.Body.txtName.Text, this.OnlySingleType);
                            break;
                    }
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.End);
                }
                catch (Exception ex)
                {
                    EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                    return;
                }
            })));
            if (!bindingList.Any())
            {
                CommonUtil.ShowErrorMsg(DE0030_Messages.MsgErrorNotExistDatas);
                return;
            }
            // グリッドデータセット
            this.Body.dgvMaster.DataSource = bindingList;

            // フォーカス
            this.Body.dgvMaster.Focus();

        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            // 検索条件
            this.Body.txtName.Text = string.Empty;
            this.Body.txtName.ImeMode = this.IME;

            // グリッド
            this.Body.dgvMaster.Rows.Clear();
            this.Body.dgvMaster.ReadOnly = true;
            this.Body.dgvMaster.AllowUserToAddRows = false;
            this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].HeaderText = Enums.MstTypes.First(n => n.Key == this.MstType).Value;
            this.Body.lblName.Text = Enums.MstTypes.First(n => n.Key == this.MstType).Value;
            switch (this.MstType)
            {
                case Enums.MstType.ShikiriRate:
                    this.Body.txtName.TextAlign = HorizontalAlignment.Right;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].Width = 130;
                    this.Body.txtName.Width = 90;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].HeaderText += "(%)";
                    this.Body.lblName.Text += "(%)";
                    break;
                case Enums.MstType.RiekiRate:
                    this.Body.txtName.TextAlign = HorizontalAlignment.Right;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].Width = 130;
                    this.Body.txtName.Width = 90;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].HeaderText += "(%)";
                    this.Body.lblName.Text += "(%)";
                    break;
                default:
                    this.Body.txtName.TextAlign = HorizontalAlignment.Left;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    break;

            }
            // 画面タイトル
            this.Body.Text = this.Body.Text.Replace("●●", Enums.MstTypes.First(n => n.Key == this.MstType).Value);

            // 検索
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
            // フォーカス
            this.Body.txtName.Focus();
        }
        #endregion
    }
}
