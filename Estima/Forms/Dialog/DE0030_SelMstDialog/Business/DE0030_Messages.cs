﻿namespace Estima.Forms.Dialog.DE0030.Business
{
    internal static class DE0030_Messages
    {
        internal const string MsgAskUpdate                   = "更新しますか？";
        internal const string MsgErrorForbidDelRow           = "この行は削除できません";
        internal const string MsgErrorDeleteRowJissekiExist  = "作業実績がある行は削除できません";
        internal const string MsgErrorNotSelectedRow         = "行を選択してください";
        internal const string MsgErrorNotExistDatas          = "対象のデータがありません";
        internal const string MsgInfoFinishUpdate            = "更新しました";
        internal const string MsgErrorNotSavedRowExist       = "入力内容が更新されていません";
        internal const string MsgErrorRequired               = "{0}を入力してください";
        internal const string MsgAskExistUpdateDatas         = "未更新のデータがありますが\r\nよろしいですか？";
        internal const string MsgErrorExistUpdateDatas       = "未更新のデータがあります";
        internal const string MsgErrorDouple                 = "{0}が重複しています";
        internal const string MsgErrorMissEmpty              = "有効な{0}を入力してください";
    }
}
