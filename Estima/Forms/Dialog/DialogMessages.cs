﻿namespace Estima.Forms.Dialog
{
    public static class DialogMessages
    {
        public const string MsgAskUpdate               = "更新しますか？";
        public const string MsgErrorDeletedRowSelected = "非表示扱いの行は実績登録できません";
        public const string MsgErrorMissEmpty          = "有効な{0}を入力してください";
        public const string MsgErrorNotChangedPassword = "パスワードが変更されていません";
        public const string MsgErrorUnmatchedPassword  = "パスワードが一致していません";
        public const string MsgErrorUsedPassword       = "過去のパスワードは使用できません";
        public const string MsgErrorWrongPassword      = "担当者IDまたはパスワードに誤りがあります";
        public const string MsgInfoFinishUpdate        = "更新しました";
        public const string MsgErrorNotSelectedRow     = "行を選択してください";
        public const string MsgErrorNotExistDatas      = "対象のデータがありません";

    }
}