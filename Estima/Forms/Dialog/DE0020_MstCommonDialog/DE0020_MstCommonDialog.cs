﻿using Estima.Forms.Dialog.DE0020.Business;
using EstimaLib.Const;
using EstimaLib.Util;
using System;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0020
{
    public partial class DE0020_MstCommonDialog : Form
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="mstType"></param>
        /// <param name="imeMode"></param>
        public DE0020_MstCommonDialog(
              Enums.MstType mstType
            , ImeMode imeMode
            )
        {
            this.InitializeComponent();
            try
            {
                new DE0020_FormLogic(this, mstType, imeMode);
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Text);
            }
        }
    }
}
