﻿using EstimaLib.Const;
using EstimaLib.Util;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Estima.Forms.Dialog.DE0020.Business
{
    internal class DE0020_Service
    {
        #region MX05ShohinSize
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal BindingList<DE0020_GridDto> GetGridDatas_MX05(string name)
        {
            string cmOutName = EstimaUtil.GetCmout(string.IsNullOrEmpty(name));
            var ret = new BindingList<DE0020_GridDto>();
            var list = new List<DE0020_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX05.ShohinSizeID   AS ID ");
            sb.AppendLine($",   MX05.ShohinSizeName AS ColName ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX05ShohinSize AS MX05 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutName}AND MX05.ShohinSizeName LIKE @name ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    MX05.ShohinSizeName ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.AddWithValue("@name", "%" + name + "%");
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new DE0020_GridDto();
                            dto.ID = CommonUtil.TostringNullForbid(dr[nameof(dto.ID)]);
                            dto.ColName = CommonUtil.TostringNullForbid(dr[nameof(dto.ColName)]);
                            dto.UsedByEstima = this.JudgeExistEstimaHavingSize(dto.ColName);
                            dto.Status = Enums.EditStatus.Show;
                            list.Add(dto);
                        }
                    }
                }
            }
            list = list.OrderBy(n => n.ColName.Replace("X", "x").Count(x => x == 'x'))
                       .ThenBy(n => CommonUtil.CutAnAlpha(n.ColName.Replace("X", "x") + "xZZZxZZZ").Split('x')[0])
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ColName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[0]))
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ColName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[1]))
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ColName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[2]))
                       .ToList();
            list.ForEach(n => ret.Add(n));
            return ret;
        }
        /// <summary>
        /// ID取得
        /// </summary>
        
        /// <returns></returns>
        internal int GetMaxID_MX05()
        {
            var ret = 0;

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MAX(MX05.ShohinSizeID) AS MaxID ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX05ShohinSize AS MX05 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// データ登録
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        internal void AddData_MX05(
              string id
            , string name
            )
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO MX05ShohinSize( ");
            sb.AppendLine($"    ShohinSizeID ");
            sb.AppendLine($",   ShohinSizeName ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @id ");
            sb.AppendLine($",   @name ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($") ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@id", id));
                    cmd.Parameters.Add(new MySqlParameter("@name", name));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// データ更新
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        internal void UpdData_MX05(
              string id
            , string name
            )
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"UPDATE ");
            sb.AppendLine($"    MX05ShohinSize ");
            sb.AppendLine($"SET ");
            sb.AppendLine($"    ShohinSizeName = @name ");
            sb.AppendLine($",   UpdIPAddress = '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   UpdHostName = '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   UpdDate = CURRENT_TIMESTAMP ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    ShohinSizeID = @id ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@id", id));
                    cmd.Parameters.Add(new MySqlParameter("@name", name));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// データ削除
        /// </summary>
        /// <param name="id"></param>
        internal void DelData_MX05(string id)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM ");
            sb.AppendLine($"    MX05ShohinSize ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    ShohinSizeID = @id ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 対象のサイズを含む商品データが存在するか？
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        internal bool JudgeExistEstimaHavingSize(string size)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"WITH ");
            sb.AppendLine($" Count1 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX05ShohinSize AS MX05 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX05.ShohinSizeName = MX03.Size1 ");
            sb.AppendLine($"    AND MX05.ShohinSizeName = @size ");
            sb.AppendLine($") ");
            sb.AppendLine($",Count2 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX05ShohinSize AS MX05 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX05.ShohinSizeName = MX03.Size2 ");
            sb.AppendLine($"    AND MX05.ShohinSizeName = @size ");
            sb.AppendLine($") ");
            sb.AppendLine($",Count3 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX05ShohinSize AS MX05 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX05.ShohinSizeName = MX03.Size3 ");
            sb.AppendLine($"    AND MX05.ShohinSizeName = @size ");
            sb.AppendLine($") ");
            sb.AppendLine($",Count4 AS( ");
            sb.AppendLine($"    SELECT COUNT(*) AS Count FROM MX03Shohin AS MX03 ");
            sb.AppendLine($"    INNER JOIN ");
            sb.AppendLine($"        MX05ShohinSize AS MX05 ");
            sb.AppendLine($"    ON ");
            sb.AppendLine($"        MX05.ShohinSizeName = MX03.Size4 ");
            sb.AppendLine($"    AND MX05.ShohinSizeName = @size ");
            sb.AppendLine($") ");
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    Count1.Count ");
            sb.AppendLine($"+   Count2.Count ");
            sb.AppendLine($"+   Count3.Count ");
            sb.AppendLine($"+   Count4.Count AS Count ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    Count1 ");
            sb.AppendLine($",   Count2 ");
            sb.AppendLine($",   Count3 ");
            sb.AppendLine($",   Count4 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@size", size) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return 0 < CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return false;
        }
        #endregion

        #region MX06AddressOffice
        /// <summary>
        /// グリッドデータ取得
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal BindingList<DE0020_GridDto> GetGridDatas_MX06(string name)
        {
            string cmOutName = EstimaUtil.GetCmout(string.IsNullOrEmpty(name));
            var ret = new BindingList<DE0020_GridDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX06.OfficeID   AS ID ");
            sb.AppendLine($",   MX06.OfficeName AS ColName ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX06AddressOffice AS MX06 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutName}MX06.OfficeName = @name ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    MX06.OfficeName ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@name", name));
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new DE0020_GridDto();
                            dto.ID = CommonUtil.TostringNullForbid(dr[nameof(dto.ID)]);
                            dto.ColName = CommonUtil.TostringNullForbid(dr[nameof(dto.ColName)]);
                            dto.UsedByEstima = false;
                            dto.Status = Enums.EditStatus.Show;
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// ID取得
        /// </summary>
        /// <returns></returns>
        internal int GetMaxID_MX06()
        {
            var ret = 0;

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MAX(MX06.OfficeID) AS MaxID ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX06AddressOffice AS MX06 ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// データ登録
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        internal void AddData_MX06(
              string id
            , string name
            )
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO MX06AddressOffice( ");
            sb.AppendLine($"    OfficeID ");
            sb.AppendLine($",   OfficeName ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @id ");
            sb.AppendLine($",   @name ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($") ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@id", id));
                    cmd.Parameters.Add(new MySqlParameter("@name", name));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// データ更新
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        internal void UpdData_MX06(
              string id
            , string name
            )
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"UPDATE ");
            sb.AppendLine($"    MX06AddressOffice ");
            sb.AppendLine($"SET ");
            sb.AppendLine($"    OfficeName = @name ");
            sb.AppendLine($",   UpdIPAddress = '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   UpdHostName = '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   UpdDate = CURRENT_TIMESTAMP ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    OfficeID = @id ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@id", id));
                    cmd.Parameters.Add(new MySqlParameter("@name", name));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// データ削除
        /// </summary>
        /// <param name="id"></param>
        internal void DelData_MX06(string id)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"DELETE FROM ");
            sb.AppendLine($"    MX06AddressOffice ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    OfficeID = @id ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@id", id));
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion
    }
}
