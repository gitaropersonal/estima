﻿using Estima.Forms.Application.MST.MST0020.Business;
using EstimaLib.Const;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Estima.Forms.Dialog.DE0020.Business
{
    internal class DE0020_FormLogic
    {
        #region Member
        /// <summary>
        /// ボディパネル
        /// </summary>
        private DE0020_MstCommonDialog Body { get; set; }
        /// <summary>
        /// マスタ種別
        /// </summary>
        private Enums.MstType MstType = Enums.MstType.None;
        /// <summary>
        /// IMEモード
        /// </summary>
        private ImeMode IME { get; set; }
        /// <summary>
        /// 個別サービス
        /// </summary>
        private readonly DE0020_Service PersonalService = new DE0020_Service();
        /// <summary>
        /// 削除されたデータ
        /// </summary>
        private List<DE0020_GridDto> DeletedDatas = new List<DE0020_GridDto>();
        /// <summary>
        /// 列ヘッダdto
        /// </summary>
        private static DE0020_GridDto CellHeaderDto = new DE0020_GridDto();
        private static DE0020_GridDto CellHeaderNameDto = new DE0020_GridDto();
        private readonly string[] EditableGridCells = new string[]
        {
              nameof(CellHeaderNameDto.ID)
            , nameof(CellHeaderNameDto.ColName)
        };
        /// <summary>
        /// 現在の列インデックス
        /// </summary>
        private int CurrColIndex = 0;
        /// <summary>
        /// 編集フラグ
        /// </summary>
        private bool EditedFlg = false;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="mstType"></param>
        internal DE0020_FormLogic(
              DE0020_MstCommonDialog body
            , Enums.MstType mstType
            , ImeMode imeMode
            )
        {
            this.Body = body;
            this.MstType = mstType;
            this.IME = imeMode;
            new InitControl(this.Body);
            this.Init();

            // クリアボタン押下
            this.Body.btnClear.Click += (s, e) =>
            {
                CommonUtil.BtnClearClickEvent(
                      new Action(() => this.Init())
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 終了ボタン押下
            this.Body.btnClose.Click += this.BtnCloseClickEvent;
            // 終了処理
            this.Body.FormClosing += this.ClosingEvent;
            // 検索ボタン押下
            this.Body.btnSearch.Click += (s, e) => 
            {
                CommonUtil.EventWithValidateUpdExist(
                      new Action(() => this.BtnSearchClickEvent(s, e))
                    , new Func<bool>(this.ValidateUnUpdateDataExist)
                );
            };
            // 追加ボタン押下
            this.Body.btnAdd.Click += this.BtnAddClickEvent;
            // 更新ボタン押下
            this.Body.btnUpdate.Click += this.BtnUpdClickEvent;
            // グリッドCellEnter
            this.Body.dgvMaster.CellEnter += this.GridCelEnter;
            // グリッドセルクリック
            this.Body.dgvMaster.CellClick += this.GridCellClick;
            // グリッドRowPostPaint
            this.Body.dgvMaster.RowPostPaint += this.GridRowPostPaintEvent;
            // グリッドCellDoubleClick
            this.Body.dgvMaster.CellDoubleClick += this.GridCellDoubleClickEvent;
            // グリッドCellValueChanged
            this.Body.dgvMaster.CellValueChanged += this.GridCellValueChanged;
            // グリッドEditingControlShowing
            this.Body.dgvMaster.EditingControlShowing += this.GridKeyPress;
            // ショートカットキー
            this.Body.KeyPreview = true;
            this.Body.KeyDown += (s, e) =>
            {
                if (!e.Alt)
                {
                    return;
                }
                switch (e.KeyCode)
                {
                    case Keys.A:
                        this.Body.btnClear.Focus();
                        this.Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        this.Body.btnClose.Focus();
                        this.Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        this.Body.btnSearch.Focus();
                        this.Body.btnSearch.PerformClick();
                        break;
                    case Keys.I:
                        this.Body.btnAdd.Focus();
                        this.Body.btnAdd.PerformClick();
                        break;
                    case Keys.O:
                        this.Body.btnUpdate.Focus();
                        this.Body.btnUpdate.PerformClick();
                        break;
                }
            };
        }
        #endregion

        #region Event
        /// <summary>
        /// グリッドセルクリック
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellClick(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            var dgv = (DataGridView)s;
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            if (dgv.ReadOnly)
            {
                return;
            }
            if (dgv.DataSource == null)
            {
                return;
            }
            var status = dgv.Rows[e.RowIndex].Cells[nameof(CellHeaderNameDto.Status)].Value.ToString();
            if (status == Enums.EditStatus.None.ToString())
            {
                return;
            }
            try
            {
                switch (dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].OwningColumn.Name)
                {
                    case nameof(CellHeaderNameDto.BtnDelete):
                        // 削除ボタン押下
                        this.GridButtonClickEventDelete(s, e);
                        break;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// グリッドCellDoubleClick
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellDoubleClickEvent(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            ((DataGridView)s).Rows[e.RowIndex].Selected = true;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCelEnter(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }
            this.CurrColIndex = e.ColumnIndex;
            var tgtCol = this.Body.dgvMaster.Columns[e.ColumnIndex];
            if (tgtCol == null)
            {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName)
            {
                default:
                    Body.dgvMaster.ImeMode = this.Body.txtName.ImeMode;
                    break;
            }
        }
        /// <summary>
        /// グリッドセルキー入力制御
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridKeyPress(
              object s
            , DataGridViewEditingControlShowingEventArgs e
            )
        {
            if (!this.Body.dgvMaster.Focused)
            {
                return;
            }
            //表示されているコントロールがDataGridViewTextBoxEditingControlか調べる
            if (!(e.Control is DataGridViewTextBoxEditingControl))
            {
                return;
            }
            //編集のために表示されているコントロールを取得
            var tb = (DataGridViewTextBoxEditingControl)e.Control;

            //イベントハンドラを削除
            tb.KeyPress -= new KeyPressEventHandler(CommonUtil.GridCellKeyPressUpperInteger);
            tb.KeyPress -= new KeyPressEventHandler(CommonUtil.GridCellKeyPressFloat);

            //列ごとに処理分岐
            string tgtColName = this.Body.dgvMaster.Columns[this.CurrColIndex].Name;
            if (tgtColName != nameof(CellHeaderDto.ColName))
            {
                return;
            }
            switch (this.MstType)
            {
                case Enums.MstType.ShikiriRate:
                    tb.KeyPress += new KeyPressEventHandler(CommonUtil.GridCellKeyPressFloat);
                    break;
                case Enums.MstType.RiekiRate:
                    tb.KeyPress += new KeyPressEventHandler(CommonUtil.GridCellKeyPressUpperInteger);
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            // イベント一旦削除
            var dgv = (DataGridView)s;
            dgv.CellValueChanged -= this.GridCellValueChanged;

            var dto = new DE0020_GridDto();
            DataGridViewRow r = dgv.Rows[e.RowIndex];
            DataGridViewCell c = r.Cells[e.ColumnIndex];
            string tgtCellName = c.OwningColumn.Name;
            var tgtCellVal = c.Value;

            if (this.EditableGridCells.Contains(tgtCellName))
            {
                // グリッドCellValueChanged（編集可能セル）
                string type = "string";
                switch (this.MstType)
                {
                    case Enums.MstType.RiekiRate:
                        type = "int";
                        break;
                    case Enums.MstType.ShikiriRate:
                        type = "float";
                        break;
                }
                this.EditableCellValueChanged(tgtCellVal, tgtCellName, e.RowIndex, type);

                // セル背景色
                c.Style.BackColor = Colors.BackColorCellEdited;

                // ステータス更新
                string identifyCol = CommonUtil.TostringNullForbid(r.Cells[nameof(dto.ID)].Value);
                string status = CommonUtil.TostringNullForbid(r.Cells[nameof(dto.Status)].Value);
                if (!string.IsNullOrEmpty(identifyCol)
                    && (status == Enums.EditStatus.Show.ToString() || status == Enums.EditStatus.Update.ToString()))
                {
                    r.Cells[nameof(dto.Status)].Value = Enums.EditStatus.Update;
                }
                else
                {
                    r.Cells[nameof(dto.Status)].Value = Enums.EditStatus.Insert;
                }
            }
            // イベント回復
            dgv.CellValueChanged += this.GridCellValueChanged;

            // 使用可否切り替え（更新ボタン）
            this.EditedFlg = true;
            this.SwitchEnabledChangeSearch();
        }
        /// <summary>
        /// グリッドRowPostPaint
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridRowPostPaintEvent(
              object s
            , DataGridViewRowPostPaintEventArgs e
            )
        {
            GridRowUtil<DE0020_GridDto>.SetRowNum(s, e);
        }
        /// <summary>
        /// 行削除ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridButtonClickEventDelete(
              object s
            , DataGridViewCellEventArgs e
            )
        {
            try
            {
                // 行データ取得
                var dgv = (DataGridView)s;
                DataGridViewRow r = dgv.Rows[e.RowIndex];
                var rowDto = GridRowUtil<DE0020_GridDto>.GetRowModel(r);
                if (this.MstType == Enums.MstType.ShohinSize)
                {
                    // Validate
                    if (this.PersonalService.JudgeExistEstimaHavingSize(rowDto.ColName))
                    {
                        CommonUtil.ShowErrorMsg(DE0020_Messages.MsgErrorExistEstimaHavingSize);
                        return;
                    }
                }
                // 削除行データを保存
                this.DeletedDatas.Add(rowDto);
                this.Body.dgvMaster.Rows.Remove(r);
                // 行削除によって行がなくなった場合の処理
                if (this.Body.dgvMaster.Rows.Count == 0)
                {
                    this.Body.dgvMaster.Rows.Clear();
                    this.Body.dgvMaster.DataSource = new BindingList<DE0020_GridDto>();
                }
                // 使用可否切替（検索ボタン押下時）
                this.EditedFlg = true;
                this.SwitchEnabledChangeSearch();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnCloseClickEvent(
              object s
            , EventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            bool cancel = false;
            try
            {
                var drConfirm = CommonUtil.ShowInfoMsgOKCancel(MST0020_Messages.MsgAskFinish);
                if (drConfirm != DialogResult.OK)
                {
                    cancel = true;
                    return;
                }
                if (!this.ValidateUnUpdateDataExist())
                {
                    drConfirm = CommonUtil.ShowInfoExcramationOKCancel(MST0020_Messages.MsgAskExistUpdateDatas);
                    if (drConfirm != DialogResult.OK)
                    {
                        cancel = true;
                        return;
                    }
                }
                this.Body.Close();
            }
            finally
            {
                if (cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 終了イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void ClosingEvent(
              object s
            , FormClosingEventArgs e
            )
        {
            this.Body.FormClosing -= this.ClosingEvent;
            try
            {
                var drConfirm = CommonUtil.ShowInfoMsgOKCancel(MST0020_Messages.MsgAskFinish);
                if (drConfirm != DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
                if (!this.ValidateUnUpdateDataExist())
                {
                    drConfirm = CommonUtil.ShowInfoExcramationOKCancel(MST0020_Messages.MsgAskExistUpdateDatas);
                    if (drConfirm != DialogResult.OK)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }
            finally
            {
                if (e.Cancel)
                {
                    this.Body.FormClosing += this.ClosingEvent;
                }
            }
        }
        /// <summary>
        /// 検索ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnSearchClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        /// <summary>
        /// 追加ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnAddClickEvent(
              object s
            , EventArgs e
            )
        {
            try
            {
                // グリッドデータ取得
                var datas = (BindingList<DE0020_GridDto>)this.Body.dgvMaster.DataSource;

                // 追加行作成
                var rowDto = new DE0020_GridDto()
                {
                    ColName = string.Empty,
                    Status = Enums.EditStatus.Insert,
                };
                rowDto.ID = this.GetNextID();

                // 行追加
                bool dataSourceRefreshFlg = false;
                int addRowIdx = 0;
                if (datas == null)
                {
                    datas = new BindingList<DE0020_GridDto>();
                    dataSourceRefreshFlg = true;
                    addRowIdx = 0;
                }
                else
                {
                    // 先頭行が空行の場合、データソースのリフレッシュが必要
                    var topRowDto = GridRowUtil<DE0020_GridDto>.GetRowModel(this.Body.dgvMaster.Rows[0]);
                    dataSourceRefreshFlg = string.IsNullOrEmpty(topRowDto.ID) && topRowDto.Status == Enums.EditStatus.None;
                }
                if (dataSourceRefreshFlg)
                {
                    // データソースリフレッシュする場合の処理
                    var newData = new BindingList<DE0020_GridDto>();
                    newData.Add(rowDto);
                    this.Body.dgvMaster.DataSource = newData;
                }
                else
                {
                    datas.Insert(addRowIdx, rowDto);
                    this.Body.dgvMaster.DataSource = datas;
                }
                // グリッド背景色をセット
                DataGridViewRow r = this.Body.dgvMaster.Rows[addRowIdx];
                var dto = GridRowUtil<DE0020_GridDto>.GetRowModel(r);
                foreach (DataGridViewCell c in r.Cells)
                {
                    if (this.EditableGridCells.Contains(c.OwningColumn.Name))
                    {
                        c.Style.BackColor = Colors.BackColorBtnReadOnly;
                        c.ReadOnly = false;
                    }
                }
                // 行追加不可にする
                this.Body.dgvMaster.AllowUserToAddRows = false;
                this.Body.dgvMaster.Rows[addRowIdx].Cells[nameof(CellHeaderNameDto.ColName)].Style.BackColor = Colors.BackColorCellEdited;

                // 使用可否切替（検索ボタン押下時）
                this.EditedFlg = true;
                this.SwitchEnabledChangeSearch();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
        }
        /// <summary>
        /// 更新ボタン押下イベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnUpdClickEvent(
              object s
            , EventArgs e
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var gridRowDtoList = GridRowUtil<DE0020_GridDto>.GetAllRowsModel(this.Body.dgvMaster.DataSource);

                // 更新時Validate
                if (!this.UpdValidate(gridRowDtoList))
                {
                    return;
                }
                // 確認ダイアログ
                if (CommonUtil.ShowInfoMsgOKCancel(DE0020_Messages.MsgAskUpdate) != DialogResult.OK)
                {
                    return;
                }
                // 更新処理
                var dataSource = GridRowUtil<DE0020_GridDto>.GetAllRowsModel(this.Body.dgvMaster.DataSource);
                bool success = true;
                CommonUtil.ShowProgressDialog(new Action(() =>
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.Start);
                    success = this.AddUpdDatas(dataSource);
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogUpdate, Enums.LogStatusKbn.End);
                }));
                if (!success)
                {
                    return;
                }
                // 更新完了メッセージ
                CommonUtil.ShowInfoMsgOK(DE0020_Messages.MsgInfoFinishUpdate);

                // 再検索
                this.SearchLogic();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
            }
            finally
            {
                Cursor.Current = preCursor;
            }
        }
        #endregion

        #region Business
        /// <summary>
        /// グリッドCellValueChanged（編集可能セル）
        /// </summary>
        /// <param name="tgtCellVal"></param>
        /// <param name="tgtCellName"></param>
        /// <param name="rowIndex"></param>
        /// <param name="valueType"></param>
        /// <returns></returns>
        private bool EditableCellValueChanged(
              object tgtCellVal
            , string tgtCellName
            , int rowIndex
            , string valueType
            )
        {
            try
            {
                if (tgtCellVal == null)
                {
                    return false;
                }
                string inputVal = CommonUtil.TostringNullForbid(tgtCellVal);
                switch (valueType)
                {
                    case "float":
                        float flVal;
                        if (float.TryParse(inputVal, out flVal))
                        {
                            this.Body.dgvMaster.Rows[rowIndex].Cells[tgtCellName].Value = inputVal;
                            return true;
                        }
                        this.Body.dgvMaster.Rows[rowIndex].Cells[tgtCellName].Value = string.Empty;
                        return false;
                    case "int":
                        int intVal;
                        if (int.TryParse(inputVal, out intVal))
                        {
                            this.Body.dgvMaster.Rows[rowIndex].Cells[tgtCellName].Value = inputVal;
                            return true;
                        }
                        this.Body.dgvMaster.Rows[rowIndex].Cells[tgtCellName].Value = string.Empty;
                        return false;
                    default:
                        this.Body.dgvMaster.Rows[rowIndex].Cells[tgtCellName].Value = inputVal;
                        return true;
                }
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                return false;
            }
        }
        /// <summary>
        /// 検索処理
        /// </summary>
        private void SearchLogic()
        {
            // グリッド初期化
            this.Body.dgvMaster.Rows.Clear();

            // グリッドデータ
            var bindingList = new BindingList<DE0020_GridDto>();
            CommonUtil.ShowProgressDialog((new Action(() =>
            {
                try
                {
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.Start);
                    switch (this.MstType)
                    {
                        case Enums.MstType.ShohinSize:
                            bindingList = this.PersonalService.GetGridDatas_MX05(this.Body.txtName.Text);
                            break;
                        case Enums.MstType.AddressOffice:
                            bindingList = this.PersonalService.GetGridDatas_MX06(this.Body.txtName.Text);
                            break;
                    }
                    EstimaUtil.AddLogInfo(this.Body.Text, Messages.OpeLogSearch, Enums.LogStatusKbn.End);
                }
                catch (Exception ex)
                {
                    EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                    return;
                }
            })));
            if (!bindingList.Any())
            {
                CommonUtil.ShowErrorMsg(DE0020_Messages.MsgErrorNotExistDatas);
                return;
            }
            // グリッドデータセット
            this.Body.dgvMaster.DataSource = bindingList;
            this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].ReadOnly = false;
            if (this.MstType == Enums.MstType.ShohinSize)
            {
                foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
                {
                    var rowDto = GridRowUtil<DE0020_GridDto>.GetRowModel(r);
                    if (rowDto.UsedByEstima)
                    {
                        r.Cells[nameof(CellHeaderDto.ColName)].ReadOnly = true;
                        r.Cells[nameof(CellHeaderDto.ColName)].Style.BackColor = Colors.BackColorBtnReadOnly;
                    }
                }
            }
            this.Body.dgvMaster.AllowUserToAddRows = false;

            // 編集フラグ
            this.EditedFlg = false;

            // フォーカス
            this.Body.dgvMaster.Focus();

        }
        /// <summary>
        /// DBへデータ登録・更新
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        private bool AddUpdDatas(List<DE0020_GridDto> gridRowDtoList)
        {
            try
            {
                // 削除用データ作成
                var delDatas = this.DeletedDatas.Where(n => n.Status != Enums.EditStatus.Insert).ToList();

                // 登録用データ作成
                var addDatas = this.CreateAddDatas(gridRowDtoList);

                // 更新用データ作成
                var updDatas = this.CreateUpdDatas(gridRowDtoList);

                // 更新処理
                switch (this.MstType)
                {
                    case Enums.MstType.ShohinSize:
                        delDatas.ForEach(n => this.PersonalService.DelData_MX05(n.ID));
                        updDatas.ForEach(n => this.PersonalService.UpdData_MX05(n.ID, n.ColName));
                        addDatas.ForEach(n => this.PersonalService.AddData_MX05(n.ID, n.ColName));
                        break;
                    case Enums.MstType.AddressOffice:
                        delDatas.ForEach(n => this.PersonalService.DelData_MX06(n.ID));
                        updDatas.ForEach(n => this.PersonalService.UpdData_MX06(n.ID, n.ColName));
                        addDatas.ForEach(n => this.PersonalService.AddData_MX06(n.ID, n.ColName));
                        break;
                }
                this.DeletedDatas.Clear();
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, this.Body.Text);
                return false;
            }
            return true;
        }
        /// <summary>
        /// 登録用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<DE0020_GridDto> CreateAddDatas(List<DE0020_GridDto> gridDtoList)
        {
            var ret = new List<DE0020_GridDto>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => !string.IsNullOrEmpty(n.ColName) && n.Status == Enums.EditStatus.Insert).ToList())
            {
                // データ作成
                var entity = new DE0020_GridDto()
                {
                    ID = CommonUtil.TostringNullForbid(rowDto.ID),
                    ColName = CommonUtil.TostringNullForbid(rowDto.ColName),
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 更新用データ作成
        /// </summary>
        /// <param name="gridDtoList"></param>
        /// <returns></returns>
        private List<DE0020_GridDto> CreateUpdDatas(List<DE0020_GridDto> gridDtoList)
        {
            var ret = new List<DE0020_GridDto>();
            DateTime dtNow = DateTime.Now;
            foreach (var rowDto in gridDtoList.Where(n => !string.IsNullOrEmpty(n.ColName) && n.Status == Enums.EditStatus.Update).ToList())
            {
                // データ作成
                var entity = new DE0020_GridDto()
                {
                    ID = CommonUtil.TostringNullForbid(rowDto.ID),
                    ColName = CommonUtil.TostringNullForbid(rowDto.ColName),
                };
                ret.Add(entity);
            }
            return ret;
        }
        /// <summary>
        /// 使用可否切替（検索ボタン押下時）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void SwitchEnabledChangeSearch()
        {
            this.Body.btnUpdate.Enabled = this.EditedFlg;
        }
        /// <summary>
        /// 次のID取得
        /// </summary>
        /// <returns></returns>
        private string GetNextID()
        {
            // 最大ID（グリッド）
            int maxIDOfGrid = 0;
            if (this.Body.dgvMaster.DataSource != null)
            {
                var alldatas = GridRowUtil<DE0020_GridDto>.GetAllRowsModel((object)this.Body.dgvMaster.DataSource);
                if (alldatas != null && 0 < alldatas.Count)
                {
                    maxIDOfGrid = alldatas.ToList().Max(n => CommonUtil.ToInteger(n.ID));
                }
            }
            // 最大ID（削除データ）
            int maxIDOfDelDatas = 0;
            var delDatas = this.DeletedDatas.Where(n => n.Status != Enums.EditStatus.Insert && n.Status != Enums.EditStatus.None).ToList();
            if (delDatas != null && 0 < delDatas.Count)
            {
                maxIDOfDelDatas = delDatas.ToList().Max(n => CommonUtil.ToInteger(n.ID));
            }
            // 最大ID（DB）
            int maxIDOfDB = 0;
            switch (this.MstType)
            {
                case Enums.MstType.ShohinSize:
                    maxIDOfDB = this.PersonalService.GetMaxID_MX05();
                    break;
                case Enums.MstType.AddressOffice:
                    maxIDOfDB = this.PersonalService.GetMaxID_MX06();
                    break;
            }
            // ID選択
            var maxID = Math.Max(Math.Max(maxIDOfGrid, maxIDOfDelDatas), maxIDOfDB);
            return (maxID + 1).ToString();
        }
        #endregion

        #region Validate
        /// <summary>
        /// 更新されていない項目が存在する？
        /// </summary>
        /// <returns></returns>
        private bool ValidateUnUpdateDataExist()
        {
            foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
            {
                // 更新されていない行が含まれる場合
                var rowDto = GridRowUtil<DE0020_GridDto>.GetRowModel(r);
                if (rowDto.Status == Enums.EditStatus.Insert || rowDto.Status == Enums.EditStatus.Update)
                {
                    return false;
                }
            }
            if (this.DeletedDatas.Any(n => n.Status == Enums.EditStatus.Show || n.Status == Enums.EditStatus.Update))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <param name="gridRowDtoList"></param>
        /// <returns></returns>
        private bool UpdValidate(List<DE0020_GridDto> gridRowDtoList)
        {
            // グリッドデータ取得
            var allGridDtoList = GridRowUtil<DE0020_GridDto>.GetAllRowsModel(this.Body.dgvMaster.DataSource).ToList();
            allGridDtoList = allGridDtoList.Where(n => n.Status == Enums.EditStatus.Insert || n.Status == Enums.EditStatus.Update).ToList();
            // 更新対象件数チェック
            if (!allGridDtoList.Any() && !this.DeletedDatas.Any(n => n.Status != Enums.EditStatus.Insert))
            {
                CommonUtil.ShowErrorMsg(DE0020_Messages.MsgErrorNotExistDatas);
                return false;
            }
            // グリッドセル空文字チェック
            if (!this.ValidateEmpty())
            {
                return false;
            }
            if (this.MstType == Enums.MstType.ShohinSize)
            {
                // サイズの禁止文字チェック
                if (!this.ValidateForbiddenStringOfSize())
                {
                    return false;
                }
            }
            // 重複チェック
            if (!this.ValidateDouple(allGridDtoList))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// グリッドセル空文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateEmpty()
        {
            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
            {

                var rowDto = GridRowUtil<DE0020_GridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.Insert && rowDto.Status != Enums.EditStatus.Update)
                {
                    continue;
                }
                if (string.IsNullOrEmpty(rowDto.ColName))
                {
                    // 名称
                    headerTxt = this.Body.dgvMaster.Columns[nameof(rowDto.ColName)].HeaderText;
                    if (!errColNames.Contains(headerTxt))
                    {
                        errColNames.Add(headerTxt);
                    }
                    r.Cells[nameof(rowDto.ColName)].Style.BackColor = Colors.BackColorCellError;
                    result = false;
                }
            }
            if (!result)
            {
                string msg = string.Format(DE0020_Messages.MsgErrorMissEmpty, string.Join("、", errColNames));
                CommonUtil.ShowErrorMsg(msg);
            }
            return result;
        }
        /// <summary>
        /// サイズの禁止文字チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateForbiddenStringOfSize()
        {
            bool result = true;
            string headerTxt = string.Empty;
            var errColNames = new List<string>();
            foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
            {

                var rowDto = GridRowUtil<DE0020_GridDto>.GetRowModel(r);
                if (rowDto.Status != Enums.EditStatus.Insert && rowDto.Status != Enums.EditStatus.Update)
                {
                    continue;
                }
                if (rowDto.ColName.Contains("x") || rowDto.ColName.Contains("X"))
                {
                    // 名称
                    headerTxt = this.Body.dgvMaster.Columns[nameof(rowDto.ColName)].HeaderText;
                    if (!errColNames.Contains(headerTxt))
                    {
                        errColNames.Add(headerTxt);
                    }
                    r.Cells[nameof(rowDto.ColName)].Style.BackColor = Colors.BackColorCellError;
                    result = false;
                }
            }
            if (!result)
            {
                CommonUtil.ShowErrorMsg(DE0020_Messages.MsgErrorForbiddenStringOfSize);
            }
            return result;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <returns></returns>
        private bool ValidateDouple(List<DE0020_GridDto> keyInfos)
        {
            // 検索データ絞り込み
            var oldDatas = new List<DE0020_GridDto>();
            switch (this.MstType)
            {
                case Enums.MstType.ShohinSize:
                    oldDatas = this.PersonalService.GetGridDatas_MX05(string.Empty).ToList();
                    break;
                case Enums.MstType.AddressOffice:
                    oldDatas = this.PersonalService.GetGridDatas_MX06(string.Empty).ToList();
                    break;
            }
            string msg = string.Empty;
            bool resutlt = true;
            foreach (var keyInfo in keyInfos)
            {
                if (this.ValidateDoupleInsert(oldDatas, keyInfo) && this.ValidateDoupleUpdatet(keyInfos, keyInfo))
                {
                    continue;
                }
                // 背景色編集
                foreach (DataGridViewRow r in this.Body.dgvMaster.Rows)
                {
                    var rowDto = GridRowUtil<DE0020_GridDto>.GetRowModel(r);
                    if (rowDto.ColName == keyInfo.ColName)
                    {
                        r.Cells[nameof(rowDto.ColName)].Style.BackColor = Colors.BackColorCellError;
                    }
                }
                resutlt = false;
            }
            if (!resutlt)
            {
                // 重複キー
                var dto = new DE0020_GridDto();
                msg = this.Body.dgvMaster.Columns[nameof(dto.ColName)].HeaderText;
                CommonUtil.ShowErrorMsg(string.Format(DE0020_Messages.MsgErrorDouple, msg));
            }
            return resutlt;
        }
        /// <summary>
        /// 重複チェック（新規登録）
        /// </summary>
        /// <param name="oldDatas"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDoupleInsert(
              List<DE0020_GridDto> oldDatas
            , DE0020_GridDto keyInfo
            )
        {
            if (keyInfo.Status == Enums.EditStatus.Insert
                    && oldDatas.Exists(n => n.ColName == keyInfo.ColName))
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック（更新）
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateDoupleUpdatet(
              List<DE0020_GridDto> keyInfos
            , DE0020_GridDto keyInfo
            )
        {
            if (1 < keyInfos.Where(n => n.ColName == keyInfo.ColName).ToList().Count)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            // 検索条件
            this.Body.txtName.Text = string.Empty;
            this.Body.txtName.ImeMode = this.IME;

            // 削除データ
            this.DeletedDatas.Clear();

            // グリッド
            this.Body.dgvMaster.Rows.Clear();
            this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].ReadOnly = true;
            this.Body.dgvMaster.AllowUserToAddRows = false;
            this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].HeaderText = Enums.MstTypes.First(n => n.Key == this.MstType).Value;
            this.Body.lblName.Text = Enums.MstTypes.First(n => n.Key == this.MstType).Value;
            switch (this.MstType)
            {
                case Enums.MstType.ShohinSize:
                    this.Body.txtName.TextAlign = HorizontalAlignment.Left;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.UsedByEstima)].Visible = true;
                    break;
                case Enums.MstType.AddressOffice:
                    this.Body.txtName.TextAlign = HorizontalAlignment.Left;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderNameDto.UsedByEstima)].Visible = false;
                    break;
                default:
                    this.Body.txtName.TextAlign = HorizontalAlignment.Left;
                    this.Body.dgvMaster.Columns[nameof(CellHeaderDto.ColName)].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    break;

            }
            // 編集フラグ
            this.EditedFlg = false;

            // フォーカス
            this.Body.txtName.Focus();
        }
        #endregion
    }
}
