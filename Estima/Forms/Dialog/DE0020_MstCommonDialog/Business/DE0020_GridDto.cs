﻿using EstimaLib.Const;

namespace Estima.Forms.Dialog.DE0020.Business
{
    public class DE0020_GridDto
    {
        public DE0020_GridDto()
        {
            this.BtnDelete = "削除";
            this.Status = Enums.EditStatus.None;
        }
        public string ID { get; set; }
        public string BtnDelete { get; set; }
        public string ColName { get; set; }
        public bool UsedByEstima { get; set; }
        public Enums.EditStatus Status { get; set; }
    }
}
