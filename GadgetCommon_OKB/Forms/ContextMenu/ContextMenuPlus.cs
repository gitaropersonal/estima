﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GadgetCommon.Forms.ContextMenu
{
    public partial class ContextMenuPlus : Form
    {
        #region Property
        /// <summary>
        /// 選択されたメニュ番号
        /// </summary>
        public int SelectedMenuNum { get; set; }
        #endregion

        #region Member
        /// <summary>
        /// イベントリスト
        /// </summary>
        private List<string> EventList = new List<string>();
        /// <summary>
        /// 画面サイズ
        /// </summary>
        private const int ScreenHeight = 272;
        private const int ScreenWidth  = 264;
        /// <summary>
        /// フッターサイズ
        /// </summary>
        private const int FooterHeight = 48;
        /// <summary>
        /// ボタンサイズ
        /// </summary>
        private const int ButtonHeight = 35;
        /// <summary>
        /// 表示メニュ最大数
        /// </summary>
        private const int MaxMenuCount = 5;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="eventList"></param>
        /// <param name="location"></param>
        public ContextMenuPlus(
              List<string> eventList
            , Point location
            )
        {
            this.InitializeComponent();
            this.Location = location;
            this.SelectedMenuNum = 1;
            this.DialogResult = DialogResult.Cancel;
            new InitControl(this);
            for (int i = 0; i < eventList.Count && i < MaxMenuCount; i++)
            {
                this.EventList.Add(eventList[i]);
            }
            if (!this.EventList.Any())
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
            // 初期化
            this.Init();

            // ボタン押下処理
            this.button1.Click += (s, e) => { this.BtnMenuclick(1); };
            this.button2.Click += (s, e) => { this.BtnMenuclick(2); };
            this.button3.Click += (s, e) => { this.BtnMenuclick(3); };
            this.button4.Click += (s, e) => { this.BtnMenuclick(4); };
            this.button5.Click += (s, e) => { this.BtnMenuclick(5); };
            // キャンセルボタン押下処理
            this.btnCancel.Click += (s, e) =>
            {
                this.Close();
            };
        }
        #endregion

        #region Func
        /// <summary>
        /// オンアクティベート検知
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDeactivate(EventArgs e)
        {
            base.OnDeactivate(e);
            Close();
        }
        /// <summary>
        /// メニュボタン押下処理
        /// </summary>
        /// <param name="num"></param>
        private void BtnMenuclick(int num)
        {
            this.SelectedMenuNum = num;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region Init
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init()
        {
            this.StartPosition = FormStartPosition.Manual;
            
            // アイコン
            this.Icon = SystemIcons.Information;

            // ボタンVisible
            this.button2.Visible = 2 <= this.EventList.Count;
            this.button3.Visible = 3 <= this.EventList.Count;
            this.button4.Visible = 4 <= this.EventList.Count;
            this.button5.Visible = 5 <= this.EventList.Count;

            // ボタンテキスト
            this.button1.Text = this.EventList[0];
            if (2 <= this.EventList.Count)
            {
                this.button2.Text = this.EventList[1];
            }
            if (3 <= this.EventList.Count)
            {
                this.button3.Text = this.EventList[2];
            }
            if (4 <= this.EventList.Count)
            {
                this.button4.Text = this.EventList[3];
            }
            if (5 <= this.EventList.Count)
            {
                this.button5.Text = this.EventList[4];
            }
            // 画面サイズ
            this.Height = ScreenHeight - (ButtonHeight * (MaxMenuCount - this.EventList.Count)) + FooterHeight - 40;
            this.Width = ScreenWidth;
        }
        #endregion
    }
}
