﻿using GadgetCommon.Const;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace GadgetCommon.Forms
{
    public class InitControl
    {
        #region Member
        /// <summary>
        /// グリッドの行の高さ
        /// </summary>
        private const int DefaultGridRowHeight = 32;
        /// <summary>
        /// ボディパネル
        /// </summary>
        private Form Body = new Form();
        #endregion

        #region Contstructor
        /// <summary>
        /// コントロール初期化
        /// </summary>
        /// <param name="body"></param>
        public InitControl(Form body)
        {
            // フォーム背景色
            this.Body = body;
            this.Body.BackColor = Colors.BackColorForm;
            this.Body.StartPosition = FormStartPosition.CenterScreen;

            // 全コントロール取得
            List<Control> controls = CommonUtil.GetAllControls<Control>(this.Body);
            foreach (var control in controls)
            {
                switch (control.GetType())
                {
                    case Type @_ when @_ == typeof(Button):
                        this.InitControlButton((Button)control);
                        break;
                    case Type @_ when @_ == typeof(TextBox):
                        this.InitControlTextBox((TextBox)control);
                        break;
                    case Type @_ when @_ == typeof(ComboBox):
                        this.InitControlComboBox((ComboBox)control);
                        break;
                    case Type @_ when @_ == typeof(NumericUpDown):
                        this.InitControlNumericUpdown((NumericUpDown)control);
                        break;
                    case Type @_ when @_ == typeof(DataGridView):
                        this.InitControlGrid((DataGridView)control);
                        break;
                    case Type @_ when @_ == typeof(GroupBox):
                        this.InitControlGroupBox((GroupBox)control);
                        break;
                    case Type @_ when @_ == typeof(CheckBox):
                        this.InitControlOthes(control);
                        break;
                    case Type @_ when @_ == typeof(RadioButton):
                        this.InitControlOthes(control);
                        break;
                    case Type @_ when @_ == typeof(MonthCalendar):
                        this.InitControlOthes(control);
                        break;
                    case Type @_ when @_ == typeof(DateTimePicker):
                        this.InitControlOthes(control);
                        break;
                }
            }
        }
        #endregion

        #region InitControls
        /// <summary>
        /// 初期化（ボタン）
        /// </summary>
        /// <param name="button"></param>
        private void InitControlButton(Button button)
        {
            // ボタン背景色セット
            if (button.Enabled)
            {
                button.BackColor = Colors.BackColorBtnOffEnter;
                button.ForeColor = Colors.ForeColorClick;
            }
            else
            {
                button.BackColor = Colors.BackColorBtnReadOnly;
                button.ForeColor = Colors.ForeColorClick;
            }
            // フォーカス時に背景色を自動編集する処理を追加
            button.GotFocus -= this.BtnEventGotFocus;
            button.GotFocus += this.BtnEventGotFocus;

            // ロストフォーカス時に背景色を自動編集する処理を追加
            button.Leave -= this.BtnEventLeave;
            button.Leave += this.BtnEventLeave;

            // 使用可否変更時に背景色を自動編集する処理を追加
            button.EnabledChanged -= this.BtnEventLeave;
            button.EnabledChanged += this.BtnEventLeave;
        }
        /// <summary>
        /// 初期化（テキストボックス）
        /// </summary>
        /// <param name="txtBox"></param>
        private void InitControlTextBox(TextBox txtBox)
        {
            // テキストボックス背景色セット
            txtBox.BackColor = Color.Empty;
            txtBox.ForeColor = SystemColors.ControlText;

            // フォーカスインイベント
            txtBox.GotFocus -= this.TextBoxEventGotFocus;
            txtBox.GotFocus += this.TextBoxEventGotFocus;

            txtBox.Leave -= this.TextBoxEventLeave;
            txtBox.Leave += this.TextBoxEventLeave;

            // Enterキー押下イベント
            txtBox.KeyPress -= this.ControlsKeyPress;
            txtBox.KeyPress += this.ControlsKeyPress;
        }
        /// <summary>
        /// 初期化（コンボボックス）
        /// </summary>
        /// <param name="cmbBox"></param>
        private void InitControlComboBox(ComboBox cmbBox)
        {
            // コンボボックス背景色セット
            cmbBox.BackColor = Color.Empty;
            cmbBox.ForeColor = SystemColors.ControlText;

            cmbBox.GotFocus -= this.CmbBoxEventGotFocus;
            cmbBox.GotFocus += CmbBoxEventGotFocus;

            cmbBox.Leave -= this.CmbBoxEventLeave;
            cmbBox.Leave += this.CmbBoxEventLeave;

            // Enterキー押下イベント
            cmbBox.KeyPress -= this.ControlsKeyPress;
            cmbBox.KeyPress += this.ControlsKeyPress;
        }
        /// <summary>
        /// 初期化（数値コントロール）
        /// </summary>
        /// <param name="numeric"></param>
        private void InitControlNumericUpdown(NumericUpDown numeric)
        {
            // Enterキー押下イベント
            numeric.BackColor = Color.Empty;
            numeric.ForeColor = SystemColors.ControlText;

            // フォーカスインイベント
            numeric.GotFocus -= this.NumericEventGotFocus;
            numeric.GotFocus += this.NumericEventGotFocus;

            numeric.Leave -= this.NumericEventLeave;
            numeric.Leave += this.NumericEventLeave;

            // Enterキー押下イベント
            numeric.KeyPress -= this.ControlsKeyPress;
            numeric.KeyPress += this.ControlsKeyPress;
        }
        /// <summary>
        /// 初期化（グリッド）
        /// </summary>
        /// <param name="grid"></param>
        private void InitControlGrid(DataGridView grid)
        {
            // Enterキー押下イベント
            grid.KeyPress -= this.ControlsKeyPress;
            grid.KeyPress += this.ControlsKeyPress;

            // 行の高さを固定
            grid.RowTemplate.Height = DefaultGridRowHeight;

            // ソートの禁止
            foreach (DataGridViewColumn c in grid.Columns)
            {
                c.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            // 表示処理の高速化
            var flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod;
            var styles = ControlStyles.DoubleBuffer;
            grid.GetType().InvokeMember("SetStyle", flags, null, grid, new object[] { styles, true });
        }
        /// <summary>
        /// 初期化（グループボックス）
        /// </summary>
        /// <param name="grp"></param>
        private void InitControlGroupBox(GroupBox grp)
        {
            grp.BackColor = Colors.BackColorGroup;
        }
        /// <summary>
        /// 初期化（その他コントロール）
        /// </summary>
        /// <param name="control"></param>
        private void InitControlOthes(Control control)
        {
            control.KeyPress -= this.ControlsKeyPress;
            control.KeyPress += this.ControlsKeyPress;
        }
        #endregion

        #region ボタンイベント
        /// <summary>
        /// ボタンGotFocusイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnEventGotFocus(
              object s
            , EventArgs e
            )
        {
            ((Button)s).BackColor = Colors.BackColorBtnOnEnter;
            ((Button)s).ForeColor = Colors.ForeColorClick;
        }
        /// <summary>
        /// ボタンLeaveイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void BtnEventLeave(
              object s
            , EventArgs e
            )
        {
            Button button = (Button)s;
            if (button.Enabled)
            {
                button.BackColor = Colors.BackColorBtnOffEnter;
                button.ForeColor = Colors.ForeColorClick;
                return;
            }
            button.BackColor = Colors.BackColorBtnReadOnly;
            button.ForeColor = Colors.ForeColorClick;
        }
        #endregion

        #region テキストボックスイベント
        /// <summary>
        /// Enterキー押下イベント
        /// </summary>
        /// <param name="keyChar"></param>
        /// <param name="Body"></param>
        private void ControlsKeyPress(
              object s
            , KeyPressEventArgs e
            )
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.Body.SelectNextControl(this.Body.ActiveControl, true, true, true, true);
            }
        }
        /// <summary>
        /// テキストボックスGotFocusイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void TextBoxEventGotFocus(
              object s
            , EventArgs e
            )
        {
            ((TextBox)s).BackColor = Colors.BackColorBtnOnEnter;
            ((TextBox)s).ForeColor = Colors.ForeColorClick;
            ((TextBox)s).SelectAll();
        }
        /// <summary>
        /// テキストボックスLeaveイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void TextBoxEventLeave(
              object s
            , EventArgs e
            )
        {
            TextBox textBox = (TextBox)s;
            if (textBox.Enabled)
            {
                textBox.BackColor = Color.Empty;
                textBox.ForeColor = Colors.ForeColorClick;
                return;
            }
            textBox.BackColor = Colors.BackColorBtnReadOnly;
            textBox.ForeColor = Colors.ForeColorClick;
        }
        /// <summary>
        /// 数値コントロールGotFocusイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void NumericEventGotFocus(
              object s
            , EventArgs e
            )
        {
            var numeric = (NumericUpDown)s;
            numeric.BackColor = Colors.BackColorBtnOnEnter;
            numeric.ForeColor = Colors.ForeColorClick;
            int numLength = numeric.Value.ToString().Length;
            if (numLength == 0)
            {
                return;
            }
            numeric.Select(0, numeric.Text.Length);
        }
        /// <summary>
        /// テキストボックスLeaveイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void NumericEventLeave(
              object s
            , EventArgs e
            )
        {
            NumericUpDown numeric = (NumericUpDown)s;
            if (numeric.Enabled)
            {
                numeric.BackColor = Color.Empty;
                numeric.ForeColor = Colors.ForeColorClick;
                return;
            }
            numeric.BackColor = Colors.BackColorBtnReadOnly;
            numeric.ForeColor = Colors.ForeColorClick;
        }
        #endregion

        #region コンボボックスイベント
        /// <summary>
        /// コンボボックスGotFocusイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void CmbBoxEventGotFocus(
              object s
            , EventArgs e)
        {
            ((ComboBox)s).BackColor = Colors.BackColorBtnOnEnter;
            ((ComboBox)s).ForeColor = Colors.ForeColorClick;
        }
        /// <summary>
        /// コンボボックスLeaveイベント
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void CmbBoxEventLeave(
              object s
            , EventArgs e
            )
        {
            ComboBox cmbBox = (ComboBox)s;
            if (cmbBox.Enabled)
            {
                cmbBox.BackColor = Color.Empty;
                cmbBox.ForeColor = Colors.ForeColorClick;
                return;
            }
            cmbBox.BackColor = Colors.BackColorBtnReadOnly;
            cmbBox.ForeColor = Colors.ForeColorClick;
        }
        #endregion
    }
}
