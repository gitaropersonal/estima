﻿using System.Drawing;
using System.Windows.Forms;

namespace GadgetCommon.Forms.MessageBoxPlus
{
    public partial class MessageBoxPlus : Form
    {
        #region Property
        public string Message { get; set; }
        public string ApErrorMessage { get; set; }
        public string Title { get; set; }
        public MessageBoxButtons MessageBoxButtons { get; set;}
        public MessageBoxIcon IconType { get; set; }
        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="messageBoxButtons"></param>
        /// <param name="icon"></param>
        /// <param name="apErrorMessage"></param>
        public MessageBoxPlus(
              string message
            , string title
            , MessageBoxButtons messageBoxButtons
            , MessageBoxIcon icon
            , string apErrorMessage = ""
            )
        {
            this.InitializeComponent();
            this.Message = message;
            this.ApErrorMessage = apErrorMessage;
            this.Title = title;
            this.MessageBoxButtons = messageBoxButtons;
            this.IconType = icon;
            new InitControl(this);
            this.Init();

            // OKボタン押下処理
            this.btnOK.Click += (s, e) =>
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            };
            // キャンセルボタン押下処理
            this.btnCancel.Click += (s, e) =>
            {
                this.Close();
            };
        }
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            this.DialogResult = DialogResult.Cancel;
            this.Text = this.Title;
            this.lblMessage.Text = this.Message;
            if (string.IsNullOrEmpty(this.ApErrorMessage))
            {
                this.txtApError.Visible = false;
                this.pnlApError.Visible = false;
                this.pnlMessage.Dock = DockStyle.Fill;
                this.lblMessage.Dock = DockStyle.Fill;
            }
            else
            {
                this.Width += 300;
                this.Height += 300;
                this.pnlApError.Visible = true;
                this.txtApError.Visible = true;
                this.txtApError.Text = this.ApErrorMessage;
            }
            switch (this.MessageBoxButtons)
            {
                case MessageBoxButtons.OK:
                    this.pnlCancel.Visible = false;
                    this.btnOK.Focus();
                    break;
                case MessageBoxButtons.OKCancel:
                    this.pnlCancel.Visible = true;
                    this.btnCancel.Focus();
                    break;
            }
            switch (this.IconType)
            {
                case MessageBoxIcon.Information:
                    this.Icon = SystemIcons.Information;
                    break;
                case MessageBoxIcon.Exclamation:
                    this.Icon = SystemIcons.Exclamation;
                    break;
                case MessageBoxIcon.Error:
                    this.Icon = SystemIcons.Error;
                    break;
            }
            this.picIcon.Image = this.Icon.ToBitmap();
        }
    }
}
