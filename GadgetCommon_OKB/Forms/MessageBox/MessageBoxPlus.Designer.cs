﻿
namespace GadgetCommon.Forms.MessageBoxPlus
{
    partial class MessageBoxPlus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCancel = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnOK = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlApError = new System.Windows.Forms.Panel();
            this.txtApError = new System.Windows.Forms.TextBox();
            this.pnlMessage = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblMessage = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.picIcon = new System.Windows.Forms.PictureBox();
            this.pnlCancel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlApError.SuspendLayout();
            this.pnlMessage.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCancel
            // 
            this.pnlCancel.Controls.Add(this.btnCancel);
            this.pnlCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlCancel.Location = new System.Drawing.Point(346, 0);
            this.pnlCancel.Name = "pnlCancel";
            this.pnlCancel.Padding = new System.Windows.Forms.Padding(1, 5, 9, 5);
            this.pnlCancel.Size = new System.Drawing.Size(100, 49);
            this.pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCancel.Location = new System.Drawing.Point(1, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 39);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "キャンセル";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnOK);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(246, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10, 5, 5, 5);
            this.panel2.Size = new System.Drawing.Size(100, 49);
            this.panel2.TabIndex = 11;
            // 
            // btnOK
            // 
            this.btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOK.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnOK.Location = new System.Drawing.Point(10, 5);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(85, 39);
            this.btnOK.TabIndex = 11;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pnlCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 138);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(446, 49);
            this.panel1.TabIndex = 10;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.AutoSize = true;
            this.panel3.Controls.Add(this.pnlApError);
            this.panel3.Controls.Add(this.pnlMessage);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5, 30, 5, 5);
            this.panel3.Size = new System.Drawing.Size(446, 138);
            this.panel3.TabIndex = 12;
            // 
            // pnlApError
            // 
            this.pnlApError.Controls.Add(this.txtApError);
            this.pnlApError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlApError.Location = new System.Drawing.Point(5, 64);
            this.pnlApError.Name = "pnlApError";
            this.pnlApError.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.pnlApError.Size = new System.Drawing.Size(436, 69);
            this.pnlApError.TabIndex = 4;
            this.pnlApError.Visible = false;
            // 
            // txtApError
            // 
            this.txtApError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtApError.Location = new System.Drawing.Point(0, 10);
            this.txtApError.Multiline = true;
            this.txtApError.Name = "txtApError";
            this.txtApError.ReadOnly = true;
            this.txtApError.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtApError.Size = new System.Drawing.Size(436, 59);
            this.txtApError.TabIndex = 5;
            this.txtApError.TabStop = false;
            this.txtApError.Text = "＜エラーメッセージ表示＞";
            this.txtApError.WordWrap = false;
            // 
            // pnlMessage
            // 
            this.pnlMessage.Controls.Add(this.panel5);
            this.pnlMessage.Controls.Add(this.panel6);
            this.pnlMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMessage.Location = new System.Drawing.Point(5, 30);
            this.pnlMessage.Name = "pnlMessage";
            this.pnlMessage.Size = new System.Drawing.Size(436, 34);
            this.pnlMessage.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblMessage);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(75, 0);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(0, 5, 10, 0);
            this.panel5.Size = new System.Drawing.Size(361, 34);
            this.panel5.TabIndex = 2;
            // 
            // lblMessage
            // 
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMessage.Location = new System.Drawing.Point(0, 5);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(351, 29);
            this.lblMessage.TabIndex = 4;
            this.lblMessage.Text = "＜メッセージ表示＞";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.picIcon);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(75, 34);
            this.panel6.TabIndex = 0;
            // 
            // picIcon
            // 
            this.picIcon.InitialImage = null;
            this.picIcon.Location = new System.Drawing.Point(33, 0);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new System.Drawing.Size(36, 33);
            this.picIcon.TabIndex = 6;
            this.picIcon.TabStop = false;
            // 
            // MessageBoxPlus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 187);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Meiryo UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageBoxPlus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "＜タイトル＞";
            this.pnlCancel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.pnlApError.ResumeLayout(false);
            this.pnlApError.PerformLayout();
            this.pnlMessage.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Panel pnlCancel;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnOK;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pnlMessage;
        private System.Windows.Forms.Panel pnlApError;
        private System.Windows.Forms.TextBox txtApError;
        private System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.PictureBox picIcon;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblMessage;
    }
}