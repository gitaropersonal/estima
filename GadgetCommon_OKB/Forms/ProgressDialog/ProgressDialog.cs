﻿using GadgetCommon.Const;
using GadgetCommon.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GadgetCommon.Forms
{
    public partial class ProgressDialog : Form
    {
        #region Member
        /// <summary>
        /// 実行関数
        /// </summary>
        public Delegate Method { get; set; }
        /// <summary>
        /// 処理成功フラグ
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// バックグラウンド処理
        /// </summary>
        private BackgroundWorker BackgroundWorker = new BackgroundWorker();
        /// <summary>
        /// バックグラウンド処理で発生したエラー
        /// </summary>
        public Exception Error;
        #endregion

        #region Constructor
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ProgressDialog()
        {
            this.InitializeComponent();
            this.Init();

            // バックグラウンド処理の設定
            this.BackgroundWorker = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
            };
            this.BackgroundWorker.DoWork += this.DoBackgroundWork;
            // バックグラウンド処理完了後
            this.BackgroundWorker.RunWorkerCompleted += (s, e) =>
            {
                if (e.Error != null)
                {
                    this.Error = e.Error;
                }
                this.Close();
            };
            // フォームロード
            this.Load += (s, e) =>
            {
                this.BackgroundWorker.RunWorkerAsync();
            };
        }
        #endregion

        #region Method
        /// <summary>
        /// バックグラウンド処理
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void DoBackgroundWork(
              object s
            , DoWorkEventArgs e
            )
        {
            try
            {
                // Formを閉じる
                Action endForm = (() => { this.Close(); });
                // 処理の実行
                this.Method.DynamicInvoke();
                // 処理が終了すると画面が閉じられる
                this.Invoke(endForm);
                // フラグ更新
                this.Success = true;
            }
            catch(Exception ex)
            {
                this.Error = ex;
                this.Success = false;
            }
        }
        /// <summary>
        /// 初期化処理
        /// </summary>
        public void Init()
        {
            // コントロール初期化
            this.InitControl();
            // メンバ初期化
            this.Success = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ShowInTaskbar = false;
            this.ControlBox = false;
        }
        /// <summary>
        /// コントロール初期化
        /// </summary>
        private void InitControl()
        {
            // フォーム背景色
            this.BackColor = Colors.BackColorForm;
            this.StartPosition = FormStartPosition.CenterScreen;

            // 全コントロール取得
            List<Control> controls = CommonUtil.GetAllControls<Control>(this);
            foreach (var control in controls)
            {
                if (control.GetType() == typeof(Button))
                {
                    // ボタン背景色セット
                    Button button = (Button)control;
                    if (button.Enabled)
                    {
                        button.BackColor = Colors.BackColorBtnOffEnter;
                        button.ForeColor = Colors.ForeColorClick;
                    }
                    else
                    {
                        button.BackColor = Colors.BackColorBtnReadOnly;
                        button.ForeColor = Colors.ForeColorClick;
                    }
                    continue;
                }
                if (control.GetType() == typeof(TextBox))
                {
                    // テキストボックス背景色セット
                    TextBox txtBox = (TextBox)control;
                    txtBox.BackColor = Color.Empty;
                    txtBox.ForeColor = SystemColors.ControlText;
                    continue;
                }
            }
            this.progressBar.Style = ProgressBarStyle.Marquee;
        }
        #endregion
    }
}
