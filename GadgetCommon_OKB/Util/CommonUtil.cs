﻿using GadgetCommon.Const;
using GadgetCommon.Forms;
using GadgetCommon.Forms.MessageBoxPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Deployment.Application;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace GadgetCommon.Util
{
    public static class CommonUtil
    {
        #region コントロール

        #region ボタンイベント
        /// <summary>
        /// クリアボタン押下イベント
        /// </summary>
        /// <param name="initDelegate"></param>
        public static void BtnClearClickEvent(Delegate initDelegate)
        {
            var drConfirm = ShowInfoMsgOKCancel("クリアしますか？");
            if (drConfirm != DialogResult.OK)
            {
                return;
            }
            initDelegate.DynamicInvoke();
        }
        /// <summary>
        /// クリアボタン押下イベント
        /// </summary>
        /// <param name="initDelegate"></param>
        /// <param name="validateExistNotUpdData"></param>
        public static void BtnClearClickEvent(
              Delegate initDelegate
            , Func<bool> validateExistNotUpdData
            )
        {
            var drConfirm = ShowInfoMsgOKCancel("クリアしますか？");
            if (drConfirm != DialogResult.OK)
            {
                return;
            }
            if (!validateExistNotUpdData.Invoke())
            {
                drConfirm = ShowInfoExcramationOKCancel("未更新のデータがありますが\r\nよろしいですか？");
                if (drConfirm != DialogResult.OK)
                {
                    return;
                }
            }
            initDelegate.DynamicInvoke();
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="body"></param>
        public static void BtnCloseClickEvent(Form body)
        {
            var drConfirm = ShowInfoMsgOKCancel("終了しますか？");
            if (drConfirm != DialogResult.OK)
            {
                return;
            }
            body.Close();
        }
        /// <summary>
        /// 終了ボタン押下イベント
        /// </summary>
        /// <param name="body"></param>
        /// <param name="validateExistNotUpdData"></param>
        public static void BtnCloseClickEvent(
              Form body
            , Func<bool> validateExistNotUpdData
            )
        {
            var drConfirm = ShowInfoMsgOKCancel("終了しますか？");
            if (drConfirm != DialogResult.OK)
            {
                return;
            }
            if (!validateExistNotUpdData.Invoke())
            {
                drConfirm = ShowInfoExcramationOKCancel("未更新のデータがありますが\r\nよろしいですか？");
                if (drConfirm != DialogResult.OK)
                {
                    return;
                }
            }
            body.Close();
        }
        /// <summary>
        /// 何かしらの処理を未更新データ存在チェック実行の上で行う
        /// </summary>
        /// <param name="method"></param>
        /// <param name="validateExistNotUpdData"></param>
        public static void EventWithValidateUpdExist(
              Action method
            , Func<bool> validateExistNotUpdData
            )
        {
            if (!validateExistNotUpdData.Invoke())
            {
                var drConfirm = ShowInfoExcramationOKCancel("未更新のデータがありますが\r\nよろしいですか？");
                if (drConfirm != DialogResult.OK)
                {
                    return;
                }
            }
            method.Invoke();
        }
        #endregion

        #region グリッドセルイベント
        /// <summary>
        /// 数値のみ入力許可
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void GridCellKeyPressInteger(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && e.KeyChar != '\b' && e.KeyChar != '-')
            {
                e.Handled = true;
                return;
            }
        }
        /// <summary>
        /// 数値のみ入力許可
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void GridCellKeyPressUpperInteger(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
                return;
            }
        }
        /// <summary>
        /// 数値と小数点のみ入力許可
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void GridCellKeyPressFloat(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && e.KeyChar != '\b' && e.KeyChar != '.')
            {
                e.Handled = true;
                return;
            }
        }
        #endregion

        #region 右クリックメニュの表示位置取得
        /// <summary>
        /// 右クリックメニュの表示位置取得
        /// </summary>
        /// <param name="dgv"></param>
        /// <returns></returns>
        public static Point GetShowMenuPoint(DataGridView dgv)
        {
            var r = dgv.GetCellDisplayRectangle(
                dgv.CurrentCell.ColumnIndex
              , dgv.CurrentCell.RowIndex
              , false
            );
            return new Point(r.X, r.Y);
        }
        #endregion

        /// <summary>
        /// 全コントロール取得
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="top"></param>
        /// <returns></returns>
        public static List<T> GetAllControls<T>(Control top) where T : Control
        {
            List<T> buf = new List<T>();
            foreach (Control ctrl in top.Controls)
            {
                if (ctrl is T) buf.Add((T)ctrl);
                buf.AddRange(GetAllControls<T>(ctrl));
            }
            return buf;
        }
        /// <summary>
        /// グリッドセルValidate（空文字）
        /// </summary>
        /// <param name="r"></param>
        /// <param name="headerTxt"></param>
        /// <param name="cellName"></param>
        /// <param name="errColNames"></param>
        /// <param name="result"></param>
        public static void ValidateEmpty(
              DataGridViewRow r
            , string headerTxt
            , string cellName
            , ref List<string> errColNames
            , ref bool result
            )
        {
            if (string.IsNullOrEmpty(TostringNullForbid(r.Cells[cellName].Value)))
            {
                r.Cells[cellName].Style.BackColor = Color.Red;
                if (!errColNames.Contains(headerTxt))
                {
                    errColNames.Add(headerTxt);
                }
                result = false;
            }
        }
        /// <summary>
        /// クリックワンスのバージョン取得
        /// </summary>
        /// <returns></returns>
        public static string GetVersion()
        {
            if (ApplicationDeployment.IsNetworkDeployed)
            {
                var version = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                return $"{version.Major}.{version.Minor}.{version.Build}.{version.Revision}";
            }
            return "1.0.0.XXX";
        }
        /// <summary>
        /// IPアドレス取得
        /// </summary>
        /// <returns></returns>
        public static string GetIpAddress()
        {
            IPAddress[] adrList = Dns.GetHostAddresses(Environment.MachineName);
            foreach (IPAddress address in adrList)
            {
                // IPv4 を返却する
                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    return address.ToString();
                }
            }
            return null;
        }
        /// <summary>
        /// テキストボックスのゼロパディング
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        public static void ZeroPaddingTextBox(
              object s
            , EventArgs e
            )
        {
            ((TextBox)s).Text = (new[] { ((TextBox)s).Text })
                                    .Where(a => !string.IsNullOrEmpty(a))
                                    .Select(a => a.PadLeft(((TextBox)s).MaxLength, '0'))
                                    .FirstOrDefault();
        }
        #endregion

        #region 文字列
        /// <summary>
        /// 数値以外除去
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int CutAInteger(string str)
        {
            return ToInteger(Regex.Replace(CutSpace(str), "[^0-9]", string.Empty));
        }
        /// <summary>
        /// アルファベット以外除去
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CutAnAlpha(string str)
        {
            return TostringNullForbid(Regex.Replace(CutSpace(str), "[^a-zA-Z]", string.Empty));
        }
        /// <summary>
        /// 空文字削除
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CutSpace(string str)
        {
            return TostringNullForbid(str).Replace("　", string.Empty).Replace(" ", string.Empty);
        }
        /// <summary>
        /// Substring（右から）
        /// </summary>
        /// <param name="target"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string SubstringRight(
              string target
            , int length
            )
        {
            return target.Substring(target.Length - length, length);
        }
        /// <summary>
        /// Substring（Byte数）
        /// </summary>
        /// <param name="s"></param>
        /// <param name="startIndex"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string SubstringByte(
              string s
            , int startIndex
            , int count
            )
        {
            System.Text.Encoding sjis = System.Text.Encoding.GetEncoding("shift_jis");
            byte[] b = sjis.GetBytes(s);
            return sjis.GetString(b, startIndex, count);
        }
        /// <summary>
        /// 文字列に含まれる半角記号を除外
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CutHalfMarkChars(string text)
        {
            // Nullまたは空文字の場合はそのまま返却
            if (string.IsNullOrWhiteSpace(text)) return text;

            string result = string.Empty;
            string[] str = new string[text.Length];
            for (int i = 0; i < text.Length; i++)
            {
                str[i] = text.Substring(i, 1);
            }
            foreach (var item in str)
            {
                if (Regex.IsMatch(item, "^[!-/:-@¥[-`{-~]+$"))
                {
                    continue;
                }
                result += item;
            }
            return result;
        }
        /// <summary>
        /// 文字列に含まれる半角記号を除外（除外の例外を引数で設定）
        /// </summary>
        /// <param name="text"></param>
        /// <param name="exStr"></param>
        /// <returns></returns>
        public static string CutHalfMarkCharsEx(
              string text
            , string exStr = ""
            )
        {
            // Nullまたは空文字の場合はそのまま返却
            if (string.IsNullOrWhiteSpace(text)) return text;

            string result = string.Empty;
            string[] str = new string[text.Length];
            for (int i = 0; i < text.Length; i++)
            {
                str[i] = text.Substring(i, 1);
            }
            foreach (var item in str)
            {
                if (Regex.IsMatch(item, "^[!-/:-@¥[-`{-~]+$") && item != exStr)
                {
                    continue;
                }
                result += item;
            }
            return result;
        }
        /// <summary>
        /// 文字列→数値→文字列変換
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string StrToIntToString(string str)
        {
            if (!int.TryParse(str, out int intVal))
            {
                return string.Empty;
            }
            return intVal.ToString();
        }
        /// <summary>
        /// 文字列型の数値に前ゼロをつける（2桁）
        /// </summary>
        /// <param name="strInt"></param>
        /// <returns></returns>
        public static string StrIntToStrPreZero2(string strInt)
        {
            if (string.IsNullOrEmpty(strInt))
            {
                return decimal.Zero.ToString().PadLeft(2, '0');
            }
            var ret = int.Parse(strInt).ToString().PadLeft(2, '0');
            return ret;
        }
        /// <summary>
        /// 文字列から数字のみ抜き出す
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SelNumericFromStr(string str)
        {
            return Regex.Replace(str, @"[^0-9]", "");
        }
        /// <summary>
        /// Tostring（null非許容）
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string TostringNullForbid(object o)
        {
            if (o == null)
            {
                return string.Empty;
            }
            return o.ToString();
        }
        /// <summary>
        /// Nullチェック付きSubstring
        /// </summary>
        /// <param name="o"></param>
        /// <param name="startIdx"></param>
        /// <param name="endIdx"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        public static string SubstringCheckNull(
              object o
            , int startIdx
            , int endIdx
            , string defaultVal = ""
            )
        {
            if (o == null)
            {
                return defaultVal;
            }
            if (string.IsNullOrEmpty(o.ToString()))
            {
                return defaultVal;
            }
            return o.ToString().Substring(startIdx, endIdx);
        }
        /// <summary>
        /// CSV出力文字の不正値を削除
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string RemoveCsvIllegal(object val)
        {
            if (val == null)
            {
                return string.Empty;
            }
            string str = val.ToString();
            return str.Replace("\"", string.Empty).Replace(",", string.Empty);
        }
        /// <summary>
        /// CSV出力文字の不正値を削除
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string RemoveUnderBar(object val)
        {
            if (val == null)
            {
                return string.Empty;
            }
            string str = val.ToString();
            return str.Replace("_", string.Empty);
        }
        /// <summary>
        /// DB登録文字の不正値を削除
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string RemoveSingleQuote(object val)
        {
            if (val == null)
            {
                return string.Empty;
            }
            string str = val.ToString();
            return str.Replace("'", string.Empty);
        }
        /// <summary>
        /// 文字列型フラグをbool型に変換
        /// </summary>
        /// <param name="blFlg"></param>
        /// <returns></returns>
        public static string ConvertFlgToStrFlg(bool blFlg)
        {
            return blFlg ? "1" : "0";
        }
        /// <summary>
        /// 条件に応じてコメントアウト取得
        /// </summary>
        /// <param name="Joken"></param>
        /// <returns></returns>
        public static string GetCmout(bool Joken)
        {
            return Joken ? "-- " : string.Empty;
        }
        #endregion

        #region ダイアログ
        private const string DialogTitleConfirm = "確認";
        private const string DialogTitleExcramation = "警告";
        private const string DialogTitleError = "エラー";

        /// <summary>
        /// スタックトレース整形
        /// </summary>
        /// <param name="stackTrace"></param>
        /// <returns></returns>
        public static string EditErrorStackTrace(string stackTrace)
        {
            string ret = string.Empty;
            var traceArray = stackTrace.Split(new string[] { "場所" }, StringSplitOptions.None);
            foreach (var trace in traceArray)
            {
                string newTrace = trace.Trim();
                if (string.IsNullOrEmpty(newTrace))
                {
                    continue;
                }
                ret += $"場所：{newTrace}{Environment.NewLine}";
            }
            return ret;
        }
        /// <summary>
        /// エラーメッセージ表示処理
        /// </summary>
        /// <param name="msg"></param>
        public static void ShowErrorMsg(string msg)
        {
            new MessageBoxPlus(
                  msg
                , DialogTitleError
                , MessageBoxButtons.OK
                , MessageBoxIcon.Error
            ).ShowDialog();
        }
        /// <summary>
        /// エラーメッセージ表示処理
        /// </summary>
        /// <param name="ex"></param>
        public static void ShowErrorMsg(Exception ex)
        {
            string apErrorMessage = string.Concat(
                  ex.Message
                , Environment.NewLine
                , "-----------------------------------------------------------"
                , Environment.NewLine
                , EditErrorStackTrace(ex.StackTrace)
            );
            new MessageBoxPlus(
                  "システムエラーが発生しました"
                , DialogTitleError
                , MessageBoxButtons.OK
                , MessageBoxIcon.Error
                , apErrorMessage
            ).ShowDialog();
        }
        /// <summary>
        /// 警告メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfoExcramationOK(string msg)
        {
            return new MessageBoxPlus(
                  msg
                , DialogTitleExcramation
                , MessageBoxButtons.OK
                , MessageBoxIcon.Exclamation
            ).ShowDialog();
        }
        /// <summary>
        /// 警告メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfoExcramationOKCancel(string msg)
        {
            return new MessageBoxPlus(
                  msg
                , DialogTitleExcramation
                , MessageBoxButtons.OKCancel
                , MessageBoxIcon.Exclamation
            ).ShowDialog();
        }
        /// <summary>
        /// 確認メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfoMsgOKCancel(string msg)
        {
            return new MessageBoxPlus(
                  msg
                , DialogTitleConfirm
                , MessageBoxButtons.OKCancel
                , MessageBoxIcon.Information
            ).ShowDialog();
        }
        /// <summary>
        /// 確認メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfoMsgOK(string msg)
        {
            return new MessageBoxPlus(
                   msg
                , DialogTitleConfirm
                , MessageBoxButtons.OK
                , MessageBoxIcon.Information
            ).ShowDialog();
        }
        /// <summary>
        /// プログレスダイアログでメソッド実行
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public static void ShowProgressDialog(Action method)
        {
            using (var prgBar = new ProgressDialog())
            {
                prgBar.Method = method;
                prgBar.ShowDialog();
                if (!prgBar.Success)
                {
                    throw prgBar.Error;
                }
            }
        }
        #endregion

        #region タイプ変換
        private const string MinProjectSpan = "1900/01";

        /// <summary>
        /// dtoを配列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static object[] DtoToArray<T>(this T data)
        {
            List<object> values = new List<object>();
            TypeDescriptor.GetProperties(typeof(T))
                .Cast<PropertyDescriptor>()
                .ToList()
                .ForEach(s => values.Add(s.GetValue(data)));
            return values.ToArray();
        }
        /// <summary>
        /// Excelのカラム名的なアルファベット文字列を数値変換
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static int AlphaToInt(string self)
        {
            int result = 0;
            if (string.IsNullOrEmpty(self)) return result;

            char[] chars = self.ToCharArray();
            int len = self.Length - 1;
            foreach (var c in chars)
            {
                int asc = (int)c - 64;
                if (asc < 1 || asc > 26) return 0;
                result += asc * (int)Math.Pow((double)26, (double)len--);
            }
            return result;
        }
        /// <summary>
        /// Excelのカラム名的なアルファベット文字列へ変換
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string ToAlphabet(this int self)
        {
            if (self <= 0)
            {
                return string.Empty;
            }
            int n = self % 26;
            n = (n == 0) ? 26 : n;
            string s = ((char)(n + 64)).ToString();
            if (self == n)
            {
                return s;
            }
            return ((self - n) / 26).ToAlphabet() + s;
        }
        /// <summary>
        /// Int型変換
        /// </summary>
        /// <param name="o"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static int ToInteger(
              object o
            , int format = 0
            )
        {
            if (o == null)
            {
                return format;
            }
            if (int.TryParse(o.ToString().Replace(",", string.Empty), out int i))
            {
                return i;
            }
            return format;
        }
        /// <summary>
        /// Float型変換
        /// </summary>
        /// <param name="o"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static float ToFloat(
              object o
            , float format = 0
            )
        {
            if (o == null)
            {
                return format;
            }
            if (float.TryParse(o.ToString().Replace(",", string.Empty), out float i))
            {
                return i;
            }
            return format;
        }
        /// <summary>
        /// DateTime型変換
        /// </summary>
        /// <param name="o"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(
              object o
            , string format = "yyyy/MM/dd HH:mm:ss"
            )
        {
            if (o == null)
            {
                return DateTime.MinValue;
            }
            if (DateTime.TryParse(o.ToString(), out DateTime dt))
            {
                return DateTime.Parse(DateTime.Parse(o.ToString()).ToString(format));
            }
            return DateTime.Parse(MinProjectSpan + "/01");
        }
        /// <summary>
        /// DateTime型変換
        /// </summary>
        /// <param name="yyyymm"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(
              string yyyymm
            )
        {
            if (string.IsNullOrEmpty(yyyymm) || yyyymm.Length != 6)
            {
                return DateTime.MinValue;
            }
            string strDate = $@"{yyyymm.Substring(0, 4)}/{yyyymm.Substring(4, 2)}/1";
            DateTime dt;
            if (!DateTime.TryParse(strDate, out dt))
            {
                return DateTime.MinValue;
            }
            return dt;
        }
        /// <summary>
        /// 文字列型日付変換
        /// </summary>
        /// <param name="yyyymm"></param>
        /// <param name="endOfMonth"></param>
        /// <returns></returns>
        public static string ConvertYYYYMMToYYYYMMDDSlash(
              string yyyymm
            , bool endOfMonth = true
            )
        {
            if (yyyymm.Length != 6)
            {
                return DateTime.MinValue.ToString(Formats.YYYYMMDD_Slash);
            }
            var dt = DateTime.Parse($"{yyyymm.Substring(0, 4)}/{yyyymm.Substring(4, 2)}/01");
            if (endOfMonth)
            {
                return dt.AddMonths(1).AddDays(-1).ToString(Formats.YYYYMMDD_Slash);
            }
            return dt.ToString(Formats.YYYYMMDD_Slash);
        }
        /// <summary>
        /// 入力文字列を年月（YYYY/MM）に変換
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ConvertInputToStrYM(string input)
        {
            if (input.Length < 6)
            {
                return string.Empty;
            }
            if (input.Contains("/"))
            {
                input = input.Replace("/", string.Empty);
            }
            if (!int.TryParse(input, out int intText))
            {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(input) || input.Length != 6)
            {
                return string.Empty;
            }
            string strDt = string.Join("/", input.Substring(0, 4), input.Substring(4, 2), "1");
            DateTime retDt;
            if (!DateTime.TryParse(strDt, out retDt))
            {
                return string.Empty;
            }
            return retDt.ToString(Formats.YYYYMM_Slash);
        }
        /// <summary>
        /// 入力文字列を年月日（YYYY/MM/DD）に変換
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ConvertInputToStrYMD(string input)
        {
            input = input.Replace("/", string.Empty);
            if (string.IsNullOrEmpty(input) || input.Length != 8)
            {
                return string.Empty;
            }
            string strDt = $"{input.Substring(0, 4)}/{input.Substring(4, 2)}/{input.Substring(6, 2)}";
            DateTime retDt;
            if (!DateTime.TryParse(strDt, out retDt))
            {
                return string.Empty;
            }
            return retDt.ToString(Formats.YYYYMMDD_Slash);
        }
        /// <summary>
        /// 文字列型フラグをbool型に変換
        /// </summary>
        /// <param name="blFlg"></param>
        /// <returns></returns>
        public static int ConvertFlgToVal(bool? blFlg)
        {
            if (blFlg == null)
            {
                return 0;
            }
            return blFlg.Value ? 1 : 0;
        }
        /// <summary>
        /// 文字列型フラグをbool型に変換
        /// </summary>
        /// <param name="blFlg"></param>
        /// <returns></returns>
        public static bool ConvertFlgToBool(object o)
        {
            return ToInteger(CommonUtil.TostringNullForbid(o)) == 1 ? true : false;
        }
        #endregion

        #region ファイル操作
        /// <summary>
        /// 既存ファイル削除
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }
        /// <summary>
        /// フォルダ作成
        /// </summary>
        public static void CreateFolder(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
        /// <summary>
        /// フォルダ削除
        /// </summary>
        public static void DeleteFolder(string path)
        {
            if (Directory.Exists(path))
            {
                Directory.Delete(path);
            }
        }
        /// <summary>
        /// ファイルを開きっぱなしにしていないかチェック
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static bool ValidateFileOpened(string fullPath)
        {
            try
            {
                if (!File.Exists(fullPath))
                {
                    return true;
                }
                using (var fs = File.OpenWrite(fullPath))
                {
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
