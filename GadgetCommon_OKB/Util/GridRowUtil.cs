﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GadgetCommon.Util
{
    public static class GridRowUtil<T> where T : new()
    {
        /// <summary>
        /// フォントサイズ
        /// </summary>
        public const float DefaultFontSize = 12;

        /// <summary>
        /// 行モデル取得
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public static T GetRowModel(DataGridViewRow r)
        {
            var ret = new T();
            ret.GetType().GetProperties().ToList().ForEach(p => p.SetValue(ret, r.Cells[p.Name].Value));
            return ret;
        }
        /// <summary>
        /// 全行モデル取得
        /// </summary>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public static List<T> GetAllRowsModel(object dataSource)
        {
            return ((BindingList<T>)dataSource).ToList();
        }
        /// <summary>
        /// 行番号の描画
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        /// <param name="FontSize"></param>
        public static void SetRowNum(
              object s
            , DataGridViewRowPostPaintEventArgs e
            , float FontSize = DefaultFontSize
            )
        {
            var dgv = (DataGridView)s;
            if (!dgv.RowHeadersVisible)
            {
                return;
            }
            //行番号を描画する範囲を決定する
            var rect = new Rectangle(
                  e.RowBounds.Left
                , e.RowBounds.Top
                , dgv.RowHeadersWidth
                , e.RowBounds.Height
            );
            rect.Inflate(-2, -2);
            // フォントサイズセット
            var newFont = new Font(
                  e.InheritedRowStyle.Font.FontFamily
                , FontSize
                , FontStyle.Regular
            );
            //行番号を描画する
            TextRenderer.DrawText(
                  e.Graphics
                , (e.RowIndex + 1).ToString()
                , newFont
                , rect
                , e.InheritedRowStyle.ForeColor
                , TextFormatFlags.Right | TextFormatFlags.VerticalCenter
            );
        }
    }
}
