﻿using System;
using System.Windows.Forms;
using WK.Libraries.BetterFolderBrowserNS;

namespace GadgetCommon.Util
{
    public static class SelectFolderUtil
    {
        #region Property
        /// <summary>
        /// 選択されたフォルダパス
        /// </summary>
        public static string SelectedFolderPath
        {
            get { return selectedFolderPath; }
        }
        private static string selectedFolderPath = string.Empty;
        #endregion

        #region Member
        /// <summary>
        /// フォルダ選択ダイアログ表示数（最大1とする）
        /// </summary>
        private static int DialogCount = 0;
        /// <summary>
        /// 検索ダイアログで選択したパスの環境変数
        /// </summary>
        private static string EnvironmentVariableName = $"{Application.ProductName}_CURRENT_DIR_PATH";
        #endregion

        #region Public
        /// <summary>
        /// ダイアログ表示
        /// </summary>
        /// <returns></returns>
        public static bool ShowDialog()
        {
            if (0 < DialogCount)
            {
                return false;
            }
            // 環境変数から前回の検索先を取得
            selectedFolderPath = string.Empty;
            string initDirectry = Environment.GetEnvironmentVariable(
                  EnvironmentVariableName
                , EnvironmentVariableTarget.User
            );
            DialogCount++;
            var dialog = new BetterFolderBrowser()
            {
                Multiselect = false,
                RootFolder = initDirectry,
            };
            try
            {
                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    // キャンセルの場合はダイアログを閉じる
                    return false;
                }
                // クラス外部から選択フォルダが参照できるよう、変数に格納
                selectedFolderPath = dialog.SelectedFolder;
                // 環境変数更新
                if (initDirectry != dialog.SelectedFolder)
                {
                    CommonUtil.ShowProgressDialog(new Action(() =>
                    {
                        Environment.SetEnvironmentVariable(
                              EnvironmentVariableName
                            , dialog.SelectedFolder
                            , EnvironmentVariableTarget.User
                        );
                    }));
                }
                return true;
            }
            finally
            {
                DialogCount = 0;
            }
        }
        #endregion
    }
}
