﻿using System.Drawing;

namespace GadgetCommon.Const
{
    public static class Colors
    {
        /// <summary>
        /// 画面コントロールの背景色
        /// </summary>
        public static Color BackColorGroup = Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(240)))), ((int)(((byte)(150)))));
        public static Color BackColorForm = Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(230)))), ((int)(((byte)(100)))));
        public static Color BackColorBtnOffEnter = Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
        public static Color BackColorBtnOnEnter = Color.Cyan;
        public static Color BackColorBtnReadOnly = Color.LightGray;
        public static Color BackColorUnabled = Color.Gray;
        public static Color BackColorEmpty = Color.Empty;

        /// <summary>
        /// グリッド編集時のセル背景色
        /// </summary>
        public static Color BackColorCellEdited = Color.Yellow;
        public static Color BackColorCellError = Color.Red;

        /// <summary>
        /// 文字色
        /// </summary>
        public static Color ForeColorClick = SystemColors.ControlText;
        public static Color ForeColorTimeTrackerY = Color.Red;
    }
}
