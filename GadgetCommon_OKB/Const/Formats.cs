﻿namespace GadgetCommon.Const
{
    public static class Formats
    {
        public const string YYYYMMDD_Slash          = "yyyy/MM/dd";
        public const string YYYYMM                  = "yyyyMM";
        public const string YYMM                    = "yyMM";
        public const string YYYYMMDD                = "yyyyMMdd";
        public const string YYYYMM_Slash            = "yyyy/MM";
        public const string YYYYM_JP                = "yyyy年M月";
        public const string YYYYMM_JP               = "yyyy年MM月";
        public const string YYYYMD_JP               = "yyyy年M月d日";
        public const string YYYY_JP                 = "yyyy年";
        public const string YYMM_JP                 = "yy年MM月";
        public const string MD_SLASH                = "M/d";
        public const string PadLeftZero2            = "00";
        public const string Float1                  = "#0.0";
        public const string Float2                  = "#0.00";
        public const string Float3                  = "#0.000";
        public const string Percent1                = "#0.0%";
        public const string YYYYMMDDHHMM_Slash      = "yyyy/MM/dd HH:mm";
        public const string YYYYMMDDHHMMSSFFF_Slash = "yyyy/MM/dd HH:mm:ss.FFF";
        public const string HMM_Colon               = "H:mm";
        public const string HMM_ColonOver24         = "[H]:mm";
        public const string HHMM_Colon              = "HH:mm";
    }
}
