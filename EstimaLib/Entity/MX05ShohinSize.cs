﻿namespace EstimaLib.Entity
{
    public class MX05ShohinSize : EntityBase
    {
        public int ShohinSizeID { get; set; }
        public string ShohinSizeName { get; set; }
    }
}
