﻿namespace EstimaLib.Entity
{
    public class MX04ChildCD : EntityBase
    {
        public string ChildCD { get; set; }
        public int ChildSeq { get; set; }
        public string ChildCDName { get; set; }
    }
}
