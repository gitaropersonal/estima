﻿namespace EstimaLib.Entity
{
    public class EX02EstimaBody : EntityBase
    {
        public string EstimaID { get; set; }
        public int MeisaiNo { get; set; }
        public int MeisaiType { get; set; }
        public string ShohinName { get; set; }
        public int Suryo { get; set; }
        public int Tanka { get; set; }
        public int Kingaku { get; set; }
        public float ConsumptionRate { get; set; }
        public float InspectionRate { get; set; }
        public float ExpenceRate { get; set; }
    }
}
