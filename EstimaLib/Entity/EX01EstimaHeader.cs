﻿using System;

namespace EstimaLib.Entity
{
    public class EX01EstimaHeader : EntityBase
    {
        public string EstimaID { get; set; }
        public int EstimaKbn { get; set; }
        public string EstimateNo { get; set; }
        public string OrderNo { get; set; }
        public string ProcessingNo { get; set; }
        public DateTime HakkoDate { get; set; }
        public DateTime LimitDate { get; set; }
        public string AddressCompany { get; set; }
        public string AddressOffice { get; set; }
        public string AddressTanto { get; set; }
        public string AddressSuppliers { get; set; }
        public string FieldName { get; set; }
        public string FieldRange { get; set; }
        public string DeadLine { get; set; }
        public float PlumbingRate { get; set; }
        public float HousingRate { get; set; }
        public int? Postage { get; set; }
        public string Condition1 { get; set; }
        public string Condition2 { get; set; }
        public string Condition3 { get; set; }
        public string Condition4 { get; set; }
        public string Condition5 { get; set; }
        public string Condition6 { get; set; }
        public string Condition7 { get; set; }
        public string Condition8 { get; set; }
        public string Condition9 { get; set; }
        public string Condition10 { get; set; }
        public string Condition11 { get; set; }
        public string Condition12 { get; set; }
        public string Condition13 { get; set; }
        public string Condition14 { get; set; }
        public string Condition15 { get; set; }
    }
}
