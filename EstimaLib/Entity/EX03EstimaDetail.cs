﻿namespace EstimaLib.Entity
{
    public class EX03EstimaDetail : EntityBase
    {
        public string EstimaID { get; set; }
        public int MeisaiNo { get; set; }
        public int MeisaiType { get; set; }
        public int Seq { get; set; }
        public int ShohinID { get; set; }
        public string ShohinCD { get; set; }
        public string ParentCDName { get; set; }
        public string ZaishitsuName { get; set; }
        public string ShohinSizeName { get; set; }
        public string ShohinName { get; set; }
        public int Tanka { get; set; }
        public int Suryo { get; set; }
        public string Unit { get; set; }
        public int BDCount { get; set; }
        public int ManualFlg { get; set; }
    }
}
