﻿namespace EstimaLib.Entity
{
    public class WX02TempEstimaRate : EntityBase
    {
        public string TempEstimaID { get; set; }
        public int MeisaiNo { get; set; }
        public float ConsumptionRate { get; set; }
        public float InspectionRate { get; set; }
        public float ExpenceRate { get; set; }

    }
}
