﻿using System;

namespace EstimaLib.Entity
{
    public abstract class EntityBase
    {
        public string AddIPAddress { get; set; }
        public string AddHostName { get; set; }
        public DateTime AddDate { get; set; }
        public string UpdIPAddress { get; set; }
        public string UpdHostName { get; set; }
        public DateTime UpdDate { get; set; }
    }
}
