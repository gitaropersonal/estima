﻿namespace EstimaLib.Entity
{
    public class LX01LogInfo : EntityBase
    {
        public int LogType { get; set; }
        public string ScreenName { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string Version { get; set; }
    }
}
