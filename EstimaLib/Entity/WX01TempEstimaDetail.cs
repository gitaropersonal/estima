﻿using System;

namespace EstimaLib.Entity
{
    public class WX01TempEstimaDetail
    {
        public string TempEstimaID { get; set; }
        public int MeisaiNo { get; set; }
        public int MeisaiType { get; set; }
        public int Seq { get; set; }
        public int ShohinID { get; set; }
        public string ShohinCD { get; set; }
        public string ParentCDName { get; set; }
        public string ShohinSizeName { get; set; }
        public string ShohinName { get; set; }
        public int Tanka { get; set; }
        public int Suryo { get; set; }
        public string Unit { get; set; }
        public int BDCount { get; set; }
        public int ManualFlg { get; set; }
        public string AddIPAddress { get; set; }
        public string AddHostName { get; set; }
        public DateTime AddDate { get; set; }
        public string UpdIPAddress { get; set; }
        public string UpdHostName { get; set; }
        public DateTime UpdDate { get; set; }

    }
}
