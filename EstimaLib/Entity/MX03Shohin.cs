﻿namespace EstimaLib.Entity
{
    public class MX03Shohin : EntityBase
    {
        public int ShohinID { get; set; }
        public string ParentCD { get; set; }
        public int ParentSeq { get; set; }
        public string ChildCD { get; set; }
        public int ChildSeq { get; set; }
        public string SizeCD { get; set; }
        public string ShohinCD { get; set; }
        public string Size1 { get; set; }
        public string Size2 { get; set; }
        public string Size3 { get; set; }
        public string Size4 { get; set; }
        public string ShohinSize { get; set; }
        public string TaishoYM { get; set; }
        public float Price { get; set; }
        public float ShikiriRate { get; set; }
        public float RiekiRate { get; set; }
        public float Shikiri { get; set; }
        public float? WeldingSize1 { get; set; }
        public float? WeldingSize2 { get; set; }
        public float? ScrewSize1 { get; set; }
        public float? ScrewSize2 { get; set; }
        public float? ScrewSize3 { get; set; }
        public float? SaddleSize { get; set; }
        public float? GBSize { get; set; }
        public float? InchDia { get; set; }
        public int? WeldingCount1 { get; set; }
        public int? WeldingCount2 { get; set; }
        public int? ScrewCount1 { get; set; }
        public int? ScrewCount2 { get; set; }
        public int? ScrewCount3 { get; set; }
        public int? SaddleCount { get; set; }
        public int? GBCount { get; set; }
        public int? BDCount { get; set; }
        public string ProcessingChildCD1 { get; set; }
        public int? ProcessingSeq1 { get; set; }
        public string ProcessingChildCD2 { get; set; }
        public int? ProcessingSeq2 { get; set; }
        public string ProcessingChildCD3 { get; set; }
        public int? ProcessingSeq3 { get; set; }
        public string ProcessingChildCD4 { get; set; }
        public int? ProcessingSeq4 { get; set; }
        public string ProcessingChildCD5 { get; set; }
        public int? ProcessingSeq5 { get; set; }
        public string ProcessingChildCDs { get; set; }
    }
}
