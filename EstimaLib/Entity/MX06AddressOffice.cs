﻿namespace EstimaLib.Entity
{
    public class MX06AddressOffice : EntityBase
    {
        public int OfficeID { get; set; }
        public string OfficeName { get; set; }
    }
}
