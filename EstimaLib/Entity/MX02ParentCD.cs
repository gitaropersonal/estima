﻿namespace EstimaLib.Entity
{
    public class MX02ParentCD: EntityBase
    {
        public string ParentCD { get; set; }
        public int ParentSeq { get; set; }
        public string ParentCDName { get; set; }
        public int DetailKbn { get; set; }
    }
}
