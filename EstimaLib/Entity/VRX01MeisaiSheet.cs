﻿namespace EstimaLib.Entity
{
    public class VRX01MeisaiSheet
    {
        public string EstimaID { get; set; }
        public int MeisaiNo { get; set; }
        public string ParentCD { get; set; }
        public int DetailKbn { get; set; }
        public int Seq { get; set; }
        public string Hinmei { get; set; }
        public string Bunrui { get; set; }
        public string ParentCDName { get; set; }
        public string ShohinNameDetail { get; set; }
        public string ShohinSizeName { get; set; }
        public int Suryo { get; set; }
        public string Unit { get; set; }
        public int Tanka { get; set; }
        public int Kingaku { get; set; }
    }
}
