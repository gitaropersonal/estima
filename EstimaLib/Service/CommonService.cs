﻿using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Util;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EstimaLib.Service
{
    public class CommonService
    {
        #region マスタ（単一）
        /// <summary>
        /// マスタ情報取得（子商品コード）
        /// </summary>
        /// <param name="childCD"></param>
        /// <returns></returns>
        public MX04ChildCD SelEntityChildCD(string childCD)
        {
            var ret = new MX04ChildCD();
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX04.ChildCD ");
            sb.AppendLine($",   MX04.ChildCDName ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX04ChildCD AS MX04 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    MX04.ChildCD = @childCD ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@childCD", childCD) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.ChildCD = CommonUtil.TostringNullForbid(dr[nameof(ret.ChildCD)]);
                            ret.ChildCDName = CommonUtil.TostringNullForbid(dr[nameof(ret.ChildCDName)]);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// マスタ情報取得（親コード）
        /// </summary>
        /// <param name="parendCD"></param>
        /// <param name="processing"></param>
        /// <returns></returns>
        public MX02ParentCD SelEntityParentCD(
              string parendCD
            , bool processing = false
            )
        {
            var ret = new MX02ParentCD();

            string cmOutProcessing = CommonUtil.GetCmout(!processing);

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX02.ParentCD ");
            sb.AppendLine($",   MX02.ParentCDName ");
            sb.AppendLine($",   MX02.DetailKbn ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX02ParentCD AS MX02 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    MX02.ParentCD = @parentCD ");
            sb.AppendLine($"{cmOutProcessing}AND MX02.DetailKbn = @detailKbn ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@parentCD", parendCD) { DbType = DbType.String });
                    cmd.Parameters.Add(new MySqlParameter("@detailKbn", (int)Enums.DetailKbn.ProcessingCost) { DbType = DbType.Int32 });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.ParentCD = CommonUtil.TostringNullForbid(dr[nameof(ret.ParentCD)]);
                            ret.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(ret.ParentCDName)]);
                            ret.DetailKbn = CommonUtil.ToInteger(dr[nameof(ret.DetailKbn)]);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion

        #region マスタ（リスト）
        /// <summary>
        /// マスタデータ取得（対象年月）
        /// </summary>
        /// <returns></returns>
        public List<string> SelTaishoYM()
        {
            var ret = new List<string>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX01.TaishoYM ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX01TaishoYM AS MX01 ");
            sb.AppendLine($"ORDER BY");
            sb.AppendLine($"     MX01.TaishoYM ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.Add(CommonUtil.TostringNullForbid(dr[0]));
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// マスタ情報取得（サイズ）
        /// </summary>
        /// <returns></returns>
        public List<MX05ShohinSize> SelMstSize()
        {
            var ret = new List<MX05ShohinSize>();
            var list = new List<MX05ShohinSize>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX05.ShohinSizeID ");
            sb.AppendLine($",   MX05.ShohinSizeName ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX05ShohinSize AS MX05 ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    MX05.ShohinSizeName ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new MX05ShohinSize();
                            dto.ShohinSizeID = CommonUtil.ToInteger(dr[nameof(dto.ShohinSizeID)]);
                            dto.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinSizeName)]);
                            list.Add(dto);
                        }
                    }
                }
            }
            list = list.OrderBy(n => n.ShohinSizeName.Replace("X", "x").Count(x => x == 'x'))
                       .ThenBy(n => CommonUtil.CutAnAlpha(n.ShohinSizeName.Replace("X", "x") + "xZZZxZZZ").Split('x')[0])
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ShohinSizeName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[0]))
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ShohinSizeName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[1]))
                       .ThenBy(n => CommonUtil.CutAInteger((Regex.Replace(n.ShohinSizeName.Replace("X", "x"), "[^a-zA-Z]", "999") + "xZZZxZZZ").Split('x')[2]))
                       .ToList();
            list.ForEach(n => ret.Add(n));
            return ret;
        }
        /// <summary>
        /// マスタデータ取得（営業所）
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<MX06AddressOffice> SelMstAddressOffice(string name = "")
        {
            string cmOutName = EstimaUtil.GetCmout(string.IsNullOrEmpty(name));
            var ret = new List<MX06AddressOffice>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    MX06.OfficeID ");
            sb.AppendLine($",   MX06.OfficeName ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    MX06AddressOffice AS MX06 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    0 = 0 ");
            sb.AppendLine($"{cmOutName}MX06.OfficeName = @name ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    MX06.OfficeName ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@name", name));
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new MX06AddressOffice();
                            dto.OfficeID = CommonUtil.ToInteger(dr[nameof(dto.OfficeID)]);
                            dto.OfficeName = CommonUtil.TostringNullForbid(dr[nameof(dto.OfficeName)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion

        #region 伝票
        /// <summary>
        /// 伝票ヘッダー取得
        /// </summary>
        /// <param name="estimaID"></param>
        /// <returns></returns>
        public EX01EstimaHeader SelEstimaHeader(string estimaID)
        {
            var ret = new EX01EstimaHeader();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    EX01.EstimaID ");
            sb.AppendLine($",   EX01.EstimaKbn ");
            sb.AppendLine($",   EX01.EstimateNo ");
            sb.AppendLine($",   EX01.OrderNo ");
            sb.AppendLine($",   EX01.ProcessingNo ");
            sb.AppendLine($",   EX01.HakkoDate ");
            sb.AppendLine($",   EX01.LimitDate ");
            sb.AppendLine($",   EX01.AddressCompany ");
            sb.AppendLine($",   EX01.AddressOffice ");
            sb.AppendLine($",   EX01.AddressTanto ");
            sb.AppendLine($",   EX01.AddressSuppliers ");
            sb.AppendLine($",   EX01.FieldName ");
            sb.AppendLine($",   EX01.FieldRange ");
            sb.AppendLine($",   EX01.DeadLine ");
            sb.AppendLine($",   EX01.PlumbingRate ");
            sb.AppendLine($",   EX01.HousingRate ");
            sb.AppendLine($",   EX01.Postage ");
            sb.AppendLine($",   EX01.Condition1 ");
            sb.AppendLine($",   EX01.Condition2 ");
            sb.AppendLine($",   EX01.Condition3 ");
            sb.AppendLine($",   EX01.Condition4 ");
            sb.AppendLine($",   EX01.Condition5 ");
            sb.AppendLine($",   EX01.Condition6 ");
            sb.AppendLine($",   EX01.Condition7 ");
            sb.AppendLine($",   EX01.Condition8 ");
            sb.AppendLine($",   EX01.Condition9 ");
            sb.AppendLine($",   EX01.Condition10 ");
            sb.AppendLine($",   EX01.Condition11 ");
            sb.AppendLine($",   EX01.Condition12 ");
            sb.AppendLine($",   EX01.Condition13 ");
            sb.AppendLine($",   EX01.Condition14 ");
            sb.AppendLine($",   EX01.Condition15 ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX01EstimaHeader AS EX01 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EX01.EstimaID = @estimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ret.EstimaID = CommonUtil.TostringNullForbid(dr[nameof(ret.EstimaID)]);
                            ret.EstimaKbn = CommonUtil.ToInteger(dr[nameof(ret.EstimaKbn)]);
                            ret.EstimateNo = CommonUtil.TostringNullForbid(dr[nameof(ret.EstimateNo)]);
                            ret.OrderNo = CommonUtil.TostringNullForbid(dr[nameof(ret.OrderNo)]);
                            ret.ProcessingNo = CommonUtil.TostringNullForbid(dr[nameof(ret.ProcessingNo)]);
                            ret.HakkoDate = CommonUtil.ToDateTime((object)dr[nameof(ret.HakkoDate)]);
                            ret.LimitDate = CommonUtil.ToDateTime((object)dr[nameof(ret.LimitDate)]);
                            ret.AddressCompany = CommonUtil.TostringNullForbid(dr[nameof(ret.AddressCompany)]);
                            ret.AddressOffice = CommonUtil.TostringNullForbid(dr[nameof(ret.AddressOffice)]);
                            ret.AddressTanto = CommonUtil.TostringNullForbid(dr[nameof(ret.AddressTanto)]);
                            ret.AddressSuppliers = CommonUtil.TostringNullForbid(dr[nameof(ret.AddressSuppliers)]);
                            ret.FieldName = CommonUtil.TostringNullForbid(dr[nameof(ret.FieldName)]);
                            ret.FieldRange = CommonUtil.TostringNullForbid(dr[nameof(ret.FieldRange)]);
                            ret.DeadLine = CommonUtil.TostringNullForbid(dr[nameof(ret.DeadLine)]);
                            ret.PlumbingRate = CommonUtil.ToFloat(dr[nameof(ret.PlumbingRate)]);
                            ret.HousingRate = CommonUtil.ToFloat(dr[nameof(ret.HousingRate)]);
                            string postage = CommonUtil.TostringNullForbid(dr[nameof(ret.Postage)]);
                            if (string.IsNullOrEmpty(postage))
                            {
                                ret.Postage = null;
                            }
                            else
                            {
                                ret.Postage = CommonUtil.ToInteger(postage);
                            }
                            ret.Condition1 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition1)]);
                            ret.Condition2 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition2)]);
                            ret.Condition3 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition3)]);
                            ret.Condition4 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition4)]);
                            ret.Condition5 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition5)]);
                            ret.Condition6 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition6)]);
                            ret.Condition7 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition7)]);
                            ret.Condition8 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition8)]);
                            ret.Condition9 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition9)]);
                            ret.Condition10 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition10)]);
                            ret.Condition11 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition11)]);
                            ret.Condition12 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition12)]);
                            ret.Condition13 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition13)]);
                            ret.Condition14 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition14)]);
                            ret.Condition15 = CommonUtil.TostringNullForbid(dr[nameof(ret.Condition15)]);
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 伝票抽出（明細）
        /// </summary>
        /// <param name="estimaID"></param>
        /// <returns></returns>
        public List<EstimaCoverDto> SelEstimaCover(string estimaID)
        {
            var ret = new List<EstimaCoverDto>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"WITH UnitPlumbing AS( ");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       EX02.EstimaID ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"   ,   1 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(EX02.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       EX02EstimaBody AS EX02 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($"   AND EX02.MeisaiNo  ");
            sb.AppendLine($"   AND EX02.MeisaiType = 1");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitHousing AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       EX02.EstimaID ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"   ,   2 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(EX02.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       EX02EstimaBody AS EX02 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($"   AND EX02.MeisaiNo  ");
            sb.AppendLine($"   AND EX02.MeisaiType IN(2) ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitProcessing AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       EX02.EstimaID ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"   ,   3 AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(EX02.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       EX02EstimaBody AS EX02 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($"   AND EX02.MeisaiNo  ");
            sb.AppendLine($"   AND EX02.MeisaiType = 3 ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitAll AS(");
            sb.AppendLine($"   SELECT ");
            sb.AppendLine($"       EX02.EstimaID ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"   ,   MIN(EX02.MeisaiType) AS MeisaiType ");
            sb.AppendLine($"   ,   SUM(EX02.Kingaku) AS Kingaku ");
            sb.AppendLine($"   FROM ");
            sb.AppendLine($"       EX02EstimaBody AS EX02 ");
            sb.AppendLine($"   WHERE  ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($"   GROUP BY ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo ");
            sb.AppendLine($"), ");
            sb.AppendLine($"UnitShohinName AS( ");
            sb.AppendLine($"   SELECT  ");
            sb.AppendLine($"       EX02.EstimaID  ");
            sb.AppendLine($"   ,   EX02.MeisaiNo  ");
            sb.AppendLine($"   ,   MinUnit.MeisaiType  ");
            sb.AppendLine($"   ,   EX02.ShohinName  ");
            sb.AppendLine($"   FROM  ");
            sb.AppendLine($"       EX02EstimaBody AS EX02  ");
            sb.AppendLine($"   INNER JOIN(  ");
            sb.AppendLine($"       SELECT  ");
            sb.AppendLine($"           EX02.EstimaID  ");
            sb.AppendLine($"       ,   EX02.MeisaiNo  ");
            sb.AppendLine($"       ,   MIN(EX02.MeisaiType) AS MeisaiType  ");
            sb.AppendLine($"       FROM  ");
            sb.AppendLine($"           EX02EstimaBody AS EX02  ");
            sb.AppendLine($"       WHERE   ");
            sb.AppendLine($"           EX02.EstimaID = @estimaID  ");
            sb.AppendLine($"       GROUP BY  ");
            sb.AppendLine($"           EX02.EstimaID   ");
            sb.AppendLine($"       ,   EX02.MeisaiNo   ");
            sb.AppendLine($"   )AS MinUnit  ");
            sb.AppendLine($"   ON  ");
            sb.AppendLine($"       EX02.EstimaID   = MinUnit.EstimaID  ");
            sb.AppendLine($"   AND EX02.MeisaiNo   = MinUnit.MeisaiNo  ");
            sb.AppendLine($"   AND EX02.MeisaiType = MinUnit.MeisaiType  ");
            sb.AppendLine($"   WHERE   ");
            sb.AppendLine($"       EX02.EstimaID = @estimaID ");
            sb.AppendLine($") ");
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    UA.EstimaID ");
            sb.AppendLine($",   UA.MeisaiNo ");
            sb.AppendLine($",   UA.MeisaiType ");
            sb.AppendLine($",   US.ShohinName ");
            sb.AppendLine($",   1 AS Suryo ");
            sb.AppendLine($",   CASE WHEN UP.Kingaku IS NULL THEN 0 ELSE UP.Kingaku END AS PlumbingKingaku ");
            sb.AppendLine($",   CASE WHEN UH.Kingaku IS NULL THEN 0 ELSE UH.Kingaku END AS HousingKingaku ");
            sb.AppendLine($",   CASE WHEN UR.Kingaku IS NULL THEN 0 ELSE UR.Kingaku END AS ProcessingKingaku ");
            sb.AppendLine($",   UA.Kingaku ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    UnitAll AS UA ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    UnitPlumbing AS UP ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.EstimaID   = UP.EstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo   = UP.MeisaiNo ");
            sb.AppendLine($"AND UP.MeisaiType = 1 ");
            sb.AppendLine($"LEFT OUTER JOIN  ");
            sb.AppendLine($"    UnitHousing AS UH ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.EstimaID   = UH.EstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo   = UH.MeisaiNo ");
            sb.AppendLine($"AND UH.MeisaiType = 2 ");
            sb.AppendLine($"LEFT OUTER JOIN  ");
            sb.AppendLine($"    UnitProcessing AS UR ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.EstimaID   = UR.EstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo   = UR.MeisaiNo ");
            sb.AppendLine($"AND UR.MeisaiType = 3 ");
            sb.AppendLine($"LEFT OUTER JOIN ");
            sb.AppendLine($"    UnitShohinName AS US ");
            sb.AppendLine($"ON");
            sb.AppendLine($"    UA.EstimaID   = US.EstimaID ");
            sb.AppendLine($"AND UA.MeisaiNo   = US.MeisaiNo ");
            sb.AppendLine($"AND UA.MeisaiType = US.MeisaiType ");
            sb.AppendLine($"WHERE  ");
            sb.AppendLine($"    UA.EstimaID = @estimaID ");
            sb.AppendLine($"ORDER BY  ");
            sb.AppendLine($"    UA.EstimaID ");
            sb.AppendLine($",   UA.MeisaiNo ");
            sb.AppendLine($";  ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new EstimaCoverDto();
                            dto.ShohinName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinName)]);
                            dto.MeisaiNo = CommonUtil.ToInteger(dr[nameof(dto.MeisaiNo)]);
                            dto.MeisaiType = CommonUtil.ToInteger(dr[nameof(dto.MeisaiType)]);
                            dto.Suryo = CommonUtil.ToInteger(dr[nameof(dto.Suryo)]);
                            dto.PlumbingKingaku = CommonUtil.ToInteger(dr[nameof(dto.PlumbingKingaku)]);
                            dto.HousingKingaku = CommonUtil.ToInteger(dr[nameof(dto.HousingKingaku)]);
                            dto.ProcessingKingaku = CommonUtil.ToInteger(dr[nameof(dto.ProcessingKingaku)]);
                            dto.Tanka = CommonUtil.ToInteger(dr[nameof(dto.Kingaku)]);
                            dto.Kingaku = CommonUtil.ToInteger(dr[nameof(dto.Kingaku)]);
                            ret.Add(dto);
                        }
                    }
                }
            }
            return ret;
        }
        #endregion

        #region ロック
        /// <summary>
        /// ロックされているか？
        /// </summary>
        /// <param name="estimaID"></param>
        /// <returns></returns>
        public bool JudgeExistLock(string estimaID)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    COUNT(*)");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX04EstimaLock AS EX04 ");
            sb.AppendLine($"WHERE  ");
            sb.AppendLine("    EstimaID = @estimaID ");
            sb.AppendLine($";  ");

            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            return 0 < CommonUtil.ToInteger(dr[0]);
                        }
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// ロックエンティティ取得
        /// </summary>
        /// <param name="estimaID"></param>
        public EX04EstimaLock SelLock(string estimaID)
        {
            var ret = new EX04EstimaLock();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    EX04.EstimaID ");
            sb.AppendLine($",   EX04.AddIPAddress ");
            sb.AppendLine($",   EX04.AddHostName ");
            sb.AppendLine($",   EX04.AddDate ");
            sb.AppendLine($",   EX04.UpdIPAddress ");
            sb.AppendLine($",   EX04.UpdHostName ");
            sb.AppendLine($",   EX04.UpdDate ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    EX04EstimaLock AS EX04 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    EX04.EstimaID = @estimaID ");
            sb.AppendLine($"; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {

                            ret.EstimaID = CommonUtil.TostringNullForbid(dr[nameof(ret.EstimaID)]);
                            ret.AddIPAddress = CommonUtil.TostringNullForbid(dr[nameof(ret.AddIPAddress)]);
                            ret.AddHostName = CommonUtil.TostringNullForbid(dr[nameof(ret.AddHostName)]);
                            ret.AddDate = CommonUtil.ToDateTime((object)CommonUtil.TostringNullForbid(dr[nameof(ret.AddDate)]));
                            ret.UpdIPAddress = CommonUtil.TostringNullForbid(dr[nameof(ret.UpdIPAddress)]);
                            ret.UpdHostName = CommonUtil.TostringNullForbid(dr[nameof(ret.UpdHostName)]);
                            ret.UpdDate = CommonUtil.ToDateTime((object)CommonUtil.TostringNullForbid(dr[nameof(ret.UpdDate)]));
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// ロック追加
        /// </summary>
        /// <param name="estimaID"></param>
        public void AddLock(string estimaID)
        {
            var sb = new StringBuilder();
            sb.AppendLine($"INSERT INTO EX04EstimaLock( ");
            sb.AppendLine($"    EstimaID ");
            sb.AppendLine($",   AddIPAddress ");
            sb.AppendLine($",   AddHostName ");
            sb.AppendLine($",   AddDate ");
            sb.AppendLine($",   UpdIPAddress ");
            sb.AppendLine($",   UpdHostName ");
            sb.AppendLine($",   UpdDate ");
            sb.AppendLine($")VALUES( ");
            sb.AppendLine($"    @estimaID ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
            sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
            sb.AppendLine($",   CURRENT_TIMESTAMP ");
            sb.AppendLine($"); ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// ロック削除
        /// </summary>
        /// <param name="estimaID"></param>
        public void DelLock(string estimaID)
        {
            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine("DELETE FROM ");
            sb.AppendLine("    EX04EstimaLock ");
            sb.AppendLine("WHERE ");
            sb.AppendLine("    EstimaID = @estimaID ");
            sb.AppendLine("; ");
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    cmd.ExecuteNonQuery();
                }
            }
        }
        #endregion
    }
}
