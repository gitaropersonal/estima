﻿namespace EstimaLib.Const
{
    public static class Messages
    {
        #region Ask
        public const string MsgAskCreate                   = "{0}を作成しますか？";
        public const string MsgAskOpenFile                 = "作成しました\r\n{0}を開きますか？";
        public const string MsgAskClear                    = "クリアしますか？";
        public const string MsgAskUpdate                   = "更新しますか？";
        public const string MsgAskDelete                   = "削除しますか？";
        public const string MsgAskUnmatchedNengetsu        = "対象年月と異なるファイルを選択している可能性があります。\r\nよろしいですか？";
        public const string MsgAskUnmatchedDetailName      = "入力内容に元の詳細内容が含まれていません。\r\n集計に影響する可能性がありますが、よろしいですか？";
        public const string MsgAskClose                    = "終了しますか？";
        public const string MsgAskLogOff                   = "ログオフしますか？";
        #endregion

        #region Error
        public const string MsgErrorWrongPassword          = "担当者IDまたはパスワードに誤りがあります";
        public const string MsgErrorNotExistInMaster       = "マスタに存在しないID・コードが入力されています";
        public const string MsgErrorWrongStartDate         = "過去の管理開始日より後の日付を入力してください";
        public const string MsgErrorLargeSmallCollision    = "{0}は{1}より大きい値を入力してください";
        public const string MsgErrorMissEmpty              = "有効な{0}を入力してください";
        public const string MsgErrorWrongLengthOfProjectCD = "プロジェクトコードは8桁としてください";
        public const string MsgErrorUnmatchedPassword      = "パスワードが一致していません";
        public const string MsgErrorNotChangedPassword     = "パスワードが変更されていません";
        public const string MsgErrorUsedPassword           = "過去のパスワードは使用できません";
        public const string MsgErrorNotSelectedFile        = "ファイルを選択してください";
        public const string MsgErrorNotExistFile           = "ファイルがありません";
        public const string MsgErrorWrongFormatFile        = "ファイル形式が不正です";
        public const string MsgErrorWrongNendo             = "ファイル内の年度が画面と一致していません";
        public const string MsgErroWrongInput              = "{0}が不正です";
        public const string MsgErrorDouple                 = "{0}が重複しています";
        public const string MsgErrorWorkDetailDouple       = "{0}が重複しています。\r\n※非表示の作業も確認してみてください";
        public const string MsgErrorNotSelectedRow         = "行を選択してください。";
        public const string MsgErrorNotExistDatas          = "対象のデータがありません";
        public const string MsgErrorFileAlreadyOpend       = "ファイルがすでに開かれています。";
        public const string MsgErrorNotExistTemplate       = "テンプレートファイルが存在しません。";
        public const string MsgErrorDoupleBoot             = "多重起動はできません";
        #endregion

        #region Info
        public const string MsgInfoFinishUpdate            = "更新しました";
        public const string MsgInfoFinishDelete            = "削除しました";
        public const string MsgInfoFileCreateStart         = "{0}作成処理：開始 対象者：{1}";
        public const string MsgInfoFileCreateEnd           = "{0}作成処理：終了 対象者：{1}";
        #endregion

        #region オペレーションログ
        public static string OpeLogSearch = "検索処理";
        public static string OpeLogUpdate = "更新処理";
        public static string OpeLogHakko  = "発行処理";

        public static string OpeLogShowEstimaData = "書類データ表示処理";
        public static string OpeLogDelEstimaData  = "書類データ削除処理";
        public static string OpeLogCopyEstimaData = "書類データ複製処理";

        public static string OpeLogClearScreen    = "初期化処理";
        public static string OpeLogAddLockEntity  = "ロック追加処理";
        public static string OpeLogDelLockEntity  = "ロック解除処理";
        #endregion
    }
}
