﻿using System.Collections.Generic;

namespace EstimaLib.Const
{
    public static class Enums
    {
        /// <summary>
        /// 権限区分
        /// </summary>
        public enum KengenKbn
        {
            None = -1,
            Normal = 1,
            Admin = 10,
        }
        /// <summary>
        /// 担当区分
        /// </summary>
        public enum TantoKbn
        {
            None = -1,
            Naiko = 0,
            Inin = 1,
            Haken = 2,
        }
        /// <summary>
        /// 役職区分
        /// </summary>
        public enum YakushokuKbn
        {
            None = -1,
            Tanto = 0,
            Kacho = 1,
            Bucho = 2,
            CEO = 10,
        }
        /// <summary>
        /// グリッドのセル編集区分
        /// </summary>
        public enum EditStatus
        {
            None = 0,
            Show = 1,
            Insert = 10,
            Update = 20,
        }
        /// <summary>
        /// システムメニュ
        /// </summary>
        public enum SysType
        {
            Estima = 1,
            Master = 2,
            System = 3,
            Data = 4,
        }
        /// <summary>
        /// ログ種別
        /// </summary>
        public enum LogType
        {
            Info = 1,
            Error = 2,
        }
        /// <summary>
        /// ログステータス区分
        /// </summary>
        public enum LogStatusKbn
        {
            Start = 1,
            Stop = 3,
            End = 5,
        }
        /// <summary>
        /// マスタ種別
        /// </summary>
        public enum MstType
        {
            None = 0,
            PlumbingType = 9,
            ShohinName = 10,
            Zaishitsu = 11,
            ShohinSize = 12,
            ShikiriRate = 13,
            RiekiRate = 14,
            AddressOffice = 15,
        }
        /// <summary>
        /// 単位（配管）
        /// </summary>
        public enum PlumbingUnit
        {
            M = 0,
            Number = 1,
            Count = 2,
            Sheets = 3,
            Set = 4,
        }
        /// <summary>
        /// スクリーンの表示モード（書類作成画面）
        /// </summary>
        public enum ScreenModeEditEstima
        {
            Add = 0,
            Edit = 10,
            ReadOnly = 15,
            Copy = 20,
        }
        /// <summary>
        /// スクリーンの表示モード（商品マスタ画面）
        /// </summary>
        public enum ScreenModeMstShohin
        {
            Screen = 0,
            DialogSingle = 10,
            DialogSingleProcessing = 11,
            DialogMulti = 20,
            DialogMultiProcessing = 21,
        }
        /// <summary>
        /// 明細内訳区分
        /// </summary>
        public enum DetailKbn
        {
            Plumbing = 1,
            Housing = 3,
            Joint = 5,
            ProcessingCost = 10,
            Consumable = 12,
            Inspection = 14,
            Expense = 70,
            //Postage = 80,
            //Util = 90,
            All = 1000,
            TabPlumbing = 1100,
            TabHousing = 1200,
            TabProcessing = 1300,
        }
        /// <summary>
        /// 見積書区分
        /// </summary>
        public enum EstimaKbn
        {
            Estimate = 1,
            Invoice = 2,
        }
        /// <summary>
        /// ログイン権限マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<KengenKbn, string>> KengenKbns = new Dictionary<KengenKbn, string>() {
            { KengenKbn.Normal, "一般"},
            { KengenKbn.Admin, "管理者"},
        };
        /// <summary>
        /// 担当区分マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<TantoKbn, string>> TantoKbns = new Dictionary<TantoKbn, string>() {
            { TantoKbn.Naiko, "内工"},
            { TantoKbn.Inin , "委任"},
            { TantoKbn.Haken, "派遣"},
        };
        /// <summary>
        /// 役職区分マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<YakushokuKbn, string>> YakushokuKbns = new Dictionary<YakushokuKbn, string>() 
        {
            { YakushokuKbn.Tanto, "一般"},
            { YakushokuKbn.Kacho, "課長"},
            { YakushokuKbn.Bucho, "部長"},
            { YakushokuKbn.CEO  , "CEO"},
        };
        /// <summary>
        /// ログ情報マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<LogType, string>> LogTypes = new Dictionary<LogType, string>()
        {
            { LogType.Info, "Info"},
            { LogType.Error, "Error"},
        };
        /// <summary>
        /// マスタ種別リスト
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<MstType, string>> MstTypes = new Dictionary<MstType, string>()
        {
            { MstType.PlumbingType , "配管種別"},
            { MstType.ShohinName   , "商品名"},
            { MstType.Zaishitsu    , "材質"},
            { MstType.ShohinSize   , "サイズ"},
            { MstType.ShikiriRate  , "仕切率"},
            { MstType.RiekiRate    , "利益率"},
            { MstType.AddressOffice, "宛先（営業所）"},
        };
        /// <summary>
        /// 単位（配管）マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<PlumbingUnit, string>> PlumbingUnits = new Dictionary<PlumbingUnit, string>()
        {
            { PlumbingUnit.M     , "m"},
            { PlumbingUnit.Number, "本"},
            { PlumbingUnit.Count , "個"},
            { PlumbingUnit.Sheets, "枚"},
            { PlumbingUnit.Set   , "式"},
        };
        /// <summary>
        /// 明細内訳区分マスタ
        /// </summary>
        public static readonly IEnumerable<KeyValuePair<DetailKbn, string>> DetailKbns = new Dictionary<DetailKbn, string>()
        {
            { DetailKbn.Plumbing      , "配管"},
            { DetailKbn.Housing       , "ハウジング"},
            { DetailKbn.Joint         , "継手"},
            { DetailKbn.ProcessingCost, "配管加工費"},
            { DetailKbn.Consumable    , "消耗品雑材"},
            { DetailKbn.Inspection    , "検査費"},
            { DetailKbn.Expense       , "経費"},
            //{ DetailKbn.Postage       , "送料"},
            //{ DetailKbn.Util          , "汎用"},
        };
    }
}
