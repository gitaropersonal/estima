﻿using EstimaLib.Dto;
using GadgetCommon.Util;
using System;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.IO;

namespace EstimaLib.Const
{
    public static class Config
    {
        #region Member
        /// <summary>
        /// FileMap
        /// </summary>
        private static ExeConfigurationFileMap FileMap = LoadFileMap();
        /// <summary>
        /// Configuration
        /// </summary>
        private static Configuration EsitmaConfig = LoadConfig();
        #endregion

        #region Property
        /// <summary>
        /// 会社情報
        /// </summary>
        /// <returns></returns>
        public static CompanyInfoDto CompanyInfo
        {
            get {
                return new CompanyInfoDto()
                {
                    CompanyName = LoadAppSetting("CompanyName"),
                    CompanyAddress = LoadAppSetting("CompanyAddress"),
                    CompanyTel = LoadAppSetting("CompanyTel"),
                    CompanyFax = LoadAppSetting("CompanyFax"),
                };
            }
        }
        /// <summary>
        /// 伝票テンプレートのパス取得
        /// </summary>
        /// <returns></returns>
        public static string TempletePathEstima
        {
            get { return Path.Combine(Environment.CurrentDirectory, "Templete", "伝票テンプレート.xlsx"); } 
        }
        /// <summary>
        /// 消耗品雑材率
        /// </summary>
        /// <returns></returns>
        public static float ConsumptionRate
        {
            get { return CommonUtil.ToFloat(LoadAppSetting("ConsumptionRate")); }
        }
        /// <summary>
        /// 検査費
        /// </summary>
        /// <returns></returns>
        public static float InspectionRate
        {
            get { return CommonUtil.ToFloat(LoadAppSetting("InspectionRate")); }
        }
        /// <summary>
        /// 経費率
        /// </summary>
        /// <returns></returns>
        public static float ExpenceRate
        {
            get { return CommonUtil.ToFloat(LoadAppSetting("ExpenceRate")); }
        }
        #endregion

        #region Method
        /// <summary>
        /// SQLコネクション作成
        /// </summary>
        /// <returns></returns>
        public static MySqlConnection CreateSqlConnection()
        {
            return new MySqlConnection(LoadDbConnection());
        }
        /// <summary>
        /// FileMapロード
        /// </summary>
        /// <returns></returns>
        private static ExeConfigurationFileMap LoadFileMap()
        {
            return new ExeConfigurationFileMap { ExeConfigFilename = Path.Combine(Environment.CurrentDirectory, "Estima.config") };
        }
        /// <summary>
        /// ロード
        /// </summary>
        /// <returns></returns>
        private static Configuration LoadConfig()
        {
            return ConfigurationManager.OpenMappedExeConfiguration(FileMap, ConfigurationUserLevel.None);
        }
        /// <summary>
        /// 設定値取得（接続文字列）
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string LoadDbConnection()
        {
            return EsitmaConfig.ConnectionStrings.ConnectionStrings["DBConnString"].ConnectionString;
        }
        /// <summary>
        /// 設定値取得
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string LoadAppSetting(string key)
        {
            return EsitmaConfig.AppSettings.Settings[key].Value;
        }
        #endregion
    }
}
