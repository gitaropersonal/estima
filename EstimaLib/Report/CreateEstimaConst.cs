﻿namespace EstimaLib.Report
{
    internal static class CreateEstimaConst
    {
        // シート名
        internal const string SheetNameCover = "表紙";
        internal const string SheetNameBody = "見積内訳書";
        // メッセージ
        internal const string MsgConsiderShikiri = "御社仕切合計金額 ¥{0}- にて御検討をお願いします";
        // SEQ
        internal enum RowSeq
        {
              Summary = 1000
            , Total = 10000
        }
        // 行番号
        internal const int BodyStartRowNum = 6;
        // 高さ
        internal const double BodySheetRowHeight = 19.5;
        // 条件の規定値
        internal const int DefaultConditionCount = 9;
        // セルの値
        internal const string ShohinNamePostage = "運搬交通費";
    }
}
