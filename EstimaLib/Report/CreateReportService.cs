﻿using EstimaLib.Const;
using EstimaLib.Entity;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace EstimaLib.Report
{
    internal class CreateReportService
    {
        /// <summary>
        /// 明細シートコンテンツ取得
        /// </summary>
        /// <param name="estimaID"></param>
        /// <returns></returns>
        internal List<VRX01MeisaiSheet> GetContentBodySheet(string estimaID)
        {
            var ret = new List<VRX01MeisaiSheet>();

            // SQL文の作成
            var sb = new StringBuilder();
            sb.AppendLine($"SELECT ");
            sb.AppendLine($"    VRX01.EstimaID ");
            sb.AppendLine($",   VRX01.MeisaiNo ");
            sb.AppendLine($",   VRX01.ParentCD ");
            sb.AppendLine($",   VRX01.DetailKbn ");
            sb.AppendLine($",   VRX01.Seq ");
            sb.AppendLine($",   VRX01.Hinmei ");
            sb.AppendLine($",   VRX01.Bunrui ");
            sb.AppendLine($",   VRX01.ParentCDName ");
            sb.AppendLine($",   VRX01.ShohinNameDetail ");
            sb.AppendLine($",   VRX01.ShohinSizeName ");
            sb.AppendLine($",   VRX01.Suryo ");
            sb.AppendLine($",   VRX01.Unit ");
            sb.AppendLine($",   VRX01.Tanka ");
            sb.AppendLine($",   VRX01.Kingaku ");
            sb.AppendLine($"FROM ");
            sb.AppendLine($"    VRX01MeisaiSheet AS VRX01 ");
            sb.AppendLine($"WHERE ");
            sb.AppendLine($"    VRX01.EstimaID = @estimaID ");
            sb.AppendLine($"ORDER BY ");
            sb.AppendLine($"    VRX01.EstimaID ");
            sb.AppendLine($",   VRX01.MeisaiNo ");
            sb.AppendLine($",   VRX01.DetailKbn ");
            sb.AppendLine($",   VRX01.Seq ");
            sb.AppendLine($"; ");

            int oldMeisaiNo = 0;
            int oldDetailKbn = 0;
            using (var cn = Config.CreateSqlConnection())
            {
                using (var cmd = cn.CreateCommand())
                {
                    cn.Open();
                    cmd.CommandText = sb.ToString();
                    cmd.Parameters.Add(new MySqlParameter("@estimaID", estimaID) { DbType = DbType.String });
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var dto = new VRX01MeisaiSheet();
                            dto.EstimaID = CommonUtil.TostringNullForbid(dr[nameof(dto.EstimaID)]);
                            dto.MeisaiNo = CommonUtil.ToInteger(dr[nameof(dto.MeisaiNo)]);
                            dto.ParentCD = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentCD)]);
                            dto.DetailKbn = CommonUtil.ToInteger(dr[nameof(dto.DetailKbn)]);
                            dto.Seq = CommonUtil.ToInteger(dr[nameof(dto.Seq)]);
                            dto.Hinmei = CommonUtil.TostringNullForbid(dr[nameof(dto.Hinmei)]);
                            dto.Bunrui = CommonUtil.TostringNullForbid(dr[nameof(dto.Bunrui)]);
                            dto.ParentCDName = CommonUtil.TostringNullForbid(dr[nameof(dto.ParentCDName)]);
                            dto.ShohinNameDetail = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinNameDetail)]);
                            dto.ShohinSizeName = CommonUtil.TostringNullForbid(dr[nameof(dto.ShohinSizeName)]);
                            dto.Suryo = CommonUtil.ToInteger(dr[nameof(dto.Suryo)]);
                            dto.Unit = CommonUtil.TostringNullForbid(dr[nameof(dto.Unit)]);
                            dto.Tanka = CommonUtil.ToInteger(dr[nameof(dto.Tanka)]);
                            dto.Kingaku = CommonUtil.ToInteger(dr[nameof(dto.Kingaku)]);
                            if (0 < ret.Count && oldDetailKbn != dto.DetailKbn)
                            {
                                // 小計行追加
                                ret.Add(this.CerateSummaryRowDto(dto.EstimaID, oldMeisaiNo, oldDetailKbn));
                                if (oldMeisaiNo != dto.MeisaiNo)
                                {
                                    // 合計行追加
                                    ret.Add(this.CreateTotalRowDto(estimaID, oldMeisaiNo));
                                }
                            }
                            ret.Add(dto);

                            // インクリメント
                            oldMeisaiNo = dto.MeisaiNo;
                            oldDetailKbn = dto.DetailKbn;
                        }
                    }
                }
            }
            // 最後の小計行追加
            ret.Add(this.CerateSummaryRowDto(estimaID, oldMeisaiNo, oldDetailKbn));
            // 最後の合計行追加
            ret.Add(this.CreateTotalRowDto(estimaID, oldMeisaiNo));
            return ret;
        }
        /// <summary>
        /// 小計行dto作成
        /// </summary>
        /// <param name="estimaID"></param>
        /// <param name="meisaiNo"></param>
        /// <param name="detailKbn"></param>
        /// <returns></returns>
        private VRX01MeisaiSheet CerateSummaryRowDto(
              string estimaID
            , int meisaiNo
            , int detailKbn
            )
        {
            return new VRX01MeisaiSheet()
            {
                EstimaID = estimaID,
                MeisaiNo = meisaiNo,
                ParentCD = string.Empty,
                DetailKbn = detailKbn,
                Seq = (int)CreateEstimaConst.RowSeq.Summary,
                Hinmei = string.Empty,
                Bunrui = string.Empty,
                ParentCDName = string.Empty,
                ShohinNameDetail = string.Empty,
                ShohinSizeName = "（小計）",
                Unit = "-",
            };
        }
        /// <summary>
        /// 合計行dto作成
        /// </summary>
        /// <param name="estimaID"></param>
        /// <param name="meisaiNo"></param>
        /// <returns></returns>
        private VRX01MeisaiSheet CreateTotalRowDto(
              string estimaID
            , int meisaiNo
            )
        {
            return new VRX01MeisaiSheet()
            {
                EstimaID = estimaID,
                MeisaiNo = meisaiNo,
                ParentCD = string.Empty,
                DetailKbn = (int)CreateEstimaConst.RowSeq.Total,
                Seq = (int)CreateEstimaConst.RowSeq.Total,
                Hinmei = string.Empty,
                Bunrui = string.Empty,
                ParentCDName = string.Empty,
                ShohinNameDetail = string.Empty,
                ShohinSizeName = "（合計）",
                Unit = "-",
            };
        }
    }
}
