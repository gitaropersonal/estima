﻿using EstimaLib.Const;
using EstimaLib.Dto;
using EstimaLib.Entity;
using EstimaLib.Service;
using EstimaLib.Util;
using GadgetCommon.Const;
using GadgetCommon.Util;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EstimaLib.Report
{
    public class CreateEstimaLogic
    {
        #region Property
        /// <summary>
        /// 作成した帳票のフルパス
        /// </summary>
        public string EstimaFullPath { get; set; }
        #endregion

        #region Member
        /// <summary>
        /// 専用サービス
        /// </summary>
        private readonly CreateReportService PersonalService = new CreateReportService();
        /// <summary>
        /// 共通サービス
        /// </summary>
        private readonly CommonService CommonService = new CommonService();
        /// <summary>
        /// 書類ID
        /// </summary>
        private string EstimaID = string.Empty;
        #endregion

        #region Public
        /// <summary>
        /// ファイルパス取得
        /// </summary>
        /// <param name="estimaID"></param>
        /// <param name="selectedPath"></param>
        /// <returns></returns>
        public string GetFilePath(
              string estimaID
            , string selectedPath
            )
        {
            // 帳票データ取得
            var header = this.CommonService.SelEstimaHeader(estimaID);

            // ファイルパス作成
            string filePath = selectedPath;
            switch (header.EstimaKbn)
            {
                case (int)Enums.EstimaKbn.Estimate:
                    filePath = Path.Combine(filePath, $"{header.EstimateNo} {header.AddressCompany} {header.HakkoDate.ToString(Formats.YYYYMMDD)}.xlsx");
                    break;
                case (int)Enums.EstimaKbn.Invoice:
                    filePath = Path.Combine(filePath, $"{header.ProcessingNo} {header.AddressCompany} {header.HakkoDate.ToString(Formats.YYYYMMDD)}.xlsx");
                    break;
            }
            return filePath;
        }
        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="estimaID"></param>
        /// <param name="selectedPath"></param>
        public bool Main(
              string estimaID
            , string selectedPath
            )
        {
            try
            {
                this.EstimaID = estimaID;

                // 帳票パス取得
                this.EstimaFullPath = this.GetFilePath(estimaID, selectedPath);

                // テンプレートコピー
                this.CopyFromTemplete();

                // 帳票データ取得
                var header = this.CommonService.SelEstimaHeader(this.EstimaID);

                // 明細データ取得
                var bodies = this.CommonService.SelEstimaCover(this.EstimaID);

                // 表紙データ転記
                this.PostingCover(header, bodies);

                // 明細シート転記
                this.PostingDetails(estimaID, header.EstimaKbn, bodies);
            }
            catch (Exception ex)
            {
                EstimaUtil.ShowErrorMsg(ex, "伝票作成ロジック");
                return false;
            }
            return true;
        }
        #endregion

        #region Business
        /// <summary>
        /// テンプレートからコピー
        /// </summary>
        private void CopyFromTemplete()
        {
            // テンプレートからコピー
            string copyPath = $"{this.EstimaFullPath}★";
            CommonUtil.DeleteFile(copyPath);
            File.Copy(Config.TempletePathEstima, copyPath);
            using (var xlsxFile = File.OpenRead(copyPath))
            {
                using (var package = new ExcelPackage(new FileInfo(copyPath)))
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    package.SaveAs(new FileInfo(Config.TempletePathEstima));
                }
            };
            CommonUtil.DeleteFile(this.EstimaFullPath);
            File.Copy(copyPath, this.EstimaFullPath);
            CommonUtil.DeleteFile(copyPath);
        }
        /// <summary>
        /// セル結合
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="range"></param>
        private void MergeCells(
              ExcelWorksheet sheet
            , string range
            )
        {
            if (!sheet.Cells[range].Merge)
            {
                sheet.Cells[range].Merge = true;
            }
        }
        /// <summary>
        /// 仕切金額計算
        /// </summary>
        /// <param name="header"></param>
        /// <param name="bodies"></param>
        /// <returns></returns>
        private int[] CalcShikiriKingaku(
              EX01EstimaHeader header
            , List<EstimaCoverDto> bodies
            )
        {
            int sumProcessing = 0;
            int sumHousing = 0;
            foreach (var dto in bodies)
            {
                sumProcessing += dto.PlumbingKingaku;
                sumProcessing += dto.ProcessingKingaku;
                sumHousing += dto.HousingKingaku;
            }
            int sumShikiriProcessing = (int)Math.Round(
                  (double)((sumProcessing + CommonUtil.ToInteger(header.Postage)) * header.PlumbingRate)
                , 0
                , MidpointRounding.AwayFromZero
            );
            int sumShikiriHousing = (int)Math.Round(
                  (double)(sumHousing * header.HousingRate)
                , 0
                , MidpointRounding.AwayFromZero
            );
            return new int[] { sumShikiriProcessing, sumShikiriHousing };
        }
        #endregion

        #region 表紙
        /// <summary>
        /// 表紙データ転記
        /// </summary>
        /// <param name="header"></param>
        /// <param name="bodies"></param>
        private void PostingCover(
              EX01EstimaHeader header
            , List<EstimaCoverDto> bodies
            )
        {
            // 転記
            string tempPath = $"{this.EstimaFullPath}★";
            CommonUtil.DeleteFile(tempPath);
            File.Copy(this.EstimaFullPath, tempPath);
            CommonUtil.DeleteFile(this.EstimaFullPath);
            using (var xlsxFile = File.OpenRead(tempPath))
            {
                using (var package = new ExcelPackage(new FileInfo(tempPath)))
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    var sheet = package.Workbook.Worksheets[CreateEstimaConst.SheetNameCover];
                    // ヘッダー
                    this.PostingHeaderInfo(sheet, header);
                    // 送料
                    var postageDto = new EstimaCoverDto()
                    {
                        ShohinName = CreateEstimaConst.ShohinNamePostage,
                        Suryo = 1,
                        Kingaku = (header.Postage == null) ? -1 : header.Postage.Value,
                    };
                    bodies.Add(postageDto);
                    // 明細
                    int startRowNum = this.PostingBodyInfo(sheet, bodies);
                    // 見積条件
                    this.PostingConditionInfo(sheet, header, startRowNum);
                    // 仕切金額
                    if (header.EstimaKbn == (int)Enums.EstimaKbn.Estimate)
                    {   
                        var plumbingHousingPair =  this.CalcShikiriKingaku(header, bodies);
                        if (0 < plumbingHousingPair[0])
                        {
                            sheet.Cells["Y21"].Value = plumbingHousingPair[0];
                        }
                        if (0 < plumbingHousingPair[1])
                        {
                            sheet.Cells["Y22"].Value = plumbingHousingPair[1];
                        }
                        string sumShikiriKingaku = (plumbingHousingPair[0] + plumbingHousingPair[1]).ToString("#,0");
                        sheet.Cells["O23"].Value = string.Format(CreateEstimaConst.MsgConsiderShikiri, sumShikiriKingaku);
                    }
                    // 保存
                    package.SaveAs(new FileInfo(this.EstimaFullPath));
                }
            };
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// ヘッダー情報転記
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="header"></param>
        private void PostingHeaderInfo(
              ExcelWorksheet sheet
            , EX01EstimaHeader header
            )
        {
            switch (header.EstimaKbn)
            {
                case (int)Enums.EstimaKbn.Estimate:
                    sheet.Cells["A1"].Value = "御　見　積　書";
                    sheet.Cells["AA5"].Value = "見積№";
                    sheet.Cells["AD5"].Style.Numberformat.Format = "@";
                    sheet.Cells["AD5"].Value = header.EstimateNo;
                    sheet.Cells["AA5:AJ5"].Style.Border.Bottom.Style = ExcelBorderStyle.Hair;
                    sheet.Cells["AA7"].Value = "有効期限";
                    sheet.Cells["AE7"].Value = header.LimitDate.ToString(Formats.YYYYMD_JP);
                    sheet.Cells["A12"].Value = "下記の通り御見積申し上げます。";
                    sheet.Cells["AA7:AJ7"].Style.Border.Bottom.Style = ExcelBorderStyle.Hair;
                    sheet.Cells["A24"].Value = "合計金額";
                    break;
                case (int)Enums.EstimaKbn.Invoice:
                    sheet.Cells["A1"].Value = "御請求明細内訳書";
                    sheet.Cells["AA4"].Value = "発注№";
                    sheet.Cells["AD4"].Style.Numberformat.Format = "@";
                    sheet.Cells["AD4"].Value = header.OrderNo;
                    sheet.Cells["AA4:AJ4"].Style.Border.Bottom.Style = ExcelBorderStyle.Hair;
                    sheet.Cells["AA5"].Value = "加工№";
                    sheet.Cells["AD5"].Style.Numberformat.Format = "@";
                    sheet.Cells["AD5"].Value = header.ProcessingNo;
                    sheet.Cells["AA5:AJ5"].Style.Border.Bottom.Style = ExcelBorderStyle.Hair;
                    sheet.Cells["A12"].Value = "下記の通り御請求申し上げます。";
                    sheet.Cells["A24"].Value = "御請求金額";
                    break;
            }
            sheet.Cells["X10"].Value = Config.CompanyInfo.CompanyName;
            sheet.Cells["Y12"].Value = Config.CompanyInfo.CompanyAddress;
            sheet.Cells["AD13"].Value = Config.CompanyInfo.CompanyTel;
            sheet.Cells["AD14"].Value = Config.CompanyInfo.CompanyFax;
            if (!string.IsNullOrEmpty(header.AddressCompany))
            {
                sheet.Cells["A7"].Value = header.AddressCompany;
                sheet.Cells["O7"].Value = "御中";
            }
            string addressJoin = string.Empty;
            if (!string.IsNullOrEmpty(header.AddressCompany)) { addressJoin += $"{header.AddressOffice} "; };
            if (!string.IsNullOrEmpty(header.AddressTanto)) { addressJoin += $"{header.AddressTanto}様 "; };
            if (!string.IsNullOrEmpty(header.AddressOffice)) { addressJoin += $"{header.AddressSuppliers}様分"; };
            sheet.Cells["A9"].Value = addressJoin;
            sheet.Cells["E14"].Value = header.FieldName;
            sheet.Cells["E17"].Value = header.FieldRange;
            sheet.Cells["E20"].Value = header.DeadLine;
            sheet.Cells["AA6"].Value = "作成日";
            sheet.Cells["AE6"].Value = header.HakkoDate.ToString(Formats.YYYYMD_JP);
            sheet.Cells["AA6:AJ6"].Style.Border.Bottom.Style = ExcelBorderStyle.Hair;
        }
        /// <summary>
        /// 明細情報転記
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="bodies"></param>
        private int PostingBodyInfo(
              ExcelWorksheet sheet
            , List<EstimaCoverDto> bodies
            )
        {
            const int BodyStartRowNum = 29;
            int rowIndex = BodyStartRowNum;
            if (1 < bodies.Count)
            {
                // 行追加
                sheet.InsertRow(BodyStartRowNum + 1, bodies.Count - 1, BodyStartRowNum + 1);
            }
            bool topRow = true;
            foreach (var body in bodies)
            {
                // 値
                if (body.ShohinName == "運搬交通費")
                {
                    this.PostingBodyInfoPostage(sheet, rowIndex, body);
                }
                else
                {
                    sheet.Cells[$"A{rowIndex}"].Value = body.ShohinName;
                    sheet.Cells[$"S{rowIndex}"].Value = body.Suryo;
                    sheet.Cells[$"V{rowIndex}"].Value = "式";
                    sheet.Cells[$"Y{rowIndex}"].Value = body.Tanka;
                    sheet.Cells[$"AE{rowIndex}"].Value = body.Kingaku;
                }
                // セル結合
                this.MergeCells(sheet, $"A{rowIndex}:R{rowIndex}");
                this.MergeCells(sheet, $"S{rowIndex}:U{rowIndex}");
                this.MergeCells(sheet, $"V{rowIndex}:X{rowIndex}");
                this.MergeCells(sheet, $"Y{rowIndex}:AD{rowIndex}");
                this.MergeCells(sheet, $"AE{rowIndex}:AJ{rowIndex}");

                // 形式
                sheet.Cells[$"Y{rowIndex}"].Style.Numberformat.Format = "#,0";

                // 配置
                sheet.Cells[$"A{rowIndex}:R{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                sheet.Cells[$"A{rowIndex}:R{rowIndex}"].Style.Indent++;
                sheet.Cells[$"S{rowIndex}:U{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet.Cells[$"V{rowIndex}:X{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet.Cells[$"Y{rowIndex}:AD{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Cells[$"AE{rowIndex}:AJ{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                if (!topRow)
                {
                    sheet.Cells[$"A{rowIndex}:AJ{rowIndex}"].Style.Border.BorderAround(ExcelBorderStyle.Hair);
                }
                rowIndex++;
                topRow = false;
            }
            // 合計金額
            sheet.Cells[$"J25"].Formula = $"AE{rowIndex}";

            // 合計行
            sheet.Cells[$"S{rowIndex}:U{rowIndex}"].Formula = $"SUM(S{BodyStartRowNum}:U{rowIndex - 1})";
            sheet.Cells[$"V{rowIndex}:X{rowIndex}"].Value = "-";
            sheet.Cells[$"Y{rowIndex}:AD{rowIndex}"].Formula = $"SUM(S{BodyStartRowNum}:U{rowIndex - 1})";
            sheet.Cells[$"Y{rowIndex}:AD{rowIndex}"].Value = "-";
            sheet.Cells[$"AE{rowIndex}:AJ{rowIndex}"].Formula = $"SUM(AE{BodyStartRowNum}:AE{rowIndex - 1})";

            // 配置
            sheet.Cells[$"S{rowIndex}:X{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[$"Y{rowIndex}:AD{rowIndex}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            // 罫線
            sheet.Cells[$"A{rowIndex}:AJ{rowIndex}"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            sheet.Cells[$"A{BodyStartRowNum}:A{rowIndex - 1}"].Style.Border.Left.Style = ExcelBorderStyle.Double;
            sheet.Cells[$"AJ{BodyStartRowNum}:AJ{rowIndex - 1}"].Style.Border.Right.Style = ExcelBorderStyle.Double;

            // 見積条件欄が始まる行番号を返す
            return rowIndex + 3;
        }
        /// <summary>
        /// 明細情報転記（送料）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowIndex"></param>
        /// <param name="body"></param>
        private void PostingBodyInfoPostage(
              ExcelWorksheet sheet
            , int rowIndex
            , EstimaCoverDto body
            )
        {
            sheet.Cells[$"A{rowIndex}"].Value = body.ShohinName;
            sheet.Cells[$"S{rowIndex}"].Value = body.Suryo;
            sheet.Cells[$"V{rowIndex}"].Value = "式";
            switch (body.Kingaku)
            {
                case -1:
                    sheet.Cells[$"Y{rowIndex}"].Value = "-";
                    sheet.Cells[$"AE{rowIndex}"].Value = "実費精算 ";
                    break;
                case 0:
                    sheet.Cells[$"Y{rowIndex}"].Value = "-";
                    sheet.Cells[$"AE{rowIndex}"].Value = "運賃無し ";
                    break;
                default:
                    sheet.Cells[$"Y{rowIndex}"].Value = body.Kingaku;
                    sheet.Cells[$"AE{rowIndex}"].Value = body.Kingaku;
                    break;
            }
        }
        /// <summary>
        /// 見積条件転記
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="header"></param>
        /// <param name="startRowNum"></param>
        private void PostingConditionInfo(
              ExcelWorksheet sheet
            , EX01EstimaHeader header
            , int startRowNum
            )
        {
            switch (header.EstimaKbn)
            {
                case (int)Enums.EstimaKbn.Estimate:
                    sheet.Cells[$"A{startRowNum - 1}"].Value = "御　見　積　条　件";
                    break;
                case (int)Enums.EstimaKbn.Invoice:
                    sheet.Cells[$"A{startRowNum - 1}"].Value = "御　請　求　条　件";
                    break;
            }
            var conditions = new List<string>();
            this.AddCondition(ref conditions, header.Condition1);
            this.AddCondition(ref conditions, header.Condition2);
            this.AddCondition(ref conditions, header.Condition3);
            this.AddCondition(ref conditions, header.Condition4);
            this.AddCondition(ref conditions, header.Condition5);
            this.AddCondition(ref conditions, header.Condition6);
            this.AddCondition(ref conditions, header.Condition7);
            this.AddCondition(ref conditions, header.Condition8);
            this.AddCondition(ref conditions, header.Condition9);
            this.AddCondition(ref conditions, header.Condition10);
            this.AddCondition(ref conditions, header.Condition11);
            this.AddCondition(ref conditions, header.Condition12);
            this.AddCondition(ref conditions, header.Condition13);
            this.AddCondition(ref conditions, header.Condition14);
            this.AddCondition(ref conditions, header.Condition15);
            if (CreateEstimaConst.DefaultConditionCount < conditions.Count)
            {
                sheet.InsertRow(startRowNum + 1, conditions.Count - CreateEstimaConst.DefaultConditionCount, startRowNum + 1);
            }
            foreach (var condition in conditions)
            {
                int index = conditions.IndexOf(condition) + startRowNum;
                sheet.Cells[$"A{index}"].Value = conditions.IndexOf(condition) + 1;
                sheet.Cells[$"D{index}"].Value = condition;
                this.MergeCells(sheet, $"A{index}:C{index}");
                this.MergeCells(sheet, $"D{index}:AJ{index}");
            }
        }
        /// <summary>
        /// 条件追加
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="condition"></param>
        private void AddCondition(
              ref List<string> conditions
              ,string condition
            )
        {
            if (!string.IsNullOrEmpty(condition)) { conditions.Add(condition); };
        }
        #endregion

        #region 明細シート
        /// <summary>
        /// 明細データ転記
        /// </summary>
        /// <param name="estimaID"></param>
        /// <param name="estimaKbn"></param>
        /// <param name="bodies"></param>
        private void PostingDetails(
              string estimaID
            , int estimaKbn
            , List<EstimaCoverDto> bodies
            )
        {
            // 明細データ取得
            var contents = this.PersonalService.GetContentBodySheet(estimaID);
            // 明細シート複製
            this.CopyBodySheets(estimaKbn, contents);
            // ファイル作成
            string tempPath = $"{this.EstimaFullPath}★";
            CommonUtil.DeleteFile(tempPath);
            File.Copy(this.EstimaFullPath, tempPath);
            CommonUtil.DeleteFile(this.EstimaFullPath);
            // ファイルオープン
            using (var xlsxFile = File.OpenRead(tempPath))
            {
                using (var package = new ExcelPackage(new FileInfo(tempPath)))
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    // 明細番号一覧取得
                    var meisaiNoList = contents.GroupBy(x => new { estimaID = x.EstimaID, meisaiNo = x.MeisaiNo }).ToList();
                    int meisaiNoIndex = 0;
                    foreach (var meisaiNo in meisaiNoList)
                    {
                        meisaiNoIndex = meisaiNoList.IndexOf(meisaiNo) + 1;
                        // シート取得
                        var sheet = package.Workbook.Worksheets[meisaiNoIndex];
                        // タイトル
                        string title = string.Empty;
                        switch (estimaKbn)
                        {
                            case (int)Enums.EstimaKbn.Estimate:
                                title = "見　積　内　訳　書";
                                break;
                            case (int)Enums.EstimaKbn.Invoice:
                                title = "請　求　内　訳　書";
                                break;
                        }
                        sheet.Cells[$"I2"].Value = $"{meisaiNoIndex}/{meisaiNoList.Count}";
                        // 品名
                        sheet.Cells[$"A2"].Value = bodies.First(n => n.MeisaiNo == meisaiNo.Key.meisaiNo).ShohinName;
                        // データ抽出
                        var tgtDatas = contents.Where(n => n.MeisaiNo == meisaiNo.Key.meisaiNo).ToList();
                        // 行追加
                        sheet.InsertRow(CreateEstimaConst.BodyStartRowNum, tgtDatas.Count, CreateEstimaConst.BodyStartRowNum);
                        // 抽出したデータを1行ずつループ
                        int seqNo = 1;
                        int oldDetailKbn = 0;
                        string oldParentCD = string.Empty;
                        int rowNum = CreateEstimaConst.BodyStartRowNum;
                        var sumSuryoCellList = new List<string>();
                        var sumKingakuCellList = new List<string>();
                        var currHinmei = string.Empty;
                        bool changeHinmeiFlg = false;
                        foreach (var rowData in tgtDatas)
                        {
                            // 転記
                            sheet.Cells[$"D{rowNum}"].Value = rowData.ShohinNameDetail;
                            sheet.Cells[$"E{rowNum}"].Value = rowData.ShohinSizeName;
                            sheet.Cells[$"F{rowNum}"].Value = rowData.Suryo;
                            sheet.Cells[$"G{rowNum}"].Value = rowData.Unit;
                            sheet.Cells[$"I{rowNum}"].Value = rowData.Kingaku;
                            switch (rowData.Seq)
                            {
                                case (int)CreateEstimaConst.RowSeq.Summary:
                                    // 小計行の編集
                                    this.EditRowSummary(
                                          sheet
                                        , tgtDatas
                                        , rowData
                                        , rowNum
                                        , ref sumSuryoCellList
                                        , ref sumKingakuCellList
                                    );
                                    break;
                                case (int)CreateEstimaConst.RowSeq.Total:
                                    // 合計行の編集
                                    this.EditRowTotal(
                                          sheet
                                        , rowNum
                                        , sumSuryoCellList
                                        , sumKingakuCellList
                                    );
                                    break;
                                default:
                                    // 明細行の編集
                                    this.EditRowBody(
                                          sheet
                                        , tgtDatas
                                        , rowData
                                        , rowNum
                                        , oldDetailKbn
                                        , oldParentCD
                                        , ref changeHinmeiFlg
                                        , ref currHinmei
                                        , ref seqNo
                                    );
                                    break;
                            }
                            // 行の高さ
                            sheet.Rows[rowNum].Height = CreateEstimaConst.BodySheetRowHeight;
                            // 罫線セット
                            this.SetBorderLine(sheet, rowData, rowNum, oldDetailKbn, changeHinmeiFlg);
                            // フォントサイズ自動フィット設定
                            for (int i = 1; i < CommonUtil.AlphaToInt("I"); i++)
                            {
                                string range = $"{CommonUtil.ToAlphabet(i)}{rowNum}";
                                sheet.Cells[range].Style.ShrinkToFit = true;
                            }
                            // インクリメント
                            rowNum++;
                            oldDetailKbn = rowData.DetailKbn;
                            oldParentCD = rowData.ParentCD;
                        }
                        // フッター（会社名）
                        string oldFooter = sheet.HeaderFooter.OddFooter.RightAlignedText;
                        sheet.HeaderFooter.OddFooter.RightAlignedText = oldFooter.Replace("＜会社名＞", Config.CompanyInfo.CompanyName);
                        // 罫線セット
                        sheet.Cells[$"A6:I{rowNum - 1}"].Style.Border.BorderAround(ExcelBorderStyle.Double);
                    }
                    // 保存
                    package.SaveAs(new FileInfo(this.EstimaFullPath));
                }
            };
            // 不要なファイルを削除
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// 明細シート複製
        /// </summary>
        /// <param name="estimaKbn"></param>
        /// <param name="contents"></param>
        private void CopyBodySheets(
              int estimaKbn
            , List<VRX01MeisaiSheet> contents
            )
        {
            // シート複製
            var meisaiNoList = contents.GroupBy(x => new { estimaID = x.EstimaID, meisaiNo = x.MeisaiNo }).ToList();
            var sheetNameList = new List<string>();
            switch (estimaKbn)
            {
                case (int)Enums.EstimaKbn.Estimate:
                    meisaiNoList.ForEach(n => sheetNameList.Add($"見積内訳({meisaiNoList.IndexOf(n) + 1})"));
                    break;
                case (int)Enums.EstimaKbn.Invoice:
                    meisaiNoList.ForEach(n => sheetNameList.Add($"請求内訳({meisaiNoList.IndexOf(n) + 1})"));
                    break;
            }
            // コピー元のシート名
            const string originSheetName = CreateEstimaConst.SheetNameBody;

            // ファイル作成
            string tempPath = $"{this.EstimaFullPath}★";
            CommonUtil.DeleteFile(tempPath);
            File.Copy(this.EstimaFullPath, tempPath);
            CommonUtil.DeleteFile(this.EstimaFullPath);

            // ファイルオープン
            using (var xlsxFile = File.OpenRead(tempPath))
            {
                using (var package = new ExcelPackage(new FileInfo(tempPath)))
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    foreach (var sheetName in sheetNameList)
                    {
                        // シート取得
                        package.Workbook.Worksheets.Copy(originSheetName, sheetName);
                    }
                    // コピー元のシート削除
                    package.Workbook.Worksheets.Delete(originSheetName);
                    // 保存
                    package.SaveAs(new FileInfo(this.EstimaFullPath));
                }
            };
            // 不要なファイルを削除
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// 罫線セット
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowData"></param>
        /// <param name="rowNum"></param>
        /// <param name="oldDetailKbn"></param>
        /// <param name="changeHinmeiFlg"></param>
        private void SetBorderLine(
              ExcelWorksheet sheet
            , VRX01MeisaiSheet rowData
            , int rowNum
            , int oldDetailKbn
            , bool changeHinmeiFlg
            )
        {
            if (oldDetailKbn == rowData.DetailKbn)
            {
                sheet.Cells[$"A{rowNum}:I{rowNum}"].Style.Border.Top.Style = ExcelBorderStyle.Hair;
            }
            else
            {
                if (changeHinmeiFlg)
                {
                    sheet.Cells[$"A{rowNum}:I{rowNum}"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                }
                else
                {
                    sheet.Cells[$"A{rowNum}"].Style.Border.Top.Style = ExcelBorderStyle.Hair;
                    sheet.Cells[$"B{rowNum}:I{rowNum}"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                }
            }
            for (int i = 1; i < CommonUtil.AlphaToInt("I"); i++)
            {
                sheet.Cells[$"{CommonUtil.ToAlphabet(i)}{rowNum}"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            }
        }
        /// <summary>
        /// 明細行の編集
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="tgtDatas"></param>
        /// <param name="rowData"></param>
        /// <param name="rowNum"></param>
        /// <param name="oldDetailKbn"></param>
        /// <param name="oldParentCD"></param>
        /// <param name="changeHinmeiFlg"></param>
        /// <param name="currHinmei"></param>
        /// <param name="seqNo"></param>
        private void EditRowBody(
              ExcelWorksheet sheet
            , List<VRX01MeisaiSheet> tgtDatas
            , VRX01MeisaiSheet rowData
            , int rowNum
            , int oldDetailKbn
            , string oldParentCD
            , ref bool changeHinmeiFlg
            , ref string currHinmei
            , ref int seqNo
            )
        {
            changeHinmeiFlg = false;
            if (currHinmei != rowData.Hinmei)
            {
                sheet.Cells[$"A{rowNum}"].Value = $"({seqNo}){rowData.Hinmei}";
                changeHinmeiFlg = true;
                seqNo++;
            }
            currHinmei = rowData.Hinmei;
            if (oldDetailKbn != rowData.DetailKbn)
            {
                sheet.Cells[$"B{rowNum}"].Value = rowData.Bunrui;
            }
            if (oldParentCD != rowData.ParentCD)
            {
                sheet.Cells[$"C{rowNum}"].Value = rowData.ParentCDName;
            }
            sheet.Cells[$"H{rowNum}"].Value = rowData.Tanka;
        }
        /// <summary>
        /// 小計行の編集
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="tgtDatas"></param>
        /// <param name="rowData"></param>
        /// <param name="rowNum"></param>
        /// <param name="sumSuryoCellList"></param>
        /// <param name="sumKingakuCellList"></param>
        private void EditRowSummary(
              ExcelWorksheet sheet
            , List<VRX01MeisaiSheet> tgtDatas
            , VRX01MeisaiSheet rowData
            , int rowNum
            , ref List<string> sumSuryoCellList
            , ref List<string> sumKingakuCellList
            )
        {
            sheet.Cells[$"E{rowNum}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            sheet.Cells[$"H{rowNum}"].Value = "-";
            var tgtSeqDatas = tgtDatas.Where(n => n.Seq < rowData.Seq);
            int minRowNum = tgtDatas.IndexOf(tgtSeqDatas.First(n => n.DetailKbn == rowData.DetailKbn)) + CreateEstimaConst.BodyStartRowNum;
            int maxRowNum = tgtDatas.IndexOf(tgtSeqDatas.Last(n => n.DetailKbn == rowData.DetailKbn)) + CreateEstimaConst.BodyStartRowNum;
            sheet.Cells[$"F{rowNum}"].Formula = $"SUM(F{minRowNum}:F{maxRowNum})";
            sheet.Cells[$"I{rowNum}"].Formula = $"SUM(I{minRowNum}:I{maxRowNum})";
            sumSuryoCellList.Add($"F{rowNum}");
            sumKingakuCellList.Add($"I{rowNum}");
        }
        /// <summary>
        /// 合計行の編集
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowNum"></param>
        /// <param name="sumSuryoCellList"></param>
        /// <param name="sumKingakuCellList"></param>
        private void EditRowTotal(
              ExcelWorksheet sheet
            , int rowNum
            , List<string> sumSuryoCellList
            , List<string> sumKingakuCellList
            )
        {
            sheet.Cells[$"E{rowNum}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            sheet.Cells[$"H{rowNum}"].Value = "-";
            sheet.Cells[$"F{rowNum}"].Formula = $"SUM({string.Join(",", sumSuryoCellList)})";
            sheet.Cells[$"I{rowNum}"].Formula = $"SUM({string.Join(",", sumKingakuCellList)})";
        }
        #endregion
    }
}
