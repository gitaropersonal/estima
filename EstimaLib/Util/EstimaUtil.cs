﻿using EstimaLib.Const;
using GadgetCommon.Util;
using MySql.Data.MySqlClient;
using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace EstimaLib.Util
{
    public static class EstimaUtil
    {
        #region 伝票
        /// <summary>
        /// 伝票ID作成
        /// </summary>
        /// <returns></returns>
        public static string CreateEstimaID()
        {
            return $"{DateTime.Now.ToString("yyyyMMddHHmmss")}_{CommonUtil.GetIpAddress()}";
        }
        #endregion

        #region 文字列
        /// <summary>
        /// 条件に応じてコメントアウト取得
        /// </summary>
        /// <param name="Joken"></param>
        /// <returns></returns>
        public static string GetCmout(bool Joken)
        {
            return Joken ? "-- " : string.Empty;
        }
        #endregion

        #region 計算
        /// <summary>
        /// 仕切値計算
        /// </summary>
        /// <param name="price"></param>
        /// <param name="shikiriRate"></param>
        /// <param name="profitRate"></param>
        /// <returns></returns>
        public static int CalcShikiri(
              int price
            , float shikiriRate
            , float profitRate
            )
        {
            double rate = (100 - profitRate) / 100.00;
            var ret = (int)Math.Round((price * shikiriRate / 100.00) / rate, MidpointRounding.AwayFromZero);
            return ret;
        }
        /// <summary>
        /// インチダイア計算
        /// </summary>
        /// <param name="weldingCount1"></param>
        /// <param name="weldingCount2"></param>
        /// <param name="weldingSize1"></param>
        /// <param name="weldingSize2"></param>
        /// <returns></returns>
        public static float? CalcInchDia(
              int weldingCount1
            , int weldingCount2
            , float weldingSize1
            , float weldingSize2
            )
        {
            var ret = weldingCount1 * weldingSize1 + weldingCount2 * weldingSize2;
            if (ret == 0)
            {
                return null;
            }
            return ret;
        }
        /// <summary>
        /// BD数計算
        /// </summary>
        /// <param name="weldingCount1"></param>
        /// <param name="weldingCount2"></param>
        /// <returns></returns>
        public static int? CalcBDCount(
              int weldingCount1
            , int weldingCount2
            )
        {
            var ret = weldingCount1 + weldingCount2;
            if (ret == 0)
            {
                return null;
            }
            return ret;
        }
        #endregion

        #region ダイアログ
        /// <summary>
        /// エラーメッセージ表示
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="screenName"></param>
        public static void ShowErrorMsg(Exception ex, string screenName)
        {
            CommonUtil.ShowErrorMsg(ex);
            AddLogError(ex, screenName);
        }
        #endregion

        #region ログ
        /// <summary>
        /// ログ情報登録（Info）
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="msg"></param>
        /// <param name="delay"></param>
        public static void AddLogInfo(
              string screenName
            , string msg
            , bool delay = true
            )
        {
            // ログ情報登録
            if (delay)
            {
                Thread.Sleep(500);
                AddLogEntity(Enums.LogType.Info, screenName, msg, string.Empty);
                Thread.Sleep(500);
                return;
            }
            AddLogEntity(Enums.LogType.Info, screenName, msg, string.Empty);
        }
        /// <summary>
        /// ログ情報登録（Info）
        /// </summary>
        /// <param name="screenName"></param>
        /// <param name="msg"></param>
        /// <param name="statusKbn"></param>
        /// <param name="delay"></param>
        public static void AddLogInfo(
              string screenName
            , string msg
            , Enums.LogStatusKbn statusKbn
            )
        {
            Cursor preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            string newMsg = string.Empty;
            switch (statusKbn)
            {
                case Enums.LogStatusKbn.Start:
                    newMsg = $"{msg} - 開始";
                    break;
                case Enums.LogStatusKbn.Stop:
                    newMsg = $"{msg} - 中止";
                    break;
                case Enums.LogStatusKbn.End:
                    newMsg = $"{msg} - 終了";
                    break;
            }
            // ログ情報登録
            AddLogEntity(Enums.LogType.Info, screenName, newMsg, string.Empty);
            Cursor.Current = preCursor;
        }
        /// <summary>
        /// ログ情報登録（Error）
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="screenName"></param>
        public static void AddLogError(Exception ex, string screenName)
        {
            // ログ情報登録
            AddLogEntity(Enums.LogType.Error, screenName, ex.Message, ex.StackTrace);
        }
        /// <summary>
        /// ログ情報登録
        /// </summary>
        /// <param name="type"></param>
        /// <param name="message"></param>
        private static void AddLogEntity(
              Enums.LogType type
            , string screenName
            , string message
            , string stackTrace = ""
            )
        {
            try
            {
                // スタックトレースの整形
                string newStackTrace = string.Empty;
                if (type == Enums.LogType.Error)
                {
                    newStackTrace += CommonUtil.EditErrorStackTrace(stackTrace);
                }
                // SQL文の作成
                var sb = new StringBuilder();
                sb.AppendLine($"INSERT INTO LX01LogInfo( ");
                sb.AppendLine($"    LogType ");
                sb.AppendLine($",   ScreenName ");
                sb.AppendLine($",   Message ");
                sb.AppendLine($",   StackTrace ");
                sb.AppendLine($",   Version ");
                sb.AppendLine($",   AddIPAddress ");
                sb.AppendLine($",   AddHostName ");
                sb.AppendLine($",   AddDate ");
                sb.AppendLine($",   UpdIPAddress ");
                sb.AppendLine($",   UpdHostName ");
                sb.AppendLine($",   UpdDate ");
                sb.AppendLine($")VALUES( ");
                sb.AppendLine($"    @logType ");
                sb.AppendLine($",   @screenName ");
                sb.AppendLine($",   @message ");
                sb.AppendLine($",   @stackTrace ");
                sb.AppendLine($",   @version ");
                sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
                sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
                sb.AppendLine($",   CURRENT_TIMESTAMP ");
                sb.AppendLine($",   '{CommonUtil.GetIpAddress()}' ");
                sb.AppendLine($",   '{System.Net.Dns.GetHostName()}' ");
                sb.AppendLine($",   CURRENT_TIMESTAMP ");
                sb.AppendLine(") ");
                sb.AppendLine("; ");

                using (var cn = Config.CreateSqlConnection())
                {
                    using (var cmd = cn.CreateCommand())
                    {
                        cn.Open();
                        cmd.CommandText = sb.ToString();
                        cmd.Parameters.Add(new MySqlParameter("@logType", (int)type));
                        cmd.Parameters.Add(new MySqlParameter("@screenName", screenName));
                        cmd.Parameters.Add(new MySqlParameter("@message", message));
                        cmd.Parameters.Add(new MySqlParameter("@stackTrace", newStackTrace));
                        cmd.Parameters.Add(new MySqlParameter("@version", CommonUtil.GetVersion()));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch { }
        }
        #endregion
    }
}
