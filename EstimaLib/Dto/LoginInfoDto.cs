﻿namespace EstimaLib.Dto
{
    public class LoginInfoDto
    {
        public string TantoID { get; set; }
        public string TantoName { get; set; }
        public int KengenKbn { get; set; }
        public string BushoCD { get; set; }
        public int Seq { get; set; }
    }
}
