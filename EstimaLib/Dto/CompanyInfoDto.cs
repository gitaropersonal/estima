﻿namespace EstimaLib.Dto
{
    public class CompanyInfoDto
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyTel { get; set; }
        public string CompanyFax { get; set; }
    }
}
