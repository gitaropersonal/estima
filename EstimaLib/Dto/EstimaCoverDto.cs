﻿namespace EstimaLib.Dto
{
    public class EstimaCoverDto
    {
        public int MeisaiNo { get; set; }
        public int MeisaiType { get; set; }
        public string ShohinName { get; set; }
        public int Suryo { get; set; }
        public int Tanka { get; set; }
        public int PlumbingKingaku { get; set; }
        public int HousingKingaku { get; set; }
        public int ProcessingKingaku { get; set; }
        public int Kingaku { get; set; }
    }
}
